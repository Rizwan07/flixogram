//
//  TableVideoCell.h
//  Flickogram

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface TableVideoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnailVideo;

- (void)setThumbnail:(NSString*)thumbnailURL;

@end
