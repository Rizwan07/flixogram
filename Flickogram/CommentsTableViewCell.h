//
//  CommentsTableViewCell.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface CommentsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet EGOImageView *photo;
@property (nonatomic, weak) IBOutlet UILabel *username;
@property (nonatomic, weak) IBOutlet UILabel *comment;
@property (nonatomic, weak) IBOutlet UILabel *date;

- (void)setPlaceholder;
- (void)setPhotoUser:(NSString*)photoUser;

@end
