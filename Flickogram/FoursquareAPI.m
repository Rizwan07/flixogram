//
//  FoursquareAPI.m
//  Flickogram
//


#import "FoursquareAPI.h"
#import "AFNetworking.h"
#import "FlickogramConstantsURLAPI.h"

@implementation FoursquareAPI

- (void)searchVenuesNearLatitude:(NSString *)latitude
						   limit:(NSString *)limitVenues
						  radius:(NSString *)radiusSearch
					  clientDate:(NSString *)date
						   query:(NSString *)query
						delegate:(id<FoursquareDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:latitude forKey:@"ll"];
	[parameters setObject:limitVenues forKey:@"limit"];
	[parameters setObject:radiusSearch forKey:@"radius"];
	[parameters setObject:query forKey:@"query"];
	[parameters setObject:@"LCGAL0PWO0QFLBDLQUZPPH0MXBPFKPFA3UOWOO5HZO4VPW10" forKey:@"client_id"];
	[parameters setObject:@"QETYSURXKYWQT5ALC2WU2A4CW2ZU30TDQHJU0O13ARSPWN5C" forKey:@"client_secret"];
	[parameters setObject:date forKey:@"v"];
	
	NSURL *serviceURL = [NSURL URLWithString:FoursquareBaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:FoursquareSearch parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 if ([[JSON objectForKey:@"response"] objectForKey:@"venues"]) {
																								 [delegate searchCompleteSuccess:[[JSON objectForKey:@"response"] objectForKey:@"venues"]];
																							 } else {
																								 NSString *errorString = @"Try again later!";
																								 [delegate searchFailedWithError:errorString];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							  NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate searchFailedWithError:errorString];
																						 }];
    [operation start];
}


@end
