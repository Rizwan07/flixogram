//
//  HashtagVideoViewController.h
//  Flickogram


#import "BaseViewController.h"

#import "GridVideo.h"
#import "ManagerFlickogramAPI.h"
#import "PullTableView.h"

@interface HashtagVideoViewController : BaseViewController <FlickogramSearchDelegate,
															GridVideoDelegate,
															UITableViewDelegate,
															UITableViewDataSource,
															PullTableViewDelegate>
{
	GridVideo *gridVideoView;
	NSInteger currentPage;
	NSMutableArray *hashtagsVideosArray;
	UISegmentedControl *segmentedControl;
	BOOL isShowMore;
}
@property (nonatomic, strong) NSString *hashtag;
@property (nonatomic, assign) NSInteger countVideo;
@property (nonatomic, weak) IBOutlet UILabel *hashtagLabel;
@property (nonatomic, weak) IBOutlet UILabel *countVideoLabel;
@property (nonatomic, weak) IBOutlet PullTableView *table;

@end
