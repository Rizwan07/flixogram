//
//  SingletonAuthorizationUser.m
//  Odesker
//


#import "SingletonAuthorizationUser.h"

@implementation SingletonAuthorizationUser

@synthesize userID = _userID;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize userName = _userName;
@synthesize email = _email;
@synthesize password = _password;
@synthesize profilePicture = _profilePicture;

+ (SingletonAuthorizationUser*)authorizationUser
{
	static SingletonAuthorizationUser *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SingletonAuthorizationUser alloc] init];
    });
    return sharedInstance;}



@end
