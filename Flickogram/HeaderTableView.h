//
//  HeaderTableView.h
//  Flickogram


#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface HeaderTableView : UIView

@property (nonatomic, weak) IBOutlet UILabel *userName;
@property (nonatomic, weak) IBOutlet EGOImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *locationName;
@property (nonatomic, weak) IBOutlet UIButton *reflickButton;

@end
