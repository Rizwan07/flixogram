//
//  TrendingPopularViewController.h
//  Flickogram
//

#import "BaseViewController.h"

#import "ManagerFlickogramAPI.h"
#import "PullTableView.h"

typedef enum {
	PopularVideosAPI,
	TrendingVideosAPI,
} FlickogramVideoAPI;

typedef enum {
	Today,
	ThisWeek,
	All
} ViewType;

@interface TrendingPopularViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, FlickogramVideoDelegate, PullTableViewDelegate> {
	NSMutableArray *videosArray;
	NSInteger currentPage;
	BOOL isShowMore;
	ViewType type;
	NSString *currentType;
	BOOL isLoadingVideo;
    IBOutlet UIButton *btnToday;
    IBOutlet UIButton *btnThisWeek;
    IBOutlet UIButton *btnAll;
}

@property (nonatomic, weak) IBOutlet PullTableView *table;
@property (nonatomic) FlickogramVideoAPI selectedVideoAPI;

- (IBAction)showTodayVideos:(id)sender;
- (IBAction)showThisWeekVideos:(id)sender;
- (IBAction)showAllVideos:(id)sender;

@end
