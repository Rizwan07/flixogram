//
//  CommentsTableViewCell.m
//  Flickogram
//


#import "CommentsTableViewCell.h"

@implementation CommentsTableViewCell
@synthesize comment = _comment;
@synthesize photo = _photo;
@synthesize username = _username;
@synthesize date = _date;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPlaceholder 
{
	self.photo.placeholderImage = [UIImage imageNamed:@"Profile-thumb"];
}

- (void)setPhotoUser:(NSString *)photoUser 
{
	_photo.imageURL = [NSURL URLWithString:photoUser];
}

- (void)willMoveToSuperview:(UIView *)newSuperview 
{
	[super willMoveToSuperview:newSuperview];
	
	if(!newSuperview) {
		[_photo cancelImageLoad];
	}
}


@end
