//
//  ExploreTableViewCell.m
//  Flickogram
//

#import "ExploreTableViewCell.h"

@implementation ExploreTableViewCell
@synthesize username = _username;
@synthesize thumbUser = _thumbUser;
@synthesize userRealName = _userRealName;
@synthesize searchString = _searchString;
@synthesize countVideos = _countVideos;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPlaceholder 
{
	self.thumbUser.placeholderImage = [UIImage imageNamed:@"Profile-thumb"];
}

- (void)setPhotoUser:(NSString *)photoUser 
{
	self.thumbUser.imageURL = [NSURL URLWithString:photoUser];
}

- (void)willMoveToSuperview:(UIView *)newSuperview 
{
	[super willMoveToSuperview:newSuperview];
	
	if(!newSuperview) {
		[self.thumbUser cancelImageLoad];
	}
}
@end
