//
//  NearbyVenuesViewController.m
//  Flickogram


#import "NearbyVenuesViewController.h"

@interface NearbyVenuesViewController ()

@end

@implementation NearbyVenuesViewController
@synthesize table = _table;
@synthesize nameForSearch = _nameForSearch;
@synthesize location = _location;
@synthesize venues = _venues;
@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Nearby Venues";
	self.navigationController.navigationBarHidden = NO;

	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(backToShare) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	self.venues = [[NSMutableArray alloc] init];
	searchStatus = @"Searching...";
	
	NSDate *today = [NSDate date];
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat: @"YYYYMMdd"];
	NSString * dateString = [dateFormat stringFromDate:today];

	[[ManagerFlickogramAPI foursquareExemplar] searchVenuesNearLatitude:self.location
																  limit:@"50" 
																 radius:@"5000" 
															 clientDate:dateString
																  query:self.nameForSearch
															   delegate:self];

}

- (void)backToShare
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger count = [self.venues count];
	if (count > 0) {
		return [self.venues count] + 1;
	} 
	return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *CellIdentifier = @"CellFoursquare";
	UITableViewCell	*cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	
	if (indexPath.row == ([tableView numberOfRowsInSection:0] - 1)) {
		UIImageView *imageFoursquare = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poweredByFoursquare_gray"]];
		imageFoursquare.frame = CGRectMake(40, -10, 241, 60);
		[cell addSubview:imageFoursquare];
		imageFoursquare = nil;
	}
	else  if ([self.venues count] > 0) {
		NSDictionary *currentVenue = [NSDictionary dictionaryWithDictionary:[self.venues objectAtIndex:indexPath.row]];
		if ([currentVenue objectForKey:@"name"]) {
			cell.textLabel.text = [currentVenue objectForKey:@"name"];
		}
		if ([currentVenue objectForKey:@"address"]) {
			cell.detailTextLabel.text = [currentVenue objectForKey:@"address"];
		}
	} else {
		cell.textLabel.text = searchStatus;
		cell.textLabel.textColor = [UIColor lightGrayColor];
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if ([self.venues count]) {
		if (indexPath.row != ([tableView numberOfRowsInSection:0] - 1)) {
			[self.delegate venueSelected:[self.venues objectAtIndex:indexPath.row]];
			//[self.navigationController popViewControllerAnimated:YES];
		}
	}
}

#pragma mark - Foursquare delegate 
- (void)searchCompleteSuccess:(NSDictionary *)data
{
	if ([data count] > 0) {
		for (NSDictionary *venue in data) {
			NSMutableDictionary *newVenue = [[NSMutableDictionary alloc] init];
			//add name venue
			if ([venue objectForKey:@"name"]) { 
				[newVenue setObject:[venue objectForKey:@"name"] forKey:@"name"];
			}
		
			//add address
			if ([[venue objectForKey:@"location"] objectForKey:@"address"]) {
				[newVenue setObject:[[venue objectForKey:@"location"] objectForKey:@"address"] forKey:@"address"];
			}
			
			//add lat
			if ([[venue objectForKey:@"location"] objectForKey:@"lat"]) {
				[newVenue setObject:[[venue objectForKey:@"location"] objectForKey:@"lat"] forKey:@"lat"];
			}
			
			//add lng
			if ([[venue objectForKey:@"location"] objectForKey:@"lng"]) {
				[newVenue setObject:[[venue objectForKey:@"location"] objectForKey:@"lng"] forKey:@"lng"];
			}
			[self.venues addObject:newVenue];
		}
	} else {
		searchStatus = @"No results.";
	}
	[self.table reloadData];
}

- (void)searchFailedWithError:(NSString *)error
{
	NSLog(@"%@", error);
}

@end
