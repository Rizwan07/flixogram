//
//  FlickogramConstantsAPI.m
//  Flickogram
//


#import "FlickogramConstantsURLAPI.h"

@implementation FlickogramConstantsURLAPI

NSString * const BaseURL = @"http://developer.avenuesocial.com/azeemsal/flixogram/services/";
NSString * const FoursquareBaseURL = @"https://api.foursquare.com/v2/venues/";

NSString * const LoginMethodAPI =  @"index/login/";
NSString * const SignupMethodAPI = @"index/signup/";
NSString * const AboutUsAPI = @"index/contents/aboutus";

NSString * const UserProfileAPI = @"user/userProfile";
NSString * const SearchUsersByTagsAPI = @"user/searchUsersByTags/";
NSString * const SuggestedVideoAPI = @"user/getSuggestedUsers/";
NSString * const UpdateProfilePictureAPI = @"user/updateProfilePicture/";
NSString * const DeleteProfilePictureAPI = @"user/deleteProfilePicture/";
NSString * const UpdatePublicPofileAPI = @"user/updatePublicProfile/";
NSString * const UpdatePrivateProfileAPI = @"user/updatePrivateProfile/";
NSString * const UserVideosPrivateAPI = @"user/updateUserVideosArePrivate/";
NSString * const BlockUserAPI = @"user/blockUser/";
NSString * const ReportUserAPI = @"user/reportUser/";

NSString * const CreateVideoAPI = @"video/addVideo/";
NSString * const GetFollowingVideosAPI = @"video/getFollowingVideos";
NSString * const GetVideoDetailsAPI = @"video/getVideoDetails";
NSString * const LikeVideoAPI = @"video/likeVideo";
NSString * const AddCommentAPI = @"video/addComments/";
NSString * const RemoveCommentAPI = @"video/removeComment/";
NSString * const RemoveVideoAPI = @"video/removeVideo/";
NSString * const SearchVideoAPI = @"video/getVideosByTags/";
NSString * const ExploreVideosAPI = @"video/getExplorerVideos/";
NSString * const ReflickVideoAPI = @"video/reflickVideo/";
NSString * const UserLikedVideosAPI = @"video/getUserLikedVideos/";
NSString * const GetPopularVideo = @"video/getPopularVideos/";
NSString * const GetMostWantedLikedVideosAPI = @"video/getMostLikedVideos/";
NSString * const GetTrendingVideosAPI = @"video/getTrendingVideos/";
NSString * const ReportVideoAPI = @"video/reportVideo/";

NSString * const NotificationFollowingAPI = @"notification/followings/";
NSString * const NotificationNewsAPI = @"notification/news/";

NSString * const FindFriendsAPI = @"followings/findFriends/";
NSString * const FollowUnFollowUserAPI = @"followings/followUnFollowUser/";
NSString * const UserFollowersAPI = @"followings/getUserFollowers/";
NSString * const UserFollowingsAPI = @"followings/getUserFollowings/";

NSString * const FoursquareSearch = @"search";

@end
