//
//  LikedViewController.m
//  Flixogram
//
//  Created by Rizwan on 5/26/14.
//
//

#import "LikedViewController.h"

#import "SingletonAuthorizationUser.h"
#import "GridVideoCell.h"
#import "TableVideoCell.h"
#import "VideoDetailViewController.h"


@interface LikedViewController () {
    IBOutlet PullTableView *_table;
}
- (void)parseVideoFromDictionary:(NSDictionary *)videos;

@end

@implementation LikedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UIImage *selectedImage = [UIImage imageNamed:@"tab4-active"];
		UIImage *unselectedImage = [UIImage imageNamed:@"tab4-inactive"];
		UITabBarItem *tabBarItem = [[UITabBarItem alloc] init];
		[tabBarItem setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
		self.tabBarItem = tabBarItem;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
//    if (_selectedVideoAPI == UserLikedVideosAPI) {
        self.navigationItem.title = @"Liked Videos";
        self.screenName = @"Liked Videos";
//    }
//    else {
//        self.navigationItem.title = @"Most Viewed Videos";
//        self.screenName = @"Most Viewed Videos";
//    }
    
	//back button
    if ([self.navigationController.viewControllers count] > 1) {
        
        UIImage *backButtonImage = [UIImage imageNamed:@"back"];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
        [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem = backItem;
	}
    //	//Segmented Control
    //	NSArray *itemArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"tableViewSelected"],
    //						  [UIImage imageNamed:@"gridView"], nil];
    //	segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    //	segmentedControl.frame = CGRectMake(0, 0, 96, 32);
    //	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    //	segmentedControl.tintColor = [UIColor clearColor];
    //	[segmentedControl setWidth:48 forSegmentAtIndex:0];
    //	[segmentedControl setWidth:48 forSegmentAtIndex:1];
    //	[segmentedControl addTarget:self action:@selector(changeSegment) forControlEvents:UIControlEventValueChanged];
    //	segmentedControl.selectedSegmentIndex = 0;
    //	self.navigationItem.titleView = segmentedControl;
    
	
	videosArray = [[NSMutableArray alloc] init];
	currentPage = 1;
	isShowMore = NO;
    
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *currentPageInString = [NSString stringWithFormat:@"%d", currentPage];
//	if (self.selectedVideoAPI == UserLikedVideosAPI) {
		[[ManagerFlickogramAPI videoExemplar] getUserLikedVideosForUserID:userID
																   onPage:currentPageInString
															 withDelegate:self];
//	} else if (self.selectedVideoAPI == MostLikedVideosAPI) {
//		[[ManagerFlickogramAPI videoExemplar] getMostLikedVideosForUserID:userID
//																   onPage:currentPageInString
//																 delegate:self];
//	}
//	
	//pull to refresh
	_table.pullArrowImage = [UIImage imageNamed:@"whiteArrow"];
	_table.pullBackgroundColor = [UIColor clearColor];
	_table.pullTextColor = [UIColor whiteColor];
	_table.loadMoreEnabled = NO;
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)changeSegment
{
	NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
	UIImage *firstImage;
	UIImage *secondImage;
	switch (selectedSegment) {
		case 0: {
			firstImage = [UIImage imageNamed:@"tableViewSelected"];
			secondImage = [UIImage imageNamed:@"gridView"];
		}
			break;
		case 1: {
			firstImage = [UIImage imageNamed:@"tableView"];
			secondImage = [UIImage imageNamed:@"gridViewSelected"];
		}
			break;
	}
	[segmentedControl setImage:firstImage forSegmentAtIndex:0];
	[segmentedControl setImage:secondImage forSegmentAtIndex:1];
	[_table reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) (count + 9) / 10;
 		return countCells;
	} else {
		return [videosArray count];
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		return 135;
	} else {
		return 160;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		static NSString *cellId = @"GridVideoCell";
		GridVideoCell *cell = (GridVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GridVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (GridVideoCell *) currentObject;
				}
				break;
			}
		}
		
		NSInteger currentCell =	indexPath.row * 10;
		NSInteger countVideos = [videosArray count];
		
		int currentButton = 0;
		for (int i = currentCell; (i < countVideos && i < currentCell+10); i++) {
			NSDictionary *videoDetail = [videosArray objectAtIndex:i];
			NSURL *thumbnailURL = [NSURL URLWithString:[videoDetail objectForKey:@"thumbnail"]];
			EGOImageButton *currentImageButton = [cell setThumbImageOfURL:thumbnailURL ForButton:currentButton];
			currentImageButton.tag = i;
			[currentImageButton addTarget:self action:@selector(showVideoForEGOImageButton:) forControlEvents:UIControlEventTouchUpInside];
			currentButton++;
		}
		return cell;
	} else {
		static NSString *cellIdentifier = @"TableVideoCell";
		TableVideoCell *cell = (TableVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TableVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (TableVideoCell *) currentObject;
				}
				break;
			}
		}
		NSDictionary *videoDetail = [videosArray objectAtIndex:indexPath.row];
		NSString *thumbnailURL = [videoDetail objectForKey:@"thumbnail"];
		[cell setThumbnail:thumbnailURL];
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 1) {
		VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
		videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexPath.row] mutableCopy];
		[self.navigationController pushViewController:videoDetailVC animated:YES];
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (isShowMore) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) ((count + 9) / 10) - 1;
		if (((segmentedControl.selectedSegmentIndex ==0) && (indexPath.row == countCells)) ||
			((segmentedControl.selectedSegmentIndex == 1) && (indexPath.row == count - 1))) {
			isShowMore = NO;
			currentPage++;
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
			NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
//			if (self.selectedVideoAPI == UserLikedVideosAPI) {
				[[ManagerFlickogramAPI videoExemplar] getUserLikedVideosForUserID:authUser
																		   onPage:pageInStringFormat
																	 withDelegate:self];
//			} else if (self.selectedVideoAPI == MostLikedVideosAPI) {
//				[[ManagerFlickogramAPI videoExemplar] getMostLikedVideosForUserID:authUser
//																		   onPage:pageInStringFormat
//																		 delegate:self];
//			}
		}
	}
}

- (void)showVideoForEGOImageButton:(id)sender
{
	EGOImageButton *selectedButton = (EGOImageButton *)sender;
	NSInteger indexVideo = selectedButton.tag;
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexVideo] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}

#pragma mark - User Liked Videos Delegate
- (void)getUserLikedVideosSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isShowMore = [[data objectForKey:@"show_more"] boolValue];
	if ([data objectForKey:@"videos"]) {
		NSDictionary *videos = [data objectForKey:@"videos"];
        [videosArray removeAllObjects];
		[self parseVideoFromDictionary:videos];
	} else if (_table.pullTableIsRefreshing) {
		[self refreshTable];
	}
}

- (void)getUserLikedVideosFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self refreshTable];
}

#pragma mark - Most Liked Videos Delegate
- (void)getMostLikedVideosSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isShowMore = [[data objectForKey:@"show_more"] boolValue];
	if ([data objectForKey:@"videos"]) {
		NSDictionary *videos = [data objectForKey:@"videos"];
		[self parseVideoFromDictionary:videos];
	} else if (_table.pullTableIsRefreshing) {
		[self refreshTable];
	}
}

- (void)getMostLikedVideosFailedWithError:(NSString *)error
{
	[self refreshTable];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)parseVideoFromDictionary:(NSDictionary *)videos
{
	if (_table.pullTableIsRefreshing) {
		NSString *lastDateAdded = @"0";
		if ([videosArray count] > 0) {
			lastDateAdded = [[videosArray objectAtIndex:0] objectForKey:@"date_added"];
		}
		NSInteger currentVideo = 0;
		BOOL isMoreVideo = YES;
		while (isMoreVideo) {
			NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
			if ([videos objectForKey:numberUserString]) {
				NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
				NSString *newDateAdded = [videoDetails objectForKey:@"date_added"];
                
				//compare last date with new date and stop if equal
				if ([lastDateAdded isEqualToString:newDateAdded]) {
					[self refreshTable];
					return;
				} else {
					[videosArray insertObject:videoDetails atIndex:currentVideo];
					
					if (isShowMore) {
						[videosArray removeLastObject];
					}
				}
				currentVideo++;
			} else {
				isMoreVideo = NO;
			}
		}
		[self refreshTable];
		return;
	}
	
	NSInteger currentVideo = 0;
	BOOL isMoreVideo = YES;
	while (isMoreVideo) {
		NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
		if ([videos objectForKey:numberUserString]) {
			NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
			[videosArray addObject:videoDetails];
			currentVideo++;
		} else {
			isMoreVideo = NO;
		}
	}
	[_table reloadData];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
	// Code to actually refresh goes here.
    _table.pullLastRefreshDate = [NSDate date];
    _table.pullTableIsRefreshing = NO;
	[_table reloadData];
}

- (void) loadMoreDataToTable
{
    // Code to actually load more data goes here.
    _table.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
//	if (self.selectedVideoAPI == UserLikedVideosAPI) {
		[[ManagerFlickogramAPI videoExemplar] getUserLikedVideosForUserID:userID
																   onPage:@"1"
															 withDelegate:self];
//	} else if (self.selectedVideoAPI == MostLikedVideosAPI) {
//		[[ManagerFlickogramAPI videoExemplar] getMostLikedVideosForUserID:userID
//																   onPage:@"1"
//																 delegate:self];
//	}
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

@end
