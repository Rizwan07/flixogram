//
//  FlickogramEffect.h
//  Flickogram
//
//  Created by Andrey Kudievskiy on 23.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlickogramEffect : NSObject

@property (nonatomic, strong) NSString *nameEffect;

@end
