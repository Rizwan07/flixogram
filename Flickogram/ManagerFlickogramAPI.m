//
//  ManagerFlickogramAPI.m
//  Flickogram
//


#import "ManagerFlickogramAPI.h"

@implementation ManagerFlickogramAPI

static AuthorizationFlickogramAPI *authorizationExemplar_;
static VideoFlickogramAPI *videoExemplar_;
static FoursquareAPI *foursquareExemplar_;
static NotificationsFlickogramAPI *notificationsExemplar_;
static SearchFlickogramAPI *searchExemplar_;

+ (AuthorizationFlickogramAPI *)authorizationExemplar
{
	@synchronized(self) {
		if (!authorizationExemplar_) {
			authorizationExemplar_ = [[AuthorizationFlickogramAPI alloc] init];
		}
	}
	return authorizationExemplar_;
}

+ (VideoFlickogramAPI *)videoExemplar
{
	@synchronized(self) {
		if (!videoExemplar_) {
			videoExemplar_ = [[VideoFlickogramAPI alloc] init];
		}
	}
	return videoExemplar_;
}

+ (FoursquareAPI *)foursquareExemplar 
{
	@synchronized(self) {
		if (!foursquareExemplar_) {
			foursquareExemplar_ = [[FoursquareAPI alloc] init];
		}
	}
	return foursquareExemplar_;
}

+ (NotificationsFlickogramAPI *)notificationsExemplar 
{
	@synchronized(self) {
		if (!notificationsExemplar_) {
			notificationsExemplar_ = [[NotificationsFlickogramAPI alloc] init];
		}
	}
	return notificationsExemplar_;
}

+ (SearchFlickogramAPI *)searchExemplar
{
	@synchronized(self) {
		if (!searchExemplar_) {
			searchExemplar_= [[SearchFlickogramAPI alloc] init];
		}
	}
	return searchExemplar_;
}
@end
 