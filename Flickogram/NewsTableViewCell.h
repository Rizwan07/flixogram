//
//  NewsTableViewCell.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "LORichTextLabel.h"
#import "EGOImageButton.h"

@interface NewsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet LORichTextLabel *message;
@property (nonatomic, weak) IBOutlet UILabel *date;
@property (nonatomic, weak) IBOutlet EGOImageButton *photoUser;

- (void)setPhoto:(NSString*)photoUser;
- (void)setPlaceholder; 

@end
