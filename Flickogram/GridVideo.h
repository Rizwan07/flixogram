//
//  GridVideo.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
#import "EGOImageButton.h"

@protocol GridVideoDelegate <NSObject>
@optional
- (void)showVideoByIndexArray:(NSInteger)indexArray;
- (void)showMoreVideos;
@end

@interface GridVideo : UIView {
	NSInteger currentVideo;
	CGPoint startPoint;
	NSInteger currentIndexArray;
	UIButton *showMoreButton;
	NSInteger tempValue;
	NSInteger tempValueY;
	NSInteger currentNumberButton;
}

@property (nonatomic, strong) UIScrollView *scroll;
@property (nonatomic, assign) BOOL isShowMoreButton;
@property (nonatomic, assign) id<GridVideoDelegate>delegate;

- (void)showGridVideoInQuantity:(NSInteger)countVideo fromDictionary:(NSDictionary *)videos;
- (void)showGridVideoInQuantity:(NSInteger)countVideo fromArray:(NSArray *)videos;
- (void)resetData;

@end
