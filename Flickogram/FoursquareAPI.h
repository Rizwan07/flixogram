//
//  FoursquareAPI.h
//  Flickogram
//


#import <Foundation/Foundation.h>
#import "FlickogramProtocolsAPI.h"

@interface FoursquareAPI : NSObject

- (void)searchVenuesNearLatitude:(NSString *)latitude
						   limit:(NSString *)limitVenues
						  radius:(NSString *)radiusSearch
					  clientDate:(NSString *)date
						   query:(NSString *)query
						delegate:(id<FoursquareDelegate>)delegate;
@end
