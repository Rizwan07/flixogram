//
//  SuggestedViewController.h
//  Flickogram


#import "BaseViewController.h"

#import "ManagerFlickogramAPI.h"

@interface SuggestedViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, FlickogramVideoDelegate, FlickogramAuthorizationDelegate>
{
	NSMutableArray *suggestedUsers;
}

@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic) BOOL isShowDoneButton;
@end
