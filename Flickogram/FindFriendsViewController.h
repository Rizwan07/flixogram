//
//  FindFriendsViewController.h
//  Flickogram


#import "BaseViewController.h"

@interface FindFriendsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate> {
	NSArray *searchMethods;
	NSArray *twitterAccounts;
}

@property (nonatomic) BOOL isShowBackButton;
@property (nonatomic) BOOL isShowSuggestedUsers;
@end
