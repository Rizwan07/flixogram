//
//  UploadVideoViewController.m
//  Flickogram
//


#import "UploadVideoViewController.h"
#import "ManagerFlickogramAPI.h"
#import "SingletonAuthorizationUser.h"
#import <FacebookSDK/FacebookSDK.h>
#import "FlickogramConstants.h"
#import "TwitterSettings.h"
#import <Accounts/Accounts.h>
#import "RecordViewController.h"

@interface UploadVideoViewController ()
- (void)backToRecord;
- (void)setGeotagCellElements;
- (NSDictionary *)setStateShareSocialNetworks;
- (void)switchState:(id)sender;
- (void)chooseTwitterAccount;
- (void)openSession;
@end

@implementation UploadVideoViewController
@synthesize caption = _caption;
@synthesize videoForUpload = _videoForUpload;
@synthesize table = _table;
@synthesize toggleEnableGeotag;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.screenName = @"Upload Video";
    
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(backToRecord) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	//set Next button
	UIImage *nextButtonImage = [UIImage imageNamed:@"next"];
	UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[nextButton setBackgroundImage:nextButtonImage forState:UIControlStateNormal];
	nextButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[nextButton addTarget:self action:@selector(uploadVideo) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *nextItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
	self.navigationItem.rightBarButtonItem = nextItem;
	
	self.caption = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 280, 30)];
	self.caption.placeholder = @"Caption";
	self.caption.delegate = self;

	isSharingFacebook = NO;
	isSharingTwitter = NO;
	isSharingYoutube = NO;
	
	[self setGeotagCellElements];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)backToRecord
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)uploadVideo
{
    
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	if (userID == nil) {
		userID = @"";
	}
	NSString *titleVideo = self.caption.text;
	if ([titleVideo length] == 0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
														message:@"Video caption is missing" 
													   delegate:self 
											  cancelButtonTitle:nil 
											  otherButtonTitles:@"OK", nil];
		alert.tag = 0;
		[alert show];
		return;
	}
	
	NSDictionary *stateShareSocialNetwork = [self setStateShareSocialNetworks];
	
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:titleVideo forKey:@"Caption"];
	[parameters setObject:userID forKey:@"UserID"];
	[parameters setObject:[self.videoForUpload absoluteString] forKey:@"VideoURL"];
	[parameters setObject:stateShareSocialNetwork forKey:@"Sharing"];
	if (geotagInfo) {
		[parameters setObject:geotagInfo forKey:@"Geotag"];
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationUploadVideoOnServer
														object:self 
													  userInfo:parameters];

	//root view controller - remove all observes
	RecordViewController *recordView = [self.navigationController.viewControllers objectAtIndex:0];
	[[NSNotificationCenter defaultCenter] removeObserver:recordView.panelEffects.effects];
	[[NSNotificationCenter defaultCenter] removeObserver:recordView];
	
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self.caption resignFirstResponder];
	return YES;
}

//add buttons and labels
- (void)setGeotagCellElements
{
	[toggleEnableGeotag setFrame:CGRectMake(5, 5, 30, 30)];
	[toggleEnableGeotag addTarget:self action:@selector(toggleGeotagState) forControlEvents:UIControlEventTouchUpInside];
	
	titleSelectedLocation = [[UILabel alloc] initWithFrame:CGRectMake(35, 10, 250, 20)];
	titleSelectedLocation.text = @"Enable Geotag";
	titleSelectedLocation.backgroundColor = [UIColor clearColor];
	titleSelectedLocation.textColor = [UIColor lightGrayColor];
	
	labelLocation = [[UILabel alloc] initWithFrame:CGRectMake(210, 10, 65, 20)];
	labelLocation.text = @"location";
	labelLocation.backgroundColor = [UIColor clearColor];
	labelLocation.hidden = YES;
}

- (void)toggleGeotagState
{
	toggleEnableGeotag.selected = !toggleEnableGeotag.selected;
	if (toggleEnableGeotag.selected) {
		toggleEnableGeotag.highlighted = YES;
		titleSelectedLocation.text = @"Geotagged";
		titleSelectedLocation.textColor = [UIColor blackColor];
		labelLocation.hidden = NO;
	} else {
		toggleEnableGeotag.highlighted = NO;
		titleSelectedLocation.text = @"Enagle Geotag";
		titleSelectedLocation.textColor = [UIColor lightGrayColor];
		labelLocation.hidden = YES;
	}
	[self.table reloadData];
}

#pragma mark - Table Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	switch (section) {
		case 0:
			return 2;
			break;
		case 1:
            return 2;
			//return 3; //sharing cells
			break;
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //Create Cell
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle 
									  reuseIdentifier:CellIdentifier];
    }
	if (indexPath.section == 0) { //geotag cell
		if (indexPath.row == 0) {
			[cell.contentView addSubview:self.caption];
		} else {
			if (toggleEnableGeotag.selected) {
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			}
			[cell.contentView addSubview:toggleEnableGeotag];
			[cell.contentView addSubview:titleSelectedLocation];
			[cell.contentView addSubview:labelLocation];
		}
		
	} else {
		socialNetwork = indexPath.row;
		switch (socialNetwork) {
			case FacebookCell: {
				cell.textLabel.text = @"Facebook";
				cell.detailTextLabel.text = @"Video will post on your Timeline";
				//cell.imageView.image = [UIImage imageNamed:@"btn-fb"];
				cell.detailTextLabel.font = [UIFont systemFontOfSize:11];
                
				if ((FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) ||
					FBSession.activeSession.state == FBSessionStateOpen) {
					cell.accessoryType = UITableViewCellAccessoryNone;
					UISwitch *switchShareFacebook = [[UISwitch alloc] initWithFrame:CGRectMake(225, 10, 69, 27)];
					switchShareFacebook.tag = FacebookCell;
					switchShareFacebook.on = isSharingFacebook;
					[switchShareFacebook addTarget:self action:@selector(switchState:) forControlEvents:UIControlEventTouchUpInside];
					[self removeUnnecessarySubview:[UIButton class] fromCell:cell];
					[cell addSubview:switchShareFacebook];
				} else {
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					UIButton *configButton = [UIButton buttonWithType:UIButtonTypeCustom];
					[configButton setTitle:@"Configure" forState:UIControlStateNormal];
					configButton.titleLabel.font = [UIFont systemFontOfSize:13];
					[configButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
					[configButton addTarget:self action:@selector(openSession) forControlEvents:UIControlEventTouchUpInside];
					configButton.frame = CGRectMake(220, 0, 60, 44);
					[self removeUnnecessarySubview:[UISwitch class] fromCell:cell];
					[cell addSubview:configButton];
				}
				break;
			}
			case TwitterCell: {
				cell.textLabel.text = @"Twitter";
				cell.detailTextLabel.text = @"Video link to your video will be tweeted";
				//cell.imageView.image = [UIImage imageNamed:@"btn-twitter"];
				cell.detailTextLabel.font = [UIFont systemFontOfSize:11];
                
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				
				if ([defaults objectForKey:@"TwitterAccountIdentifier"]) {
					cell.accessoryType = UITableViewCellAccessoryNone;
					UISwitch *switchShareTwitter = [[UISwitch alloc] initWithFrame:CGRectMake(225, 10, 69, 27)];
					switchShareTwitter.tag = TwitterCell;
					switchShareTwitter.on = isSharingTwitter;
					[switchShareTwitter addTarget:self action:@selector(switchState:) forControlEvents:UIControlEventTouchUpInside];
					[self removeUnnecessarySubview:[UIButton class] fromCell:cell];
					[cell addSubview:switchShareTwitter];
				}	else {
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					UIButton *configButton = [UIButton buttonWithType:UIButtonTypeCustom];
					[configButton setTitle:@"Configure" forState:UIControlStateNormal];
					configButton.titleLabel.font = [UIFont systemFontOfSize:13];
					[configButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
					[configButton addTarget:self action:@selector(chooseTwitterAccount) forControlEvents:UIControlEventTouchUpInside];
					configButton.frame = CGRectMake(220, 0, 60, 44);
					[self removeUnnecessarySubview:[UISwitch class] fromCell:cell];
					[cell addSubview:configButton];
				}
				break;
			}
			case YoutubeCell: {
				cell.textLabel.text = @"Youtube";
				cell.detailTextLabel.text = @"Video will be uploaded to your account";
				//cell.imageView.image = [UIImage imageNamed:@"btn-youtube"];
				cell.detailTextLabel.font = [UIFont systemFontOfSize:11];
				// Get the saved authentication, if any, from the keychain.
				BOOL authSuccess = NO;
				GTMOAuth2Authentication *auth = [self getAuthYoutube];
				if (auth) {
					// if the auth object contains an access token, didAuth is now true
					BOOL didAuth = [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:@"My App:Flickogram"
																			   authentication:auth];
					if (didAuth) {
						// retain the authentication object, which holds the auth tokens
						// we can determine later if the auth object contains an access token
						// by calling its -canAuthorize method
						BOOL isSignedIn = [auth canAuthorize];
						if (isSignedIn) {
							authSuccess = YES;
							authTokenYoutube = auth;
							cell.accessoryType = UITableViewCellAccessoryNone;
							UISwitch *switchShareYoutube = [[UISwitch alloc] initWithFrame:CGRectMake(225, 10, 69, 27)];
							switchShareYoutube.tag = YoutubeCell;
							switchShareYoutube.on = isSharingYoutube;
							[switchShareYoutube addTarget:self action:@selector(switchState:) forControlEvents:UIControlEventTouchUpInside];
							[self removeUnnecessarySubview:[UIButton class] fromCell:cell];
							[cell addSubview:switchShareYoutube];
						} 
					}
				}				
				if (!authSuccess) {
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					UIButton *configButton = [UIButton buttonWithType:UIButtonTypeCustom];
					[configButton setTitle:@"Configure" forState:UIControlStateNormal];
					configButton.titleLabel.font = [UIFont systemFontOfSize:13];
					[configButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
					[configButton addTarget:self action:@selector(loginOnYoutube) forControlEvents:UIControlEventTouchUpInside];
					configButton.frame = CGRectMake(220, 0, 60, 44);
					[self removeUnnecessarySubview:[UISwitch class] fromCell:cell];
					[cell addSubview:configButton];
				}
				break;
			}
		}
	}
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	if ((indexPath.section == 0) && (indexPath.row == 1) && (toggleEnableGeotag.selected)) {
		FoursquareViewController *foursquare = [[FoursquareViewController alloc] init];
		foursquare.delegate = self;
		[self.navigationController pushViewController:foursquare animated:YES];
	}
	if ((indexPath.section == 0) && (indexPath.row == 1) && !toggleEnableGeotag.selected) {
		[self toggleGeotagState];
	}
}

- (void)removeUnnecessarySubview:(Class)class fromCell:(UITableViewCell *)cell
{
	for (id subview in cell.subviews) {
		if ([[subview class] isEqual:class]) {
			[subview removeFromSuperview];
		}
	}
}

- (void)reloadTable
{	
	[self.table reloadData];
}

- (void)switchState:(id)sender
{
    if (_duration > 30.0) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"Large video could not share on social media"
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    else {
        UISwitch *switcherState = (UISwitch *)sender;
        switch (switcherState.tag) {
            case FacebookCell: {
                isSharingFacebook = switcherState.on;
                break;
            }
            case TwitterCell:{
                isSharingTwitter = switcherState.on;
                break;
            }
            case YoutubeCell: {
                isSharingYoutube = switcherState.on;
                break;
            }
        }
    }
}

#pragma mark - FoursquareViewController
- (void)venueSelected:(NSDictionary *)venueSelected
{
	labelLocation.hidden = YES;
	titleSelectedLocation.text = [venueSelected objectForKey:@"name"];
	geotagInfo = [venueSelected copy];
}

#pragma mark - Sharing
//Open session for facebook
- (void)openSession
{
	NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_actions", nil];
	[FBSession openActiveSessionWithPermissions:permissions
                                   allowLoginUI:YES
                              completionHandler:
	 ^(FBSession *session, 
	   FBSessionState state, NSError *error) {
		 if (error) {
			 UIAlertView *alertView = [[UIAlertView alloc]
									   initWithTitle:nil
									   message:error.localizedDescription
									   delegate:nil
									   cancelButtonTitle:@"OK"
									   otherButtonTitles:nil];
			 alertView.tag = 3;
			 [alertView show];
		 } else {
			 [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:YES];
		 }    
		 
	 }];  
}
   
//Choose twitter account
- (void)chooseTwitterAccount
{
	//  First, we need to obtain the account instance for the user's Twitter account
	ACAccountStore *store = [[ACAccountStore alloc] init];
	ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
	//  Request permission from the user to access the available Twitter accounts
	
	//IOS 6 work
	if ([store respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)]) {
		if( [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] ) {
			[store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
				if (!granted) {
					// The user rejected your request
					NSLog(@"User rejected access to the account.");
				}
				else {
					// Grab the available accounts
					twitterAccounts = [store accountsWithAccountType:twitterAccountType];
					if ([twitterAccounts count] > 0) {
						if ([twitterAccounts count] > 1) {
							[self performSelectorOnMainThread:@selector(showActionSheet) withObject:self waitUntilDone:NO];
						} else {
							// Use the first account for simplicity
							ACAccount *account = [twitterAccounts objectAtIndex:0];
							NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
							[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
							[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:YES];
						}
					} else {
						[self performSelectorOnMainThread:@selector(showAlertViewTwitterForIOS6) withObject:nil waitUntilDone:NO];
					}
				}
				
			}];
		} else {
			[self performSelectorOnMainThread:@selector(showAlertViewTwitterForIOS6) withObject:nil waitUntilDone:NO];
		}
	}
	//ios 5
	else {
		//[store requestAccessToAccountsWithType:twitterAccountType withCompletionHandler:^(BOOL granted, NSError *error) {
        [store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
			if (!granted) {
				// The user rejected your request
				NSLog(@"User rejected access to the account.");
			}
			else {
				// Grab the available accounts
				twitterAccounts = [store accountsWithAccountType:twitterAccountType];
				if ([twitterAccounts count] > 0) {
					if ([twitterAccounts count] > 1) {
						[self performSelectorOnMainThread:@selector(showActionSheet) withObject:self waitUntilDone:NO];
					} else {
						// Use the first account for simplicity
						ACAccount *account = [twitterAccounts objectAtIndex:0];
						NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
						[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
						[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:YES];
					}
				} else {
					[self performSelectorOnMainThread:@selector(showAlertViewTwitter) withObject:nil waitUntilDone:NO];
				}
			}
		}];
	}
}

//Work with Youtube
- (GTMOAuth2Authentication *)getAuthYoutube {
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:kMyClientID clientSecret:kMyClientSecret];
	return auth;
}

- (void)loginOnYoutube
{
	NSString *scope = @"https://gdata.youtube.com";
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	
	
	GTMOAuth2ViewControllerTouch *viewController;
	viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:scope
																 clientID:kMyClientID
															 clientSecret:kMyClientSecret
														 keychainItemName:kKeychainItemName
																 delegate:self
														 finishedSelector:@selector(viewController:finishedWithAuth:error:)];
	
	[[self navigationController] pushViewController:viewController animated:YES];
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
	if (error != nil) {
		// Authentication failed
		NSLog(@"%@", error);
	} else {
		// Authentication succeeded
		authTokenYoutube = auth;
		[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
	}
}

- (NSMutableDictionary *)setStateShareSocialNetworks
{
	NSMutableDictionary *stateShareSocialNetworks = [[NSMutableDictionary alloc] init];
	NSNumber *facebookSharing = [NSNumber numberWithBool:isSharingFacebook];
	[stateShareSocialNetworks setObject:facebookSharing forKey:@"FacebookSharing"];
	
	NSNumber *twitterSharing = [NSNumber numberWithBool:isSharingTwitter];
	[stateShareSocialNetworks setObject:twitterSharing forKey:@"TwitterSharing"];

	NSNumber *youtubeSharing = [NSNumber numberWithBool:isSharingYoutube];
	[stateShareSocialNetworks setObject:youtubeSharing forKey:@"YoutubeSharing"];
	if (isSharingYoutube) {
		[stateShareSocialNetworks setObject:authTokenYoutube forKey:@"AuthTokenYoutube"];
	}
							  
	return stateShareSocialNetworks;
}

#pragma mark - Alerts and action
- (void)showActionSheet
{
	UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Twitter"
														delegate:self 
											   cancelButtonTitle:nil
										  destructiveButtonTitle:nil 
											   otherButtonTitles:nil];
	for (ACAccount *account in twitterAccounts) {
		[action addButtonWithTitle:account.username];
	}
	NSInteger numberOfButtons = action.numberOfButtons;
	[action addButtonWithTitle:@"Cancel"];
	action.cancelButtonIndex = numberOfButtons;
	[action showInView:self.view];
}

- (void)showAlertViewTwitterForIOS6
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
													message:@"There are no Twitter accounts configured. You can add or create a Twitter account in iPhone Settings."
												   delegate:self cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

- (void)showAlertViewTwitter
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts" 
													message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings." 
												   delegate:self cancelButtonTitle:@"Cancel" 
										  otherButtonTitles:@"Settings", nil];
	alert.tag = 1;
	[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch (alertView.tag) {
		case 0: { //video title is missing alert view
			break;
		}
		case 1: { //Twitter alert view
			if (buttonIndex == 1) {
				[TwitterSettings openTwitterAccounts];
			}
			break;
		}
	}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSInteger count = [twitterAccounts count];
	if (buttonIndex != count) {
		ACAccount *account = [twitterAccounts objectAtIndex:buttonIndex];
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
		[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
	}
}
@end
