//
//  VideoDetailViewController.m
//  Flickogram

#import "VideoDetailViewController.h"
#import "ManagerFlickogramAPI.h"
#import "MainTableViewCell.h"
#import "HeaderTableView.h"
#import "LikersTableViewController.h"
#import "SingletonAuthorizationUser.h"
#import "EGOImageView.h"
#import "NIAttributedLabel.h"
#import "ProfileViewController.h"
#import "YTVimeoExtractor/YTVimeoExtractor.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>

#import "GDataServiceGoogleYouTube.h"
#import "GDataEntryYouTubeUpload.h"

#import "MainViewController.h"

#import "ECSlidingViewController.h"
#import "CustomTabBarController.h"

@interface VideoDetailViewController () <NIAttributedLabelDelegate> {
    
    NSString *videoIDForReporting;
    UIWebView *webView;
    __weak IBOutlet UIButton *btnCross;
}
- (IBAction)crossBtnPressed:(id)sender;

@end

@implementation VideoDetailViewController
@synthesize table = _table;
@synthesize videoDetail = _videoDetail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Video";
    
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	self.navigationItem.title = @"Video";
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	
	currentCellSectionWithPlayingVideo = 0;
	
	NSInteger countLikes = [[self.videoDetail objectForKey:@"likes"] integerValue];
	NSInteger countComments = [[self.videoDetail objectForKey:@"comments"] integerValue];
	
	if (countLikes || countComments) {
		[[ManagerFlickogramAPI videoExemplar] getVideoDetailsOfVideoID:[self.videoDetail objectForKey:@"id"]
																userID:[self.videoDetail objectForKey:@"user_id"]
															indexArray:0
														  withDelegate:self];
	}
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (!self.player.fullscreen) {
		[self.player stop];
		[self removeMovieNotificationHandlers];
		self.player = nil;
		[self.table reloadData];
	}
}

#pragma mark - Get Video Details Delegate
- (void)getVideoDetailsSuccesWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)index
{
	NSString *getVideoID = [[data objectForKey:@"video_data"] objectForKey:@"id"];
	NSString *currentVideoID = [self.videoDetail objectForKey:@"id"];
	if ([getVideoID isEqualToString:currentVideoID]) {
		NSInteger countComments = [[[data objectForKey:@"video_data"] objectForKey:@"comments"] integerValue];
		NSInteger countLikes = [[[data objectForKey:@"video_data"] objectForKey:@"likes"] integerValue];
		
		if (countLikes) {
			[self.videoDetail setObject:[data objectForKey:@"video_likes"] forKey:@"video_likes"];
		}
		if (countComments) {
			[self.videoDetail setObject:[data objectForKey:@"video_comments"] forKey:@"video_comments"];
		}
	}
    [self.table reloadData];
}

- (void)getVideoDetailsFailedWithError:(NSString *)error
{
	NSLog(@"%@", error);
}

#pragma mark - Like Video Delegate
- (void)likeVideoSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *username = [[SingletonAuthorizationUser authorizationUser] userName];
	NSString *profilePicture = [[SingletonAuthorizationUser authorizationUser] profilePicture];
	
	NSInteger countLikes = [[self.videoDetail objectForKey:@"likes"] integerValue];
	
	if (countLikes) {
		NSString *keyLikeData = [NSString stringWithFormat:@"%d", countLikes];
		NSMutableDictionary *newLikeData = [[NSMutableDictionary alloc] init];
		[[self.videoDetail objectForKey:@"video_likes"] setObject:newLikeData forKey:keyLikeData];
		[[[self.videoDetail objectForKey:@"video_likes"] objectForKey:keyLikeData] setObject:userID forKey:@"user_id"];
		[[[self.videoDetail objectForKey:@"video_likes"] objectForKey:keyLikeData] setObject:profilePicture forKey:@"profile_picture"];
		[[[self.videoDetail objectForKey:@"video_likes"] objectForKey:keyLikeData] setObject:username forKey:@"user_name"];
		
		countLikes++;
		NSString *newCountLikes = [NSString stringWithFormat:@"%d", countLikes];
		[self.videoDetail setObject:newCountLikes forKey:@"likes"];
		
	} else {
		NSMutableDictionary *newVideoLikes = [[NSMutableDictionary alloc] init];
		[self.videoDetail setObject:newVideoLikes forKey:@"video_likes"];
		
		NSMutableDictionary *keyLikeData = [[NSMutableDictionary alloc] init];
		[[self.videoDetail objectForKey:@"video_likes"] setObject:keyLikeData forKey:@"0"];
		[[[self.videoDetail objectForKey:@"video_likes"] objectForKey:@"0"] setObject:userID forKey:@"user_id"];
		[[[self.videoDetail objectForKey:@"video_likes"] objectForKey:@"0"] setObject:profilePicture forKey:@"profile_picture"];
		[[[self.videoDetail objectForKey:@"video_likes"] objectForKey:@"0"] setObject:username forKey:@"user_name"];
		
		[self.videoDetail setObject:@"1" forKey:@"likes"];
	}
	[self.player stop];
	[self removeMovieNotificationHandlers];
	[self.table reloadData];
}

- (void)likeVideoFiledWithError:(NSString *)error
{
	NSLog(@"%@", error);
}

#pragma mark - Remove Video Delegate
- (void)removeVideoSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString *selectedUserID = [self.videoDetail objectForKey:@"user_id"];
	NSString *authorizedUserID = [[SingletonAuthorizationUser authorizationUser] userID];
	if ([selectedUserID isEqualToString:authorizedUserID]) {
		[self.table reloadData];
	}
    [self back];
}

-(void)removeVideoFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
    [self back];
}

#pragma mark - Add Comments Delegate
- (void)addCommentSuccessWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex
{
	NSMutableDictionary *commentDetails = [data objectForKey:@"comment_details"];
	NSString *getVideoID = [commentDetails objectForKey:@"video_id"];
	NSString *currentVideoID = [self.videoDetail objectForKey:@"id"];
	if ([getVideoID isEqualToString:currentVideoID]) {
		
		NSTimeInterval interval = [[NSDate date]timeIntervalSince1970];
		
		NSString *dateAdded = [NSString stringWithFormat:@"%d", (int)interval];
		[commentDetails setObject:dateAdded forKey:@"date_added"];
		
		NSInteger countComments = [[self.videoDetail objectForKey:@"comments"] integerValue];
		
		if (countComments) {
			NSDictionary *commentsDictionary = [self.videoDetail objectForKey:@"video_comments"];
			NSMutableArray *comments = [self parseDictionaryComments:commentsDictionary];
			[comments insertObject:commentDetails atIndex:0];
			NSDictionary *newComments = [self indexKeyedDictionaryFromArray:comments];
			
			[self.videoDetail setObject:newComments forKey:@"video_comments"];
			
			countComments++;
			NSString *newCountComments = [NSString stringWithFormat:@"%d", countComments];
			[self.videoDetail setObject:newCountComments forKey:@"comments"];
		} else {
			NSMutableDictionary *newVideoComments = [[NSMutableDictionary alloc] init];
			[self.videoDetail setObject:newVideoComments forKey:@"video_comments"];
			[[self.videoDetail objectForKey:@"video_comments"] setObject:commentDetails forKey:@"0"];
			[self.videoDetail setObject:@"1" forKey:@"comments"];
		}
		[self.player stop];
		[self removeMovieNotificationHandlers];
		[self.table reloadData];
	}
}

- (NSMutableArray *)parseDictionaryComments:(NSDictionary *)comments
{
	NSMutableArray *commentsArray = [[NSMutableArray alloc] init];
	NSInteger currentComment = 0;
	BOOL isMore = YES;
	while (isMore) {
		NSString *numberComment = [NSString stringWithFormat:@"%d", currentComment];
		if ([comments objectForKey:numberComment]) {
			[commentsArray addObject:[comments objectForKey:numberComment]];
			currentComment++;
		} else {
			isMore = NO;
		}
	}
	return commentsArray;
}

- (NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array
{
	id objectInstance;
	NSUInteger indexKey = 0;
	
	NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
	for (objectInstance in array) {
		[mutableDictionary setObject:objectInstance forKey:[NSString stringWithFormat:@"%d",indexKey]];
		indexKey++;
	}
	return mutableDictionary;
}

- (void)addCommentFailedWithError:(NSString *)error
{
	NSLog(@"%@", error);
}


#pragma mark - LBYouTubePlayerViewControllerDelegate

-(void)youTubePlayerViewController:(LBYouTubePlayerViewController *)controller didSuccessfullyExtractYouTubeURL:(NSURL *)videoURL {
	NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:controller.currentIndexArray];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:path];
	CGRect rectActivity = cell.activity.frame;
	cell.activity.frame = CGRectMake(rectActivity.origin.x + 10,
									 rectActivity.origin.y,
									 rectActivity.size.width,
									 rectActivity.size.height);
	
	self.player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
	[self.player prepareToPlay];
	[self.player.view setFrame: CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height) ];
	[cell.playbackBackground addSubview: self.player.view];
	cell.playbackBackground.hidden = NO;
	[self installMovieNotificationObservers];
	[self.player play];
	
	[cell addSubview:cell.activity];
	cell.activity.hidden = NO;
	[cell.activity startAnimating];

}

-(void)youTubePlayerViewController:(LBYouTubePlayerViewController *)controller failedExtractingYouTubeURLWithError:(NSError *)error {
    NSLog(@"Failed loading video due to error:%@", error);
}

#pragma mark - CommentsTableView Delegate
- (void)updateCommentsDictionary:(NSDictionary *)videoComments forIndexArray:(NSInteger)currentIndex
{
	if ([self.videoDetail objectForKey:@"video_comments"]) {
		[self.videoDetail setObject:videoComments forKey:@"video_comments"];
		NSInteger countComments = [[self.videoDetail objectForKey:@"comments"] integerValue];
		countComments--;
		NSString *countCommentString = [NSString stringWithFormat:@"%d", countComments];
		[self.videoDetail setObject:countCommentString forKey:@"comments"];
		[self.table reloadData];
	}
}

- (void)removeVideoCommentKeyForIndexArray:(NSInteger)currentIndex
{
	if ([self.videoDetail objectForKey:@"video_comments"]) {
		[self.videoDetail removeObjectForKey:@"video_comments"];
		[self.videoDetail setObject:@"0" forKey:@"comments"];
		[self.table reloadData];
	}
}

#pragma mark - Reflick Delegate
- (void)reflickVideoSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
													message:@"Success"
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
}

- (void)reflickVideoFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:error
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
	NSLog(@"%@", error);
}

#pragma mark - Actions for HeaderTableView button
- (void)touchReflickButton:(id)sender
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *videoID = [self.videoDetail objectForKey:@"id"];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI videoExemplar] reflickVideoForUserID:authUser videoID:videoID delegate:self];
}

#pragma mark - Actions for MainTableViewCell buttons
- (void)toggleLikeStateInSection:(id)sender
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *videoID = [self.videoDetail objectForKey:@"id"];
	BOOL isAlreadyLiked = NO;
	NSInteger countLikes = [[self.videoDetail objectForKey:@"likes"] integerValue];
	if (countLikes) {
		if ([self.videoDetail objectForKey:@"video_likes"]) {
			for (NSString *likeKey in [self.videoDetail objectForKey:@"video_likes"]) {
				NSString *likeID = [[[self.videoDetail objectForKey:@"video_likes"] objectForKey:likeKey] objectForKey:@"user_id"];
				if ([userID isEqualToString:likeID]) {
					isAlreadyLiked = YES;
					break;
				} else {
					isAlreadyLiked = NO;
				}
			}
			if (isAlreadyLiked) {
				NSLog(@"AlreadyLiked");
			}
		}
	}
	if (!isAlreadyLiked) {
		[[ManagerFlickogramAPI videoExemplar] likeVideoForUserID:userID videoID:videoID indexArray:0 withDelegate:self];
	}
}

- (void)touchShowMoreButton:(id)sender
{
	LikersTableViewController *likers = [[LikersTableViewController alloc] init];
	likers.videoLikes = [NSDictionary dictionaryWithDictionary:[self.videoDetail objectForKey:@"video_likes"]];
	[self.player stop];
	[self removeMovieNotificationHandlers];
	[self.navigationController pushViewController:likers animated:YES];
}

- (void)touchPlayVideoButton:(id)sender
{
	NSString *videoType = [self.videoDetail objectForKey:@"video_type"];
	NSString *videoUrl = [self.videoDetail objectForKey:@"video_url"];
    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:path];
	if ([videoType isEqualToString:@"flickogram"]) {
		CGRect rectActivity = cell.activity.frame;
		cell.activity.frame = CGRectMake(rectActivity.origin.x + 10,
										 rectActivity.origin.y,
										 rectActivity.size.width,
										 rectActivity.size.height);
		
		self.player = [[MPMoviePlayerController alloc] initWithContentURL: [NSURL URLWithString:videoUrl]];
		[self.player prepareToPlay];
		[self.player.view setFrame: CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height) ];
		[cell.playbackBackground addSubview: self.player.view];
		cell.playbackBackground.hidden = NO;
		[self installMovieNotificationObservers];
		
		[cell addSubview:cell.activity];
		cell.activity.hidden = NO;
		[cell.activity startAnimating];

		[self.player play];
		
	} else if ([videoType isEqualToString:@"youtube"]) {
//		LBYouTubePlayerViewController *yotubeController = [[LBYouTubePlayerViewController alloc] initWithYouTubeURL:[NSURL URLWithString:videoURL]];
//		yotubeController.currentIndexArray = 0;
//		yotubeController.delegate = self;
//		yotubeController.quality = LBYouTubePlayerQualityLarge;
        
        //videoUrl = @"https://www.youtube.com/v/sWcKtJj5_oY";
        
        videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
        if ([videoUrl rangeOfString:@"https"].location == NSNotFound) {
            videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"http" withString:@"https"];
        }
        
        NSLog(@"url %@, component %@", videoUrl, [videoUrl lastPathComponent]);
        
        NSString *embedHTML = @"\
        <html><head>\
        <style type=\"text/css\">\
        body {\
        background-color: transparent;\
        color: black;\
        }\
        </style>\
        </head><body style=\"margin:0\">\
        <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
        width=\"%0.0f\" height=\"%0.0f\"></embed>\
        </body></html>";
        
        NSString *html = [NSString stringWithFormat:embedHTML, videoUrl, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height];
        
        webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height)];
        
        [webView loadHTMLString:html baseURL:nil];
        [cell.playbackBackground addSubview:webView];
        [cell.playbackBackground setHidden:NO];
	}
    else if ([videoType isEqualToString:@"vimeo"]) {
        NSString *strNewUrl = [NSString stringWithFormat:@"http://player.vimeo.com/video/%@", [videoUrl lastPathComponent]];
        
//        webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height)];
//        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strNewUrl]]];
//        
//        [cell.playbackBackground addSubview:webView];
//        [cell.playbackBackground setHidden:NO];
        
        webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
        [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strNewUrl]]];
        
        [self.view addSubview:webView];
        [self.view bringSubviewToFront:btnCross];
        [btnCross setHidden:NO];
        
//        [YTVimeoExtractor fetchVideoURLFromURL:strNewUrl //@"http://vimeo.com/58600663"
//                                       quality:YTVimeoVideoQualityLow
//                             completionHandler:^(NSURL *videoURL, NSError *error, YTVimeoVideoQuality quality) {
//                                 if (error) {
//                                     // handle error
//                                     NSLog(@"Video URL: %@", [videoURL absoluteString]);
//                                 } else {
//                                     NSLog(@"Video URL: %@", [videoURL absoluteString]);
//                                     dispatch_async(dispatch_get_main_queue(), ^{
//                                         
//                                         self.player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
//                                         //[self.player prepareToPlay];
//                                         //self.player.movieSourceType = MPMovieSourceTypeStreaming;
//                                         [self installMovieNotificationObservers];
//                                         
//                                         [self.player.view setFrame: CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height) ];
//                                         [cell.playbackBackground addSubview:self.player.view];
//                                         cell.playbackBackground.hidden = NO;
//                                         
//                                         [cell addSubview:cell.activity];
//                                         cell.activity.hidden = NO;
//                                         [cell.activity startAnimating];
//                                         
//                                         [self.player play];
//                                     });
//                                 }
//                             }];
    }
	
	UIButton *currentButton = (UIButton *)sender;
	NSInteger sectionTable = currentButton.tag;
	currentCellSectionWithPlayingVideo = sectionTable;
}

- (void)touchSendButton:(id)sender
{
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	NSString *comment = cell.textField.text;
	if ([comment length]) {
		NSString *videoID = [self.videoDetail objectForKey:@"id"];
		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
		[[ManagerFlickogramAPI videoExemplar] addCommentOnVideo:videoID userID:userID comment:comment indexArray:0 withDelegate:self];
		cell.textField.text = @"";
		[cell.textField resignFirstResponder];
	}
}

- (void)touchShowCommentsButton:(id)sender
{
	CommentsTableViewController *comments = [[CommentsTableViewController alloc] init];
	comments.currentIndexArray = 0;
	comments.delegate = self;
	if ([self.videoDetail objectForKey:@"video_comments"]) {
		comments.userComments = [[self.videoDetail objectForKey:@"video_comments"] copy];
	}
	[self.player stop];
	[self removeMovieNotificationHandlers];
	[self.navigationController pushViewController:comments animated:YES];
}

- (void)touchStreamButton:(id)sender
{
	UIButton *currentButton = (UIButton *)sender;
	NSInteger sectionTable = currentButton.tag;
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	videoIDForReporting = [self.videoDetail objectForKey:@"user_id"];
	
	if ([userID isEqualToString:videoIDForReporting]) {
        //		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Video Options"
        //															delegate:self
        //												   cancelButtonTitle:@"Cancel"
        //											  destructiveButtonTitle:@"Delete" otherButtonTitles:nil];
        //		action.tag = sectionTable;
        //		[action showFromTabBar:self.tabBarController.tabBar];
		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Video Options"
															delegate:self
												   cancelButtonTitle:@"Cancel"
											  destructiveButtonTitle:@"Delete" otherButtonTitles:@"Save in Gallery",
                                 @"Share with Youtube",//@"Email Video",
                                 @"Share with Facebook",
                                 @"Share with Twitter", nil];
        
        //NSLog(@"buttons %@", [action valueForKey:@"_buttons"]);
        
        [[[action valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"btn-save.png"] forState:UIControlStateNormal];
        [[[action valueForKey:@"_buttons"] objectAtIndex:2] setImage:[UIImage imageNamed:@"btn-youtube.png"] forState:UIControlStateNormal];
        [[[action valueForKey:@"_buttons"] objectAtIndex:3] setImage:[UIImage imageNamed:@"btn-fb.png"] forState:UIControlStateNormal];
        [[[action valueForKey:@"_buttons"] objectAtIndex:4] setImage:[UIImage imageNamed:@"btn-twitter.png"] forState:UIControlStateNormal];
        
		action.tag = sectionTable;
		[action showFromTabBar:self.tabBarController.tabBar];
	} else {
		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"In order to share other's video you have to reflix it"//@"Video Options"
															delegate:self
												   cancelButtonTitle:@"Cancel"
											  destructiveButtonTitle:@"Flag for review"
												   otherButtonTitles:nil];
		action.tag = 88888888;
		[action showFromTabBar:self.tabBarController.tabBar];
	}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (actionSheet.tag == 88888888) {
		if (buttonIndex == 0) {
			UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Reason for reporting:"
																delegate:self
													   cancelButtonTitle:@"Cancel"
												  destructiveButtonTitle:nil
													   otherButtonTitles:@"Nudity", @"Copyright", @"Terms of Use Violation", nil];
			action.tag = 99999999;
			[action showFromTabBar:self.tabBarController.tabBar];
		}
	} else if (actionSheet.tag == 99999999) {
		if (buttonIndex == 0) {
			[self reportVideoWithReason:@"Nudity"];
		} else if (buttonIndex == 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Who owns this photo?"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Submit", nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            alert.tag = 99999999;
            [alert show];
        } else if (buttonIndex == 2) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reason for reporting:"
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Submit", nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            alert.tag = 99999999;
            [alert show];
        }
	}
    else {
		if (buttonIndex == 0) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Deletion"
															message:@"Delete this video?"
														   delegate:self
												  cancelButtonTitle:@"Don't Delete"
												  otherButtonTitles:@"Delete", nil];
			alert.tag = actionSheet.tag;
			[alert show];
		}
        else if (buttonIndex == 1) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[self.videoDetail objectForKey:@"video_url"]]];
                //CGFloat time = [NSDate timeIntervalSinceReferenceDate];
                NSString *fileStr = [@"Documents/FlixogramVid" stringByAppendingPathExtension:@"mov"];
                NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:fileStr];
                NSError *error;
                BOOL isSaved = [data writeToFile:filePath options:NSDataWritingAtomic error:&error];
                if (isSaved) {
                    NSFileManager *fileManage = [[NSFileManager alloc] init];
                    if ([fileManage fileExistsAtPath:filePath isDirectory:NO]) {
                        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath)) {
                            UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
                        }
                    }
                }
                else {
                    NSLog(@"not saved with error %@", error.localizedDescription);
                }
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
            });
            
            //linkForSharing = [self.videoDetail objectForKey:@"video_url"];
        }
        else if (buttonIndex == 2) {
            //NSString *videoURLForSharing = [NSURL URLWithString:[self.videoDetail objectForKey:@"video_url"]];
            if ([[self.videoDetail objectForKey:@"video_url"] rangeOfString:@"youtube"].location != NSNotFound) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Youtube video can not share on Youtube" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else {
                
                GTMOAuth2Authentication *auth = [self getAuthYoutube];
				if (auth) {
					// if the auth object contains an access token, didAuth is now true
					BOOL didAuth = [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:@"My App:Flickogram"
																			   authentication:auth];
					if (didAuth) {
						// retain the authentication object, which holds the auth tokens
						// we can determine later if the auth object contains an access token
						// by calling its -canAuthorize method
						BOOL isSignedIn = [auth canAuthorize];
						if (isSignedIn) {
                            
//                            self.table.refreshEnabled = NO;
//                            isUploadVideoFile = YES;
//                            uploadPercentage = 0;
//                            currentServiceName = @"Youtube Service";
                            [self.table reloadData];
                            //authToken = [statesSharingInSocialNetworks objectForKey:@"AuthTokenYoutube"];
                            
                            //videoIDForReporting = [NSURL URLWithString:[[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_url"]];
                            
                            if (authToken == nil || authToken == NULL) {
                                authToken = [self getAuthYoutube];
                                if (authToken == nil || authToken == NULL) {
                                    
                                    [self loginOnYoutube];
                                }
                                else {
                                    [self getAuthTokenYoutube];
                                }
                            }
                            else {
                                [self getAuthTokenYoutube];
                            }
						}
					}
                    else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                        message:@"Please login with your Youtube account in Profile sharing settings"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
				}
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"Please login with your Youtube account in Profile settings"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                
            }
        }
        else if (buttonIndex == 3) {
//            captionForSharing = [self.videoDetail  objectForKey:@"video_description"];
//            linkForSharing = [self.videoDetail objectForKey:@"video_url"];
//            videoTitle = [self.videoDetail objectForKey:@"video_title"];
            
//            UINavigationController *navController = [(CustomTabBarController *)self.slidingViewController.topViewController viewControllers][0];
//            NSLog(@"sliding viewController %@", [navController viewControllers]);

            [self openSession];
            //[self openSession];
            //[self performSelectorInBackground:@selector(openSession) withObject:nil];
        }
        else if (buttonIndex == 4) {
//            captionForSharing = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_title"];
//            linkForSharing = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_url"];
            //videoTitle = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_title"];
//            UINavigationController *navController = [(CustomTabBarController *)self.slidingViewController.topViewController viewControllers][0];

            [self shareTwitter];
        }
	}
}

//Work with Youtube
- (GTMOAuth2Authentication *)getAuthYoutube {
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:kMyClientID clientSecret:kMyClientSecret];
	return auth;
}

- (void)loginOnYoutube
{
	NSString *scope = @"https://gdata.youtube.com";
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	
	GTMOAuth2ViewControllerTouch *viewController;
	viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:scope
																clientID:kMyClientID
															clientSecret:kMyClientSecret
														keychainItemName:kKeychainItemName
																delegate:self
														finishedSelector:@selector(viewController:finishedWithAuth:error:)];
	
	[[self navigationController] pushViewController:viewController animated:YES];
}
- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
	if (error != nil) {
		// Authentication failed
		NSLog(@"%@", error);
	} else {
		// Authentication succeeded
		authToken = auth;
		[self performSelectorOnMainThread:@selector(getAuthTokenYoutube) withObject:nil waitUntilDone:NO];
	}
}

//Share Youtube
- (void)getAuthTokenYoutube
{
	NSString *urlStr = @"https://accounts.google.com/o/oauth2/auth";
	NSURL *url = [NSURL URLWithString:urlStr];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[authToken authorizeRequest:request
                       delegate:self
              didFinishSelector:@selector(authentication:request:finishedWithError:)];
}

- (void)authentication:(GTMOAuth2Authentication *)auth
               request:(NSMutableURLRequest *)request
     finishedWithError:(NSError *)error {
	if (error != nil) {
		// Authorization failed
		NSLog(@"Auth token = %@", error);
	} else {
		// Authorization succeeded
		authToken = auth;
		[self uploadVideoOnYoutube];
	}
}

- (GDataServiceGoogleYouTube *)youTubeService {
	static GDataServiceGoogleYouTube* service = nil;
	if (!service) {
		service = [[GDataServiceGoogleYouTube alloc] init];
		[service setShouldCacheDatedData:YES];
		[service setServiceShouldFollowNextLinks:YES];
		[service setIsServiceRetryEnabled:YES];
	}
	return service;
}

- (void)uploadVideoOnYoutube
{
    //[progressView setProgress:0.0];
    
    NSString *devKey = @"AI39si7GQ3cqX8NdiVh7vJgFU6HNjcedSFcVT877fa2YFBDqLI7RsrvKxamkKAU5PXrPUeLIDujoU6wbYk4D-S-sEthpJARG0A";
	
    GDataServiceGoogleYouTube *service = [self youTubeService];
	[service setAuthorizer:authToken];
	[[service authorizer] setShouldAuthorizeAllRequests:YES];
    [service setYouTubeDeveloperKey:devKey];
    
    NSURL *videoURLForSharing = [NSURL URLWithString:[self.videoDetail objectForKey:@"video_url"]];
    
    if ([[videoURLForSharing host] rangeOfString:@"vimeo"].location == NSNotFound) {
        
        if ([[videoURLForSharing host] rangeOfString:@"youtube"].location != NSNotFound) {
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Youtube video can not upload on youtube" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            NSString *strVideoUrl = [videoURLForSharing absoluteString];
            
            NSArray *arr = [strVideoUrl componentsSeparatedByString:@"="];
            if ([arr count] > 0) {
                if ([arr[1] rangeOfString:@"&"].location != NSNotFound) {
                    arr = [arr[1] componentsSeparatedByString:@"&"];
                    strVideoUrl = arr[0];
                }
                else {
                    strVideoUrl = arr[1];
                }
            }
            
            NSLog(@"video url %@, id %@", [videoURLForSharing host], strVideoUrl);
            
            //videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:@"VpZmIiIXuZ0"];
            
            //[self youtubeShareNowWithServiceObj:service data:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://r5---sn-a8au-p5qz.googlevideo.com/videoplayback?ms=au&id=o-AOQqCWTdlD2u_gZpWrKn6_D4bKp7uLmJT09ulnWfd_Q8&ratebypass=yes&mv=m&mt=1397473450&expire=1397499384&sver=3&sparams=id%2Cip%2Cipbits%2Citag%2Cratebypass%2Csource%2Cupn%2Cexpire&ipbits=0&source=youtube&upn=FMs3dfmCrBA&signature=E476E66E85714DE7E7FFA5C28FF6D1CB3E6896CB.7A7C28E0B0730C38AA759AFE70149B736557139A&ip=162.210.196.172&key=yt5&fexp=931323%2C927618%2C932103%2C938648%2C914092%2C916612%2C927619%2C937417%2C913434%2C936916%2C934022%2C936923&itag=22"]]];
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            
            progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
            progressView.center = self.view.center;
            [progressView setProgress:0.0];
            [self.view addSubview:progressView];
            
            [self youtubeShareNowWithServiceObj:service data:[NSData dataWithContentsOfURL:videoURLForSharing]];
        }
    }
    else {
        NSString *strNewUrl = [NSString stringWithFormat:@"http://player.vimeo.com/video/%@", [videoURLForSharing lastPathComponent]];
        
        [YTVimeoExtractor fetchVideoURLFromURL:strNewUrl //@"http://vimeo.com/58600663"
                                       quality:YTVimeoVideoQualityLow
                             completionHandler:^(NSURL *videoURL, NSError *error, YTVimeoVideoQuality quality) {
                                 if (error) {
                                     // handle error
                                     NSLog(@"Video URL: %@", [videoURL absoluteString]);
                                 } else {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                                         
                                         progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
                                         progressView.center = CGPointMake(self.view.center.x/2, 20);
                                         [progressView setProgress:0.0];
                                         [self.view addSubview:progressView];

                                         [self youtubeShareNowWithServiceObj:service data:[NSData dataWithContentsOfURL:videoURL]];
                                     });
                                 }
                             }];
        
    }
    
}

- (void)youtubeShareNowWithServiceObj:(GDataServiceGoogleYouTube *)service data:(NSData *)data {
    
    NSURL *url = [GDataServiceGoogleYouTube youTubeUploadURLForUserID:kGDataServiceDefaultUser];
    
    // load the file data
    NSURL *videoURLForSharing = [NSURL URLWithString:[self.videoDetail objectForKey:@"video_url"]];
    NSString *path = [videoURLForSharing path];
    //NSData *data = [NSData dataWithContentsOfURL:videoURLForSharing];
    NSString *filename = [path lastPathComponent];
    
    // gather all the metadata needed for the mediaGroup
    NSString *titleStr = @"YouTubeVideo";
    GDataMediaTitle *title = [GDataMediaTitle textConstructWithString:titleStr];
    
    NSString *categoryStr = @"Entertainment";
    GDataMediaCategory *category = [GDataMediaCategory mediaCategoryWithString:categoryStr];
    [category setScheme:kGDataSchemeYouTubeCategory];
    
    NSString *descStr = @"Video";
    GDataMediaDescription *desc = [GDataMediaDescription textConstructWithString:descStr];
    
    NSString *keywordsStr = @"Flixogram";
    GDataMediaKeywords *keywords = [GDataMediaKeywords keywordsWithString:keywordsStr];
    
    BOOL isPrivate = NO;
    
    GDataYouTubeMediaGroup *mediaGroup = [GDataYouTubeMediaGroup mediaGroup];
    [mediaGroup setMediaTitle:title];
    [mediaGroup setMediaDescription:desc];
    [mediaGroup addMediaCategory:category];
    [mediaGroup setMediaKeywords:keywords];
    [mediaGroup setIsPrivate:isPrivate];
    
    NSString *mimeType = [GDataUtilities MIMETypeForFileAtPath:path
											   defaultMIMEType:@"video/mov"];
    
    // create the upload entry with the mediaGroup and the file data
    GDataEntryYouTubeUpload *entry;
    entry = [GDataEntryYouTubeUpload uploadEntryWithMediaGroup:mediaGroup
                                                          data:data
                                                      MIMEType:mimeType
                                                          slug:filename];
	
    SEL progressSel = @selector(ticket:hasDeliveredByteCount:ofTotalByteCount:);
    [service setServiceUploadProgressSelector:progressSel];
    
    GDataServiceTicket *ticket;
    ticket = [service fetchEntryByInsertingEntry:entry
                                      forFeedURL:url
                                        delegate:self
                               didFinishSelector:@selector(uploadTicket:finishedWithEntry:error:)];
    
    [self setUploadTicket:ticket];
    
}

// progress callback
- (void)ticket:(GDataServiceTicket *)ticket hasDeliveredByteCount:(unsigned long long)numberOfBytesRead
ofTotalByteCount:(unsigned long long)dataLength
{
    CGFloat percent = (CGFloat) numberOfBytesRead / (CGFloat) dataLength;
    NSLog(@"percent %f", percent);
    [progressView setProgress:percent];
    if (percent == 1.0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Vidoe uploaded on Youtube" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [progressView removeFromSuperview];
    }
    
    
//	uploadPercentage = (CGFloat) numberOfBytesRead / (CGFloat) dataLength;
//	[progressView setProgress:uploadPercentage];
    
    
}



// upload callback
- (void)uploadTicket:(GDataServiceTicket *)ticket
   finishedWithEntry:(GDataEntryYouTubeVideo *)videoEntry
               error:(NSError *)error {
//	isUploadVideoFile = NO;
//	if ([self.followingVideos count]) {
//		[self reloadVideo];
//	}
	
    if (error != nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:[NSString stringWithFormat:@"Error: %@",
                                                                 [error description]]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        
        [alert show];
    }
    [self setUploadTicket:nil];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	//self.table.refreshEnabled = YES;
}

#pragma mark -
#pragma mark Setters

- (GDataServiceTicket *)uploadTicket {
    return mUploadTicket;
}

- (void)setUploadTicket:(GDataServiceTicket *)ticket {
    mUploadTicket = ticket;
}

- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:nil
                                                               message:@"Video saved to Gallery"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
        [successAlert show];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == 99999999) {
		if (buttonIndex == 1) {
			NSString *reason = [[alertView textFieldAtIndex:0] text];
			[self reportVideoWithReason:reason];
		}
	} else if (alertView.tag == 10000) {
        if (buttonIndex == 1) {
            [self.slidingViewController resetTopView];
            //[self logoutAndReset];
        }
        
    } else {
		if (buttonIndex == 1) {
			//NSDictionary *data = [self.followingVideos objectAtIndex:alertView.tag];
			//NSString *videoID = [data objectForKey:@"id"];
            
            NSString *videoID = [NSURL URLWithString:[self.videoDetail objectForKey:@"id"]];
            
			NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			[[ManagerFlickogramAPI videoExemplar] removeVideoID:videoID userID:userID indexArray:alertView.tag withDelegate:self];
		}
	}
}

- (void)reportVideoWithReason:(NSString *)reasonReport
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI videoExemplar] reportVideo:videoIDForReporting
										   reporterID:userID
										 reasonReport:reasonReport
											 delegate:self];
}

#pragma mark - Report User Delegate
- (void)reportVideoSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Thanks"
														   message:@"We'll review this user as soon as possible."
														  delegate:nil
												 cancelButtonTitle:@"Dismiss"
												 otherButtonTitles:nil];
	[successAlert show];
}

#pragma mark - Sharing
//Open session for facebook
- (void)openSession
{
    //NSArray *permissions = @[@"publish_stream", @"publish_actions"];
    NSArray *permissions = @[@"publish_actions"];
	[FBSession openActiveSessionWithPermissions:permissions
                                   allowLoginUI:YES
                              completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                  [FBSession setActiveSession:session];
                                  if (error) {
                                      UIAlertView *alertView = [[UIAlertView alloc]
                                                                initWithTitle:nil
                                                                message:error.localizedDescription
                                                                delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                                      [alertView show];
                                  } else {
                                      if (state == FBSessionStateOpen) {
                                          
                                          [self shareFacebook];
                                      }
                                  }
                              }];
}
//facebook share
- (void)shareFacebook
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSString *linkForSharing = [self.videoDetail objectForKey:@"video_url"];
    NSString *videoTitle = [self.videoDetail objectForKey:@"video_title"];
    NSString *captionForSharing = [self.videoDetail objectForKey:@"video_description"];
    if ([linkForSharing rangeOfString:@"www.youtube.com"].location != NSNotFound) {
        
        NSDictionary *params = @{@"name": videoTitle,
                                 @"link": linkForSharing,//@"video.mov": data,
                                 @"contentType": @"video/quicktime",
                                 @"description": [captionForSharing stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
        [self postToFBWithGraphPath:@"me/feed" andParams:params];
        
    } else {
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:linkForSharing]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            if (data) {
                NSString *captionForSharing = [self.videoDetail objectForKey:@"video_description"];

                NSDictionary *params = @{//@"link" : @"http://developer.avenuesocial.com/azeemsal/flickogram/images/profile_pictures/1390909163.jpg",
                                         @"name": videoTitle,
                                         @"video.mov": data,
                                         @"contentType": @"video/quicktime",
                                         @"description": [captionForSharing stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
                //NSLog(@"hello %@", params);
                [self postToFBWithGraphPath:@"me/videos" andParams:params];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"This video can not upload to Facebook"
                                               delegate:self
                                      cancelButtonTitle:@"OK!"
                                      otherButtonTitles:nil] show];
                    
                });
            }
        }];
    }
}

- (void)postToFBWithGraphPath:(NSString *)graphPath andParams:(NSDictionary *)params {
    
    [FBRequestConnection startWithGraphPath:graphPath
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                  NSString *alertText;
                                  if (error) {
                                      alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d", error.domain, error.code];
                                  } else {
                                      alertText = @"Video posted to Facebook";
                                      //alertText = [NSString stringWithFormat:@"Posted action, id: %@", [result objectForKey:@"id"]];
                                  }
                                  // Show the result in an alert
                                  [[[UIAlertView alloc] initWithTitle:@"Result"
                                                              message:alertText
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK!"
                                                    otherButtonTitles:nil] show];
                              });
                          }];
}

//twitter share
- (void)shareTwitter
{
	//First, we need to obtain the account instance for the user's Twitter account
    
    ACAccountStore *store = [[ACAccountStore alloc] init];
	ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    if ([store respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)]) {
		if( [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] ) {
            
            [store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
				if (!granted) {
					// The user rejected your request
					NSLog(@"User rejected access to the account.");
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
				}
				else {
					// Grab the available accounts
					NSArray *twitterAccounts = [store accountsWithAccountType:twitterAccountType];
					if ([twitterAccounts count] > 0) {
                        NSString *captionForSharing = [self.videoDetail objectForKey:@"video_title"];
                        NSString *linkForSharing = [self.videoDetail objectForKey:@"video_url"];
                        
                        ACAccount *account = [twitterAccounts objectAtIndex:0];
                        // Now make an authenticated request to our endpoint
                        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                        NSString *status = [NSString stringWithFormat:@"%@, %@", [captionForSharing stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                            linkForSharing];
                        [params setObject:status forKey:@"status"];
                        
                        //  The endpoint that we wish to call
                        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1/statuses/update.json"];
                        //  Build the request with our parameter
                        
                        //TWRequest *request = [[TWRequest alloc] initWithURL:url parameters:params requestMethod:TWRequestMethodPOST];
                        SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:params];
                        
                        // Attach the account object to this request
                        [request setAccount:account];
                        [request performRequestWithHandler: ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                            if (!responseData) {
                                // inspect the contents of error
                                NSLog(@"%@", error);
                            }
                            else {
                                NSError *jsonError;
                                NSArray *timeline = [NSJSONSerialization JSONObjectWithData:responseData
                                                                                    options:NSJSONReadingMutableLeaves
                                                                                      error:&jsonError];
                                if (timeline) {
                                    // at this point, we have an object that we can parse
                                    NSLog(@"%@", timeline);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                                        message:@"Video link Tweeted"
                                                                                       delegate:nil
                                                                              cancelButtonTitle:@"OK"
                                                                              otherButtonTitles:nil];
                                        [alert show];
                                    });
                                }
                                else {
                                    // inspect the contents of jsonError
                                    NSLog(@"%@", jsonError);
                                }
                            }
                        }];
					}
				}
			}];
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Please setup Twitter account in your settings"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
            
        }
	}
}

//- (void)touchStreamButton:(id)sender
//{
//    
//	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
//	NSString *selectedVideoUserID = [self.videoDetail objectForKey:@"user_id"];
//	
//	if ([userID isEqualToString:selectedVideoUserID]) {
//		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Video Options"
//															delegate:self
//												   cancelButtonTitle:@"Cancel"
//											  destructiveButtonTitle:@"Delete" otherButtonTitles:nil];
//		action.tag = 0;
//        [action showFromTabBar:self.tabBarController.tabBar];
//	}
//}
//
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//	if (buttonIndex == 0) {
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Deletion" message:@"Delete this video?" delegate:self cancelButtonTitle:@"Don't Delete" otherButtonTitles:@"Delete", nil];
//		alert.tag = actionSheet.tag;
//		[alert show];
//	}
//	
//}
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//	if (buttonIndex == 1) {
//		NSString *videoID = [self.videoDetail objectForKey:@"id"];
//		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
//		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//		[[ManagerFlickogramAPI videoExemplar] removeVideoID:videoID userID:userID indexArray:alertView.tag withDelegate:self];
//	}
//}

#pragma mark - Install notifications for MPMoviePlayer
/* Register observers for the various movie object notifications. */
-(void)installMovieNotificationObservers
{
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadStateDidChange:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:self.player];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.player];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                 name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                               object:self.player];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:self.player];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerDidEnterFullscreen:)
                                                 name:MPMoviePlayerDidEnterFullscreenNotification
                                               object:self.player];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayrDidExitFullscreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.player];
}

#pragma mark Remove Movie Notification Handlers

/* Remove the movie notification observers from the movie object. */
-(void)removeMovieNotificationHandlers
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:self.player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:self.player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.player];
	[[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerDidEnterFullscreenNotification object:self.player];
	[[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:self.player];
}

#pragma mark Movie Notification Handlers

/*  Notification called when the movie finished playing. */
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    NSNumber *reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	switch ([reason integerValue])
	{
            /* The end of the movie was reached. */
		case MPMovieFinishReasonPlaybackEnded:
            /*
             Add your code here to handle MPMovieFinishReasonPlaybackEnded.
             */
			cell.activity.hidden = YES;
			NSLog(@"Finish");
			break;
            
            /* An error was encountered during playback. */
		case MPMovieFinishReasonPlaybackError:
            NSLog(@"An error was encountered during playback");
			break;
            
            /* The user stopped playback. */
		case MPMovieFinishReasonUserExited:
			cell.activity.hidden = YES;
			NSLog(@"stop");
			break;
            
		default:
			break;
	}
}

/* Handle movie load state changes. */
- (void)loadStateDidChange:(NSNotification *)notification
{
	MPMoviePlayerController *player = notification.object;
	MPMovieLoadState loadState = player.loadState;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	
	/* The load state is not known at this time. */
	if (loadState & MPMovieLoadStateUnknown)
	{
		NSLog(@"LoadStateUnknown");
	}
	
	/* The buffer has enough data that playback can begin, but it
	 may run out of data before playback finishes. */
	if (loadState & MPMovieLoadStatePlayable)
	{
		cell.activity.hidden = YES;
		NSLog(@"LoadStatePlayable");
	}
	
	/* Enough data has been buffered for playback to continue uninterrupted. */
	if (loadState & MPMovieLoadStatePlaythroughOK)
	{
        // Add an overlay view on top of the movie view
		cell.activity.hidden = YES;
		NSLog(@"LoadStatePlaythroughOK");
	}
	
	/* The buffering of data has stalled. */
	if (loadState & MPMovieLoadStateStalled)
	{
		cell.activity.hidden = NO;
		[cell.activity startAnimating];
		NSLog(@"LoadStateStalled");
	}
}

/* Called when the movie playback state has changed. */
- (void) moviePlayBackStateDidChange:(NSNotification*)notification
{
	MPMoviePlayerController *player = notification.object;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	/* Playback is currently stopped. */
	if (player.playbackState == MPMoviePlaybackStateStopped)
	{
		cell.activity.hidden = YES;
		NSLog(@"PlaybackStopped");
	}
	/*  Playback is currently under way. */
	else if (player.playbackState == MPMoviePlaybackStatePlaying)
	{
		cell.activity.hidden = YES;
		NSLog(@"PlaybackPlaying");
	}
	/* Playback is currently paused. */
	else if (player.playbackState == MPMoviePlaybackStatePaused)
	{
		cell.activity.hidden = YES;
		NSLog(@"PlaybackPause");
	}
	/* Playback is temporarily interrupted, perhaps because the buffer
	 ran out of content. */
	else if (player.playbackState == MPMoviePlaybackStateInterrupted)
	{
		NSLog(@"PlaybackInterrupted");
	}
}

/* Notifies observers of a change in the prepared-to-play state of an object
 conforming to the MPMediaPlayback protocol. */
- (void) mediaIsPreparedToPlayDidChange:(NSNotification*)notification
{
	// Add an overlay view on top of the movie view
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	cell.activity.hidden = NO;
	[cell.activity startAnimating];
}

- (void)moviePlayerDidEnterFullscreen:(NSNotification *)notification
{
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	cell.activity.hidden = YES;
}

- (void)moviePlayrDidExitFullscreen:(NSNotification *)notification
{
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	cell.activity.hidden = YES;
}


#pragma mark - TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	HeaderTableView *header;
	NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"HeaderTableView" owner:nil options:nil];
	for (id currentObject in topLevelObjects) {
		if ([currentObject isKindOfClass:[UIView class]]) {
			header = (HeaderTableView *) currentObject;
			break;
		}
	}
	

	header.userName.text = [self.videoDetail objectForKey:@"user_name"];
	if ([self.videoDetail objectForKey:@"location_name"] != [NSNull null]) {
		header.locationName.text = [self.videoDetail objectForKey:@"location_name"];
	}
	
	NSString *currentUser = [self.videoDetail objectForKey:@"user_id"];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	
	//show reflick button
	if (![authUser isEqualToString:currentUser]) {
		header.reflickButton.hidden = NO;
		header.reflickButton.tag = section;
		[header.reflickButton addTarget:self action:@selector(touchReflickButton:) forControlEvents:UIControlEventTouchUpInside];
	}
	
	header.imageView.placeholderImage = [UIImage imageNamed:@"Profile-thumb"];
	header.imageView.imageURL = [NSURL URLWithString:[self.videoDetail objectForKey:@"profile_picture"]];
	return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 46;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int height = 0;
    switch ([[self.videoDetail valueForKey:@"comments"] intValue]) {
        case 0:
            height = 0;
            break;
        case 1:
            height = 20;
            break;
        case 2:
            height = 40;
            break;
        case 3:
            height = 60;
            break;
        default:
            height = 80;
            break;
    }
	return 290 + height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *uniqueIdentifier = @"videoCell";
	MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:uniqueIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MainTableViewCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (MainTableViewCell *) currentObject;
				NSInteger likeCount = [[self.videoDetail objectForKey:@"likes"] integerValue];
				if (likeCount) {
					if (likeCount > 7) likeCount = 7;
					[cell initLikedImage:likeCount];
				}
				break;
			}
		}
	}
	
	[cell setDelegateForEGOImageView];
    [cell.textField setDelegate:self];
	cell.countComments.text = [NSString stringWithString:[self.videoDetail objectForKey:@"comments"]];
	cell.countLikes.text = [NSString stringWithString:[self.videoDetail objectForKey:@"likes"]];
	cell.countViews.text = [NSString stringWithFormat:@"%@ views",[self.videoDetail objectForKey:@"views"]];
	cell.imageThumbnailVideo.imageURL = [NSURL URLWithString:[self.videoDetail objectForKey:@"thumbnail"]];
	
    BOOL isLiked = NO;
	NSInteger countLikes = [cell.countLikes.text integerValue];
	if (countLikes) {
		if ([self.videoDetail objectForKey:@"video_likes"]) {
			for (NSString *likeKey in [self.videoDetail objectForKey:@"video_likes"]) {
				NSString *likeID = [[[self.videoDetail objectForKey:@"video_likes"] objectForKey:likeKey] objectForKey:@"user_id"];
				if ([[[SingletonAuthorizationUser authorizationUser] userID] isEqualToString:likeID]) {
					isLiked = YES;
					break;
				} else {
					isLiked = NO;
				}
			}
		}
	}

    NSString *videoType = [self.videoDetail objectForKey:@"video_type"];
	NSString *videoUrl = [self.videoDetail objectForKey:@"video_url"];
    
    NSLog(@"video type %@", videoType);
    
    if ([videoType isEqualToString:@"youtube"]) {
        
        videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
        if ([videoUrl rangeOfString:@"https"].location == NSNotFound) {
            videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"http" withString:@"https"];
        }
        
        NSLog(@"url %@, component %@", videoUrl, [videoUrl lastPathComponent]);
        
        NSString *embedHTML = @"\
        <html><head>\
        <style type=\"text/css\">\
        body {\
        background-color: transparent;\
        color: black;\
        }\
        </style>\
        </head><body style=\"margin:0\">\
        <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
        width=\"%0.0f\" height=\"%0.0f\"></embed>\
        </body></html>";
        
        NSString *html = [NSString stringWithFormat:embedHTML, videoUrl, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height];
        
        webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height)];
        
        [webView loadHTMLString:html baseURL:nil];
        [cell.playbackBackground addSubview:webView];
        [cell.playbackBackground setHidden:NO];
	}
    else {
//        if ([videoType isEqualToString:@"vimeo"]) {
//            NSString *strNewUrl = [NSString stringWithFormat:@"http://player.vimeo.com/video/%@", [videoUrl lastPathComponent]];
//            
//            webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height)];
//            [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strNewUrl]]];
//            
//            [cell.playbackBackground addSubview:webView];
//            [cell.playbackBackground setHidden:NO];
//        }
//        else {
            cell.playButton.tag = indexPath.section;
            [cell.playButton addTarget:self action:@selector(touchPlayVideoButton:) forControlEvents:UIControlEventTouchUpInside];
//        }

    }
    
    [cell.likeButton setSelected:isLiked];
	cell.likeButton.tag = indexPath.section;
	[cell.likeButton addTarget:self action:@selector(toggleLikeStateInSection:) forControlEvents:UIControlEventTouchUpInside];
	
//    UINavigationController *navController = [(CustomTabBarController *)self.slidingViewController.topViewController viewControllers][0];
//    NSLog(@"sliding viewController %@", [navController viewControllers]);

	
//    [cell.streamButton setHidden:YES];
	cell.streamButton.tag = indexPath.section;
	[cell.streamButton addTarget:self action:@selector(touchStreamButton:) forControlEvents:UIControlEventTouchUpInside];
	
	cell.sendButton.tag = indexPath.section;
	[cell.sendButton addTarget:self action:@selector(touchSendButton:) forControlEvents:UIControlEventTouchUpInside];
	
	cell.showComments.tag = indexPath.section;
	[cell.showComments addTarget:self action:@selector(touchShowCommentsButton:) forControlEvents:UIControlEventTouchUpInside];
	
	if ([self.videoDetail objectForKey:@"video_likes"]) {
		NSDictionary *profilePicturesLikeUsers = [self.videoDetail objectForKey:@"video_likes"];
		NSInteger alreadyLiked = [[self.videoDetail objectForKey:@"likes"] integerValue];
		if (alreadyLiked) {
			int i = 0;
			while  (i < alreadyLiked && i < 7) {
				NSString *numberLikeUserPhoto = [NSString stringWithFormat:@"%d", i];
				NSString *pathLikePhoto = [[profilePicturesLikeUsers objectForKey:numberLikeUserPhoto] objectForKey:@"profile_picture"];
				[cell setLikedImageOnURL:pathLikePhoto forCurrentIndex:i];
				i++;
			}
			if (alreadyLiked > 7) {
				UIButton *showMoreButton = [cell addShowMoreButton];
				showMoreButton.tag = indexPath.section;
				[showMoreButton addTarget:self action:@selector(touchShowMoreButton:) forControlEvents:UIControlEventTouchUpInside];
			}
		}
	}
    
    if ([[self.videoDetail objectForKey:@"comments"] intValue] > 0) {
        
        int countComments = [[self.videoDetail objectForKey:@"comments"] intValue];
        
        for (int i = 0; i < countComments && i < 4; i++) {
            NSString *strCommnent = [[[self.videoDetail objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"comments"];
            NSString *strName = [[[self.videoDetail objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"user_name"];
            
            if ([strName length] > 0 && [strCommnent length] > 0) {
                
                NIAttributedLabel *lblAttrText = [[NIAttributedLabel alloc] initWithFrame:CGRectMake(22, i*20, 264, 20)];
                [lblAttrText setDelegate:self];
                [lblAttrText setTag:indexPath.section];
                
                if (i == 1 && [[self.videoDetail objectForKey:@"comments"] intValue] > 4) {
                    [lblAttrText setText:[NSString stringWithFormat:@"view all %d messages", [[self.videoDetail objectForKey:@"comments"] intValue]]];
                    [lblAttrText addLink:[NSURL fileURLWithPath:lblAttrText.text] range:[lblAttrText.text rangeOfString:lblAttrText.text]];
                    //[lblAttrText setFont:[UIFont boldSystemFontOfSize:10] range:[lblAttrText.text rangeOfString:strName]];
                    [lblAttrText setFont:[UIFont systemFontOfSize:10]];
                    [lblAttrText setLinkColor:[UIColor lightGrayColor]];
                }
                else {
                    
                    if (i != 0  && [[self.videoDetail objectForKey:@"comments"] intValue] > 4) {
                        
                        strName = [[[self.videoDetail objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"user_name"];
                        strCommnent = [[[self.videoDetail objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"comments"];
                        //[lblAttrText setTag:i];
                    }
                    
                    [lblAttrText setText:[NSString stringWithFormat:@"%@ %@", strName, strCommnent]];
                    [lblAttrText addLink:[NSURL fileURLWithPath:strName] range:[lblAttrText.text rangeOfString:strName]];
                    [lblAttrText setFont:[UIFont systemFontOfSize:12]];
                    [lblAttrText setFont:[UIFont boldSystemFontOfSize:14] range:[lblAttrText.text rangeOfString:strName]];
                }
                [lblAttrText setNumberOfLines:0];
                [lblAttrText setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblAttrText setTextColor:[UIColor darkGrayColor]];
                [lblAttrText setBackgroundColor:[UIColor clearColor]];
                [lblAttrText setDeferLinkDetection:YES];
                
                
                //[lblAttrText setFrame:CGRectMake(22, i*20, 264, 20)];
                
                [cell.viewComments addSubview:lblAttrText];
            }
        }
        
    }
    
	return cell;
}

- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    
    NSString *strImageData = [result.URL.relativeString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //NSLog(@"relative String %@", strImageData);
    
    if ([strImageData rangeOfString:@"view all"].location != NSNotFound) {
        [self touchShowCommentsButton:attributedLabel];
    }
    else {
        NSDictionary *data = [self.videoDetail objectForKey:@"video_comments"];
        NSLog(@"Date %@", data);
        int countComments = [[self.videoDetail objectForKey:@"comments"] intValue];
        
        for (int i = 0 ; i < countComments && i < 4; i++) {
            
            if (i == 0 || countComments <= 4) {
                if ([[[data objectForKey:[NSString stringWithFormat:@"%d", i]] objectForKey:@"user_name"] isEqualToString:strImageData]) {
                    NSLog(@"found1 %@", strImageData);
                    
                    ProfileViewController *profile = [[ProfileViewController alloc] init];
                    profile.selectedUserID = [[data objectForKey:[NSString stringWithFormat:@"%d", i]] objectForKey:@"user_id"];
                    profile.hasParentNavigationView = YES; //subview for it's view
                    [self.navigationController pushViewController:profile animated:YES];
                    
                    break;
                }
            }
            else {
                if ([[[data objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"user_name"] isEqualToString:strImageData]) {
                    NSLog(@"found3 %@", strImageData);
                    
                    ProfileViewController *profile = [[ProfileViewController alloc] init];
                    profile.selectedUserID = [[data objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"user_id"];
                    profile.hasParentNavigationView = YES; //subview for it's view
                    [self.navigationController pushViewController:profile animated:YES];
                    
                    
                    break;
                }
            }
        }
        
        
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
        CGRect rect = self.view.frame;
        
        CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
        if (screenRect.size.height == 568)
        {
            rect.origin.y -= 50;
        }
        else {
            rect.origin.y -= 160;
        }
        self.view.frame = rect;
    } completion:^(BOOL finished) {
        
    }];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        
        CGRect rect = self.view.frame;
        
        CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
        if (screenRect.size.height == 568)
        {
            rect.origin.y += 50;
        }
        else {
            rect.origin.y += 160;
        }
        self.view.frame = rect;
    } completion:^(BOOL finished) {
        
    }];
    return YES;
}

- (IBAction)crossBtnPressed:(id)sender {
    
    [webView setHidden:YES];
    [webView removeFromSuperview];
    [btnCross setHidden:YES];
}
@end
