//
//  LikedViewController.h
//  Flixogram
//
//  Created by Rizwan on 5/26/14.
//
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"
#import "PullTableView.h"

@interface LikedViewController : BaseViewController <FlickogramVideoDelegate,
                                                        UITableViewDataSource,
                                                        UITableViewDelegate,
                                                        PullTableViewDelegate>
{
	UISegmentedControl *segmentedControl;
	NSMutableArray *videosArray;
	NSInteger currentPage;
	BOOL isShowMore;
}

@end
