//
//  NotificationsAPI.m
//  Flickogram
//


#import "NotificationsFlickogramAPI.h"
#import "FlickogramConstantsURLAPI.h"
#import "AFNetworking.h"

@implementation NotificationsFlickogramAPI
@synthesize delegate = _delegate;

- (void)notificationFollowingSectionForUserID:(NSString *)userID 
									   onPage:(NSString *)page
								 withDelegate:(id<FlickogramNotificationsDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:NotificationFollowingAPI parameters:parameters];
	
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  notificationFollowingSectionSuccessWithRecievedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate notificationFollowingSectionFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate notificationFollowingSectionFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)notificationNewsSectionForUserID:(NSString *)userID 
								  onPage:(NSString *)page 
							withDelegate:(id<FlickogramNotificationsDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:NotificationNewsAPI parameters:parameters];
	
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							NSString *status = [JSON objectForKey:@"status"];
																							if ([status isEqualToString:@"SUCCESS"]) {
																								[delegate  notificationNewsSectionSuccessWithRecievedData:JSON];
																							} else {
																								NSString *error = [JSON objectForKey:@"error_message"];
																								[delegate notificationNewsSectionFailedWithError:error];
																							}
																						} 
																						failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							NSString *errorString = @"Please confirm you have an internet connection and try again";
																							[delegate notificationNewsSectionFailedWithError:errorString];
																						}];
	[operation start];
}

- (void)getAboutUsWithDelegate:(id<FlickogramNotificationsDelegate>)delegate
{
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:AboutUsAPI parameters:nil];
	
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							NSString *status = [JSON objectForKey:@"status"];
																							if ([status isEqualToString:@"SUCCESS"]) {
																								[delegate  getAboutUsSuccessWithRecievedData:JSON];
																							} else {
																								NSString *error = [JSON objectForKey:@"error_message"];
																								[delegate getAboutUsFailedWithError:error];
																							}
																						}
																						failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							NSString *errorString = @"Please confirm you have an internet connection and try again";
																							[delegate getAboutUsFailedWithError:errorString];
																						}];
	[operation start];

}

@end
