//
//  AuthorizationFlickogramAPI.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import "FlickogramProtocolsAPI.h"

@interface AuthorizationFlickogramAPI : NSObject

@property (nonatomic, strong) id <FlickogramAuthorizationDelegate> delegate;

- (void)loginOnServerWithEmail:(NSString *)email
					  password:(NSString *)password 
					  delegate:(id<FlickogramAuthorizationDelegate>) delegate;

- (void)signupOnServerWithEmail:(NSString *)email 
					   password:(NSString *)password 
						  phone:(NSString *)phone
						picture:(UIImage *)picture 
					   username:(NSString *)username 
					   delegate:(id<FlickogramAuthorizationDelegate>)delegate;	

- (void)viewProfileUserID:(NSString *)userID 
				 viewerID:(NSString *)viewerID 
				   onPage:(NSString *)page 
				 delegate:(id<FlickogramAuthorizationDelegate>)delegate;


- (void)followUnFollowUserID:(NSString *)userID 
				  followerID:(NSString *)followerID 
				  indexArray:(NSInteger)currentIndex
				withDelegate:(id<FlickogramAuthorizationDelegate>)delegate;

- (void)updateProfilePictureForUserID:(NSString *)userID
							  picture:(UIImage *)picture
							 delegate:(id<FlickogramAuthorizationDelegate>)delegate;

- (void)deleteProfilePictureForUserID:(NSString *)userID
							 delegate:(id<FlickogramAuthorizationDelegate>)delegate;

- (void)updatePublicProfileForUserID:(NSString *)userID
							username:(NSString *)username
						   firstname:(NSString *)firstname
							lastname:(NSString *)lastname
							 website:(NSString *)website
							delegate:(id<FlickogramAuthorizationDelegate>)delegate;

- (void)updatePrivateProfileForUserID:(NSString *)userID
							birthdate:(NSString *)birthdate
							   gender:(NSString *)gender
								phone:(NSString *)phone
								email:(NSString *)email
							 delegate:(id<FlickogramAuthorizationDelegate>)delegate;

- (void)updateUserVideosPrivateForUserID:(NSString *)userID
						videosArePrivate:(NSString *)state
								delegate:(id<FlickogramAuthorizationDelegate>)delegate;

- (void)blockUser:(NSString *)userID
		blockerID:(NSString *)blockerID
		 delegate:(id<FlickogramAuthorizationDelegate>)delegate;

- (void)reportUser:(NSString *)userID
		reporterID:(NSString *)reporterID
		   reasonReport:(NSString *)reason
		 delegate:(id<FlickogramAuthorizationDelegate>)delegate;


@end
