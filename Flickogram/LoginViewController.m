//
//  AuthorizationViewController.m
//  Flickogram
//

#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "SingletonAuthorizationUser.h"
#import "FlickogramConstants.h"
#import "FlickogramAuthorization.h"

@interface LoginViewController ()
- (void)back;
- (void)closeKeyboard;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)forgotPassBtnPressed:(id)sender;
@end

@implementation LoginViewController
@synthesize email = _email;
@synthesize password = _password;
@synthesize activity = _activity;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Login";
	self.navigationItem.title = @"SIGN IN";
	
//	//set Done button
//	UIImage *doneButtonImage = [UIImage imageNamed:@"DoneNav"];
//	UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
//	[doneButton setBackgroundImage:doneButtonImage forState:UIControlStateNormal];
//	doneButton.frame = CGRectMake(0.0, 0.0, 60, 30);
//	[doneButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
//	UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
//	self.navigationItem.rightBarButtonItem = doneItem;
	
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	switch (section) {
		case 0:
			return 2;
			break;
		case 1:
			return 1;
			break;
		default:
			break;
	}
	return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									  reuseIdentifier:CellIdentifier];
    }
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Username";
			[cell.contentView addSubview:self.email];
		} else {
			cell.textLabel.text = @"Password";
			[cell.contentView addSubview:self.password];
		}
		
	} else {
		cell.textLabel.text = @"Forgot password?";
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
		
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (indexPath.section == 1) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://developer.avenuesocial.com/azeemsal/flickogram/ForgotPassword/"]];
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == self.email) {
		return [self.password becomeFirstResponder];
	}
    else {
        return [textField resignFirstResponder];
    }
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)closeKeyboard
{
	[self.email resignFirstResponder];
	[self.password resignFirstResponder];
}

- (IBAction)loginBtnPressed:(id)sender {
    
    NSString *email = self.email.text;
	NSString *password = self.password.text;
	NSUInteger lengthEmail = [email length];
	NSUInteger lengthPassword = [password length];
	if (lengthEmail == 0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please, enter your username" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
		[alert show];
		return;
	}
	if (lengthPassword == 0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please, enter your password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
		[alert show];
		return;
	}
	
	[self closeKeyboard];
	[self.activity startAnimating];
	[self.activity setHidden:NO];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI authorizationExemplar] loginOnServerWithEmail:email password:password delegate:self];
}

- (IBAction)forgotPassBtnPressed:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://developer.avenuesocial.com/azeemsal/flickogram/ForgotPassword/"]];
}

#pragma mark - Authorization Delegate
- (void)authorizationSuccessWithRecivedData:(NSDictionary *)data
{
	[self.activity stopAnimating];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString * userID = [data objectForKey:@"id"];
	NSString * username = [data objectForKey:@"user_name"];
	NSString * profilePicture = [data objectForKey:@"profile_picture"];
	
	//NSUserDefault set
	NSDictionary *authCredentials = [NSDictionary dictionaryWithObjectsAndKeys:userID, @"id", username, @"user_name", profilePicture, @"profile_picture", nil];
	[FlickogramAuthorization saveAuthCredentialsFromDictionary:authCredentials];

	
	[[SingletonAuthorizationUser authorizationUser] setUserID:userID];
	[[SingletonAuthorizationUser authorizationUser] setUserName:username];
	[[SingletonAuthorizationUser authorizationUser] setProfilePicture:profilePicture];
	
	[[SingletonAuthorizationUser authorizationUser] setEmail:[data objectForKey:@"email"]];
	[[SingletonAuthorizationUser authorizationUser] setPassword:[data objectForKey:@"password"]];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationGetFollowingVideos
														object:self 
													  userInfo:nil];
	[self dismissViewControllerAnimated:YES completion:^{
//        [[[UIAlertView alloc] initWithTitle:@"Tutorial" message:@"Tutorial Message" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }];
}

- (void)authorizationFailedWithError:(NSString *)error
{
	[self.activity stopAnimating];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

@end
