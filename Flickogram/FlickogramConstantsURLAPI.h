//
//  FlickogramConstantsAPI.h
//  Flickogram
//


#import <UIKit/UIKit.h>

@interface FlickogramConstantsURLAPI : NSObject

extern NSString * const BaseURL;
extern NSString * const FoursquareBaseURL;

extern NSString * const LoginMethodAPI;
extern NSString * const SignupMethodAPI;
extern NSString * const CreateVideoAPI;
extern NSString * const GetPopularVideo;
extern NSString * const GetFollowingVideosAPI;
extern NSString * const GetVideoDetailsAPI;
extern NSString * const LikeVideoAPI;
extern NSString * const UserProfileAPI;
extern NSString * const AddCommentAPI;
extern NSString * const RemoveCommentAPI;
extern NSString * const RemoveVideoAPI;
extern NSString * const NotificationFollowingAPI;
extern NSString * const NotificationNewsAPI;
extern NSString * const SearchVideoAPI;
extern NSString * const ExploreVideosAPI;
extern NSString * const FindFriendsAPI;
extern NSString * const SearchUsersByTagsAPI;
extern NSString * const FollowUnFollowUserAPI;
extern NSString * const UserFollowersAPI;
extern NSString * const UserFollowingsAPI;
extern NSString * const SuggestedVideoAPI;
extern NSString * const UserLikedVideosAPI;
extern NSString * const UpdateProfilePictureAPI;
extern NSString * const DeleteProfilePictureAPI;
extern NSString * const UpdatePublicPofileAPI;
extern NSString * const UpdatePrivateProfileAPI;
extern NSString * const ReflickVideoAPI;
extern NSString * const	UserVideosPrivateAPI;
extern NSString * const GetMostWantedLikedVideosAPI;
extern NSString * const GetTrendingVideosAPI;
extern NSString * const AboutUsAPI;
extern NSString * const FoursquareSearch;
extern NSString * const BlockUserAPI;
extern NSString * const ReportUserAPI;
extern NSString * const ReportVideoAPI;

@end
