//
//  YourVideoViewController.m
//  Flickogram
//

#import "YourVideoViewController.h"
#import "SingletonAuthorizationUser.h"
#import "GridVideoCell.h"
#import "TableVideoCell.h"
#import "VideoDetailViewController.h"

@interface YourVideoViewController ()
- (void)parseVideoFromDictionary:(NSDictionary *)videos;
@end

@implementation YourVideoViewController
@synthesize table = _table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Your Videos";
    
	//back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	//Segmented Control
	NSArray *itemArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"tableViewSelected"],
						  [UIImage imageNamed:@"gridView"], nil];
	segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
	segmentedControl.frame = CGRectMake(0, 0, 90, 0);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	segmentedControl.tintColor = [UIColor clearColor];
	[segmentedControl setWidth:45 forSegmentAtIndex:0];
	[segmentedControl setWidth:45 forSegmentAtIndex:1];
	[segmentedControl addTarget:self action:@selector(changeSegment) forControlEvents:UIControlEventValueChanged];
	segmentedControl.selectedSegmentIndex = 0;
	self.navigationItem.titleView = segmentedControl;
	
	
	videosArray = [[NSMutableArray alloc] init];
	currentPage = 1;
	isShowMore = NO;
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *currentPageInString = [NSString stringWithFormat:@"%d", currentPage];

	[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:userID
														   viewerID:userID
															 onPage:currentPageInString
														   delegate:self];

	//pull to refresh
	self.table.pullArrowImage = [UIImage imageNamed:@"whiteArrow"];
	self.table.pullBackgroundColor = [UIColor clearColor];
	self.table.pullTextColor = [UIColor whiteColor];
	self.table.loadMoreEnabled = NO;

}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)changeSegment
{
//	NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
//	UIImage *firstImage;
//	UIImage *secondImage;
//	switch (selectedSegment) {
//		case 0: {
//			firstImage = [UIImage imageNamed:@"tableViewSelected"];
//			secondImage = [UIImage imageNamed:@"gridView"];
//		}
//			break;
//		case 1: {
//			firstImage = [UIImage imageNamed:@"tableView"];
//			secondImage = [UIImage imageNamed:@"gridViewSelected"];
//		}
//			break;
//	}
//	[segmentedControl setImage:firstImage forSegmentAtIndex:0];
//	[segmentedControl setImage:secondImage forSegmentAtIndex:1];
	[self.table reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) (count + 9) / 10;
 		return countCells;
	} else {
		return [videosArray count];
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		return 135;
	} else {
		return 160;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		static NSString *cellId = @"GridVideoCell";
		GridVideoCell *cell = (GridVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GridVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (GridVideoCell *) currentObject;
				}
				break;
			}
		}
		
		NSInteger currentCell =	indexPath.row * 10;
		NSInteger countVideos = [videosArray count];
		
		int currentButton = 0;
		for (int i = currentCell; (i < countVideos && i < currentCell+10); i++) {
			NSDictionary *videoDetail = [videosArray objectAtIndex:i];
			NSURL *thumbnailURL = [NSURL URLWithString:[videoDetail objectForKey:@"thumbnail"]];
			EGOImageButton *currentImageButton = [cell setThumbImageOfURL:thumbnailURL ForButton:currentButton];
			currentImageButton.tag = i;
			[currentImageButton addTarget:self action:@selector(showVideoForEGOImageButton:) forControlEvents:UIControlEventTouchUpInside];
			currentButton++;
		}
		return cell;
	} else {
		static NSString *cellIdentifier = @"TableVideoCell";
		TableVideoCell *cell = (TableVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TableVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (TableVideoCell *) currentObject;
				}
				break;
			}
		}
		NSDictionary *videoDetail = [videosArray objectAtIndex:indexPath.row];
		NSString *thumbnailURL = [videoDetail objectForKey:@"thumbnail"];
		[cell setThumbnail:thumbnailURL];
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 1) {
		VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
		videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexPath.row] mutableCopy];
		[self.navigationController pushViewController:videoDetailVC animated:YES];
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (isShowMore) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) ((count + 9) / 10) - 1;
		if (((segmentedControl.selectedSegmentIndex ==0) && (indexPath.row == countCells)) ||
			((segmentedControl.selectedSegmentIndex == 1) && (indexPath.row == count - 1))) {
			isShowMore = NO;
			currentPage++;
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
			NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
			
			[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:authUser
																   viewerID:authUser
																	 onPage:pageInStringFormat
																   delegate:self];
		}
	}
}

- (void)showVideoForEGOImageButton:(id)sender
{
	EGOImageButton *selectedButton = (EGOImageButton *)sender;
	NSInteger indexVideo = selectedButton.tag;
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexVideo] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}

#pragma mark - Get User Video
- (void)getUserProfileSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSInteger countVideo = [[[data objectForKey:@"user_videos"] objectForKey:@"total_videos"] intValue];
	if (countVideo) {
		NSDictionary *videos = [[data objectForKey:@"user_videos"] objectForKey:@"videos"];
		isShowMore = [[[data objectForKey:@"user_videos"] objectForKey:@"show_more"] boolValue];
		[self parseVideoFromDictionary:videos];
	} else if (self.table.pullTableIsRefreshing) {
		[self refreshTable];
	}
}

- (void)parseVideoFromDictionary:(NSDictionary *)videos
{
	if (self.table.pullTableIsRefreshing) {
		NSString *lastDateAdded = @"0";
		if ([videosArray count] > 0) {
			lastDateAdded = [[videosArray objectAtIndex:0] objectForKey:@"date_added"];
		}
		NSInteger currentVideo = 0;
		BOOL isMoreVideo = YES;
		while (isMoreVideo) {
			NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
			if ([videos objectForKey:numberUserString]) {
				NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
				NSString *newDateAdded = [videoDetails objectForKey:@"date_added"];
				
				//compare last date with new date and stop if equal
				if ([lastDateAdded isEqualToString:newDateAdded]) {
					[self refreshTable];
					return;
				} else {
					[videosArray insertObject:videoDetails atIndex:currentVideo];
					
					if (isShowMore) {
						[videosArray removeLastObject];
					}
				}
				currentVideo++;
			} else {
				isMoreVideo = NO;
			}
		}
		[self refreshTable];
		return;
	}

	NSInteger currentVideo = 0;
	BOOL isMoreVideo = YES;
	while (isMoreVideo) {
		NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
		if ([videos objectForKey:numberUserString]) {
			NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
			[videosArray addObject:videoDetails];
			currentVideo++;
		} else {
			isMoreVideo = NO;
		}
	}
	[self.table reloadData];
}

- (void)getUserProfileFailedWithError:(NSString *)error
{
	[self refreshTable];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
	// Code to actually refresh goes here.
    self.table.pullLastRefreshDate = [NSDate date];
    self.table.pullTableIsRefreshing = NO;
	[self.table reloadData];
}

- (void) loadMoreDataToTable
{
    // Code to actually load more data goes here.
    self.table.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	
	[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:userID
														   viewerID:userID
															 onPage:@"1"
														   delegate:self];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

@end
