//
//  FlickogramFrameSlider.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface FlickogramFrameSlider : UIControl {
	NSInteger countUIImage;
	NSInteger currentNumberUIImage;
	CGFloat currentImagePositionX;
	CGFloat sizeOfSquare;
	CGFloat padding;
	AVURLAsset *sourceAsset;
	
	//if thumb is tracking
	BOOL maxThumbOn;
	BOOL minThumbOn;
	UIImageView *minThumb;
	UIImageView *maxThumb;
}

//Boundary values
@property (nonatomic, assign) CGFloat minimumValue;
@property (nonatomic, assign) CGFloat maximumValue;

//Minimaldistance between 2 thumbs
@property (nonatomic, assign) CGFloat minimumRange;

- (void)setSelectedMinimumValue:(CGFloat)val;
- (void)setSelectedMaximumValue:(CGFloat)val;
- (CGFloat)selectedMinimumValue;
- (CGFloat)selectedMaximumValue;

- (void)createFrameSliderForVideoURL:(NSURL *)moviePath;

@end
