//
//  CustomTabBarController.h
//  Flickogram

//

#import <UIKit/UIKit.h>

@interface CustomTabBarController : UITabBarController

@property (nonatomic, strong) UIButton *button;

- (void)addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage;

@end
