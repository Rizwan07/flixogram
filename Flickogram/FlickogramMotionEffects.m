//
//  FlickogramMotionEffects.m
//  Flickogram
//

#import "FlickogramMotionEffects.h"
#import "FlickogramConstants.h"

@implementation FlickogramMotionEffects
@synthesize delegate;
@synthesize isNeedRotation;

- (id) init
{
	self = [super init];
	if (self != nil) {
		self.isNeedRotation = YES;
	}
	return self;
}

- (void)applySlowMotionVideoPath:(NSURL *)urlMovie 
					startSeconds:(CGFloat)start
				   finishSeconds:(CGFloat)finish
						isRotate:(BOOL)rotate
{
	AVMutableComposition *composition = [AVMutableComposition composition];
	AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:urlMovie options:nil];
	//calculate time
	CMTimeRange editRange = CMTimeRangeMake(CMTimeMake(0, 600), CMTimeMake(sourceAsset.duration.value, sourceAsset.duration.timescale));
	
	//scale time range
	CMTime startTime = CMTimeMakeWithSeconds(start, 600);
	CMTime durationTime = CMTimeMakeWithSeconds((finish - start), 600);
	CMTimeRange timeRange = CMTimeRangeMake(startTime, durationTime);
	
	//new duration time range
	CMTime newDuration = CMTimeMakeWithSeconds(((finish-start) * 3), 600);
	
	//scale video
	NSArray *videoTracks = [sourceAsset tracksWithMediaType:AVMediaTypeVideo];
	BOOL shouldRecordVideoTrack = ([videoTracks count] > 0);
	if (shouldRecordVideoTrack)	{
		AVAssetTrack* videoTrack = [videoTracks objectAtIndex:0];
		AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
		[compositionVideoTrack insertTimeRange:editRange  ofTrack:videoTrack atTime:kCMTimeZero error:nil];
		[compositionVideoTrack scaleTimeRange:timeRange toDuration:newDuration];
		
		//rotation for recorded video without filter
		if (rotate) {
			CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(M_PI_2);
			compositionVideoTrack.preferredTransform = rotationTransform;
		}
	}
	
	//mute audio
	NSArray *audioTracks = [sourceAsset tracksWithMediaType:AVMediaTypeAudio];
    BOOL shouldRecordAudioTrack = ([audioTracks count] > 0);
	
    if (shouldRecordAudioTrack) {
        AVAssetTrack* audioTrack = [audioTracks objectAtIndex:0];  
		AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
		[compositionAudioTrack insertTimeRange:editRange  ofTrack:audioTrack atTime:kCMTimeZero error:nil];
		[compositionAudioTrack removeTimeRange:timeRange];
		[compositionAudioTrack insertEmptyTimeRange:CMTimeRangeMake(startTime, newDuration)];
	}
	
	//export video
	AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPreset640x480];
	NSString *exportVideoPath = [NSHomeDirectory() stringByAppendingPathComponent:NameMotionVideoFile];
	if([[NSFileManager defaultManager] fileExistsAtPath:exportVideoPath]) {
		[[NSFileManager defaultManager] removeItemAtPath:exportVideoPath error:nil];
	}
	
	NSURL *exportURL = [NSURL fileURLWithPath:exportVideoPath];
	exportSession.outputURL = exportURL;
	exportSession.outputFileType = @"com.apple.quicktime-movie";
	[exportSession exportAsynchronouslyWithCompletionHandler:^{
		switch (exportSession.status) {
			case AVAssetExportSessionStatusFailed:{
				NSLog (@"FAIL");
				[self.delegate applyEffectFinish:exportURL];
				break;
			}
			case AVAssetExportSessionStatusCompleted: {
				NSLog (@"SUCCESS");
				[self.delegate applyEffectFinish:exportURL];
				break;
			}
		};
	}];
}

- (void)applyStillMotionVideoPath:(NSURL *)urlMovie 
					startSeconds:(CGFloat)start
				   finishSeconds:(CGFloat)finish
						isRotate:(BOOL)rotate
{
	AVMutableComposition *composition = [AVMutableComposition composition];
	AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:urlMovie options:nil];
	//calculate time
	CMTimeRange editRange = CMTimeRangeMake(CMTimeMake(0, 600), CMTimeMake(sourceAsset.duration.value, sourceAsset.duration.timescale));
	
	//scale video
	NSArray *videoTracks = [sourceAsset tracksWithMediaType:AVMediaTypeVideo];
	BOOL shouldRecordVideoTrack = ([videoTracks count] > 0);
	if (shouldRecordVideoTrack)	{
		AVAssetTrack* videoTrack = [videoTracks objectAtIndex:0];
		AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
		[compositionVideoTrack insertTimeRange:editRange  ofTrack:videoTrack atTime:kCMTimeZero error:nil];

		//remove frames
		for (CGFloat i = start+0.1; i < finish;) {
			CMTime startTime = CMTimeMakeWithSeconds(i, 600);
			CMTime duration = CMTimeMakeWithSeconds(0.002, 600);
			CMTimeRange timeRange = CMTimeRangeMake(startTime, duration);
			CMTime newDurationFrame = CMTimeMakeWithSeconds(0.5, 600);
			[compositionVideoTrack scaleTimeRange:timeRange toDuration:newDurationFrame];
			
			i += 0.5;
            if (i > finish) {
                break;
            }
			CMTimeRange timeRangeForRemove = CMTimeRangeMake(CMTimeMakeWithSeconds((i), 600), 
															 CMTimeMakeWithSeconds(0.5, 600));
			[compositionVideoTrack removeTimeRange:timeRangeForRemove];
            
		}
		
		//rotation for recorded video without filter
		if (rotate) {
			CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(M_PI_2);
			compositionVideoTrack.preferredTransform = rotationTransform;
		}
		
	}

	//mute audio
	NSArray *audioTracks = [sourceAsset tracksWithMediaType:AVMediaTypeAudio];
    BOOL shouldRecordAudioTrack = ([audioTracks count] > 0);

	//time range
	CMTime startTime = CMTimeMakeWithSeconds(start, 600);
	CMTime durationTime = CMTimeMakeWithSeconds((finish - start), 600);
	CMTimeRange timeRange = CMTimeRangeMake(startTime, durationTime);
	
    if (shouldRecordAudioTrack) {
        AVAssetTrack* audioTrack = [audioTracks objectAtIndex:0];  
		AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
		[compositionAudioTrack insertTimeRange:editRange  ofTrack:audioTrack atTime:kCMTimeZero error:nil];
		[compositionAudioTrack removeTimeRange:timeRange];
		[compositionAudioTrack insertEmptyTimeRange:timeRange];
	}

	//export video
	AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPreset640x480];
	NSString *exportVideoPath = [NSHomeDirectory() stringByAppendingPathComponent:NameMotionVideoFile];
	if([[NSFileManager defaultManager] fileExistsAtPath:exportVideoPath]) {
		[[NSFileManager defaultManager] removeItemAtPath:exportVideoPath error:nil];
	}
	
	NSURL *exportURL = [NSURL fileURLWithPath:exportVideoPath];
	exportSession.outputURL = exportURL;
	exportSession.outputFileType = @"com.apple.quicktime-movie";
	[exportSession exportAsynchronouslyWithCompletionHandler:^{
		switch (exportSession.status) {
			case AVAssetExportSessionStatusFailed:{
				NSLog (@"FAIL %@ : %@", [exportSession error].localizedDescription, [exportSession error].localizedFailureReason);
				[self.delegate applyEffectFinish:exportURL];
				break;
			}
			case AVAssetExportSessionStatusCompleted: {
				NSLog (@"SUCCESS");
				[self.delegate applyEffectFinish:exportURL];
				break;
			}
		};
	}];
}
 
- (void)addAudio:(NSURL *)audioURL toVideo:(NSURL *)videoURL withVolume:(CGFloat)volume isRotate:(BOOL)rotate
{
	AVMutableComposition *composition = [AVMutableComposition composition];
	AVURLAsset *audioAsset = [AVURLAsset URLAssetWithURL:audioURL options:nil];
	AVURLAsset *videoAsset = [AVURLAsset URLAssetWithURL:videoURL options:nil];
	
	//calculate time
	CMTimeRange editRange = CMTimeRangeMake(CMTimeMake(0, 600), CMTimeMake(videoAsset.duration.value, videoAsset.duration.timescale));
	
	//music
	NSArray *musicTracks = [audioAsset tracksWithMediaType:AVMediaTypeAudio];
    BOOL shouldRecordAudioTrack = ([musicTracks count] > 0);
    if (shouldRecordAudioTrack) {
        AVAssetTrack* audioTrack = [musicTracks objectAtIndex:0];
		AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
		[compositionAudioTrack insertTimeRange:editRange  ofTrack:audioTrack atTime:kCMTimeZero error:nil];
	}
	
	//audio from video
	NSArray *audioTracks = [videoAsset tracksWithMediaType:AVMediaTypeAudio];
	BOOL isAudioTrack = ([audioTracks count] > 0);
    if (isAudioTrack) {
		AVAssetTrack *audioFromVideoTrack = [audioTracks objectAtIndex:0];
		AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
		[compositionAudioTrack insertTimeRange:editRange  ofTrack:audioFromVideoTrack atTime:kCMTimeZero error:nil];
	}
	
	//Set Music Volume
	NSMutableArray *trackMixArray = [NSMutableArray array];
//	for (NSInteger i = 0; i < [musicTracks count]; i++) {
//		AVMutableAudioMixInputParameters *trackMix = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:[musicTracks objectAtIndex:i]];
//		AVMutableAudioMixInputParameters *trackAudioMix = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:[audioTracks objectAtIndex:i]];
//		[trackMix setVolume:volume atTime:kCMTimeZero];
//		[trackAudioMix setVolume:1.0 atTime:kCMTimeZero];
//		[trackMixArray addObject:trackMix];
//		[trackMixArray addObject:trackAudioMix];
//	}
    
    if ([musicTracks count]) {
        AVMutableAudioMixInputParameters *trackMix = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:[musicTracks objectAtIndex:0]];
		[trackMix setVolume:volume atTime:kCMTimeZero];
		[trackMixArray addObject:trackMix];
    }
    if ([audioTracks count]) {
		AVMutableAudioMixInputParameters *trackAudioMix = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:[audioTracks objectAtIndex:0]];
		[trackAudioMix setVolume:1.0 atTime:kCMTimeZero];
		[trackMixArray addObject:trackAudioMix];
    }
    
	AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
	audioMix.inputParameters = trackMixArray;
	
	//video
	NSArray *videoTracks = [videoAsset tracksWithMediaType:AVMediaTypeVideo];
	BOOL shouldRecordVideoTrack = ([videoTracks count] > 0);
	if (shouldRecordVideoTrack)	{
		AVAssetTrack* videoTrack = [videoTracks objectAtIndex:0];
		AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
		[compositionVideoTrack insertTimeRange:editRange  ofTrack:videoTrack atTime:kCMTimeZero error:nil];
		
		//rotation for recorded video without filter
		if (rotate) {
			CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(M_PI_2);
			compositionVideoTrack.preferredTransform = rotationTransform;
		}

	}
	
	//export video
	AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPreset640x480];
	NSString *exportVideoPath = [NSHomeDirectory() stringByAppendingPathComponent:NameVideoWithMusic];
	if([[NSFileManager defaultManager] fileExistsAtPath:exportVideoPath]) {
		[[NSFileManager defaultManager] removeItemAtPath:exportVideoPath error:nil];
	}
	
	NSURL *exportURL = [NSURL fileURLWithPath:exportVideoPath];
	exportSession.outputURL = exportURL;
	exportSession.outputFileType = @"com.apple.quicktime-movie";
	exportSession.shouldOptimizeForNetworkUse = YES;
	exportSession.audioMix = audioMix;
	[exportSession exportAsynchronouslyWithCompletionHandler:^{
		switch (exportSession.status) {
			case AVAssetExportSessionStatusFailed:{
				NSLog (@"FAIL");
				[self.delegate addAudioOnVideoSuccess:exportURL];
				break;
			}
			case AVAssetExportSessionStatusCompleted: {
				NSLog (@"SUCCESS");
				[self.delegate addAudioOnVideoSuccess:exportURL];
				break;
			}
		};
	}];
}

@end
