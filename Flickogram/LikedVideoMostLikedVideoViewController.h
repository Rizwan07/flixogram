//
//  LikedVideoViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"
#import "PullTableView.h"

typedef enum {
	MostLikedVideosAPI,
	UserLikedVideosAPI,
} FlickogramAPI;

@interface LikedVideoMostLikedVideoViewController : BaseViewController <FlickogramVideoDelegate,
																			UITableViewDataSource,
																			UITableViewDelegate,
																			PullTableViewDelegate>
{
	UISegmentedControl *segmentedControl;
	NSMutableArray *videosArray;
	NSInteger currentPage;
	BOOL isShowMore;
}

@property (nonatomic) FlickogramAPI selectedVideoAPI;
@property (nonatomic, strong) IBOutlet PullTableView *table;

@end
