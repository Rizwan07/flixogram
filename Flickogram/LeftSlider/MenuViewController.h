#import "BaseViewController.h"
#import "ECSlidingViewController.h"
#import "EGOImageView.h"

@interface MenuViewController : BaseViewController

@property(nonatomic, strong)NSString *identifier;
@property (strong, nonatomic) IBOutlet EGOImageView *imageViewUser;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;

- (IBAction)setttingBtnPressed:(id)sender;
- (IBAction)mostViewedBtnPressed:(id)sender;
- (IBAction)popularBtnPressed:(id)sender;
- (IBAction)likedBtnPressed:(id)sender;
- (IBAction)trendingBtnPressed:(id)sender;
- (IBAction)aboutBtnPressed:(id)sender;
- (IBAction)logoutBtnPressed:(id)sender;
@end
