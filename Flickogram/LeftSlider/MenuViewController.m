

#import "MenuViewController.h"
#import "SingletonAuthorizationUser.h"
#import "FlickogramConstants.h"

@interface MenuViewController()
@end

@implementation MenuViewController

@synthesize identifier = _identifier;

- (void)viewDidLoad
{
  [super viewDidLoad];
    
    self.screenName = @"Menu";
    
    [self.navigationItem setTitle:@"Flixogram"];
    

    self.slidingViewController.tableMenuViewController = self;
  [self.slidingViewController setAnchorRightRevealAmount:220.0f];
  self.slidingViewController.underLeftWidthLayout = ECFullWidth;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *usernameAuth = [[SingletonAuthorizationUser authorizationUser] userName];
    NSString *thumbAuth = [[SingletonAuthorizationUser authorizationUser] profilePicture];
    [_lblUsername setText:usernameAuth];
    [SingletonAuthorizationUser authorizationUser].imageView = &_imageViewUser;
    [_imageViewUser setImageURL:[NSURL URLWithString:thumbAuth]];

}

- (IBAction)setttingBtnPressed:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationMenu object:@"Setting"];
}

- (IBAction)mostViewedBtnPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationMenu object:@"Most Viewed"];
}

- (IBAction)popularBtnPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationMenu object:@"Popular"];
}

- (IBAction)likedBtnPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationMenu object:@"Like"];
}

- (IBAction)trendingBtnPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationMenu object:@"Trend"];
}

- (IBAction)aboutBtnPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationMenu object:@"About"];
}

- (IBAction)logoutBtnPressed:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationMenu object:@"Log out"];
}

@end