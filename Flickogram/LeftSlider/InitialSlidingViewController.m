//
//  InitialSlidingViewController.m
//  ECSlidingViewController
//
//  Created by Michael Enriquez on 1/25/12.
//  Copyright (c) 2012 EdgeCase. All rights reserved.
//

#import "InitialSlidingViewController.h"
#import "CustomTabBarController.h"

#import "AuthorizationViewController.h"
#import "MainViewController.h"
#import "ExploreViewController.h"

//#import "FollowingViewController.h"
#import "LikedViewController.h"


#import "ProfileViewController.h"
#import "CustomNavController.h"

#import "AppDelegate.h"

@implementation InitialSlidingViewController

- (void)viewDidLoad {
  [super viewDidLoad];
    
    CustomTabBarController *tabBarController = [[CustomTabBarController alloc] init];
	tabBarController.tabBar.tintColor = [UIColor redColor];
	
	//Main
	MainViewController *mainController = [[MainViewController alloc] init];
	//UINavigationController *mainNavController = [[UINavigationController alloc] initWithRootViewController:mainController];
    CustomNavController *mainNavController = [[CustomNavController alloc] initWithRootViewController:mainController];
	
	//Explore
	ExploreViewController *exploreController = [[ExploreViewController alloc] init];
	CustomNavController *exploreNavController = [[CustomNavController alloc] initWithRootViewController:exploreController];
	
	//Record
	UIViewController *recordTab = [[UIViewController alloc] init];
	[tabBarController addCenterButtonWithImage:[UIImage imageNamed:@"tab3"] highlightImage:nil];
	
	//Following / News
	LikedViewController *followingController = [[LikedViewController alloc] init];
	CustomNavController *followingNavController = [[CustomNavController alloc] initWithRootViewController:followingController];
	
	//Profile
	ProfileViewController *profileController = [[ProfileViewController alloc] init];
	CustomNavController *profileNavController = [[CustomNavController alloc] initWithRootViewController:profileController];
    
	tabBarController.viewControllers = [NSArray arrayWithObjects:mainNavController, exploreNavController, recordTab, followingNavController, profileNavController, nil];
	//tabBarController.tabBar.backgroundImage = [UIImage imageNamed:@"Top-Bar-Red"];
	
	CGRect rect = tabBarController.tabBar.frame;
	CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y + 6, rect.size.width, rect.size.height); //fix UITabBarItem bug
	tabBarController.tabBar.frame = newRect;
    
    self.topViewController = tabBarController;//[storyboard instantiateViewControllerWithIdentifier:@"Gallery"];
    //NSLog(@"Top ViewController %@", self.topViewController);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
