//
//  RootViewController.m
//  Flixogram
//
//  Created by Rizwan on 1/23/14.
//
//

#import "RootViewController.h"

#import "CustomTabBarController.h"
#import "CustomNavController.h"
#import "MainViewController.h"
#import "ExploreViewController.h"
#import "FollowingViewController.h"
#import "ProfileViewController.h"

#import "RecordViewController.h"
#import "InitialSlidingViewController.h"

@interface RootViewController () <UITabBarControllerDelegate> {
    CustomTabBarController *tabBarController;
}

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isMenuVC"];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setTabBarView];
}

- (void)setTabBarView {
    
//    tabBarController = [[CustomTabBarController alloc] init];
//	tabBarController.tabBar.tintColor = [UIColor redColor];
//	
//	//Main
//	MainViewController *mainController = [[MainViewController alloc] init];
//    CustomNavController *mainNavController = [[CustomNavController alloc] initWithRootViewController:mainController];
//	
//	//Explore
//	ExploreViewController *exploreController = [[ExploreViewController alloc] init];
//	CustomNavController *exploreNavController = [[CustomNavController alloc] initWithRootViewController:exploreController];
//	
//	//Record
//	UIViewController *recordTab = [[UIViewController alloc] init];
//	[tabBarController addCenterButtonWithImage:[UIImage imageNamed:@"tab3"] highlightImage:nil];
//	
//	//Following / News
//	FollowingViewController *followingController = [[FollowingViewController alloc] init];
//	CustomNavController *followingNavController = [[CustomNavController alloc] initWithRootViewController:followingController];
//	
//	//Profile
//	ProfileViewController *profileController = [[ProfileViewController alloc] init];
//	CustomNavController *profileNavController = [[CustomNavController alloc] initWithRootViewController:profileController];
//    
//    [tabBarController setViewControllers:@[mainNavController, exploreNavController, recordTab, followingNavController, profileNavController] animated:YES];
//    [tabBarController setDelegate:self];
//	
//	CGRect rect = tabBarController.tabBar.frame;
//	CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y + 6, rect.size.width, rect.size.height); //fix UITabBarItem bug
//	tabBarController.tabBar.frame = newRect;
//    
//	[self presentViewController:tabBarController animated:NO completion:nil];
    
    InitialSlidingViewController *slidingViewController = [[InitialSlidingViewController alloc] init];
    [self presentViewController:slidingViewController animated:NO completion:nil];
}

- (void)tabBarController:(UITabBarController *)tabBarController1 didSelectViewController:(UIViewController*)viewController {
    
    if (![viewController isKindOfClass:[RecordViewController class]]) {
        
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setDuration:0.25];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:
                                      kCAMediaTimingFunctionEaseIn]];
        [tabBarController.view.window.layer addAnimation:animation forKey:@"fadeTransition"];
    }
}

- (void)selectTabBarIndex:(NSInteger)indexTabBar
{
	[tabBarController setSelectedIndex:indexTabBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
