//
//  FollowingViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"

@interface FollowingViewController : BaseViewController <FlickogramNotificationsDelegate,
													   FlickogramAuthorizationDelegate,
													   UITableViewDataSource,
													   UITableViewDelegate,
													   FlickogramVideoDelegate> {
	UISegmentedControl *segmentedControl;
	NSMutableArray *newsDataArray;
	NSMutableArray *followingsDataArray;
	NSInteger currentPageNewsNotification;
	NSInteger currentPageFollowingsNotification;
	BOOL isShowMoreNews;
	BOOL isShowMoreFollowings;
}

@property (nonatomic, weak) IBOutlet UITableView *table;

@end
