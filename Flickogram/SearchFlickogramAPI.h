//
//  SearchFlickogramAPI.h
//  Flickogram
//


#import <Foundation/Foundation.h>
#import "FlickogramProtocolsAPI.h"

@interface SearchFlickogramAPI : NSObject

- (void)getVideosByTags:(NSString *)tag
				 onPage:(NSString *)page 
		   withDelegate:(id<FlickogramSearchDelegate>)delegate;

- (void)findFriendsForUser:(NSString *)userID 
		 typeSocialNetwork:(NSString *)type 
				 friendIDs:(NSArray *)friendIDs 
			  withDelegate:(id<FlickogramSearchDelegate>)delegate;

- (void)searchUsersByTags:(NSString *)tag
				   userID:(NSString *)userID
				   onPage:(NSString *)page 
			 withDelegate:(id<FlickogramSearchDelegate>)delegate;

- (void)getListFollowersForUserID:(NSString *)userID
						 viewerID:(NSString *)viewerID
						   onPage:(NSString *)page
					 withDelegate:(id<FlickogramSearchDelegate>)delegate;

- (void)getListFollowingsForUserID:(NSString *)userID
						   vieweID:(NSString *)viewerID
							onPage:(NSString *)page
					  withDelegate:(id<FlickogramSearchDelegate>)delegate;
@end
