//
//  VideoEffects.h
//  Flickogram
//

#import <Foundation/Foundation.h>
#import "GPUImage.h"
#import "FlickogramConstants.h"

@protocol VideoEffectFrameDelegate <NSObject>

- (void)frameCompleted:(NSDictionary *)dict;

@end

typedef enum {
	FlickogramFilterTypeNormal,
	FlickogramFilterTypeAmaro,
	FlickogramFilterTypeRise,
	FlickogramFilterTypeHudson,
	FlickogramFilterTypeXPro2,
	FlickogramFilterTypeSierra,
	FlickogramFilterTypeLoFi,
	FlickogramFilterTypeEarlybird,
	FlickogramFilterTypeSutro,
	FlickogramFilterTypeToaster,
	FlickogramFilterTypeBrannan,
	FlickogramFilterTypeInkwell,
	FlickogramFilterTypeWalden,
	FlickogramFilterTypeHefe,
	FlickogramFilterTypeValencia,
	FlickogramFilterTypeNashville,
	FlickogramFilterType1977,
	FlickogramFilterTypeKelvin,
} FlickogramFilterType;

@interface VideoEffects : NSObject <GPUImageMovieWriterDelegate, GPUImageMovieDelegate> {
	FlickogramFilterType filterType;
	NSURL *moviePath;
	id filter;
	GPUImageMovieWriter *movieWriter;
	NSURL *filteredVideoURL;
	NSInteger countFrame;
}

@property (nonatomic, assign) id<VideoEffectFrameDelegate> delegate;

- (void)applyFilterByNumber:(NSInteger)selectedFilter;

@end
