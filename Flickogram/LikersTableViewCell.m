//
//  LikersTableViewCell.m
//  Flickogram
//

#import "LikersTableViewCell.h"
#import "EGOImageView.h"

@implementation LikersTableViewCell
@synthesize photo = _photo;
@synthesize username = _username;
@synthesize profileName = _profileName;
@synthesize followToggle = _followToggle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setPlaceholder 
{
	_photo.placeholderImage = [UIImage imageNamed:@"Profile-thumb"];
}

- (void)setPhotoUser:(NSString *)photoUser 
{
	_photo.imageURL = [NSURL URLWithString:photoUser];
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
	[super willMoveToSuperview:newSuperview];
	
	if(!newSuperview) {
		[_photo cancelImageLoad];
	}
}

@end
