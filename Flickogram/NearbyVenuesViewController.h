//
//  NearbyVenuesViewController.h
//  Flickogram
//


#import "BaseViewController.h"

#import "ManagerFlickogramAPI.h"

@protocol NearbyVenuesDelegate <NSObject>
- (void)venueSelected:(NSMutableDictionary *)venue;
@end

@interface NearbyVenuesViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, FoursquareDelegate> {
	NSString *searchStatus;
}

@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) NSMutableArray *venues;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *nameForSearch;
@property (nonatomic, strong) id<NearbyVenuesDelegate> delegate;

@end
