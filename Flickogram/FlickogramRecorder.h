//
//  FlickogramRecorder.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol FlickogramRecorderDelegate;

@interface FlickogramRecorder : NSObject

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic,copy) NSURL *outputFileURL;
@property (nonatomic,readonly) BOOL recordsVideo;
@property (nonatomic,readonly) BOOL recordsAudio;
@property (nonatomic,readonly,getter=isRecording) BOOL recording;
@property (nonatomic,assign) id <NSObject, FlickogramRecorderDelegate> delegate;

-(id)initWithSession:(AVCaptureSession *)session outputFileURL:(NSURL *)outputFileURL;
-(void)startRecordingWithOrientation:(AVCaptureVideoOrientation)videoOrientation;
-(void)stopRecording;

@end

@protocol FlickogramRecorderDelegate
@required
-(void)recorderRecordingDidBegin:(FlickogramRecorder *)recorder;
-(void)recorder:(FlickogramRecorder *)recorder recordingDidFinishToOutputFileURL:(NSURL *)outputFileURL error:(NSError *)error;
@end
