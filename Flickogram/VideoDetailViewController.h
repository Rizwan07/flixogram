//
//  VideoDetailViewController.h
//  Flickogram
//


#import "BaseViewController.h"

#import <MediaPlayer/MediaPlayer.h>
#import "LBYouTubePlayerViewController.h"
#import "CommentsTableViewController.h"
#import "GTMOAuth2Authentication.h"
#import "GData.h"

@interface VideoDetailViewController : BaseViewController <UITableViewDataSource,
															UITableViewDelegate,
															LBYouTubePlayerControllerDelegate,
															UIActionSheetDelegate,
															UIAlertViewDelegate,
                                                            UITextFieldDelegate,
															CommentsTableViewDelegate,
															FlickogramVideoDelegate>
{
	NSInteger currentCellSectionWithPlayingVideo;
    GTMOAuth2Authentication *authToken;
    GDataServiceTicket *mUploadTicket;
    UIProgressView *progressView;
}

@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) NSMutableDictionary *videoDetail;
@property (nonatomic, strong) MPMoviePlayerController *player;

@end
