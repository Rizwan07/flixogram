//
//  FoursquareViewController.h
//  Flickogram
//


#import "BaseViewController.h"

#import "CoreLocation/CoreLocation.h"
#import "ManagerFlickogramAPI.h"
#import "NearbyVenuesViewController.h"

@protocol FoursquareViewControllerDelegate <NSObject>
- (void)venueSelected:(NSDictionary *)venueSelected;
@end

@interface FoursquareViewController : BaseViewController <UITableViewDataSource,
														UITableViewDelegate, 
														CLLocationManagerDelegate,
														FoursquareDelegate,
														UISearchBarDelegate,
														NearbyVenuesDelegate>
{
	NearbyVenuesViewController *nearby;
	NSString *latitude;
	NSString *longitude;
}

@property (nonatomic, strong) IBOutlet UITableView *table;
@property (nonatomic, strong) IBOutlet UITableView *additionalTable;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSMutableArray *venues;
@property (nonatomic, strong) id<FoursquareViewControllerDelegate>delegate;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UIControl *controlBackground;

- (IBAction)returnNormState:(id)sender;

@end
