//
//  NotificationsAPI.h
//  Flickogram
//

#import <Foundation/Foundation.h>
#import "FlickogramProtocolsAPI.h"

@interface NotificationsFlickogramAPI : NSObject

@property (nonatomic, strong) id <FlickogramNotificationsDelegate> delegate;

- (void)notificationFollowingSectionForUserID:(NSString *)userID 
									   onPage:(NSString *)page
								 withDelegate:(id<FlickogramNotificationsDelegate>)delegate;

- (void)notificationNewsSectionForUserID:(NSString *)userID 
								  onPage:(NSString *)page 
							withDelegate:(id<FlickogramNotificationsDelegate>)delegate;

- (void)getAboutUsWithDelegate:(id<FlickogramNotificationsDelegate>)delegate;
@end
