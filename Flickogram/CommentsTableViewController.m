//
//  CommentsTableViewController.m
//  Flickogram
//


#import "CommentsTableViewController.h"
#import "CommentsTableViewCell.h"
#import "SingletonAuthorizationUser.h"

@interface CommentsTableViewController ()
- (void)parseDictionaryComments;
- (void)commentsFromArrayToDictionary;
@end

@implementation CommentsTableViewController
@synthesize userComments = _cuserComments;
@synthesize delegate = _delegate;
@synthesize currentIndexArray = _currentIndexArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	self.title = @"COMMENTS";
	
	comments = [[NSMutableArray alloc] init];
	if (self.userComments) {
		[self parseDictionaryComments];
	}
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)parseDictionaryComments
{
	NSInteger currentComment = 0;
	BOOL isMore = YES;
	while (isMore) {
		NSString *numberComment = [NSString stringWithFormat:@"%d", currentComment];
		if ([self.userComments objectForKey:numberComment]) {
			[comments addObject:[self.userComments objectForKey:numberComment]];
			currentComment++;
		} else {
			isMore = NO;
		}
	}
	[self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [comments count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *comment = [[comments objectAtIndex:indexPath.row] objectForKey:@"comments"];
	UILabel *resizableLabel = [[UILabel alloc] initWithFrame:CGRectMake(51, 20, 262, 21)];
	resizableLabel.numberOfLines = 0;
	resizableLabel.lineBreakMode = NSLineBreakByWordWrapping;
	resizableLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:14.0];
	resizableLabel.text = comment;
	[resizableLabel sizeToFit];
	CGFloat newSizeCell = resizableLabel.frame.size.height - 21;
	return 44 + newSizeCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"CommentCell";
	CommentsTableViewCell *cell = (CommentsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CommentsTableViewCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (CommentsTableViewCell *) currentObject;
				[cell setPlaceholder];
			}
			break;
		}
	}
	NSDictionary *currentCommentDetail = [comments objectAtIndex:indexPath.row];
	[cell setPhotoUser:[currentCommentDetail objectForKey:@"profile_picture"]];
	cell.username.text = [currentCommentDetail objectForKey:@"user_name"];
	cell.comment.text = [currentCommentDetail objectForKey:@"comments"];
	[cell.comment sizeToFit];
	
	NSString *dateAdded = [currentCommentDetail objectForKey:@"date_added"];
	// Your dates:
	NSDate * today = [NSDate date];
	NSDate * refDate = [NSDate dateWithTimeIntervalSince1970:[dateAdded longLongValue]];
	
	// 10 first characters of description is the calendar date:
	NSString * todayString = [[today description] substringToIndex:10];
	NSString * refDateString = [[refDate description] substringToIndex:10];
	
	if ([refDateString isEqualToString:todayString]) {
		NSDateFormatter *format = [[NSDateFormatter alloc] init];
		[format setDateFormat:@"HH:mm:ss"];
		cell.date.text = [format stringFromDate:refDate];
	} else	{
		cell.date.text = refDateString;
	} 
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	NSString *userAuthID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *currentUserID = [[comments objectAtIndex:indexPath.row] objectForKey:@"user_id"];
	if ([userAuthID isEqualToString:currentUserID]) {
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Confirm Deletion" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles:nil];
		actionSheet.tag = indexPath.row;
		[actionSheet showFromTabBar:self.tabBarController.tabBar];
	}
	
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0) {
		NSInteger currentIndex = actionSheet.tag;
		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
		NSString *videoID = [[comments objectAtIndex:currentIndex] objectForKey:@"video_id"];
		NSString *commentID = [[comments objectAtIndex:currentIndex] objectForKey:@"id"];
		[[ManagerFlickogramAPI videoExemplar] removeComment:commentID userID:userID videoID:videoID indexArray:(NSInteger)currentIndex withDelegate:self];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	}
}

- (void)removeCommentSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex
{
	[comments removeObjectAtIndex:currentIndex];
	[self.tableView reloadData];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self commentsFromArrayToDictionary];
}

- (void)removeCommentFailedWithError:(NSString *)error
{
	NSLog(@"%@",error);
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)commentsFromArrayToDictionary
{
	NSInteger count = [comments count];
	if (count) {
		NSMutableDictionary *newDictionaryFromArray = [[NSMutableDictionary alloc] init];
		for (int i = 0; i < count; i++) {
			NSString *numberInStringFormat = [NSString stringWithFormat:@"%d", i];
			NSDictionary *commentDetail = [comments objectAtIndex:i];
			[newDictionaryFromArray setObject:commentDetail forKey:numberInStringFormat];
		}
		[self.delegate updateCommentsDictionary:newDictionaryFromArray forIndexArray:self.currentIndexArray];
	} else {
		[self.delegate removeVideoCommentKeyForIndexArray:self.currentIndexArray];
	}
}

@end
