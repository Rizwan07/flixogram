//
//  CommentsTableViewController.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "ManagerFlickogramAPI.h"

@protocol CommentsTableViewDelegate <NSObject>

- (void)updateCommentsDictionary:(NSDictionary *)videoComments forIndexArray:(NSInteger)currentIndex; 
- (void)removeVideoCommentKeyForIndexArray:(NSInteger)currentIndex;

@end

@interface CommentsTableViewController : UITableViewController <UIActionSheetDelegate, FlickogramVideoDelegate> {
	NSMutableArray *comments;
}

@property (nonatomic, strong) NSDictionary *userComments;
@property (nonatomic, assign) NSInteger currentIndexArray;
@property (nonatomic, assign) id<CommentsTableViewDelegate>delegate;

@end
