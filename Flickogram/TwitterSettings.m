//
//  TwitterSettings.m
//  Flickogram
//


#import "TwitterSettings.h"

@implementation TwitterSettings

+ (BOOL)hasAccounts
{
    // For clarity
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}

+ (void)openTwitterAccounts
{
    //TWTweetComposeViewController *ctrl = [[TWTweetComposeViewController alloc] init];
    SLComposeViewController *ctrl = [[SLComposeViewController alloc] init];
    if ([ctrl respondsToSelector:@selector(alertView:clickedButtonAtIndex:)]) {
        // Manually invoke the alert view button handler
        [(id <UIAlertViewDelegate>)ctrl alertView:nil
                             clickedButtonAtIndex:kTwitterSettingsButtonIndex];
    }
	ctrl = nil;
}


@end
