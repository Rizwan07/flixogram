//
//  SearchFlickogramAPI.m
//  Flickogram
//


#import "SearchFlickogramAPI.h"
#import "FlickogramConstantsURLAPI.h"
#import "AFNetworking.h"

@implementation SearchFlickogramAPI

- (void)getVideosByTags:(NSString *)tag onPage:(NSString *)page withDelegate:(id<FlickogramSearchDelegate>)delegate 
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:tag forKey:@"tag"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:SearchVideoAPI parameters:parameters];
	
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							NSString *status = [JSON objectForKey:@"status"];
																							if ([status isEqualToString:@"SUCCESS"]) {
																								[delegate getVideoByTagsSuccessWithRecivedData:JSON];
																							} else {
																								NSString *error = [JSON objectForKey:@"error_message"];
																								[delegate getVideoByTagsFailedWithError:error];
																							}
																						} 
																						failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																						NSString *errorString = @"Please confirm you have an internet connection and try again";
																						[delegate getVideoByTagsFailedWithError:errorString];
																						}];
	[operation start];
}

- (void)findFriendsForUser:(NSString *)userID 
		 typeSocialNetwork:(NSString *)type 
				 friendIDs:(NSArray *)friendIDs 
			  withDelegate:(id<FlickogramSearchDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:type forKey:@"type"];
	NSString * result = [[friendIDs valueForKey:@"description"] componentsJoinedByString:@","];
	[parameters setObject:result forKey:@"friendIds"];
	//[parameters setObject:[NSString stringWithString:@"1464370605,100004035250714,100002425665846"] forKey:@"friendIds"];
	//[parameters setObject:@"100002248440794" forKey:@"friendIds"];
	//[parameters setObject:@"100002248440794" forKey:@"friendIds"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:FindFriendsAPI parameters:parameters];
	
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							NSString *status = [JSON objectForKey:@"status"];
																							if ([status isEqualToString:@"SUCCESS"]) {
																								[delegate findFriendsSuccessWithRecievedData:JSON];
																							} else {
																								NSString *error = [JSON objectForKey:@"error_message"];
																								[delegate findFriendsFailedWithError:error];
																							}
																						} 
																						failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							NSString *errorString = @"Please confirm you have an internet connection and try again";
																							[delegate findFriendsFailedWithError:errorString];
																						}];
	[operation start];

}

- (void)searchUsersByTags:(NSString *)tag 
				   userID:(NSString *)userID
				   onPage:(NSString *)page 
			 withDelegate:(id<FlickogramSearchDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:tag forKey:@"tag"];
	[parameters setObject:page forKey:@"page"];
	[parameters setObject:userID forKey:@"user_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:SearchUsersByTagsAPI parameters:parameters];
	
	AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							NSString *status = [JSON objectForKey:@"status"];
																							if ([status isEqualToString:@"SUCCESS"]) {
																								[delegate searchUsersByTagsSuccessWithRecivedData:JSON];
																							} else {
																								NSString *error = [JSON objectForKey:@"error_message"];
																								[delegate searchUsersByTagsFailedWithError:error];
																							}
																						} 
																						failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							NSString *errorString = @"Please confirm you have an internet connection and try again";
																							[delegate searchUsersByTagsFailedWithError:errorString];
																						}];
	[operation start];

}

- (void)getListFollowersForUserID:(NSString *)userID
						 viewerID:(NSString *)viewerID
						   onPage:(NSString *)page
					 withDelegate:(id<FlickogramSearchDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:page forKey:@"page"];
	[parameters setObject:viewerID forKey:@"viewer_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:UserFollowersAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate getListFollowersSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getListFollowersFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getListFollowersFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)getListFollowingsForUserID:(NSString *)userID
						   vieweID:(NSString *)viewerID
							onPage:(NSString *)page
					  withDelegate:(id<FlickogramSearchDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:page forKey:@"page"];
	[parameters setObject:viewerID forKey:@"viewer_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:UserFollowingsAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate getListFollowingsSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getListFollowingsFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getListFollowingsFailedWithError:errorString];
																						 }];
	[operation start];

}
@end
