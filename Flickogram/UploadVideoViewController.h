//
//  UploadVideoViewController.h
//  Flickogram
//


#import "BaseViewController.h"

#import "FoursquareViewController.h"
#import "GData.h"
#import "GTMOAuth2ViewControllerTouch.h"



typedef enum {
	FacebookCell,
	TwitterCell,
	YoutubeCell,
} SharingSocNetworkCell;

@interface UploadVideoViewController : BaseViewController <UITableViewDataSource,
														UITableViewDelegate, 
														UIAlertViewDelegate, 
														UITextFieldDelegate, 
														UIActionSheetDelegate,
													 	FoursquareViewControllerDelegate> 
{
	SharingSocNetworkCell socialNetwork;
	BOOL isSharingFacebook;
	BOOL isSharingTwitter;
	BOOL isSharingYoutube;

	GTMOAuth2Authentication *authTokenYoutube;
	GTMOAuth2Authentication *authTokenVimeo;
	
	NSArray *twitterAccounts;
	UILabel *titleSelectedLocation;
	UILabel *labelLocation;
	
	NSDictionary *geotagInfo;
}

@property (nonatomic, strong) UITextField *caption;
@property (nonatomic, strong) NSURL *videoForUpload;
@property (nonatomic, assign) CGFloat duration;
@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, weak) IBOutlet UIButton *toggleEnableGeotag;
@end
