//
//  FlickogramUtilities.m
//  Flickogram
//


#import "FlickogramUtilities.h"
#import <AVFoundation/AVFoundation.h>

@implementation FlickogramUtilities

+ (AVCaptureConnection *)connectionWithMediaType:(NSString *)mediaType fromConnections:(NSArray *)connections
{
	for ( AVCaptureConnection *connection in connections ) {
		for ( AVCaptureInputPort *port in [connection inputPorts] ) {
			if ( [[port mediaType] isEqual:mediaType] ) {
				return connection;
			}
		}
	}
	return nil;
}
@end
