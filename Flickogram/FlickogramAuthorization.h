//
//  FlickogramAuthorization.h
//  Flickogram
//


#import <Foundation/Foundation.h>

@interface FlickogramAuthorization : NSObject

+ (BOOL)checkSaveAuthCredentials;
+ (void)saveAuthCredentialsFromDictionary:(NSDictionary *)authCredentials;
+ (void)removeAuthCredentials;
+ (NSMutableDictionary *)getAuthCredentials;

@end
