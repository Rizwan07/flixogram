//
//  FlickogramVideoEditingViewController.h
//  Flickogram


#import "BaseViewController.h"

#import <AVFoundation/AVFoundation.h>
#import "FlickogramMotionEffects.h"
#import "FlickogramFrameSlider.h"
#import <MediaPlayer/MediaPlayer.h>

typedef enum {
	SlowMotion,
	StillMotion,
} MotionEffect;

@interface FlickogramVideoEditingViewController : BaseViewController <FlickogramMotionEffectsDelegate, MPMediaPickerControllerDelegate> {
	CGFloat duration;
	NSTimer *playingTimer;
	MotionEffect motion;
	NSURL *videoMotion;
	NSURL *pathForUpload;
	NSURL *urlToPlay;
	FlickogramFrameSlider *slider;
	BOOL isMusicAdded;
	BOOL rotate;
}

@property (nonatomic, unsafe_unretained) IBOutlet UIControl *videoPreviewView;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (nonatomic, strong) NSURL *pathMovie;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *play;
@property (nonatomic, unsafe_unretained) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *grayBackground;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *currentMotion;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *slowMotionButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *stillMotionButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *toggleButton;
@property (nonatomic, unsafe_unretained) IBOutlet UISlider *volumeSlider;

- (IBAction)playMovie:(id)sender;
- (IBAction)pauseMovie:(id)sender;
- (IBAction)slowMotion:(id)sender;
- (IBAction)stillMotion:(id)sender;
- (IBAction)actionToggleButton:(id)sender;

@end
