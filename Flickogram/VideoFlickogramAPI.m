//
//  VideoFlickogramAPI.m
//  Flickogram
//


#import "VideoFlickogramAPI.h"
#import "FlickogramConstantsURLAPI.h"

@implementation VideoFlickogramAPI
@synthesize delegate = _delegate;

- (void)addVideoOnService:(NSURL *) urlVideo
			   titleVideo:(NSString *)titleVideo 
				   userID:(NSString *)userID
		 descriptionVideo:(NSString *)descriptionVideo
				 tagVideo:(NSString *)tagVideo
				 latitude:(NSString *)latitude
				longitude:(NSString *)longitude
				 location:(NSString *)location
				videoType:(NSString *)videoType
			 withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:titleVideo forKey:@"title"];
    [parameters setObject:userID forKey:@"user_id"];
    [parameters setObject:tagVideo forKey:@"tags"];
	[parameters setObject:descriptionVideo forKey:@"description"];
	[parameters setObject:latitude forKey:@"latitude"];
	[parameters setObject:longitude forKey:@"longitude"];
	[parameters setObject:location forKey:@"location"];
	[parameters setObject:videoType forKey:@"videoType"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	


 
	NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" 
																		 path:CreateVideoAPI
																   parameters:parameters 
													constructingBodyWithBlock: 
									^(id <AFMultipartFormData>formData) {
										[formData appendPartWithFileURL:urlVideo name:@"video_file" error:nil]; 
    }];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate uploadVideoSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate uploadVideoFailedWithError:error];
																							 }
																							 
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){ 
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate uploadVideoFailedWithError:errorString];
																						 }];
	
	[operation setUploadProgressBlock:^(NSInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
		[delegate updateProgreesUploadWithCurrentValue:totalBytesWritten maxValue:totalBytesExpectedToWrite];
		NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
	
    [operation start];

}

- (void)getPopularVideosOnPage:(NSString *)page withType:(NSString *)type withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:type forKey:@"type"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:GetPopularVideo parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate getPopularVideoSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getPopularVideoFailedWithError:error];
																							}
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getPopularVideoFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)getFollowingVideosForUserID:(NSString *)userID
							 onPage:(NSString *)page
					   withDelegate:(id<FlickogramVideoDelegate>)delegate {
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:GetFollowingVideosAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  getFollowingVideosSuccesWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getFollowingVideosFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getFollowingVideosFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)getVideoDetailsOfVideoID:(NSString *)videoID 
						  userID:(NSString *)userID 
					  indexArray:(NSInteger)index
					withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:videoID forKey:@"video_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:GetVideoDetailsAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  getVideoDetailsSuccesWithRecivedData:JSON indexArray:index];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getVideoDetailsFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getVideoDetailsFailedWithError:errorString];
																						 }];
	[operation start];

}

- (void)likeVideoForUserID:(NSString *) userID 
				   videoID:(NSString *)videoID 
				indexArray:(NSInteger)currentIndex
			  withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:videoID forKey:@"video_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:LikeVideoAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  likeVideoSuccessWithRecivedData:JSON indexArray:currentIndex];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate likeVideoFiledWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate likeVideoFiledWithError:errorString];
																						 }];
	[operation start];

}

- (void)addCommentOnVideo:(NSString *)videoID 
				   userID:(NSString *)userID 
				  comment:(NSString *)comment 
			   indexArray:(NSInteger)currentIndex
			 withDelegate:(id<FlickogramVideoDelegate>)delegate 
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:videoID forKey:@"video_id"];
	[parameters setObject:comment forKey:@"comments"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:AddCommentAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  addCommentSuccessWithRecivedData:JSON indexArray:currentIndex];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate addCommentFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate addCommentFailedWithError:errorString];
																						 }];
	[operation start];

}

- (void)removeComment:(NSString *)commentID 
			   userID:(NSString *)userID 
			  videoID:(NSString *)videoID 
		   indexArray:(NSInteger)currentIndex
		 withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:videoID forKey:@"video_id"];
	[parameters setObject:commentID forKey:@"comment_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:RemoveCommentAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate removeCommentSuccessWithRecivedData:JSON indexArray:currentIndex];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate removeCommentFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate removeCommentFailedWithError:errorString];
																						 }];
	[operation start];

}

- (void)removeVideoID:(NSString *)videoID 
			userID:(NSString *)userID 
		 indexArray:(NSInteger)currentIndex 
	   withDelegate:(id<FlickogramVideoDelegate>)delegate 
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:videoID forKey:@"video_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:RemoveVideoAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  removeVideoSuccessWithRecivedData:JSON indexArray:currentIndex];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate removeVideoFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate removeVideoFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)getExploreVideosOnPage:(NSString *)page
				  withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:page forKey:@"page"];

	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:ExploreVideosAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate getExplorerVideosSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getExplorerVideosFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getExplorerVideosFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)getSuggestedVideosForUserID:(NSString*)userID withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:SuggestedVideoAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  getSuggestedVideosSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getSuggestedVideosFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getSuggestedVideosFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)getUserLikedVideosForUserID:(NSString *)userID
							 onPage:(NSString *)page
					   withDelegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:UserLikedVideosAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  getUserLikedVideosSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getUserLikedVideosFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getUserLikedVideosFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)reflickVideoForUserID:(NSString *)userID
					  videoID:(NSString *)videoID
					 delegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:videoID forKey:@"video_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:ReflickVideoAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  reflickVideoSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate reflickVideoFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate reflickVideoFailedWithError:errorString];
																						 }];
	[operation start];
}

- (void)getMostLikedVideosForUserID:(NSString *)userID
							 onPage:(NSString *)videoID
						   delegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:videoID forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:GetMostWantedLikedVideosAPI parameters:parameters];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate  getMostLikedVideosSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getMostLikedVideosFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getMostLikedVideosFailedWithError:errorString];
																						 }];
	[operation start];

}

- (void)getTrendingVideosOnPage:(NSString *)page
					   withType:(NSString *)type
					   delegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:type forKey:@"type"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:GetTrendingVideosAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate getTrendingVideosSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getTrendingVideosFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getTrendingVideosFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)reportVideo:(NSString *)videoID
		 reporterID:(NSString *)reporterID
	   reasonReport:(NSString *)reason
		   delegate:(id<FlickogramVideoDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:videoID forKey:@"video_id"];
	[parameters setObject:reporterID forKey:@"reporter_id"];
	[parameters setObject:reason forKey:@"reason"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:ReportVideoAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSLog(@"%@", JSON);
																							 [delegate reportVideoSuccessWithRecivedData:JSON];
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate reportVideoFailedWithError:errorString];
																						 }];
    [operation start];
}


@end
