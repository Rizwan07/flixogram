//
//  ExploreViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"
#import "GridVideo.h"
#import "PullTableView.h"

@interface ExploreViewController : BaseViewController <UISearchBarDelegate,
													 UITableViewDataSource,
													 UITableViewDelegate,
													 FlickogramSearchDelegate,
													 FlickogramVideoDelegate,
													 PullTableViewDelegate>
{
	UISegmentedControl *segmentedControl;
	NSMutableArray *usersArray;
	NSMutableArray *videosArray;
	BOOL isSearchFinished;
	BOOL isSearchStarted;
	NSInteger currentPage;
	NSInteger currentExploreVideoPage;
	GridVideo *gridVideoView;
	NSInteger countVideosByHashtag;
	BOOL isShowMoreVideo;
}

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UINavigationBar *segmentedBar;
@property (nonatomic, weak) IBOutlet UINavigationItem *segmentedItem;
@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, weak) IBOutlet PullTableView *videoTable;

- (void)resetData;

@end
