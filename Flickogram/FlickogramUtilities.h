//
//  FlickogramUtilities.h
//  Flickogram
//


#import <UIKit/UIKit.h>

@class AVCaptureConnection;

@interface FlickogramUtilities : NSObject {
	
}

+ (AVCaptureConnection *)connectionWithMediaType:(NSString *)mediaType fromConnections:(NSArray *)connections;

@end
