//
//  VideoEffects.m
//  Flickogram
//


#import "VideoEffects.h"

@interface VideoEffects()
- (void) removeFile:(NSURL *)fileURL;
@end

@implementation VideoEffects

- (id) init
{
	self = [super init];
	if (self != nil) {
		
		NSString *pathToMovie = [NSHomeDirectory() stringByAppendingPathComponent:NameRecordedVideoFile];
		moviePath = [[NSURL alloc] initFileURLWithPath:pathToMovie];

		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(changeMoviePath:)
													 name:NameNotificationChangeMoviePath
												   object:nil];
		countFrame = 0;
	}
	return self;
}

-(void)changeMoviePath:(NSNotification *)notification
{	
	NSDictionary *dict = [notification userInfo];
	moviePath = [dict objectForKey:@"MoviePath"];
}


- (void)applyFilterByNumber:(NSInteger)selectedFilter;
{
	if (selectedFilter == FlickogramFilterTypeNormal) {
		[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationStartFilteringVideo
															object:nil];
		NSDictionary *options = [NSDictionary dictionaryWithObject:moviePath 
															forKey:@"MoviePath"];
		[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationFinishFilteringVideo 
															object:self 
														  userInfo:options];
		return;
	}
	
	//get count frame in video file
	AVURLAsset *sourceAsset = [[AVURLAsset alloc] initWithURL:moviePath options:nil];
	UIInterfaceOrientation orientationVideo = [self orientationForTrack:moviePath];
	CGFloat naturalWidth = 0;
	CGFloat naturalHeight = 0;
	NSArray *videoTracks = [sourceAsset tracksWithMediaType:AVMediaTypeVideo];
	BOOL shouldRecordVideoTrack = ([videoTracks count] > 0);
	if (shouldRecordVideoTrack)	{
		AVAssetTrack *videoTrack = [videoTracks objectAtIndex:0];
		if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
			naturalWidth = videoTrack.naturalSize.height;
			naturalHeight = videoTrack.naturalSize.width;
		} else {
			naturalWidth = videoTrack.naturalSize.width;
			naturalHeight = videoTrack.naturalSize.height;
		}
		CGFloat frameRate = videoTrack.nominalFrameRate;
		CGFloat durationVideo = CMTimeGetSeconds(sourceAsset.duration);
		countFrame = frameRate * durationVideo;
	}
	
	GPUImageMovie *movieFile = [[GPUImageMovie alloc] initWithURL:moviePath];
	movieFile.delegate = self;
	
	switch (selectedFilter) {
		case FlickogramFilterTypeAmaro: {
			NSLog(@"Amaro");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[filterRGB setRed:(253.0/255.0)];
			[filterRGB setGreen:(230.0/255.0)];
			[filterRGB setBlue:(255.0/255.0)];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.1];
			
			[filterRGB addTarget:filterBrigtness];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteLight"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage 
																 smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
			
			break;
		}
		case FlickogramFilterTypeRise: {
			NSLog(@"Rise");
			filter = [[GPUImageFilterGroup alloc] init];			
			
			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[filterRGB setRed:(254.0/255.0)];
			[filterRGB setGreen:(251.0/255.0)];
			[filterRGB setBlue:(190.0/255.0)];

			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.1];
			
			[filterRGB addTarget:filterBrigtness];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteLight"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage 
																smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
			
			break;
		}
		case FlickogramFilterTypeHudson: {
			NSLog(@"Hudson");
			filter = [[GPUImageFilterGroup alloc] init];	
			
			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[filterRGB setRed:(208.0/255.0)];
			[filterRGB setGreen:(255.0/255.0)];
			[filterRGB setBlue:(253.0/255.0)];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc] init];
			[contrast setContrast:1.1];
			
			[filterRGB addTarget:contrast];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[contrast addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteLight"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
			
			break;
		}
		case FlickogramFilterTypeXPro2: {
			NSLog(@"X-pro 2");
						
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageToneCurveFilter *curvesFilter = [[GPUImageToneCurveFilter alloc] init];
			
			NSArray *redPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake((0.0/255.0), (0.0/255.0))], 
								  [NSValue valueWithCGPoint:CGPointMake((39.0/255.0), (18.0/255.0))], 
								  [NSValue valueWithCGPoint:CGPointMake((208.0/255.0), (229.0/255.0))],
								  [NSValue valueWithCGPoint:CGPointMake((255.0/255.0), (255.0/255.0))], nil];
			
			NSArray *greenPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake((0.0/255.0), (0.0/255.0))], 
									[NSValue valueWithCGPoint:CGPointMake((39.0/255.0), (18.0/255.0))], 
									[NSValue valueWithCGPoint:CGPointMake((208.0/255.0), (229.0/255.0))],
									[NSValue valueWithCGPoint:CGPointMake((255.0/255.0), (255.0/255.0))], nil];		
			
			NSArray *bluePoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake((0.0/255.0), (25.0/255.0))], 
								   [NSValue valueWithCGPoint:CGPointMake((255.0/255.0), (230.0/255.0))], nil];
			
			[curvesFilter setRedControlPoints:redPoints];
			[curvesFilter setGreenControlPoints:greenPoints];
			[curvesFilter setBlueControlPoints:bluePoints];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[curvesFilter setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc] init];
			[contrast setContrast:1.1];
			
			[curvesFilter addTarget:contrast];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[contrast addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteLight"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:curvesFilter]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
		
			break;
		}
		case FlickogramFilterTypeSierra: {
			NSLog(@"Sierra");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[filterRGB setRed:(214.0/255.0)];
			[filterRGB setGreen:(205.0/255.0)];
			[filterRGB setBlue:(196.0/255.0)];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.13];
			
			[filterRGB addTarget:filterBrigtness];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
		
			UIImage *inputImage= [UIImage imageNamed:@"vignetteLight"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
			
			break;
		}
		case FlickogramFilterTypeLoFi: {
			NSLog(@"Lo-Fi");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageSaturationFilter *saturation = [[GPUImageSaturationFilter alloc] init];
			[saturation setSaturation:0.7];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[saturation setInputRotation:kGPUImageRotateRight atIndex:0];
			}
		
			GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc] init];
			[contrast setContrast:2.0];
			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.1];
			
			[saturation addTarget:contrast];
			[contrast addTarget:filterBrigtness];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
				
			UIImage *inputImage= [UIImage imageNamed:@"vignetteMedium"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:saturation]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];

			break;
		}
		case FlickogramFilterTypeEarlybird: {
			NSLog(@"Earlybird");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageSaturationFilter *saturation = [[GPUImageSaturationFilter alloc] init];
			[saturation setSaturation:0.8];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[saturation setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc] init];
			[contrast setContrast:1.3];
			
			GPUImageRGBFilter *curvesFilter = [[GPUImageRGBFilter alloc] init];
			[curvesFilter setRed:(239.0/255.0)];
			[curvesFilter setGreen:(220.0/255.0)];
			[curvesFilter setBlue:(153.0/255.0)];
			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.15];
			
			[saturation addTarget:contrast];
			[contrast addTarget:curvesFilter];
			[curvesFilter addTarget:filterBrigtness];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteMedium"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:saturation]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];

			break;
		}
		case FlickogramFilterTypeSutro: {
			NSLog(@"Sutro");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter *curvesFilter = [[GPUImageRGBFilter alloc] init];
			[curvesFilter setRed:(213.0/255.0)];
			[curvesFilter setGreen:(184.0/255.0)];
			[curvesFilter setBlue:(156.0/255.0)];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[curvesFilter setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.1];
			
			[curvesFilter addTarget:filterBrigtness];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteMedium"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[sourcePicture addTarget:filterBlend];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:curvesFilter]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
			
			break;
		}
		case FlickogramFilterTypeToaster: {
			NSLog(@"Toaster");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageToneCurveFilter *curvesFilter = [[GPUImageToneCurveFilter alloc] init];
			NSArray *redPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake((0.0/255.0), (71.0/255.0))], 
								  [NSValue valueWithCGPoint:CGPointMake((49.0/255.0), (104.0/255.0))], 
								  [NSValue valueWithCGPoint:CGPointMake((120.0/255.0), (172.0/255.0))], 
								  [NSValue valueWithCGPoint:CGPointMake((178.0/255.0), (227.0/255.0))],
								  [NSValue valueWithCGPoint:CGPointMake((255.0/255.0), (227.0/255.0))], nil];
			
			NSArray *greenPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake((0.0/255.0), (27.0/255.0))], 
									[NSValue valueWithCGPoint:CGPointMake((33.0/255.0), (27.0/255.0))],
									[NSValue valueWithCGPoint:CGPointMake((86.0/255.0), (99.0/255.0))], 
									[NSValue valueWithCGPoint:CGPointMake((160.0/255.0), (165.0/255.0))],
									[NSValue valueWithCGPoint:CGPointMake((214.0/255.0), (222.0/255.0))],
									[NSValue valueWithCGPoint:CGPointMake((255.0/255.0), (222.0/255.0))], nil];
			
			NSArray *bluePoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake((0.0/255.0), (0.0/255.0))], 
								   [NSValue valueWithCGPoint:CGPointMake((204.0/255.0), (195.0/255.0))], 
								   [NSValue valueWithCGPoint:CGPointMake((255.0/255.0), (195.0/255.0))], nil];
			
			[curvesFilter setRedControlPoints:redPoints];
			[curvesFilter setGreenControlPoints:greenPoints];
			[curvesFilter setBlueControlPoints:bluePoints];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[curvesFilter setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.1];
			
			[curvesFilter addTarget:filterBrigtness];
			
			GPUImageSoftLightBlendFilter *filterBlend = [[GPUImageSoftLightBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
			
			GPUImageFilterGroup *filterBackgroundGroup = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter * backgroundFilterRGB = [[GPUImageRGBFilter alloc] init];
			[(GPUImageRGBFilter *)backgroundFilterRGB setRed:(255.0/255.0)];
			[(GPUImageRGBFilter *)backgroundFilterRGB setGreen:(246.0/255.0)];
			[(GPUImageRGBFilter *)backgroundFilterRGB setBlue:(105.0/255.0)];
			
			GPUImageBrightnessFilter *filterBackgroundBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBackgroundBrigtness setBrightness:0.2];
			
			[backgroundFilterRGB addTarget:filterBackgroundBrigtness];
			[filterBackgroundBrigtness addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteMedium"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[(GPUImageFilterGroup *)filterBackgroundGroup setInitialFilters:[NSArray arrayWithObject:backgroundFilterRGB]];
			[(GPUImageFilterGroup *)filterBackgroundGroup setTerminalFilter:filterBlend];
			[sourcePicture addTarget:filterBackgroundGroup];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:curvesFilter]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
			
			break;
		}
		case FlickogramFilterTypeBrannan: {
			NSLog(@"Brannan");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageToneCurveFilter *curvesFilter = [[GPUImageToneCurveFilter alloc] init];
			
			NSArray *redPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)], 
								  [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)], nil];
			
			NSArray *greenPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.066666)], 
									[NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)], nil];
			
			NSArray *bluePoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.149019)], 
								   [NSValue valueWithCGPoint:CGPointMake(1.0, 0.682352)], nil];
			
			[(GPUImageToneCurveFilter *)curvesFilter setRedControlPoints:redPoints];
			[(GPUImageToneCurveFilter *)curvesFilter setGreenControlPoints:greenPoints];
			[(GPUImageToneCurveFilter *)curvesFilter setBlueControlPoints:bluePoints];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[curvesFilter setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			// Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
			GPUImageBrightnessFilter *filterBrightness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrightness setBrightness:0];
			
			[curvesFilter addTarget:filterBrightness];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:curvesFilter]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBrightness];

			break;
		}
		case FlickogramFilterTypeInkwell: {
			NSLog(@"Inkwell");
			filter = [[GPUImageGrayscaleFilter alloc] init];

			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filter setInputRotation:kGPUImageRotateRight atIndex:0];
			}

			break;
		}
		case FlickogramFilterTypeWalden:{
			NSLog(@"Walden");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter * filterRGB = [[GPUImageRGBFilter alloc] init];
			[(GPUImageRGBFilter *) filterRGB setRed:0.9921568627451];
			[(GPUImageRGBFilter *) filterRGB setGreen:0.98039215686275];
			[(GPUImageRGBFilter *) filterRGB setBlue:0.8];
		
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}

			
			GPUImageBrightnessFilter *filterBrigtness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrigtness setBrightness:0.2];
			
			[(GPUImageFilterGroup *)filter addFilter:filterRGB];
			[(GPUImageFilterGroup *)filter addTarget:filterBrigtness];
			[filterRGB addTarget:filterBrigtness];
			
			GPUImageOverlayBlendFilter *filterBlend = [[GPUImageOverlayBlendFilter alloc] init];
			[(GPUImageFilterGroup *)filter addFilter:filterBlend];
			[filterBrigtness addTarget:filterBlend];
			
			GPUImageFilterGroup *filterBackground = [[GPUImageFilterGroup alloc] init];
			GPUImageRGBFilter * filterRGBBlue = [[GPUImageRGBFilter alloc] init];
			[(GPUImageRGBFilter *) filterRGBBlue setRed:(165.0/255.0)];
			[(GPUImageRGBFilter *) filterRGBBlue setGreen:(201.0/255.0)];
			[(GPUImageRGBFilter *) filterRGBBlue setBlue:(255.0/255.0)];
			
			[filterRGBBlue addTarget:filterBlend];
			
			UIImage *inputImage= [UIImage imageNamed:@"vignetteDark"];
			GPUImagePicture *sourcePicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
			[sourcePicture processImage];
			
			[(GPUImageFilterGroup *)filterBackground setInitialFilters:[NSArray arrayWithObject:filterRGBBlue]];
			[(GPUImageFilterGroup *)filterBackground setTerminalFilter:filterBlend];
			[sourcePicture addTarget:filterBackground];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBlend];
			
			break;
		}
		case FlickogramFilterTypeHefe: {
			NSLog(@"Hefe");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[(GPUImageRGBFilter *)filterRGB setRed:0.77647058823529];
			[(GPUImageRGBFilter *)filterRGB setGreen:0.71372549019608];
			[(GPUImageRGBFilter *)filterRGB setBlue:0.62352941176471];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}

			// Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
			GPUImageBrightnessFilter *filterBrightness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrightness setBrightness:0.1];
			
			// Contrast ranges from 0.0 to 4.0 (max contrast), with 1.0 as the normal level
			GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc] init];
			[(GPUImageContrastFilter *)contrast setContrast:1.5];
			
			GPUImageVignetteFilter *vignette = [[GPUImageVignetteFilter alloc] init];
			
			[filterRGB addTarget:filterBrightness];
			[filterBrightness addTarget:contrast];
			[contrast addTarget:vignette];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:vignette];
			
			break;
		}
		case FlickogramFilterTypeValencia: {
			NSLog(@"Valencia");
			filter = [[GPUImageFilterGroup alloc] init];

			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[(GPUImageRGBFilter *)filterRGB setRed:0.894117];
			[(GPUImageRGBFilter *)filterRGB setGreen:0.925490];
			[(GPUImageRGBFilter *)filterRGB setBlue:0.647058];

			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			// Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
			GPUImageBrightnessFilter *filterBrightness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrightness setBrightness:0.12];
			
			// Contrast ranges from 0.0 to 4.0 (max contrast), with 1.0 as the normal level
			GPUImageContrastFilter *contrast = [[GPUImageContrastFilter alloc] init];
			[(GPUImageContrastFilter *)contrast setContrast:1.3];
			
			[filterRGB addTarget:filterBrightness];
			[filterBrightness addTarget:contrast];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:contrast];

			break;
		}
		case FlickogramFilterTypeNashville: {
			NSLog(@"Nashville");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[(GPUImageRGBFilter *)filterRGB setRed:0.980392];
			[(GPUImageRGBFilter *)filterRGB setGreen:0.862745];
			[(GPUImageRGBFilter *)filterRGB setBlue:0.686274];

			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			// Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
			GPUImageBrightnessFilter *filterBrightness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrightness setBrightness:0.2];
			
			[filterRGB addTarget:filterBrightness];
			
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBrightness];
			break;
		}
		case FlickogramFilterType1977: {
			NSLog(@"1977");
			filter = [[GPUImageFilterGroup alloc] init];
			
			GPUImageRGBFilter *filterRGB = [[GPUImageRGBFilter alloc] init];
			[(GPUImageRGBFilter *)filterRGB setRed:0.835294];
			[(GPUImageRGBFilter *)filterRGB setGreen:0.525490];
			[(GPUImageRGBFilter *)filterRGB setBlue:0.694117];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filterRGB setInputRotation:kGPUImageRotateRight atIndex:0];
			}
			
			// Brightness ranges from -1.0 to 1.0, with 0.0 as the normal level
			GPUImageBrightnessFilter *filterBrightness = [[GPUImageBrightnessFilter alloc] init];
			[(GPUImageBrightnessFilter *)filterBrightness setBrightness:0.2];
			
			[filterRGB addTarget:filterBrightness];
			[(GPUImageFilterGroup *)filter setInitialFilters:[NSArray arrayWithObject:filterRGB]];
			[(GPUImageFilterGroup *)filter setTerminalFilter:filterBrightness];
			
			break;
		}
		case FlickogramFilterTypeKelvin: {
			NSLog(@"Kelvin");
			filter = [[GPUImageToneCurveFilter alloc] init];
			
			//rotation .mov files
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				[filter setInputRotation:kGPUImageRotateRight atIndex:0];
			}

			NSArray *redPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.176470)], 
								  [NSValue valueWithCGPoint:CGPointMake(0.403921, 0.780392)],
								  [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)], nil];
			
			NSArray *greenPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.117647)], 
									[NSValue valueWithCGPoint:CGPointMake(0.309803, 0.478431)], 
									[NSValue valueWithCGPoint:CGPointMake(0.796078, 0.847058)], 
									[NSValue valueWithCGPoint:CGPointMake(1.0, 0.847058)], nil];
			
			NSArray *bluePoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, 0.305882)], 
								   [NSValue valueWithCGPoint:CGPointMake(0.498039, 0.454901)], 
								   [NSValue valueWithCGPoint:CGPointMake(0.835294, 0.721568)],
								   [NSValue valueWithCGPoint:CGPointMake(1.0, 0.721568)], nil];
			
			[(GPUImageToneCurveFilter *)filter setRedControlPoints:redPoints];
			[(GPUImageToneCurveFilter *)filter setGreenControlPoints:greenPoints];
			[(GPUImageToneCurveFilter *)filter setBlueControlPoints:bluePoints];
			break;
		}
		default:
			break;
	}
	
	[movieFile addTarget:filter];
	
	NSString *filteredVideo = [NSHomeDirectory() stringByAppendingPathComponent:NameFilteredVideoFile];
	filteredVideoURL = [NSURL fileURLWithPath:filteredVideo];
	[self removeFile:filteredVideoURL];
	
	
	movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:filteredVideoURL 
														   size:CGSizeMake(naturalWidth, naturalHeight)];
	movieWriter.delegate = self;
	[filter addTarget:movieWriter];
	
	movieWriter.shouldPassthroughAudio = YES;
	movieFile.audioEncodingTarget = movieWriter;
	[movieFile enableSynchronizedEncodingUsingMovieWriter:movieWriter];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationStartFilteringVideo
														object:nil];
	[movieWriter startRecording];
	[movieFile startProcessing];	
}

- (void) removeFile:(NSURL *)fileURL
{
    NSString *filePath = [fileURL path];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSError *error;
		[fileManager removeItemAtPath:filePath error:&error];
    }
}

- (void)movieRecordingCompleted
{
	[filter removeTarget:movieWriter];
	[movieWriter finishRecording];
	filter = nil;
	movieWriter = nil;
	
	NSLog(@"Finish");
	
	NSDictionary *options = [NSDictionary dictionaryWithObject:filteredVideoURL forKey:@"MoviePath"];
	[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationFinishFilteringVideo
														object:self 
													  userInfo:options];
}

- (void)movieRecordingFailedWithError:(NSError*)error
{
	NSLog (@"Error");
}

- (void)frameCompleted:(NSInteger)currentFrame
{
	NSNumber *countFrameObj = [NSNumber numberWithInteger:countFrame];
	NSNumber *currentFrameObj = [NSNumber numberWithInteger:currentFrame];
	
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:countFrameObj, @"CountFrame", currentFrameObj, @"CurrentFrame", nil];
    
    [self.delegate frameCompleted:options];
//	[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationCompletedFilteringFrame 
//														object:self 
//													  userInfo:options];
}

- (UIInterfaceOrientation)orientationForTrack:(NSURL *)url
{
	AVURLAsset *asset = [AVURLAsset assetWithURL:url];
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
	
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}

@end
