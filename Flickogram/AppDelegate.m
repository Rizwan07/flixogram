//
//  AppDelegate.m
//  Flickogram



#import "AppDelegate.h"
#import "AuthorizationViewController.h"
#import "MainViewController.h"
#import "ExploreViewController.h"
#import "FollowingViewController.h"
#import "ProfileViewController.h"
#import "CustomNavController.h"
#import "RecordViewController.h"
#import <FacebookSDK/FacebookSDK.h>

#import "InitialSlidingViewController.h"
#import "GAI.h"

#import "RootViewController.h"

@implementation AppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.rootViewController = [[RootViewController alloc] init];
    //[self.window addSubview:[self.rootViewController view]];
    [self.window setRootViewController:self.rootViewController];
    [self.window makeKeyAndVisible];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker.
    //[[GAI sharedInstance] trackerWithTrackingId:@"UA-49206048-1"];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-49762969-1"];
    
    return YES;
}

//- (void)selectTabBarIndex:(NSInteger)indexTabBar
//{
//	[tabBarController setSelectedIndex:indexTabBar];
//}
//
//- (void)tabBarController:(UITabBarController *)tabBarController1 didSelectViewController:(UIViewController*)viewController {
//    
//    if (![viewController isKindOfClass:[RecordViewController class]]) {
//    
//        CATransition *animation = [CATransition animation];
//        [animation setType:kCATransitionFade];
//        [animation setDuration:0.25];
//        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:
//                                      kCAMediaTimingFunctionEaseIn]];
//        [tabBarController.view.window.layer addAnimation:animation forKey:@"fadeTransition"];
//    }
//}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
	return [FBSession.activeSession handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
