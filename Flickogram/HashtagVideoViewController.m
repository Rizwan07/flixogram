//
//  HashtagVideoViewController.m
//  Flickogram
//

#import "HashtagVideoViewController.h"
#import "VideoDetailViewController.h"
#import "TableVideoCell.h"
#import "GridVideoCell.h"

@interface HashtagVideoViewController ()
- (BOOL)parseVideosDictionary:(NSDictionary *)data;
@end

@implementation HashtagVideoViewController
@synthesize hashtag = _hashtag;
@synthesize countVideo = _countVideo;
@synthesize countVideoLabel = _countVideoLabel;
@synthesize hashtagLabel = _hashtagLabel;
@synthesize table = _table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.screenName = @"Hashtag";
    
	//Off Activity indicator for previous operations
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	
	//Segmented Control
	NSArray *itemArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"tableViewSelected"],
						  [UIImage imageNamed:@"gridView"], nil];
	segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
	segmentedControl.frame = CGRectMake(0, 0, 90, 0);
	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
	segmentedControl.tintColor = [UIColor clearColor];
	[segmentedControl setWidth:45 forSegmentAtIndex:0];
	[segmentedControl setWidth:45 forSegmentAtIndex:1];
	[segmentedControl addTarget:self action:@selector(changeSegment) forControlEvents:UIControlEventValueChanged];
	segmentedControl.selectedSegmentIndex = 0;
	self.navigationItem.titleView = segmentedControl;
	
	hashtagsVideosArray = [[NSMutableArray alloc] init];
	
	self.hashtagLabel.text = [NSString stringWithFormat:@"#%@", self.hashtag];
	self.countVideoLabel.text = [NSString stringWithFormat:@"%d videos", self.countVideo];
	
	currentPage = 1;
	isShowMore = NO;
	
	NSString *currentPageString = [NSString stringWithFormat:@"%d", currentPage];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI searchExemplar] getVideosByTags:self.hashtag onPage:currentPageString withDelegate:self];
	
	//pull to refresh
	self.table.pullArrowImage = [UIImage imageNamed:@"whiteArrow"];
	self.table.pullBackgroundColor = [UIColor clearColor];
	self.table.pullTextColor = [UIColor whiteColor];
	self.table.loadMoreEnabled = NO;
}

- (void)back
{
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)changeSegment
{
//	NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
//	UIImage *firstImage;
//	UIImage *secondImage;
//	switch (selectedSegment) {
//		case 0: {
//			firstImage = [UIImage imageNamed:@"tableViewSelected"];
//			secondImage = [UIImage imageNamed:@"gridView"];
//			break;
//		}
//		case 1: {
//			firstImage = [UIImage imageNamed:@"tableView"];
//			secondImage = [UIImage imageNamed:@"gridViewSelected"];
//			break;
//		}
//	}
//	[segmentedControl setImage:firstImage forSegmentAtIndex:0];
//	[segmentedControl setImage:secondImage forSegmentAtIndex:1];
	[self.table reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - GetVideyByTags
- (void)getVideoByTagsSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSInteger countVideosByHashtag = [[data objectForKey:@"total_videos"] integerValue];

	if ([[data objectForKey:@"videos"] count]) {
		BOOL parseSuccess = [self parseVideosDictionary:data];
		if (parseSuccess) {
			NSInteger currentCount = [hashtagsVideosArray count];
			if (currentCount < countVideosByHashtag) {
				isShowMore = YES;
			} else {
				isShowMore = NO;
			}
		}
	} else if (self.table.pullTableIsRefreshing) {
		[self refreshTable];
	}
}

- (BOOL)parseVideosDictionary:(NSDictionary *)data
{
	NSMutableDictionary *videos = [data objectForKey:@"videos"];
	
	if (self.table.pullTableIsRefreshing) {
		NSString *lastDateAdded = @"0";
		if ([hashtagsVideosArray count] > 0) {
			lastDateAdded = [[hashtagsVideosArray objectAtIndex:0] objectForKey:@"date_added"];
		}
		NSInteger currentVideo = 0;
		BOOL isMoreVideo = YES;
		while (isMoreVideo) {
			NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
			if ([videos objectForKey:numberUserString]) {
				NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
				NSString *newDateAdded = [videoDetails objectForKey:@"date_added"];
				
				//compare last date with new date and stop if equal
				if ([lastDateAdded isEqualToString:newDateAdded]) {
					[self refreshTable];
					return YES;
				} else {
					[hashtagsVideosArray insertObject:videoDetails atIndex:currentVideo];
					
					if (isShowMore) {
						[hashtagsVideosArray removeLastObject];
					}
				}
				currentVideo++;
			} else {
				isMoreVideo = NO;
			}
		}
		[self refreshTable];
		return YES;
	}

	
	NSInteger currentVideo = 0;
	BOOL isMoreVideo = YES;
	while (isMoreVideo) {
		NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
		if ([videos objectForKey:numberUserString]) {
			NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
			[hashtagsVideosArray addObject:videoDetails];
			currentVideo++;
		} else {
			isMoreVideo = NO;
		}
	}
	[self.table reloadData];
	return YES;
}

- (void)getVideoByTagsFailedWithError:(NSString *)error
{
	[self refreshTable];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - Grid Video Delegate
- (void)showVideoByIndexArray:(NSInteger)indexArray
{
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[hashtagsVideosArray objectAtIndex:indexArray] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}

- (void)showMoreVideos
{
	if (isShowMore) {
		isShowMore = NO;
		currentPage++;
		NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[[ManagerFlickogramAPI searchExemplar] getVideosByTags:self.hashtag onPage:pageInStringFormat withDelegate:self];
	}
}

#pragma mark - UITableView Delegate/Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		NSInteger count = [hashtagsVideosArray count];
		NSInteger countCells = (NSInteger) (count + 9) / 10;
 		return countCells;
	} else {
		return [hashtagsVideosArray count];
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		return 135;
	} else {
		return 160;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		static NSString *cellId = @"GridVideoCell";
		GridVideoCell *cell = (GridVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GridVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (GridVideoCell *) currentObject;
				}
				break;
			}
		}
		
		NSInteger currentCell =	indexPath.row * 10;
		NSInteger countVideos = [hashtagsVideosArray count];
		
		int currentButton = 0;
		for (int i = currentCell; (i < countVideos && i < currentCell+10); i++) {
			NSDictionary *videoDetail = [hashtagsVideosArray objectAtIndex:i];
			NSURL *thumbnailURL = [NSURL URLWithString:[videoDetail objectForKey:@"thumbnail"]];
			EGOImageButton *currentImageButton = [cell setThumbImageOfURL:thumbnailURL ForButton:currentButton];
			currentImageButton.tag = i;
			[currentImageButton addTarget:self action:@selector(showVideoForEGOImageButton:) forControlEvents:UIControlEventTouchUpInside];
			currentButton++;
		}
		return cell;
	} else {
		static NSString *cellIdentifier = @"TableVideoCell";
		TableVideoCell *cell = (TableVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TableVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (TableVideoCell *) currentObject;
				}
				break;
			}
		}
		NSDictionary *videoDetail = [hashtagsVideosArray objectAtIndex:indexPath.row];
		NSString *thumbnailURL = [videoDetail objectForKey:@"thumbnail"];
		[cell setThumbnail:thumbnailURL];
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 1) {
		VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
		videoDetailVC.videoDetail = [[hashtagsVideosArray objectAtIndex:indexPath.row] mutableCopy];
		[self.navigationController pushViewController:videoDetailVC animated:YES];
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (isShowMore) {
		NSInteger count = [hashtagsVideosArray count];
		NSInteger countCells = (NSInteger) ((count + 9) / 10) - 1;
		if (((segmentedControl.selectedSegmentIndex ==0) && (indexPath.row == countCells)) ||
			((segmentedControl.selectedSegmentIndex == 1) && (indexPath.row == count - 1))) {
				[self showMoreVideos];
		}
	}
}

- (void)showVideoForEGOImageButton:(id)sender
{
	EGOImageButton *selectedButton = (EGOImageButton *)sender;
	NSInteger indexVideo = selectedButton.tag;
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[hashtagsVideosArray objectAtIndex:indexVideo] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
	// Code to actually refresh goes here.
    self.table.pullLastRefreshDate = [NSDate date];
    self.table.pullTableIsRefreshing = NO;
	[self.table reloadData];
}

- (void) loadMoreDataToTable
{
    // Code to actually load more data goes here.
    self.table.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
	[[ManagerFlickogramAPI searchExemplar] getVideosByTags:self.hashtag onPage:@"1" withDelegate:self];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}


@end
