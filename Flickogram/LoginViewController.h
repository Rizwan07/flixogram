//
//  AuthorizationViewController.h
//  Flickogram


#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"

@interface LoginViewController : BaseViewController <FlickogramAuthorizationDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) IBOutlet UITextField *email;
@property (nonatomic, strong) IBOutlet UITextField *password;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activity;

@end
