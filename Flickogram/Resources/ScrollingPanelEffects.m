//
//  ScrollingPanelEffects.m
//  Flickogram
//

#import "ScrollingPanelEffects.h"


@interface ScrollingPanelEffects ()

- (void)createButtonWithNameEffect:(NSString *)name withTag:(NSUInteger)tag;
- (void)filterSelected:(UIButton *)selectedButton;

@end

@implementation ScrollingPanelEffects
@synthesize effects = _effects;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		coordX = 10; //start coordinate
		NSArray *effects = [NSArray arrayWithObjects:@"normal", @"Amora", @"Uprise", @"Itty-B", @"Mpro", @"Dasan", 
							@"Meelo", @"Mornin", @"Saturn", @"Heat", @"Brandy", @"BW", @"Hardeem", 
							@"Boss", @"Valance", @"Tenne", @"1970", @"CLT" ,nil];
		
		NSUInteger currentTag = 0;
		for (NSString *name in effects) {
			[self createButtonWithNameEffect:name withTag:currentTag];
			currentTag++;
		}
		self.effects = [[VideoEffects alloc] init];
    }
    return self;
}

- (void)createButtonWithNameEffect:(NSString *)name withTag:(NSUInteger)tag;
{
	CGRect newFrame = CGRectMake(coordX, 10, 60, 60);
	
	UIButton *effectButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[effectButton setTitle:name forState:UIControlStateNormal];
	[effectButton setImage:[UIImage imageNamed:name] forState:UIControlStateNormal];
	[effectButton addTarget:self action:@selector(filterSelected:) forControlEvents:UIControlEventTouchDown];
	[effectButton setTag:tag];
	effectButton.frame = newFrame;
	
	[self addSubview:effectButton];
	coordX += 70;
	CGSize newSize = CGSizeMake(effectButton.frame.origin.x + effectButton.frame.size.width, self.frame.size.height);	
	self.contentSize = newSize;
}

- (void)filterSelected:(UIButton *)selectedButton
{
	NSInteger selectedFilter = selectedButton.tag;
	[self.effects applyFilterByNumber:selectedFilter];
}


@end
