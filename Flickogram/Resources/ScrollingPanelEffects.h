//
//  ScrollingPanelEffects.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import "VideoEffects.h"

@interface ScrollingPanelEffects : UIScrollView {
	NSUInteger	coordX;
}

@property (nonatomic, strong) VideoEffects *effects;

@end

