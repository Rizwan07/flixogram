//
//  ProfileViewController.h
//  Flickogram

#import "BaseViewController.h"
#import "EGOImageView.h"
#import "ManagerFlickogramAPI.h"
#import "GridVideo.h"
#import "PullTableView.h"

@interface ProfileViewController : BaseViewController <FlickogramAuthorizationDelegate,
														EGOImageViewDelegate,
														GridVideoDelegate,
														UITableViewDataSource,
														UITableViewDelegate,
														PullTableViewDelegate,
														UIActionSheetDelegate,
														UIAlertViewDelegate>
{
	UISegmentedControl *segmentedControl;
	BOOL isAuthUser;
	GridVideo *gridVideoView;
	NSMutableArray *videosArray;
	NSInteger currentPage;
	BOOL isShowMoreVideo;
}

@property (nonatomic, assign) BOOL hasParentNavigationView;
@property (nonatomic, strong) NSString *selectedUserID;
@property (nonatomic, weak) IBOutlet EGOImageView *thumbUser;
@property (nonatomic, weak) IBOutlet UILabel *username;
@property (nonatomic, weak) IBOutlet UILabel *followers;
@property (nonatomic, weak) IBOutlet UILabel *followings;
@property (nonatomic, weak) IBOutlet UILabel *videos;
@property (nonatomic, weak) IBOutlet PullTableView *table;
@property (nonatomic, weak) IBOutlet UIView *followPangel;
@property (nonatomic, weak) IBOutlet UILabel *infoFollow;
@property (nonatomic, weak) IBOutlet UIButton *toggleFollow;

- (IBAction)showFollowers:(id)sender;
- (IBAction)showFollowings:(id)sender;
- (IBAction)followUnFollowEvent:(id)sender;
- (void)resetData;

@end
