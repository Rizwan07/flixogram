//
//  FindFriendsViewController.m
//  Flickogram

#import "FindFriendsViewController.h"
#import "DetailFriendsViewController.h"
#import "TwitterSettings.h"
#import <Accounts/Accounts.h>
#import <AddressBook/AddressBook.h>
#import <Social/Social.h>
#import "SuggestedViewController.h"

@interface FindFriendsViewController ()
- (void)chooseTwitterAccount;
@end

@implementation FindFriendsViewController
@synthesize isShowBackButton = _isShowBackButton;
@synthesize isShowSuggestedUsers = _isShowSuggestedUsers; //set from OptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isShowSuggestedUsers = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.screenName = @"Find Friends";
    
//	UIImage *backgroundImage = [UIImage imageNamed:@"header"];
//	[self.navigationController.navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];

	if (self.isShowBackButton) {//show from profile
		UIImage *backButtonImage = [UIImage imageNamed:@"back"];
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
		backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
		[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
		self.navigationItem.leftBarButtonItem = backItem;
	} else { //set from Sign up
		//set Next button
		UIImage *nextButtonImage = [UIImage imageNamed:@"next"];
		UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[nextButton setBackgroundImage:nextButtonImage forState:UIControlStateNormal];
		nextButton.frame = CGRectMake(0.0, 0.0, 50, 30);
		[nextButton addTarget:self action:@selector(next) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *nextItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
		self.navigationItem.rightBarButtonItem = nextItem;
	}
	
	searchMethods = [NSArray arrayWithObjects:@"From my contact list", 
					 @"Facebook friends",
					 @"Twitter friends", 
					 @"Search names and usernames", nil];
	self.navigationItem.title = @"Find friends";
}

- (void)next
{
	SuggestedViewController *suggestedUser = [[SuggestedViewController alloc] init];
	suggestedUser.isShowDoneButton = YES;
	[self.navigationController pushViewController:suggestedUser animated:YES];
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	if (self.isShowSuggestedUsers) {
		return 2; //Suggested section
	} else {
		return 1; //Standart section
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (self.isShowSuggestedUsers) {
		if (section == 0) {
			return 3;
		} else {
			return 1;
		}
	} else {
		return [searchMethods count];
	}
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
	NSString *titleForFooter = @"";
	if (section == 1) {
		titleForFooter = @"Search for users & tags in the Explore tab.";
	}
	return titleForFooter;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
                                       reuseIdentifier:CellIdentifier];
    }
	if (indexPath.section == 0) {
		cell.textLabel.text = [searchMethods objectAtIndex:indexPath.row];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	} else if (indexPath.section == 1) {
		cell.textLabel.text = @"Suggested users";
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
		
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	DetailFriendsViewController *detailFriends = [[DetailFriendsViewController alloc] init];
	
	if (self.isShowSuggestedUsers && indexPath.section == 1) { //Suggested for section 1
		SuggestedViewController *suggestedUsers = [[SuggestedViewController alloc] init];
		[self.navigationController pushViewController:suggestedUsers animated:YES];
	} else {
		if (indexPath.row == 0) { //From my contact list
			UIAlertView *alertForContactList = [[UIAlertView alloc] initWithTitle:@"Search for Your Friends in Address Book?"
																		  message:@"In order to find your friends, we need to send address book information to Flickogram's servers using a secure connection."
																		 delegate:self
																cancelButtonTitle:@"Cancel"
																otherButtonTitles:@"Allow", nil];
			alertForContactList.tag = 10;
			[alertForContactList show];
		}
	}
	if (indexPath.row == 1) {
		detailFriends.socialType = @"Facebook";
		[self.navigationController pushViewController:detailFriends animated:YES];
	} 
	if (indexPath.row == 2) {
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		if (![defaults objectForKey:@"TwitterAccountIdentifier"]) {
			[self chooseTwitterAccount];
		} else {
			detailFriends.socialType = @"Twitter";
			[self.navigationController pushViewController:detailFriends animated:YES];
		}
	}
	if (indexPath.row == 3) {
		detailFriends.socialType = @"FlixogramSearch";
		[self.navigationController pushViewController:detailFriends animated:YES];
	}
}

#pragma mark - Work with Twitter
//Choose twitter account
- (void)chooseTwitterAccount
{
	//  First, we need to obtain the account instance for the user's Twitter account
	ACAccountStore *store = [[ACAccountStore alloc] init];

	ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
	//  Request permission from the user to access the available Twitter accounts
	
	
	//IOS 6 work
	if ([store respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)]) {
		//if( [TWTweetComposeViewController canSendTweet] ) {
        if( [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] ) {
			[store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
				if (!granted) {
					// The user rejected your request
					NSLog(@"User rejected access to the account.");
				}
				else {
					// Grab the available accounts
					twitterAccounts = [store accountsWithAccountType:twitterAccountType];
					if ([twitterAccounts count] > 0) {
						if ([twitterAccounts count] > 1) {
							[self performSelectorOnMainThread:@selector(showActionSheet) withObject:self waitUntilDone:NO];
						} else {
							// Use the first account for simplicity
							ACAccount *account = [twitterAccounts objectAtIndex:0];
							NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
							[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
						}
					} else {
						[self performSelectorOnMainThread:@selector(showAlertViewTwitterForIOS6) withObject:nil waitUntilDone:NO];
					}
				}
				
			}];
		} else {
			[self performSelectorOnMainThread:@selector(showAlertViewTwitterForIOS6) withObject:nil waitUntilDone:NO];
		}
		//ios 5
	} else {
		//[store requestAccessToAccountsWithType:twitterAccountType withCompletionHandler:^(BOOL granted, NSError *error) {
        [store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
			if (!granted) {
				// The user rejected your request
				NSLog(@"User rejected access to the account.");
			}
			else {
				// Grab the available accounts
				twitterAccounts = [store accountsWithAccountType:twitterAccountType];
				if ([twitterAccounts count] > 0) {
					if ([twitterAccounts count] > 1) {
						[self performSelectorOnMainThread:@selector(showActionSheet) withObject:self waitUntilDone:NO];
					} else {
						// Use the first account for simplicity
						ACAccount *account = [twitterAccounts objectAtIndex:0];
						NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
						[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
					}
				} else {
					[self performSelectorOnMainThread:@selector(showAlertViewTwitter) withObject:nil waitUntilDone:NO];
				}
			}
		}];
	}
}

- (void)showAlertViewTwitterForIOS6
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
													message:@"There are no Twitter accounts configured. You can add or create a Twitter account in iPhone Settings."
												   delegate:self cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}
- (void)showAlertViewTwitter
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts" 
													message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings." 
												   delegate:self cancelButtonTitle:@"Cancel" 
										  otherButtonTitles:@"Settings", nil];
	[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == 10) {  //Alert for From my contact list
		if (buttonIndex == 1) {
            
            //__weak typeof(self) weakSelf = self;
            
            ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                if (granted) {
                    NSLog(@"user granted for address book");
                    
                    NSMutableArray *users = [NSMutableArray array];
                    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
                    if (allPeople) {
                        for (int index=0; index<CFArrayGetCount(allPeople); index++){
                            NSMutableDictionary *currentPerson = [[NSMutableDictionary alloc] init];
                            ABRecordRef thisPerson = CFArrayGetValueAtIndex(allPeople, index);
                            NSString *firstName = (__bridge NSString *)ABRecordCopyValue(thisPerson,
                                                                                         kABPersonFirstNameProperty);
                            NSString *lastName = (__bridge NSString *)ABRecordCopyValue(thisPerson,
                                                                                        kABPersonLastNameProperty);
                            if ([firstName length]) {
                                [currentPerson setObject:firstName forKey:@"first_name"];
                            }
                            if ([lastName length]) {
                                [currentPerson setObject:lastName forKey:@"last_name"];
                            }
                            if ([currentPerson count]) {
                                [users addObject:currentPerson];
                            }
                            CFRelease((__bridge CFTypeRef)(firstName));
                            CFRelease((__bridge CFTypeRef)(lastName));
                        }
                        CFRelease(allPeople);
                    }
                    CFRelease(addressBook);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        DetailFriendsViewController *detailFriends = [[DetailFriendsViewController alloc] init];
                        detailFriends.socialType = @"AddressBook";
                        detailFriends.usersAB = [users copy];
                        [self.navigationController pushViewController:detailFriends animated:YES];
                    });
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enable contact privacy setting for Flixogram" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    });
                }
            });
//			ABAddressBookRef addressBook = ABAddressBookCreate();
//			if (addressBook) {
//				NSMutableArray *users = [[NSMutableArray alloc] init];
//				CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
//				if (allPeople) {
//					for (int index=0; index<CFArrayGetCount(allPeople); index++){
//						NSMutableDictionary *currentPerson = [[NSMutableDictionary alloc] init];
//						ABRecordRef thisPerson = CFArrayGetValueAtIndex(allPeople, index);
//						NSString *firstName = (__bridge NSString *)ABRecordCopyValue(thisPerson,
//																					 kABPersonFirstNameProperty);
//						NSString *lastName = (__bridge NSString *)ABRecordCopyValue(thisPerson,
//																					kABPersonLastNameProperty);
//						if ([firstName length]) {
//							[currentPerson setObject:firstName forKey:@"first_name"];
//						}
//						if ([lastName length]) {
//							[currentPerson setObject:lastName forKey:@"last_name"];
//						}
//						if ([currentPerson count]) {
//							[users addObject:currentPerson];
//						}
//					}
//					CFRelease(allPeople);
//				}
//				CFRelease(addressBook);
//				detailFriends.socialType = @"AddressBook";
//				detailFriends.usersAB = [users copy];
//				[self.navigationController pushViewController:detailFriends animated:YES];
//			}
		}
	} else {
		if (buttonIndex == 1) {
			[TwitterSettings openTwitterAccounts];
		}
	}
}
- (void)showActionSheet
{
	UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Twitter"
														delegate:self 
											   cancelButtonTitle:nil
										  destructiveButtonTitle:nil 
											   otherButtonTitles:nil];
	for (ACAccount *account in twitterAccounts) {
		[action addButtonWithTitle:account.username];
	}
	NSInteger numberOfButtons = action.numberOfButtons;
	[action addButtonWithTitle:@"Cancel"];
	action.cancelButtonIndex = numberOfButtons;
	[action showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSInteger count = [twitterAccounts count];
	if (buttonIndex != count) {
		ACAccount *account = [twitterAccounts objectAtIndex:buttonIndex];
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
	}
}


@end
