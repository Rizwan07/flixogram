//
//  AuthorizationViewController.h
//  Flickogram

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"

@interface AuthorizationViewController : BaseViewController <FlickogramVideoDelegate>

@property (nonatomic, weak) IBOutlet UIToolbar *toolbar;
@property (nonatomic, weak) IBOutlet UIView *infoView;

- (IBAction)dismissInfoView:(id)sender;

@end
