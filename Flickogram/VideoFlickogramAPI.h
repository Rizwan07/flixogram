//
//  VideoFlickogramAPI.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "FlickogramProtocolsAPI.h"

@interface VideoFlickogramAPI : NSObject

@property (nonatomic, strong) id <FlickogramVideoDelegate> delegate;

- (void)addVideoOnService:(NSURL *) urlVideo
			   titleVideo:(NSString *)titleVideo 
				   userID:(NSString *)userID
		 descriptionVideo:(NSString *)descriptionVideo
				 tagVideo:(NSString *)tagVideo
				 latitude:(NSString *)latitude
				longitude:(NSString *)longitude
				 location:(NSString *)location
				videoType:(NSString *)videoType
			 withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getPopularVideosOnPage:(NSString *)page 
					  withType:(NSString *)type
				  withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getFollowingVideosForUserID:(NSString *)userID
							 onPage:(NSString *)page
					   withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getVideoDetailsOfVideoID:(NSString *)videoID
						  userID:(NSString *)userID
					  indexArray:(NSInteger)index
					withDelegate:(id<FlickogramVideoDelegate>)delegate;
						
- (void)likeVideoForUserID:(NSString *)userID 
				   videoID:(NSString *)videoID 
				indexArray:(NSInteger)currentIndex
			  withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)addCommentOnVideo:(NSString *)videoID 
				   userID:(NSString *)userID 
				  comment:(NSString *)comment 
			   indexArray:(NSInteger)currentIndex
			 withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)removeComment:(NSString *)commentID 
			   userID:(NSString *)userID 
			  videoID:(NSString *)videoID 
		   indexArray:(NSInteger)currentIndex
		 withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)removeVideoID:(NSString *)videoID 
			userID:(NSString *)userID 
		 indexArray:(NSInteger)currentIndex 
	   withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getExploreVideosOnPage:(NSString *)page
				  withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getSuggestedVideosForUserID:(NSString *)userID withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getUserLikedVideosForUserID:(NSString *)userID
							 onPage:(NSString *)page
					   withDelegate:(id<FlickogramVideoDelegate>)delegate;

- (void)reflickVideoForUserID:(NSString *)userID
					  videoID:(NSString *)videoID
					 delegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getMostLikedVideosForUserID:(NSString *)userID
							 onPage:(NSString *)videoID
						   delegate:(id<FlickogramVideoDelegate>)delegate;

- (void)getTrendingVideosOnPage:(NSString *)page
					   withType:(NSString *)type
					   delegate:(id<FlickogramVideoDelegate>)delegate;

- (void)reportVideo:(NSString *)videoID
		 reporterID:(NSString *)reporterID
	   reasonReport:(NSString *)reason
		   delegate:(id<FlickogramVideoDelegate>)delegate;
@end
