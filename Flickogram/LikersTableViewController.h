//
//  LikersTableViewController.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import "ManagerFlickogramAPI.h"

@interface LikersTableViewController : UITableViewController <FlickogramAuthorizationDelegate> {
	NSMutableArray *likers;
}

@property (nonatomic, strong) NSDictionary *videoLikes;
		   
@end
