//
//  FlickogramFrameSlider.m
//  Flickogram
//

#import "FlickogramFrameSlider.h"


@interface FlickogramFrameSlider ()
- (void)createUIImages:(CGFloat)frameTimeInSecond;

- (void)setMinThumbValue:(CGFloat)val;
- (void)setMaxThumbValue:(CGFloat)val;
- (NSUInteger)xForValue:(CGFloat)val;
- (CGFloat)valueForX:(NSUInteger)x;

@end

@implementation FlickogramFrameSlider
@synthesize minimumRange = _minimumRange;
@synthesize maximumValue = _maximumValue;
@synthesize minimumValue = _minimumValue;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		padding = 30;
		countUIImage = ((self.frame.size.width - padding * 2) / (self.frame.size.height / 2)) + 1;
		currentNumberUIImage = 0;
		currentImagePositionX = padding;
		sizeOfSquare = self.frame.size.height;
		
		minThumbOn = NO;
		maxThumbOn = NO;
		self.multipleTouchEnabled = YES;
		minThumb = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftHand"]];
		maxThumb = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rightHand"]];
		minThumb.frame = CGRectMake(padding, 0, 25, frame.size.height);
		maxThumb.frame = CGRectMake(padding, 0, 25, frame.size.height);
		
		self.minimumValue = 0;
		self.maximumValue = 1.0;
		[self setSelectedMinimumValue:0];
        [self setSelectedMaximumValue:1.0];
        
        self.backgroundColor = [UIColor redColor];
    }	
    return self;
}

#pragma mark - Create frames
- (void)createFrameSliderForVideoURL:(NSURL *)moviePath
{
	sourceAsset = [[AVURLAsset alloc] initWithURL:moviePath options:nil];
	NSArray *videoTracks = [sourceAsset tracksWithMediaType:AVMediaTypeVideo];
	BOOL shouldRecordVideoTrack = ([videoTracks count] > 0);
	if (shouldRecordVideoTrack)	{
		AVAssetTrack* videoTrack = [videoTracks objectAtIndex:0];
		CGFloat frameRate = videoTrack.nominalFrameRate;
		CGFloat durationVideo = CMTimeGetSeconds(sourceAsset.duration);
		CGFloat countFrame = frameRate * durationVideo;
		CGFloat numberFrameForDisplay = countFrame / countUIImage;
		CGFloat timeFrameForDisplay = (1 / frameRate) * numberFrameForDisplay;
		[self createUIImages:timeFrameForDisplay];
	} else {
		NSLog(@"No video");
		return;
	}
}

- (void)createUIImages:(CGFloat)frameTimeInSecond
{
	CGFloat timeForFrame = frameTimeInSecond;
	for (int i = 0; i < countUIImage ; i++) {
		[NSThread detachNewThreadSelector:@selector(loadImage:) toTarget:self withObject:[NSNumber numberWithFloat:timeForFrame]];
		timeForFrame += frameTimeInSecond;
	}
}

- (UIImage*)loadImage:(NSNumber *)displayFrame
{	
	CGFloat displayFrameForSeconds = [displayFrame floatValue];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:sourceAsset];
	generate.appliesPreferredTrackTransform = YES;
	generate.maximumSize = CGSizeMake(30, 30);
    NSError *err = NULL;
    CMTime time = CMTimeMakeWithSeconds(displayFrameForSeconds, 600);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
	[self performSelectorOnMainThread:@selector(displayImage:) withObject:[[UIImage alloc] initWithCGImage:imgRef] waitUntilDone:YES];
	return nil;
	
}

- (void)displayImage:(UIImage*)image
{
	currentNumberUIImage++;
	if (currentNumberUIImage == countUIImage) {
		[self addSubview:minThumb];
		[self addSubview:maxThumb];
		return;
	}
	UIImageView *newImageView = [[UIImageView alloc] initWithFrame:CGRectMake(currentImagePositionX,
																			  0, 
																			  sizeOfSquare,
																			  sizeOfSquare)];
	newImageView.image = image;
	[self addSubview:newImageView];
	currentImagePositionX += (sizeOfSquare / 2);
}

#pragma mark - Create Slider
- (void)setSelectedMinimumValue:(CGFloat)val
{
	[self setMinThumbValue:val];
}

- (CGFloat)selectedMinimumValue
{
	return [self valueForX:minThumb.center.x];
}

- (void)setSelectedMaximumValue:(CGFloat)val
{
	[self setMaxThumbValue:val];
}

- (CGFloat)selectedMaximumValue
{
	return [self valueForX:maxThumb.center.x];
}

#pragma mark - Logics

- (void)setMinThumbValue:(CGFloat)val
{
    
    //NSLog(@"min %f", (self.frame.size.width - padding * 2) * val / self.maximumValue + padding);
    
	CGPoint center = minThumb.center;
	center.x = [self xForValue:val];
	minThumb.center = center;
}

- (void)setMaxThumbValue:(CGFloat)val
{
    //NSLog(@"max %f", (self.frame.size.width - padding * 2) * val / self.maximumValue + padding);
	CGPoint center = maxThumb.center;
	center.x = [self xForValue:val];
	maxThumb.center = center;
}

- (NSUInteger)xForValue:(CGFloat)val
{
	return (self.frame.size.width - padding * 2) * val / self.maximumValue + padding;
}

- (CGFloat)valueForX:(NSUInteger)x
{
	return (x - padding) * self.maximumValue / (self.frame.size.width - padding * 2);
}


#pragma mark - Events

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	CGPoint touchPoint = [touch locationInView:self];
	
    int incRect = 44;
    CGRect frameMinThumb = CGRectMake(minThumb.frame.origin.x - incRect, minThumb.frame.origin.y - incRect, minThumb.frame.size.width + incRect, minThumb.frame.size.height + incRect);
    CGRect frameMaxThumb = CGRectMake(maxThumb.frame.origin.x - incRect, maxThumb.frame.origin.y - incRect, maxThumb.frame.size.width + incRect, maxThumb.frame.size.height + incRect);
    
	if (CGRectContainsPoint(frameMinThumb, touchPoint) && !minThumb.hidden)
		minThumbOn = YES;
	else if (CGRectContainsPoint(frameMaxThumb, touchPoint) && !maxThumb.hidden)
		maxThumbOn = YES;
	
	return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	if (!minThumbOn && !maxThumbOn)
		return YES;
	
	CGFloat selectedMaximumValue = [self valueForX:maxThumb.center.x];
	CGFloat selectedMinimumValue = [self valueForX:minThumb.center.x];
	CGPoint touchPoint = [touch locationInView:self];
	
	if (self.maximumValue <= self.minimumRange) return YES;
	
	if (minThumbOn)
	{
		NSUInteger leftBound = [self xForValue:self.minimumValue];
		NSUInteger rightBound = [self xForValue:(selectedMaximumValue - self.minimumRange)];
		NSUInteger point = touchPoint.x;
		if (point <= leftBound)
			point = leftBound;
		else if (point > rightBound)
			point = rightBound;
		minThumb.center = CGPointMake(point, minThumb.center.y);
    }
	else if (maxThumbOn)
	{
		NSUInteger leftBound = [self xForValue:(selectedMinimumValue + self.minimumRange)];
		NSUInteger rightBound = [self xForValue:self.maximumValue];
		NSUInteger point = touchPoint.x;
		
		if (point <= leftBound)
			point = leftBound;
		else if (point > rightBound)
			point = rightBound;
		
		maxThumb.center = CGPointMake(point, maxThumb.center.y);
	}
    
	[self sendActionsForControlEvents:UIControlEventValueChanged];
	
	[self setNeedsDisplay];
	return YES;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	minThumbOn = NO;
	maxThumbOn = NO;
}

- (void)cancelTrackingWithEvent:(UIEvent *)event
{
	minThumbOn = NO;
	maxThumbOn = NO;
}

@end
