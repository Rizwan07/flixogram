//
//  FoursquareViewController.m
//  Flickogram
//

#import "FoursquareViewController.h"

@interface FoursquareViewController ()
- (void)getVenuesNearLLCoords:(NSString *)llCoords;
- (void)startSearch;
@end

@implementation FoursquareViewController
@synthesize locationManager = _locationManager;
@synthesize table = _table;
@synthesize venues = _venues;
@synthesize delegate = _delegate;
@synthesize searchBar = _searchBar;
@synthesize controlBackground = _controlBackground;
@synthesize additionalTable = _additionalTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Locations";
    
	//set image navigationBar
	self.navigationItem.title = @"LOCATIONS";
	
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(backToShare) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	self.searchBar.barStyle = UIBarStyleDefault;
	self.searchBar.placeholder = @"Find or create a location";
	self.searchBar.tintColor = [UIColor lightGrayColor];
	self.searchBar.showsCancelButton = NO;
	self.searchBar.delegate = self;
	
	self.venues = [[NSMutableArray alloc] init];
	[self startSearch];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)backToShare
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)startSearch
{
	self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	self.locationManager.distanceFilter = kCLDistanceFilterNone;
	[self.locationManager startUpdatingLocation];
}

- (IBAction)returnNormState:(id)sender
{
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	[self.searchBar resignFirstResponder];
	self.controlBackground.hidden = YES;
	[self.searchBar setShowsCancelButton:NO animated:YES];
	self.table.scrollEnabled = YES;
	self.searchBar.text = @"";
}

#pragma mark - Table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.table) {
		return [self.venues count] + 2;
	}
	return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *CellIdentifier = @"CellFoursquare";
	UITableViewCell	*cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
	
	if (tableView == self.table) {
		if (indexPath.row == ([tableView numberOfRowsInSection:0] - 1)) {
			UIImageView *imageFoursquare = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poweredByFoursquare_gray"]];
			imageFoursquare.frame = CGRectMake(40, -10, 241, 60);
			[cell addSubview:imageFoursquare];
			imageFoursquare = nil;
		} else if (indexPath.row == 0) {
			[cell addSubview:self.searchBar];
		}
		else {
			NSDictionary *currentVenue = [NSDictionary dictionaryWithDictionary:[self.venues objectAtIndex:indexPath.row-1]];
			if ([currentVenue objectForKey:@"name"]) {
				cell.textLabel.text = [currentVenue objectForKey:@"name"];
			}
			if ([currentVenue objectForKey:@"address"]) {
				cell.detailTextLabel.text = [currentVenue objectForKey:@"address"];
			}
		}
	} else if (tableView == self.additionalTable) {
		if (indexPath.row == 0) {
			cell.textLabel.text = [NSString stringWithFormat:@"Create \"%@\"", self.searchBar.text];
			cell.detailTextLabel.text = @"Create a custom location";
		} else {
			cell.textLabel.text = [NSString stringWithFormat:@"Find \"%@\"", self.searchBar.text];
			cell.textLabel.textColor = [UIColor blueColor];
			cell.detailTextLabel.text = @"Search more places nearby";
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (tableView == self.table) {
		if (indexPath.row != ([tableView numberOfRowsInSection:0] - 1) && (indexPath.row != 0)) {
			[self.delegate venueSelected:[self.venues objectAtIndex:indexPath.row-1]];
			[self.navigationController popViewControllerAnimated:YES];
		}
	} else if (tableView == self.additionalTable) {
		if (indexPath.row == 0) {
			NSMutableDictionary *newLocation = [[NSMutableDictionary alloc] init];
			if (latitude && longitude) {
				[newLocation setObject:latitude forKey:@"lat"];
				[newLocation setObject:longitude forKey:@"lng"];
			}
			[newLocation setObject:self.searchBar.text forKey:@"name"];
			[self.delegate venueSelected:newLocation];
			[self.navigationController setNavigationBarHidden:NO];
			[self.navigationController popViewControllerAnimated:YES];
		} else {
			nearby = [[NearbyVenuesViewController alloc] init];
			nearby.delegate = self;
			NSString *llCoords = [NSString stringWithFormat:@"%@,%@", latitude, longitude];
			nearby.location = llCoords;
			nearby.nameForSearch = [self.searchBar.text copy];
			[self.navigationController pushViewController:nearby animated:YES];
		}
	}
}

#pragma mark - UISearchBar delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	self.controlBackground.hidden = NO;
	[self.searchBar setShowsCancelButton:YES animated:YES];
	self.table.scrollEnabled = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	[self.additionalTable reloadData];
	if ([searchBar.text length]) {
		self.additionalTable.hidden = NO;
	} else {
		self.additionalTable.hidden = YES;
	}
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[self returnNormState:nil];
	self.additionalTable.hidden = YES;
	[self.additionalTable reloadData];
}

#pragma mark - API Foursquare
- (void)getVenuesNearLLCoords:(NSString *)llCoords 
{
	NSDate *today = [NSDate date];
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat: @"YYYYMMdd"];
	NSString * dateString = [dateFormat stringFromDate:today];
	[[ManagerFlickogramAPI foursquareExemplar] searchVenuesNearLatitude:llCoords 
																  limit:@"50" 
																 radius:@"5000" 
															 clientDate:dateString
																  query:@"" 
															   delegate:self];
}

#pragma mark - NearbyVenuesDelegate
- (void)venueSelected:(NSMutableDictionary *)venue
{
	[self.delegate venueSelected:venue];
	[nearby.navigationController popViewControllerAnimated:NO];
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CoreLocation delegate
-(void)locationManager:(CLLocationManager *)manager 
   didUpdateToLocation:(CLLocation *)newLocation
		  fromLocation:(CLLocation *)oldLocation
{
	latitude = [[NSString alloc] initWithFormat:@"%.6f", newLocation.coordinate.latitude];
	longitude = [[NSString alloc] initWithFormat:@"%.6f", newLocation.coordinate.longitude];
	NSString *llCoords = [NSString stringWithFormat:@"%@,%@", latitude, longitude];
	
	//Location seems pretty accurate, let's use it!
	NSLog(@"latitude %+.6f, longitude %+.6f\n", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
			
	//Optional: turn off location services once we've gotten a good location
	[manager stopUpdatingLocation];

	[self getVenuesNearLLCoords:llCoords];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	if(error.code == kCLErrorDenied) {
        [manager stopUpdatingLocation];
    } else if (error.code == kCLErrorLocationUnknown) {
        [manager startUpdatingLocation];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error retrieving location"
														message:[error description]
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Foursquare delegate 
- (void)searchCompleteSuccess:(NSDictionary *)data
{
	for (NSDictionary *venue in data) {
		NSMutableDictionary *newVenue = [[NSMutableDictionary alloc] init];
		//add name venue
		if ([venue objectForKey:@"name"]) { 
			[newVenue setObject:[venue objectForKey:@"name"] forKey:@"name"];
		}
		
		//add address
		if ([[venue objectForKey:@"location"] objectForKey:@"address"]) {
			[newVenue setObject:[[venue objectForKey:@"location"] objectForKey:@"address"] forKey:@"address"];
		}
		//add lat
		if ([[venue objectForKey:@"location"] objectForKey:@"lat"]) {
			[newVenue setObject:[[venue objectForKey:@"location"] objectForKey:@"lat"] forKey:@"lat"];
		}
		//add lng
		if ([[venue objectForKey:@"location"] objectForKey:@"lng"]) {
			[newVenue setObject:[[venue objectForKey:@"location"] objectForKey:@"lng"] forKey:@"lng"];
		}
		
		[self.venues addObject:newVenue];
	}
	[self.table reloadData];
}

- (void)searchFailedWithError:(NSString *)error
{
	NSLog(@"%@", error);
}

@end
