//
//  DropDownMenuViewController.m
//  Flickogram
//

#import "DropDownMenuViewController.h"

@interface DropDownMenuViewController ()

@end

@implementation DropDownMenuViewController
@synthesize username = _username;
@synthesize	userThumb = _userThumb;
@synthesize bottomButton = _bottomButton;

@synthesize mostLiked = _mostLiked;
@synthesize popular = _popular;
@synthesize liked = _liked;
@synthesize trending = _trending;
@synthesize about = _about;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[_scrollView setContentSize:CGSizeMake(320, 305)];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
