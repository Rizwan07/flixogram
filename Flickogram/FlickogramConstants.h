//
//  FlickogramConstants.h
//  Flickogram
//


#import <Foundation/Foundation.h>

@interface FlickogramConstants : NSObject

extern NSString * const NameRecordedVideoFile;
extern NSString * const NameFilteredVideoFile;
extern NSString * const NameMotionVideoFile;
extern NSString * const NameVideoWithMusic;

//notifications
extern NSString * const NameNotificationStartFilteringVideo;
extern NSString * const NameNotificationFinishFilteringVideo;
extern NSString * const NameNotificationChangeMoviePath;
extern NSString * const NameNotificationCompletedFilteringFrame;
extern NSString * const NameNotificationUploadVideoOnServer;
extern NSString * const NameNotificationGetFollowingVideos;

extern NSString * const NameNotificationMenu;

@end
