//
//  DetailFriendsViewController.m
//  Flickogram

#import "DetailFriendsViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SingletonAuthorizationUser.h"
#import "TwitterSettings.h"
#import <Accounts/Accounts.h>
#import "LikersTableViewCell.h"

@interface DetailFriendsViewController ()
- (void)openSession;
- (void)getFriendsFacebook;
- (void)parseFaceebokFriends:(NSDictionary *)data;
- (void)parseTwitterFriends:(NSDictionary *)data;
- (void)parseAddressBookFriends;
@end

@implementation DetailFriendsViewController
@synthesize table = _table;
@synthesize socialType = _socialType;
@synthesize usersAB = _usersAB;
@synthesize searchBar = _searchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Detail Friends";
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	friendsArray = [[NSMutableArray alloc] init];
	isNoUsersFound = NO;
	
	if ([self.socialType isEqualToString:@"Facebook"]) {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		if (FBSession.activeSession.state == 513) {
			[self getFriendsFacebook];
		} else {
			[self openSession];
		}
	} else if ([self.socialType isEqualToString:@"Twitter"]) {
		[self getFriendsTwitter];
	} else if ([self.socialType isEqualToString:@"AddressBook"] && [self.usersAB count]) {
		[self parseAddressBookFriends];
	} else if ([self.socialType isEqualToString:@"FlixogramSearch"]) {
		currentPage = 1;
		self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
		self.searchBar.barStyle = UIBarStyleDefault;
		self.searchBar.placeholder = @"Search names and usernames";
		self.searchBar.tintColor = [UIColor lightGrayColor];
		self.searchBar.showsCancelButton = NO;
		self.searchBar.delegate = self;
		[self.view addSubview:self.searchBar];
		self.table.frame = CGRectMake(self.table.frame.origin.x, 
									  self.table.frame.origin.y + self.searchBar.frame.size.height,
									  self.table.frame.size.width,
									  self.table.frame.size.height - self.searchBar.frame.size.height);
	}
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (isNoUsersFound) {
		return 1;
	}
	return [friendsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"DetailFriendCell";
	LikersTableViewCell *cell = (LikersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LikersTableViewCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (LikersTableViewCell *) currentObject;
				[cell setPlaceholder];
			}
			break;
		}
	}
	if (isNoUsersFound) {
		cell.textLabel.text = @"No user found.";
		cell.accessoryType = UITableViewCellAccessoryNone;
	} else {
		NSDictionary *data = [friendsArray objectAtIndex:indexPath.row];
		cell.username.text = [data objectForKey:@"user_name"];
		cell.profileName.text = [NSString stringWithFormat:@"%@ %@", [data objectForKey:@"firstname"], [data objectForKey:@"lastname"]];
		[cell setPhotoUser:[data objectForKey:@"profile_picture"]];
		
		cell.followToggle.hidden = NO;
		cell.followToggle.tag = indexPath.row;
		[cell.followToggle addTarget:self action:@selector(toggleStateFollowing:) forControlEvents:UIControlEventTouchUpInside];
		cell.accessoryType = UITableViewCellAccessoryNone;
		
		BOOL isFollowing = [[data objectForKey:@"is_following"] boolValue];
		if (isFollowing) {
			cell.followToggle.selected = YES;
		} else {
			cell.followToggle.selected = NO;
		}
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UISearchBar delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
	[self.searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[self.searchBar setShowsCancelButton:NO];
	self.searchBar.text = @"";
	[self.searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	if ([self.searchBar.text length]) {		
		[friendsArray removeAllObjects];
		[self.table reloadData];
		NSString *searchString = self.searchBar.text;
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		currentPage = 1;
		NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
		[[ManagerFlickogramAPI searchExemplar] searchUsersByTags:searchString userID:userID onPage:pageInStringFormat withDelegate:self];
	}
}

#pragma mark - Following
- (void)toggleStateFollowing:(id)sender
{
	UIButton *followButton = (UIButton *)sender;
	NSInteger currentIndex = followButton.tag;
	NSString *userID = [[friendsArray objectAtIndex:currentIndex] objectForKey:@"id"];
	NSString *followerID = [[SingletonAuthorizationUser authorizationUser] userID];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI authorizationExemplar] followUnFollowUserID:userID followerID:followerID indexArray:currentIndex withDelegate:self];
}

- (void)followUnFollowUserSuccesWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSMutableDictionary *currentUser = [friendsArray objectAtIndex:currentIndex];
	BOOL isFollowing = [[currentUser objectForKey:@"is_following"] boolValue];
	isFollowing = !isFollowing;
	NSString *isFollowingString = [NSString stringWithFormat:@"%d", isFollowing];
	[currentUser setObject:isFollowingString forKey:@"is_following"];
	[friendsArray replaceObjectAtIndex:currentIndex withObject:currentUser];
	[self.table reloadData];
}

- (void)followUnFollowUserFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSLog(@"%@", error);
}

#pragma mark - Facebook friends search
//Open session for facebook
- (void)openSession
{
	[FBSession openActiveSessionWithPermissions:nil
                                   allowLoginUI:YES
                              completionHandler:
	 ^(FBSession *session, 
	   FBSessionState state, NSError *error) {
		 if (error) {
			 NSLog(@"openError = %@", error);
			 UIAlertView *alertView = [[UIAlertView alloc]
									   initWithTitle:@"Error"
									   message:error.localizedDescription
									   delegate:nil
									   cancelButtonTitle:@"OK"
									   otherButtonTitles:nil];
			 [alertView show];
			 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		 } else {
			 [self getFriendsFacebook];
		 }    
	 }];  
}

//facebook getFriends IDs
- (void)getFriendsFacebook
{
	[FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection,
																  id result,
																  NSError *error) {
		NSString *alertText;
		if (error) {
			alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d", error.domain, error.code];
			// Show the result in an alert
			[[[UIAlertView alloc] initWithTitle:@"Result" 
										message:alertText
									   delegate:nil
							  cancelButtonTitle:@"OK!"
							  otherButtonTitles:nil] show];	
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		} else {
			[self parseFaceebokFriends:[result objectForKey:@"data"]];
		}
	}];	
}

- (void)parseFaceebokFriends:(NSDictionary *)data 
{
	NSMutableArray *friends = [[NSMutableArray alloc] init];
	for (NSDictionary *currentFriend in data) {
		NSString *userID = [currentFriend objectForKey:@"id"];
		[friends addObject:userID];
	}
	if ([friends count]) {
		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
		[[ManagerFlickogramAPI searchExemplar] findFriendsForUser:userID typeSocialNetwork:@"facebook" friendIDs:friends withDelegate:self];
	} else {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
}

#pragma mark - Twitter search
//twitter get friends
- (void)getFriendsTwitter
{
	//First, we need to obtain the account instance for the user's Twitter account
	
    ACAccountStore *store = [[ACAccountStore alloc] init];
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if ([defaults objectForKey:@"TwitterAccountIdentifier"]) {
		NSString *twitterIdentifier = [defaults objectForKey:@"TwitterAccountIdentifier"];
		ACAccount *twitterAccount = [store accountWithIdentifier:twitterIdentifier];
		
		// Now make an authenticated request to our endpoint
		NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
		NSString *username = [NSString stringWithString:twitterAccount.username];
		[params setObject:username forKey:@"screen_name"];
		
		//  The endpoint that we wish to call
		NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/friends/ids.json"];

		//  Build the request with our parameter 
		//TWRequest *request = [[TWRequest alloc] initWithURL:url parameters:params requestMethod:TWRequestMethodGET];
        SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:url parameters:params];
		
		// Attach the account object to this request
		[request setAccount:twitterAccount];
		[request performRequestWithHandler: ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
			if (!responseData) {
				// inspect the contents of error 
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
			} 
			else {
				NSError *jsonError;
				NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseData 
																	options:NSJSONReadingMutableLeaves 
																	  error:&jsonError];            
				if (result) {                          
					// at this point, we have an object that we can parse
					[self parseTwitterFriends:[result objectForKey:@"ids"]];
				} 
				else { 
					// inspect the contents of jsonError
					[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
				}
			}
		}];
	}
}

- (void)parseTwitterFriends:(NSDictionary *)data
{
	NSMutableArray *friends = [[NSMutableArray alloc] init];
	for (NSDictionary *currentFriend in data) {
		[friends addObject:currentFriend];
	} 
	if ([friends count]) {
		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
		[[ManagerFlickogramAPI searchExemplar] findFriendsForUser:userID typeSocialNetwork:@"facebook" friendIDs:friends withDelegate:self];
	} else {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
}

#pragma mark - Address Book Friends
- (void)parseAddressBookFriends
{
	for (NSDictionary *currentFriend in self.usersAB) {
		NSString *firstName = @"";
		NSString *lastName = @"";
		if ([currentFriend objectForKey:@"first_name"]) {
			firstName = [currentFriend objectForKey:@"first_name"];
		}
		if ([currentFriend objectForKey:@"last_name"]) {
			lastName = [currentFriend objectForKey:@"last_name"];
		}
		if ([firstName length] || [lastName length]) {
			NSString *searchRequest = [NSString stringWithFormat:@"%@ %@",firstName, lastName];
			NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			[[ManagerFlickogramAPI searchExemplar] searchUsersByTags:searchRequest userID:userID onPage:@"1" withDelegate:self];
		}
	}

}

#pragma mark - Find friends Delegate 
- (void)findFriendsSuccessWithRecievedData:(NSDictionary *)data
{
	if ([data objectForKey:@"friends"]) {
		NSDictionary *friends = [data objectForKey:@"friends"];
		NSInteger currentFriend = 0;
		BOOL isMoreFriends = YES;
		while (isMoreFriends) {
			NSString *currentFriendStrFormat = [NSString stringWithFormat:@"%d",currentFriend];
			if ([friends objectForKey:currentFriendStrFormat]) {
				NSDictionary *friend = [friends objectForKey:currentFriendStrFormat];
				[friendsArray addObject:friend];
				currentFriend++;
			} else {
				isMoreFriends = NO;
			}
		}
		[self.table reloadData];
	}
	if ([data objectForKey:@"error_message"]) {
		NSString *error = [data objectForKey:@"error_message"];
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:error delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
		[alert show];
	}
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)findFriendsFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


#pragma mark - Search Users By Tags Delegate
- (void)searchUsersByTagsSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self.searchBar resignFirstResponder];
	if ([data objectForKey:@"users"]) {
		NSDictionary *users = [data objectForKey:@"users"];
		NSInteger currentUser = 0;
		BOOL isMoreUsers = YES;
		while (isMoreUsers) {
			NSString *currentUserStrFormat = [NSString stringWithFormat:@"%d",currentUser];
			if ([users objectForKey:currentUserStrFormat]) {
				NSDictionary *user = [users objectForKey:currentUserStrFormat];
				[friendsArray addObject:user];
				isNoUsersFound = NO;
				currentUser++;
			} else {
				isMoreUsers = NO;
			}
		}
		[self.table reloadData];
	}
	BOOL isShowMore = [[data objectForKey:@"show_more"] boolValue];
	if (isShowMore) {
		currentPage++;
		NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
		NSString *searchString = self.searchBar.text;
		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[[ManagerFlickogramAPI searchExemplar] searchUsersByTags:searchString userID:userID onPage:pageInStringFormat withDelegate:self];
	}
	[self.table reloadData];

}

- (void)searchUsersByTagsFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	if ([error isEqualToString:@"No users found."]) {
		if (![friendsArray count]) {
			isNoUsersFound = YES;
		} else {
			isNoUsersFound = NO;
		}
	}
	[self.table reloadData];
}


@end
