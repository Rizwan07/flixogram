//
//  ManagerFlickogramAPI.h
//  Flickogram
//


#import <Foundation/Foundation.h>
#import "AuthorizationFlickogramAPI.h"
#import "VideoFlickogramAPI.h"
#import "FoursquareAPI.h"
#import "NotificationsFlickogramAPI.h"
#import "SearchFlickogramAPI.h"

@interface ManagerFlickogramAPI : NSObject

+ (AuthorizationFlickogramAPI *)authorizationExemplar; 
+ (VideoFlickogramAPI *)videoExemplar;
+ (FoursquareAPI *)foursquareExemplar;
+ (NotificationsFlickogramAPI *)notificationsExemplar;
+ (SearchFlickogramAPI *)searchExemplar;

@end
