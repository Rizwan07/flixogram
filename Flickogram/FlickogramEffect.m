//
//  FlickogramEffect.m
//  Flickogram
//
//  Created by Andrey Kudievskiy on 23.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FlickogramEffect.h"

@implementation FlickogramEffect

@synthesize nameEffect = _nameEffect;

- (id)init
{
    self = [super init];
    if (self) {
		self.nameEffect = [[NSString alloc] init];
    }
    return self;
}

@end
