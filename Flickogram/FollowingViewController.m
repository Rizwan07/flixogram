//
//  FollowingViewController.m
//  Flickogram
//

#import "FollowingViewController.h"
#import "NewsTableViewCell.h"
#import "SingletonAuthorizationUser.h"
#import "LORichTextLabel.h"
#import "UIView+Layout.h"
#import "ProfileViewController.h"
#import "VideoDetailViewController.h"

#import "CustomNavController.h"

@interface FollowingViewController ()
- (void)getNewsNotification;
- (void)getFollowingsNotification;
- (void)reloadNotifications;
@end

@implementation FollowingViewController
@synthesize table = _table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		UIImage *selectedImage = [UIImage imageNamed:@"tab4-active"];
		UIImage *unselectedImage = [UIImage imageNamed:@"tab4-inactive"];
		UITabBarItem *tabBarItem = [[UITabBarItem alloc] init];
		[tabBarItem setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
		self.tabBarItem = tabBarItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.screenName = @"Following";
    self.title = @"Following";
    
//    UIImage *backgroundNavigationBarImage = [UIImage imageNamed:@"Top-Bar-Red"];
//	[self.navigationController.navigationBar setBackgroundImage:backgroundNavigationBarImage forBarMetrics:UIBarMetricsDefault];
	
	//set Refresh button
	UIImage *refreshButtonImage = [UIImage imageNamed:@"icon-referesh"];
	UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[refreshButton setBackgroundImage:refreshButtonImage forState:UIControlStateNormal];
	refreshButton.frame = CGRectMake(0.0, 0.0, 25, 25);
	[refreshButton addTarget:self action:@selector(reloadNotifications) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *refreshItem = [[UIBarButtonItem alloc] initWithCustomView:refreshButton];
	self.navigationItem.rightBarButtonItem = refreshItem;
	
	//NSArray *itemArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"following-active"], [UIImage imageNamed:@"news-inactive"], nil];
//    NSArray *itemArray = [NSArray arrayWithObjects:@"Following", @"News", nil];
//	segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
//	segmentedControl.frame = CGRectMake(75, 0, 170, 30);
//	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
////    [segmentedControl setBackgroundColor:[UIColor clearColor]];
////    [segmentedControl setTintColor:[UIColor clearColor]];
//	[segmentedControl setWidth:85.0 forSegmentAtIndex:0];
//	[segmentedControl setWidth:85.0 forSegmentAtIndex:1];
//	[segmentedControl addTarget:self action:@selector(changeSegment) forControlEvents:UIControlEventValueChanged];
//	segmentedControl.selectedSegmentIndex = 0;
//    
//	self.navigationItem.titleView = segmentedControl;
    
    
    NSLog(@"frame %@", NSStringFromCGRect(segmentedControl.frame));
    
	self.table.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Seperator"]];

	newsDataArray = [[NSMutableArray alloc] init];
	followingsDataArray = [[NSMutableArray alloc] init];
	
}

- (void)viewDidAppear:(BOOL)animated
{
	[segmentedControl  setSelectedSegmentIndex:0];
	[self changeSegment];
	[newsDataArray removeAllObjects];
	[followingsDataArray removeAllObjects];
	[self.table reloadData];
	currentPageNewsNotification = 1;
	currentPageFollowingsNotification = 1;
	isShowMoreNews = NO;
	isShowMoreFollowings = NO;
	[self getNewsNotification];
	[self getFollowingsNotification];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)changeSegment 
{
//	NSInteger currentSegment = segmentedControl.selectedSegmentIndex;
//	UIImage *firstImage;
//	UIImage *secondImage;
//	switch (currentSegment) {
//		case 0: {
//			firstImage = [UIImage imageNamed:@"following-active"];
//			secondImage = [UIImage imageNamed:@"news-inactive"];
//		}
//		break;
//		case 1: {
//			firstImage = [UIImage imageNamed:@"following-inactive"];
//			secondImage = [UIImage imageNamed:@"news-active"];
//		}
//		break;
//	}
//	[segmentedControl setImage:firstImage forSegmentAtIndex:0];
//	[segmentedControl setImage:secondImage forSegmentAtIndex:1];
	[self.table reloadData];
}

#pragma mark - Get notifications
- (void)getNewsNotification
{
    return;
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *numberPageInStringFormat = [NSString stringWithFormat:@"%d", currentPageNewsNotification];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI notificationsExemplar] notificationNewsSectionForUserID:userID onPage:numberPageInStringFormat withDelegate:self];

}
- (void)getFollowingsNotification 
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *numberPageInStringFormat = [NSString stringWithFormat:@"%d", currentPageFollowingsNotification];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI notificationsExemplar] notificationFollowingSectionForUserID:userID onPage:numberPageInStringFormat withDelegate:self];
}

- (void)reloadNotifications
{
	currentPageNewsNotification = 1;
	currentPageFollowingsNotification = 1;
	[followingsDataArray removeAllObjects];
	[newsDataArray removeAllObjects];
	[self.table reloadData];
	[self getNewsNotification];
	[self getFollowingsNotification];
}

#pragma mark - Notification Following Delegate
- (void)notificationFollowingSectionSuccessWithRecievedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	if ([data objectForKey:@"show_more"]) {
		isShowMoreFollowings = [[data objectForKey:@"show_more"] boolValue];
	}
	
	for (NSDictionary *currentFollowingsEvent in [data objectForKey:@"data"]) {
		[followingsDataArray addObject:currentFollowingsEvent];
	}
	[self.table reloadData];

}

- (void)notificationFollowingSectionFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - Notification News Delegate
-(void)notificationNewsSectionSuccessWithRecievedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	if ([data objectForKey:@"show_more"]) {
		isShowMoreNews = [[data objectForKey:@"show_more"] boolValue];
	}
	
	for (NSDictionary *currentNewsEvent in [data objectForKey:@"data"]) {
		[newsDataArray addObject:currentNewsEvent];
	}
	[self.table reloadData];
}

- (void)notificationNewsSectionFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - Table delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}	

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		return [followingsDataArray count];
	} else {
		return [newsDataArray count];
	}
	return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		NSDictionary *currentDictionary = [followingsDataArray objectAtIndex:indexPath.row];
		if ([[currentDictionary objectForKey:@"action"] isEqualToString:@"follow"]) {
			return 44;
		}
	}
	return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"NotificationCell";
	NewsTableViewCell *cell = (NewsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NewsTableViewCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (NewsTableViewCell *) currentObject;
			}
			break;
		}
	}
	UIFont *highlightFont = [UIFont systemFontOfSize:13];
	LORichTextLabelStyle *atStyle = [LORichTextLabelStyle styleWithFont:highlightFont color:[UIColor redColor]];
	[atStyle addTarget:self action:@selector(atSelected:)];
	
	LORichTextLabel *messageLabel = [[LORichTextLabel alloc] initWithWidth:212];
	[messageLabel setFont:[UIFont systemFontOfSize:13]];
	[messageLabel setTextColor:[UIColor blackColor]];
	[messageLabel setBackgroundColor:[UIColor clearColor]];
	[messageLabel positionAtX:100.0 andY:4.0];
	[messageLabel addStyle:atStyle forPrefix:@"@"];
	[messageLabel setTag:indexPath.row];
	
	cell.photoUser.tag = indexPath.row;
	[cell.photoUser addTarget:self action:@selector(showMoreInfo:) forControlEvents:UIControlEventTouchUpInside];
	
	if (segmentedControl.selectedSegmentIndex == 0) {
		NSDictionary *followingsEvent = [followingsDataArray objectAtIndex:indexPath.row];
		NSString *message = [followingsEvent objectForKey:@"message"];
		NSString *timeago = [followingsEvent objectForKey:@"timeago"];
		if ([[followingsEvent objectForKey:@"action"] isEqualToString:@"video_like"]) {
			if (![[followingsEvent objectForKey:@"video_thumbnail"] isEqual:[NSNull null]]) {
				[cell setPhoto:[followingsEvent objectForKey:@"video_thumbnail"]];
			}
			[messageLabel setText:message];
			cell.date.text = timeago;
			CGRect dateFrame = cell.date.frame;
			cell.date.frame = CGRectMake(dateFrame.origin.x,
										 messageLabel.frame.origin.y + messageLabel.frame.size.height + 15,
										 dateFrame.size.width,
										 dateFrame.size.height);
		} else {
			cell.photoUser.hidden = YES;
			[messageLabel positionAtX:7 andY:2 withWidth:312];
			[messageLabel setText:message];

			cell.date.frame = CGRectMake(7, 18, 312, 23);
			cell.date.text = timeago;
		}
	} else {
		NSDictionary *newsEvent = [newsDataArray objectAtIndex:indexPath.row];
		NSString *message = [newsEvent objectForKey:@"message"];
		NSString *timeago = [newsEvent objectForKey:@"timeago"];
		
		NSString *action = [newsEvent objectForKey:@"action"];
		if ([action isEqualToString:@"video_like"]) {
			NSString *videoThumb = [newsEvent objectForKey:@"video_thumbnail"];
			[cell setPhoto:videoThumb];
		} else {
			NSString *userPicture = [newsEvent objectForKey:@"user_profile_picture"];
			[cell setPhoto:userPicture];
		}
		
		//trim last whitespace
		message = [message stringByTrimmingCharactersInSet:
								   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
		[messageLabel setText:message];
		
		cell.date.text = timeago;
		CGRect dateFrame = cell.date.frame;
		cell.date.frame = CGRectMake(dateFrame.origin.x,
									 messageLabel.frame.origin.y + messageLabel.frame.size.height + 15,
									 dateFrame.size.width,
									 dateFrame.size.height);
	}
	[cell.contentView addSubview:messageLabel];
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		if (indexPath.row == [followingsDataArray count]) {
			if (isShowMoreFollowings) {
				currentPageFollowingsNotification++;
				isShowMoreFollowings = NO;
				[self getFollowingsNotification];
			}
		}
	} else {
		if (indexPath.row == [newsDataArray count]) {
			if (isShowMoreNews) {
				currentPageNewsNotification++;
				isShowMoreNews = NO;
				[self getNewsNotification];
			}
		}
	}
}

#pragma mark Handler Methods

- (NSString *)tagFromSender:(id)sender
{
	return ((UIButton *)sender).titleLabel.text;
}

- (void)atSelected:(id)sender
{
	NSString *selectedUser = [self tagFromSender:sender]; //selected username
	NSArray *username = [selectedUser componentsSeparatedByString:@"'"]; //separate string
	selectedUser = [username objectAtIndex:0]; //use string without 's element
	NSInteger selectedCell = ((UIButton *)sender).tag; //number cell for selected username
	NSDictionary *notificationDetail; //users id's
	
	if (segmentedControl.selectedSegmentIndex == 0) {
		notificationDetail = [[followingsDataArray objectAtIndex:selectedCell] objectForKey:@"notification_detail"]; //following array
	} else {
		notificationDetail = [[newsDataArray objectAtIndex:selectedCell] objectForKey:@"notification_detail"]; //news array
	}

	NSDictionary *userInfo = [notificationDetail objectForKey:selectedUser]; //get user id by username
	NSString *userID;
	NSString *objectType = [userInfo objectForKey:@"type"];
	if ([objectType isEqualToString:@"user"]) {
		userID = [userInfo objectForKey:@"id"];
	} else {
		userID = [userInfo objectForKey:@"user_id"];
	}
	
	ProfileViewController *profile = [[ProfileViewController alloc] init];
	profile.selectedUserID = userID;
	profile.hasParentNavigationView = YES; //set back button
	[self.navigationController pushViewController:profile animated:YES];
}

- (void)showMoreInfo:(id)sender
{
	UIButton *videoThumb = (UIButton *)sender;
	NSInteger currentCell = videoThumb.tag;
	NSDictionary *notification;
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	if (segmentedControl.selectedSegmentIndex == 0) {
		notification = [followingsDataArray objectAtIndex:currentCell];

	} else {
		notification = [newsDataArray objectAtIndex:currentCell];

	}
	
	NSString *action = [notification objectForKey:@"action"];
	if ([action isEqualToString:@"video_like"]) {
		NSString *videoID = [notification objectForKey:@"object_id"];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[[ManagerFlickogramAPI videoExemplar] getVideoDetailsOfVideoID:videoID
																userID:userID
															indexArray:0
														  withDelegate:self];
	} else {
		NSString *userID = [notification objectForKey:@"user_id"];
		ProfileViewController *profile = [[ProfileViewController alloc] init];
		profile.selectedUserID = userID;
		profile.hasParentNavigationView = YES; //set back button
		[self.navigationController pushViewController:profile animated:YES];
	}
}

#pragma mark - Video Detail Delegate
- (void)getVideoDetailsSuccesWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)index
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	VideoDetailViewController *videoDetail = [[VideoDetailViewController alloc] init];
	videoDetail.videoDetail = [[data objectForKey:@"video_data"] mutableCopy];
	[self.navigationController pushViewController:videoDetail animated:YES];
}

- (void)getVideoDetailsFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:error
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
}

@end
