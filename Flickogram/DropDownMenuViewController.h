//
//  DropDownMenuViewController.h
//  Flickogram

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface DropDownMenuViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet EGOImageView *userThumb;
@property (nonatomic, weak) IBOutlet UILabel *username;
@property (nonatomic, weak) IBOutlet UIButton *bottomButton;

@property (nonatomic, weak) IBOutlet UIButton *liked;
@property (nonatomic, weak) IBOutlet UIButton *mostLiked;
@property (nonatomic, weak) IBOutlet UIButton *popular;
@property (nonatomic, weak) IBOutlet UIButton *trending;
@property (nonatomic, weak) IBOutlet UIButton *about;
@property (nonatomic, weak) IBOutlet UIButton *setting;
@property (strong, nonatomic) IBOutlet UIButton *logout;

@end
