//
//  ViewController.h
//  Flickogram


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ScrollingPanelEffects.h"

@class FlickogramCaptureManager, FlickogramPreviewView, AVCaptureVideoPreviewLayer;

@interface RecordViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
	NSUInteger numberSecondsRecorded;
	BOOL isFlashMode;
	CGFloat duration;
	NSTimer *playingTimer;
	BOOL isRecordingMade; 
	BOOL isNowRecord;
	NSURL *pathMovie;
    
    BOOL canUpload;
}

@property (nonatomic, strong) FlickogramCaptureManager *captureManager; 
@property (nonatomic, strong) IBOutlet UIView *videoPreviewView;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *recordButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *cameraToggleButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *flachToggleButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *closeRecordViewButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *topBarBackground;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *showGalleryButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *showColorFilterButton;
@property (nonatomic, unsafe_unretained) IBOutlet UIView *recLabel;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *timerLabel;
@property (nonatomic, strong) NSTimer *recordTimer;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (nonatomic, strong) IBOutlet UIButton *playButton;
@property (nonatomic, strong) ScrollingPanelEffects *panelEffects;
@property (nonatomic, unsafe_unretained) IBOutlet UIProgressView *progressBar;
@property (nonatomic, unsafe_unretained) IBOutlet UIImageView *grayBackground;
@property (nonatomic, unsafe_unretained) IBOutlet UILabel *percentageCompletion;
@property (nonatomic, unsafe_unretained) IBOutlet UIView *bottomPanel;

- (IBAction)toggleRecording:(id)sender;
- (IBAction)toggleCamera:(id)sender;
- (IBAction)toggleFlash:(id)sender;
- (IBAction)pickMovieFromGallery:(id)sender;
- (IBAction)closeVideoRecord:(id)sender;

- (IBAction)showEffects:(id)sender;
- (IBAction)togglePlay:(id)sender;
- (IBAction)pauseVideo:(id)sender;
- (IBAction)showAllElements:(id)sender;
- (IBAction)showVideoMotionEditor:(id)sender;

- (void)dismissAllElements;
- (NSString*)labelStringForTime:(NSUInteger) time;

@end
