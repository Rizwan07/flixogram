//
//  ProfileViewController.m
//  Flickogram
//

#import "ProfileViewController.h"
#import "SingletonAuthorizationUser.h"
#import "DetailProfileViewController.h"
#import "VideoDetailViewController.h"
#import "TableVideoCell.h"
#import "GridVideoCell.h"
#import "OptionsViewController.h"

#import "SingletonAuthorizationUser.h"

@interface ProfileViewController ()
- (BOOL)checkIsAuthUser;
- (BOOL)parseVideoFromDictionary:(NSDictionary *)videos;
- (void)setStatusForFollowPanel:(BOOL)isFollowing;
- (void)setNewPositionForTable;
@end

@implementation ProfileViewController
@synthesize selectedUserID = _selectedUserID;
@synthesize hasParentNavigationView = _hasParentNavigationView;
@synthesize thumbUser = _thumbUser;
@synthesize followers = _followers;
@synthesize followings = _followings;
@synthesize username = _username;
@synthesize videos = _videos;
@synthesize table = _table;
@synthesize followPangel = _followPangel;
@synthesize toggleFollow = _toggleFollow;
@synthesize infoFollow = _infoFollow;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UIImage *selectedImage = [UIImage imageNamed:@"tab5-active"];
		UIImage *unselectedImage = [UIImage imageNamed:@"tab5-inactive"];
		UITabBarItem *tabBarItem = [[UITabBarItem alloc] init];
		[tabBarItem setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
		self.tabBarItem = tabBarItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Profile";
    
	[self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
//	//Segmented Control
//	NSArray *itemArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"tableViewSelected"],
//						  [UIImage imageNamed:@"gridView"], nil];
//	segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
//	segmentedControl.frame = CGRectMake(0, 0, 90, 0);
//	segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
//	segmentedControl.tintColor = [UIColor clearColor];
//	[segmentedControl setWidth:45 forSegmentAtIndex:0];
//	[segmentedControl setWidth:45 forSegmentAtIndex:1];
//	
//	[segmentedControl addTarget:self action:@selector(changeSegment) forControlEvents:UIControlEventValueChanged];
//	segmentedControl.selectedSegmentIndex = 0;
//	self.navigationItem.titleView = segmentedControl;
	
	//set Back button
	if (self.hasParentNavigationView) {
		UIImage *backButtonImage = [UIImage imageNamed:@"back"];
		UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
		backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
		[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
		self.navigationItem.leftBarButtonItem = backItem;
	}
	
	//Grid Video
	CGRect newFrame = CGRectMake(2, 37, 320, 285);
	gridVideoView = [[GridVideo alloc] initWithFrame:newFrame];
	gridVideoView.delegate = self;
	self.thumbUser.delegate = self;

	isAuthUser = [self checkIsAuthUser];
	
	UIImage *settingButtonImage = [UIImage imageNamed:@"icon-setting"];
	UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[settingButton setBackgroundImage:settingButtonImage forState:UIControlStateNormal];
	settingButton.frame = CGRectMake(0.0, 0.0, 20.0, 25.0);

	if (isAuthUser) {
		//set Settings button
		[settingButton addTarget:self action:@selector(openSettings) forControlEvents:UIControlEventTouchUpInside];
	} else {
		[settingButton addTarget:self action:@selector(reportBlockUser) forControlEvents:UIControlEventTouchUpInside];
		[self setNewPositionForTable];
	}
	UIBarButtonItem	*settingItem = [[UIBarButtonItem alloc] initWithCustomView:settingButton];
	self.navigationItem.rightBarButtonItem = settingItem;

	videosArray = [[NSMutableArray alloc] init];

	//pull to refresh
	self.table.pullArrowImage = [UIImage imageNamed:@"whiteArrow"];
	self.table.pullBackgroundColor = [UIColor clearColor];
	self.table.pullTextColor = [UIColor whiteColor];
	self.table.loadMoreEnabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
	//get user info
	[videosArray removeAllObjects];
	[self.table reloadData];
	[gridVideoView resetData];
	currentPage = 1;
	isShowMoreVideo = NO;
	isAuthUser = [self checkIsAuthUser];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
	[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:self.selectedUserID viewerID:authUser onPage:pageInStringFormat delegate:self];
}

- (void)changeSegment
{
	NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
	UIImage *firstImage;
	UIImage *secondImage;
	switch (selectedSegment) {
		case 0: {
			firstImage = [UIImage imageNamed:@"tableViewSelected"];
			secondImage = [UIImage imageNamed:@"gridView"];
		}
			break;
		case 1: {
			firstImage = [UIImage imageNamed:@"tableView"];
			secondImage = [UIImage imageNamed:@"gridViewSelected"];
		}
			break;
	}
	[segmentedControl setImage:firstImage forSegmentAtIndex:0];
	[segmentedControl setImage:secondImage forSegmentAtIndex:1];
	[self.table reloadData];
}

- (BOOL)checkIsAuthUser
{	
	NSString *authUserID = [[SingletonAuthorizationUser authorizationUser] userID];
	if (self.selectedUserID) { //set from parent view
		if (![self.selectedUserID isEqualToString:authUserID]) { //not auth user
			return NO;
		}
	}
	
	self.selectedUserID = authUserID;
	return YES;
}

- (void)setNewPositionForTable
{
	self.followPangel.hidden = NO;
	self.table.frame = CGRectMake(self.table.frame.origin.x,
								  self.table.frame.origin.y + self.followPangel.frame.size.height,
								  self.table.frame.size.width,
								  self.table.frame.size.height - self.followPangel.frame.size.height);
}

- (void)resetData
{
	self.username.text = @"";
	self.followers.text = @"";
	self.followings.text = @"";
	self.videos.text = @"";
	self.thumbUser.image = nil;
	self.selectedUserID = nil;
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)openSettings
{
	OptionsViewController *options = [[OptionsViewController alloc] init];
	[self.navigationController pushViewController:options animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Block and report methods
- (void)reportBlockUser
{
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
															 delegate:self
													cancelButtonTitle:@"Cancel"
											   destructiveButtonTitle:nil
													otherButtonTitles:@"Block user", @"Report user", nil];
	actionSheet.tag = 0;
	[actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (actionSheet.tag == 0) {
		if (buttonIndex == 0) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are you sure?"
															message:nil
														   delegate:self
												  cancelButtonTitle:@"Cancel"
												  otherButtonTitles:@"Yes, I'm sure", nil];
			alert.tag = 0;
			[alert show];
		} else if (buttonIndex == 1) {
			UIActionSheet *actionReport = [[UIActionSheet alloc] initWithTitle:@"Reason for reporting:"
																	  delegate:self
															 cancelButtonTitle:@"Cancel"
														destructiveButtonTitle:nil
															 otherButtonTitles:@"Nudity", @"Copyright", @"Abuse", @"Spam", @"Terms of Use Violation", nil];
			actionReport.tag = 1;
			[actionReport showFromTabBar:self.tabBarController.tabBar];
		}
	} else if (actionSheet.tag == 1) {
		if (buttonIndex == 0) {
			[self reportUserWithReason:@"Nudity"];
		} else if (buttonIndex == 1) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Why?"
															message:nil
														   delegate:self
												  cancelButtonTitle:@"Cancel"
												  otherButtonTitles:@"Submit", nil];
			alert.alertViewStyle = UIAlertViewStylePlainTextInput;
			alert.tag = 1;
			[alert show];
		} else if (buttonIndex == 2) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reason for reporting:"
															message:nil
														   delegate:self
												  cancelButtonTitle:@"Cancel"
												  otherButtonTitles:@"Submit", nil];
			alert.alertViewStyle = UIAlertViewStylePlainTextInput;
			alert.tag = 1;
			[alert show];
		} else if (buttonIndex == 3) {
			[self reportUserWithReason:@"Spam"];
		} else if (buttonIndex == 4) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reason for reporting:"
															message:nil
														   delegate:self
												  cancelButtonTitle:@"Cancel"
												  otherButtonTitles:@"Submit", nil];
			alert.alertViewStyle = UIAlertViewStylePlainTextInput;
			alert.tag = 1;
			[alert show];
		}
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == 0) {
		if (buttonIndex == 1) {
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			[[ManagerFlickogramAPI authorizationExemplar] blockUser:self.selectedUserID blockerID:authUser delegate:self];
		}
	} else if (alertView.tag == 1) {
		if (buttonIndex == 1) {
			NSString *reason = [[alertView textFieldAtIndex:0] text];
			[self reportUserWithReason:reason];
		}
	}
}

- (void)reportUserWithReason:(NSString *)reasonReport
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI authorizationExemplar] reportUser:self.selectedUserID
												  reporterID:authUser
												reasonReport:reasonReport
													delegate:self];
}

#pragma mark - Block user delegate
- (void)blockUserSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"User blocked"
													message:nil
												   delegate:nil
										  cancelButtonTitle:@"Dismiss"
										  otherButtonTitles:nil];
	[alert show];
}

- (void)blockUserFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:error
												   delegate:nil
										  cancelButtonTitle:@"Dismiss"
										  otherButtonTitles:nil];
	[alert show];
}

#pragma mark - Report User Delegate
- (void)reportUserSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Thanks"
														   message:@"We'll review this user as soon as possible."
														  delegate:nil
												 cancelButtonTitle:@"Dismiss"
												 otherButtonTitles:nil];
	[successAlert show];
}

- (void)reportUserFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:error
												   delegate:nil
										  cancelButtonTitle:@"Dismiss"
										  otherButtonTitles:nil];
	[alert show];
}

#pragma mark - User Profile Delegate
-(void)getUserProfileSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString *username = [[data objectForKey:@"user_detail"] objectForKey:@"user_name"];
	NSString *followers = [[data objectForKey:@"user_stats"] objectForKey:@"followers"];
	NSString *followings = [[data objectForKey:@"user_stats"] objectForKey:@"followings"];
	NSString *videos = [[data objectForKey:@"user_stats"] objectForKey:@"videos"];
	NSString  *thumbURL = [[data objectForKey:@"user_detail"] objectForKey:@"profile_picture"];
	BOOL isFollowing = [[data objectForKey:@"following"] boolValue];
	[self setStatusForFollowPanel:isFollowing];
	//http://developer.avenuesocial.com/azeemsal/flickogram/images/profile_pictures/thumb_1392795609.jpg
	self.username.text = username;
    self.navigationItem.title = username;
	self.followings.text = followings;
	self.followers.text = followers;
	self.videos.text = videos;
	self.thumbUser.imageURL = [NSURL URLWithString:thumbURL];
    [*[SingletonAuthorizationUser authorizationUser].imageView setImageURL:[NSURL URLWithString:thumbURL]];
    
    [[NSUserDefaults standardUserDefaults] setObject:thumbURL forKey:@"profile_picture"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
	
	NSInteger countVideo = [[[data objectForKey:@"user_videos"] objectForKey:@"total_videos"] intValue];
	if (countVideo) {
		NSDictionary *videos = [[data objectForKey:@"user_videos"] objectForKey:@"videos"];
		BOOL parseSuccess = [self parseVideoFromDictionary:videos];
		isShowMoreVideo = [[[data objectForKey:@"user_videos"] objectForKey:@"show_more"] boolValue];
		if (parseSuccess) {
			[self.table reloadData];
		}
	} else if (self.table.pullTableIsRefreshing) {
		[self refreshTable];
	}
}

- (BOOL)parseVideoFromDictionary:(NSDictionary *)videos
{
	if (self.table.pullTableIsRefreshing) {
		NSString *lastDateAdded = @"0";
		if ([videosArray count] > 0) {
			lastDateAdded = [[videosArray objectAtIndex:0] objectForKey:@"date_added"];
		}
		NSInteger currentVideo = 0;
		BOOL isMoreVideo = YES;
		while (isMoreVideo) {
			NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
			if ([videos objectForKey:numberUserString]) {
				NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
				NSString *newDateAdded = [videoDetails objectForKey:@"date_added"];
				
				//compare last date with new date and stop if equal
				if ([lastDateAdded isEqualToString:newDateAdded]) {
					[self refreshTable];
					return NO;
				} else {
					[videosArray insertObject:videoDetails atIndex:currentVideo];
					
					if (isShowMoreVideo) {
						[videosArray removeLastObject];
					}
				}
				currentVideo++;
			} else {
				isMoreVideo = NO;
			}
		}
		[self refreshTable];
		return NO;
	}

	NSInteger currentVideo = 0;
	BOOL isMoreVideo = YES;
	while (isMoreVideo) {
		NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
		if ([videos objectForKey:numberUserString]) {
			NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
			[videosArray addObject:videoDetails];
			currentVideo++;
		} else {
			isMoreVideo = NO;
		}
	}
	return YES;
}

- (void)getUserProfileFailedWithError:(NSString *)error
{
	[self refreshTable];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)setStatusForFollowPanel:(BOOL)isFollowing
{
	if (isFollowing) {
		self.toggleFollow.selected = YES;
		self.infoFollow.text = @"You are following this user";
	} else {
		self.toggleFollow.selected = NO;
		self.infoFollow.text = @"You are not following this user";
	}
}

#pragma mark - EGOImageView Delegate
- (void)imageViewLoadedImage:(EGOImageView *)imageView
{
	NSLog(@"Success");
}

- (void)imageViewFailedToLoadImage:(EGOImageView *)imageView error:(NSError *)error
{
	self.thumbUser.placeholderImage = [UIImage imageNamed:@"Profile-thumb"];
}

#pragma mark - IBActions
- (IBAction)showFollowers:(id)sender
{
	DetailProfileViewController *followersViewController = [[DetailProfileViewController alloc] init];
	followersViewController.title = @"Followers";
	followersViewController.detailType = ShowFollowers;
	followersViewController.selectedUserID = self.selectedUserID;
	[self.navigationController pushViewController:followersViewController animated:YES];
}

- (IBAction)showFollowings:(id)sender
{
	DetailProfileViewController *followingsViewController = [[DetailProfileViewController alloc] init];
	followingsViewController.detailType = ShowFollowings;
    followingsViewController.title = @"Following";
	followingsViewController.selectedUserID = self.selectedUserID;
	[self.navigationController pushViewController:followingsViewController animated:YES];
}

- (IBAction)showVideoForEGOImageButton:(id)sender
{
	EGOImageButton *selectedButton = (EGOImageButton *)sender;
	NSInteger indexVideo = selectedButton.tag;
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexVideo] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}

- (IBAction)followUnFollowEvent:(id)sender
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI authorizationExemplar] followUnFollowUserID:self.selectedUserID followerID:authUser indexArray:0 withDelegate:self];
}

#pragma mark - Grid Video Delegate
- (void)showVideoByIndexArray:(NSInteger)indexArray
{
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexArray] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}

- (void)showMoreVideos
{
	currentPage++;
	NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:self.selectedUserID viewerID:authUser onPage:pageInStringFormat delegate:self];
}

#pragma mark - Follow - unfollow delegate
- (void)followUnFollowUserSuccesWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString *message = [data objectForKey:@"message"];
	NSInteger countFollowers = 	[self.followers.text integerValue];
	if ([message isEqualToString:@"Followed successfully"]) {
		[self setStatusForFollowPanel:YES];
		countFollowers++;
	} else if ([message isEqualToString:@"Unfollowed successfully"]) {
		[self setStatusForFollowPanel:NO];
		countFollowers--;
	}
	self.followers.text = [NSString stringWithFormat:@"%d", countFollowers];
}

- (void)followUnFollowUserFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - UITableView Delegate/Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) (count + 9) / 10;
 		return countCells;
	} else {
		return [videosArray count];
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		return 135;
	} else {
		return 160;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 0) {
		static NSString *cellId = @"GridVideoCell";
		GridVideoCell *cell = (GridVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GridVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (GridVideoCell *) currentObject;
				}
				break;
			}
		}
		
		NSInteger currentCell =	indexPath.row * 10;
		NSInteger countVideos = [videosArray count];
		
		int currentButton = 0;
		for (int i = currentCell; (i < countVideos && i < currentCell+10); i++) {
			NSDictionary *videoDetail = [videosArray objectAtIndex:i];
			NSURL *thumbnailURL = [NSURL URLWithString:[videoDetail objectForKey:@"thumbnail"]];
			EGOImageButton *currentImageButton = [cell setThumbImageOfURL:thumbnailURL ForButton:currentButton];
			currentImageButton.tag = i;
			[currentImageButton addTarget:self action:@selector(showVideoForEGOImageButton:) forControlEvents:UIControlEventTouchUpInside];
			currentButton++;
		}
		return cell;
	} else {
	
		static NSString *cellIdentifier = @"TableVideoCell";
		TableVideoCell *cell = (TableVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TableVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (TableVideoCell *) currentObject;
				}
				break;
			}
		}
		NSDictionary *videoDetail = [videosArray objectAtIndex:indexPath.row];
		NSString *thumbnailURL = [videoDetail objectForKey:@"thumbnail"];
		[cell setThumbnail:thumbnailURL];
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (segmentedControl.selectedSegmentIndex == 1) {
		VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
		videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexPath.row] mutableCopy];
		[self.navigationController pushViewController:videoDetailVC animated:YES];
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (isShowMoreVideo) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) ((count + 9) / 10) - 1;
		if (((segmentedControl.selectedSegmentIndex ==0) && (indexPath.row == countCells)) ||
			((segmentedControl.selectedSegmentIndex == 1) && (indexPath.row == count - 1))) {
			isShowMoreVideo = NO;
			currentPage++;
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
			NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
			[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:self.selectedUserID viewerID:authUser onPage:pageInStringFormat delegate:self];
		}
	}
}


#pragma mark - Refresh and load more methods

- (void) refreshTable
{
	// Code to actually refresh goes here.
    self.table.pullLastRefreshDate = [NSDate date];
    self.table.pullTableIsRefreshing = NO;
	[self.table reloadData];
}

- (void) loadMoreDataToTable
{
    // Code to actually load more data goes here.
    self.table.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:self.selectedUserID
														   viewerID:authUser
															 onPage:@"1"
														   delegate:self];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

@end
