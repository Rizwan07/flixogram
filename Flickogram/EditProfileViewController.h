//
//  EditProfileViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"

typedef enum {
	GenderType,
	YearType,
	MonthType,
	DayType
} CurrentPickerView;

@interface EditProfileViewController : BaseViewController <UITableViewDelegate,
															UITableViewDataSource,
															FlickogramAuthorizationDelegate,
															UITextFieldDelegate,
															UIPickerViewDataSource,
															UIPickerViewDelegate>
{
	//Public section
	UITextField *username;
	UITextField *firstname;
	UITextField *lastname;
	UITextField *website;
	
	//Private section
	UITextField *phone;
	UITextField *email;
	
	//Picker Data
	UIButton *sexButton;
	UILabel *sexLabel;
	NSArray *genderData;
	NSInteger currentGender;
	
	UIButton *birthdayMonthButton;
	UILabel *birthdayMonthLabel;
	NSArray *birthdayMonthData;
	NSInteger currentBirthdayMonth;
	
	UIButton *birthdayDayButton;
	UILabel *birthdayDayLabel;
	NSMutableArray *birthdayDayData;
	NSInteger currentBirthdayDay;
	
	UIButton *birthdayYearButton;
	UILabel *birthdayYearLabel;
	NSMutableArray *birthdayYearData;
	NSInteger currentBirthdayYear;
	
	//ReusableCellIdentifiers
	NSArray *reusableIdentifiers;
	
	
	CurrentPickerView selectedPickerType;
	UIActionSheet *actionSheet;
	UIPickerView *pickerView;
}

@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
