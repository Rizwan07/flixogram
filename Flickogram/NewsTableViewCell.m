//
//  NewsTableViewCell.m
//  Flickogram
//


#import "NewsTableViewCell.h"

@implementation NewsTableViewCell
@synthesize message = _message;
@synthesize date = _date;
@synthesize photoUser = _photoUser;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPlaceholder 
{
	self.photoUser.placeholderImage = [UIImage imageNamed:@"Profile-thumb"];
}

- (void)setPhoto:(NSString *)photoUser 
{
	self.photoUser.imageURL = [NSURL URLWithString:photoUser];
}

- (void)willMoveToSuperview:(UIView *)newSuperview 
{
	[super willMoveToSuperview:newSuperview];
	
	if(!newSuperview) {
		[self.photoUser cancelImageLoad];
	}
}


@end
