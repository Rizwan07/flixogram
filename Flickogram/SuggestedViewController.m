//
//  SuggestedViewController.m
//  Flickogram

#import "SuggestedViewController.h"
#import "SuggestUserCell.h"
#import "SingletonAuthorizationUser.h"
#import "FlickogramConstants.h"

@interface SuggestedViewController ()
- (void)parseDictionary:(NSMutableDictionary *)data;
@end

@implementation SuggestedViewController
@synthesize table = _table;
@synthesize isShowDoneButton = _isShowDoneButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Suggested Users";
    
    //set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	self.title = @"Suggested Users";
	
	//if sign up
	if (self.isShowDoneButton) {
		//set Done button
		UIImage *doneButtonImage = [UIImage imageNamed:@"next"];
		UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
		[doneButton setBackgroundImage:doneButtonImage forState:UIControlStateNormal];
		doneButton.frame = CGRectMake(0.0, 0.0, 50, 30);
		[doneButton addTarget:self action:@selector(doneSignup) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
		self.navigationItem.rightBarButtonItem = doneItem;
	}
	
	suggestedUsers = [[NSMutableArray alloc] init];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI videoExemplar] getSuggestedVideosForUserID:authUser withDelegate:self];
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)doneSignup
{
	[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationGetFollowingVideos
														object:self
													  userInfo:nil];
	
	[self dismissViewControllerAnimated:YES completion:^{
//        [[[UIAlertView alloc] initWithTitle:@"Tutorial" message:@"Tutorial Message" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];

    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate/Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	NSInteger countSuggestedUsers = [suggestedUsers count];
	return countSuggestedUsers;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"SuggestUserCell";
	SuggestUserCell *cell = (SuggestUserCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SuggestUserCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (SuggestUserCell *) currentObject;
			}
			break;
		}
	}
	NSDictionary *userInfo = [[suggestedUsers objectAtIndex:indexPath.section] objectForKey:@"user"];
	NSString *username = [userInfo objectForKey:@"user_name"];
	cell.username.text = username;
	NSString *profilePicture = [userInfo objectForKey:@"profile_picture"];
	[cell setPhotoUser:profilePicture];
	cell.bottomText.text = [NSString stringWithFormat:@"Videos straight from %@", username];
	NSDictionary *videos = [[suggestedUsers objectAtIndex:indexPath.section] objectForKey:@"videos"];
	[cell setVideosThumbFromDictionary:videos];
	
	[cell.toggleFollow addTarget:self action:@selector(toggleStateFollowing:) forControlEvents:UIControlEventTouchUpInside];
	cell.toggleFollow.tag = indexPath.section;
	BOOL isFollowing = [[userInfo objectForKey:@"is_following"] boolValue];
	if (isFollowing) {
		cell.toggleFollow.selected = YES;
	} else {
		cell.toggleFollow.selected = NO;
	}
	return cell;
}

#pragma mark - Following
- (void)toggleStateFollowing:(id)sender
{
	UIButton *followButton = (UIButton *)sender;
	NSInteger currentIndex = followButton.tag;
	NSString *userID = [[[suggestedUsers objectAtIndex:currentIndex] objectForKey:@"user"]objectForKey:@"id"];
	NSString *followerID = [[SingletonAuthorizationUser authorizationUser] userID];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI authorizationExemplar] followUnFollowUserID:userID followerID:followerID indexArray:currentIndex withDelegate:self];
}

- (void)followUnFollowUserSuccesWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSMutableDictionary *currentUser = [suggestedUsers objectAtIndex:currentIndex];
	BOOL isFollowing = [[[currentUser objectForKey:@"user"]objectForKey:@"is_following"] boolValue];
	isFollowing = !isFollowing;
	NSString *isFollowingString = [NSString stringWithFormat:@"%d", isFollowing];
	[[currentUser objectForKey:@"user"] setObject:isFollowingString forKey:@"is_following"];
	[suggestedUsers replaceObjectAtIndex:currentIndex withObject:currentUser];
	[self.table reloadData];
}

- (void)followUnFollowUserFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSLog(@"%@", error);
}

#pragma mark - Suggested delegate
- (void)getSuggestedVideosSuccessWithRecivedData:(NSMutableDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self parseDictionary:data];
	NSLog(@"%@", data);
}

- (void)getSuggestedVideosFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSLog(@"%@", error);
}

- (void)parseDictionary:(NSMutableDictionary *)data
{
	if ([data objectForKey:@"users"]) {
		NSMutableDictionary *users = [data objectForKey:@"users"];
		
		for (NSMutableDictionary *currentUser in users) {
			[suggestedUsers addObject:currentUser];
		}
			 
		[self.table reloadData];
	}
}
@end
