//
//  OptionsViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import <MessageUI/MessageUI.h>
#import "ManagerFlickogramAPI.h"
#import <AddressBookUI/AddressBookUI.h>

@class MainViewController;

typedef enum {
	FirstSection,
	SecondSection,
	ThirdSection,
	FourthSection
} TableSection;

@interface OptionsViewController : BaseViewController <UITableViewDataSource,
														UITableViewDelegate,
														MFMessageComposeViewControllerDelegate,
														UIActionSheetDelegate,
														UIImagePickerControllerDelegate,
														UINavigationControllerDelegate,
														FlickogramAuthorizationDelegate,
														UIAlertViewDelegate,
														ABPeoplePickerNavigationControllerDelegate>
{
	TableSection tableSection;
	BOOL isVideosPrivate;
}

@property (nonatomic, weak) IBOutlet UITableView *table;

@end
