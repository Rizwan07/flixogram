//
//  FlickogramConstants.m
//  Flickogram
//

#import "FlickogramConstants.h"

@implementation FlickogramConstants

NSString * const NameRecordedVideoFile =  @"Documents/normal.mov";
NSString * const NameFilteredVideoFile = @"Documents/filtering.mov";
NSString * const NameMotionVideoFile = @"Documents/motion.mov";
NSString * const NameVideoWithMusic = @"Documents/musicVideo.mov";

NSString * const NameNotificationStartFilteringVideo = @"StartFilteringVideo";
NSString * const NameNotificationFinishFilteringVideo = @"FinishFilteringVideo";
NSString * const NameNotificationChangeMoviePath = @"ChangeMoviePath";
NSString * const NameNotificationCompletedFilteringFrame = @"CompletedFilteringFrame";
NSString * const NameNotificationUploadVideoOnServer = @"UploadVideo";
NSString * const NameNotificationGetFollowingVideos = @"FollowingVideos";

NSString * const NameNotificationMenu = @"MenuNotify";

@end
