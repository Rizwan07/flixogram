//
//  AuthorizationViewController.m
//  Flickogram
//


#import "AuthorizationViewController.h"
#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "GridVideo.h"
#import <FacebookSDK/FacebookSDK.h>
#import "TwitterSettings.h"

@interface AuthorizationViewController ()
- (IBAction)signupButtonClicked:(id)sender;
- (IBAction)loginButtonClicked:(id)sender;
@end

@implementation AuthorizationViewController
@synthesize toolbar = _toolbar;
@synthesize infoView = _infoView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Authorize";
    
    UIImageView *imageViewTitle = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 76, 40)];
    [imageViewTitle setImage:[UIImage imageNamed:@"logo-nav"]];
    
	self.navigationItem.titleView = imageViewTitle;
	
	[self.toolbar setBackgroundImage:[UIImage imageNamed:@"following-bar"] forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];

//	UIImage *signupImage = [UIImage imageNamed:@"Sign-up"];
//	UIButton *signupButton = [UIButton buttonWithType:UIButtonTypeCustom];
//	[signupButton setImage:signupImage forState:UIControlStateNormal];
//	signupButton.frame = CGRectMake(0.0, 0.0, 75.0, 34.0);
//	[signupButton addTarget:self action:@selector(signupButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//	UIBarButtonItem *signupBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:signupButton];
//	
//	UIImage *loginImage = [UIImage imageNamed:@"Login"];
//	UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
//	[loginButton setImage:loginImage forState:UIControlStateNormal];
//	loginButton.frame = CGRectMake(0.0, 0.0, 75.0, 34.0);
//	[loginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//	UIBarButtonItem *loginBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:loginButton];
//	UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
//	fixedSpace.width = 70.0;
//									
//	NSArray *items = [NSArray arrayWithObjects:fixedSpace, signupBarButtonItem, loginBarButtonItem, nil];
//	[self.toolbar setItems:items animated:NO];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI videoExemplar] getPopularVideosOnPage:@"1" withType:@"all" withDelegate:self];
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - authorization methods
- (IBAction)signupButtonClicked:(id)sender
{
	self.infoView.hidden = YES;
	SignUpViewController *signUp = [[SignUpViewController alloc] init];
	[self.navigationController pushViewController:signUp animated:YES];
}

- (IBAction)loginButtonClicked:(id)sender
{
	self.infoView.hidden = YES;
	LoginViewController *loginView = [[LoginViewController alloc] init];
	[self.navigationController pushViewController:loginView animated:YES];
}

- (IBAction)dismissInfoView:(id)sender
{
	self.infoView.hidden = YES;
}

#pragma mark - Get Popular Video
- (void)getPopularVideoSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSInteger countVideoOnPage = [[data objectForKey:@"videos"] count];
	CGRect newFrame = CGRectMake(2, 2, 320, 360);
	GridVideo *grid = [[GridVideo alloc] initWithFrame:newFrame];
	[self.view insertSubview:grid atIndex:0];
	[grid showGridVideoInQuantity:countVideoOnPage fromDictionary:[data objectForKey:@"videos"]];
}

- (void)getPopularVideoFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSLog(@"%@",error);
}

@end
