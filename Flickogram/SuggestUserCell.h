//
//  SuggestUserCell.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface SuggestUserCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *username;
@property (nonatomic, weak) IBOutlet EGOImageView *userThumb;
@property (nonatomic, weak) IBOutlet UILabel *bottomText;
@property (nonatomic, weak) IBOutlet EGOImageView *firstThumb;
@property (nonatomic, weak) IBOutlet EGOImageView *secondThumb;
@property (nonatomic, weak) IBOutlet EGOImageView *thirdThumb;
@property (nonatomic, weak) IBOutlet EGOImageView *fourthThumb;
@property (nonatomic, weak) IBOutlet UIButton *toggleFollow;

@property (nonatomic, weak) IBOutlet UIImageView *firstBorder;
@property (nonatomic, weak) IBOutlet UIImageView *secondBorder;
@property (nonatomic, weak) IBOutlet UIImageView *thirdBorder;
@property (nonatomic, weak) IBOutlet UIImageView *fourthBorder;

- (void)setPhotoUser:(NSString*)photoUser;
- (void)setVideosThumbFromDictionary:(NSDictionary *)videos;

@end
