//
//  DetailFriendsViewController.h
//  Flickogram


#import "BaseViewController.h"

#import "ManagerFlickogramAPI.h"

@interface DetailFriendsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, FlickogramSearchDelegate, UISearchBarDelegate,FlickogramAuthorizationDelegate> {
	NSMutableArray *friendsArray;
	NSInteger currentPage;
	BOOL isNoUsersFound;
}

@property (nonatomic, strong) NSString *socialType;
@property (nonatomic, strong) NSArray *usersAB;
@property (nonatomic, weak) IBOutlet UITableView *table;
@property (nonatomic, strong) UISearchBar *searchBar;

@end
