//
//  ExploreViewController.m
//  Flickogram
//

#import "ExploreViewController.h"
#import "ExploreTableViewCell.h"
#import "SingletonAuthorizationUser.h"
#import "HashtagVideoViewController.h"
#import "ProfileViewController.h"
#import "VideoDetailViewController.h"
#import "GridVideoCell.h"

@interface ExploreViewController ()

@end

@implementation ExploreViewController
@synthesize searchBar = _gsearchBar;
@synthesize segmentedBar = _segmentedBar;
@synthesize segmentedItem = _segmentedItem;
@synthesize table = _table;
@synthesize videoTable = _videoTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       	UIImage *selectedImage = [UIImage imageNamed:@"tab2-active"];
		UIImage *unselectedImage = [UIImage imageNamed:@"tab2-inactive"];
		UITabBarItem *tabBarItem = [[UITabBarItem alloc] init];
		[tabBarItem setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
		self.tabBarItem = tabBarItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = @"Explore";
    self.screenName = @"Explore";
	[self.segmentedBar setBackgroundImage:[UIImage imageNamed:@"header"] forBarMetrics:UIBarMetricsDefault];
	
    [_videoTable setUserInteractionEnabled:NO];
    
	//alloc and pre-set
	usersArray = [[NSMutableArray alloc] init];
	videosArray = [[NSMutableArray alloc] init];
	
	isSearchFinished = NO;
	isSearchStarted = NO;
	currentPage = 1;
	currentExploreVideoPage = 1;
	countVideosByHashtag = 0;
	isShowMoreVideo = NO;
	
//	self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
//	self.searchBar.barStyle = UIBarStyleDefault;
//	self.searchBar.placeholder = @"Search users and hashtags";
//	self.searchBar.tintColor = [UIColor lightGrayColor];
//	self.searchBar.showsCancelButton = NO;
//	self.searchBar.delegate = self;
//	[self.view addSubview:self.searchBar];
	
	//NSArray *itemArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"User-highlight"], [UIImage imageNamed:@"Hashtags-unhighlight"], nil];
    NSArray *itemArray = [NSArray arrayWithObjects:@"Users", @"Hashtags", nil];
	
	segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
	segmentedControl.frame = CGRectMake(68, 7, 184, 29);
	[segmentedControl setWidth:92.0 forSegmentAtIndex:0];
	[segmentedControl setWidth:92.0 forSegmentAtIndex:1];
	[segmentedControl setSegmentedControlStyle:UISegmentedControlStyleBar];
    [segmentedControl setBackgroundColor:[UIColor clearColor]];
//    [segmentedControl setTintColor:[UIColor clearColor]];
	[segmentedControl addTarget:self action:@selector(changeSegment) forControlEvents:UIControlEventValueChanged];
	segmentedControl.selectedSegmentIndex = 0;
    //[self.segmentedBar addSubview:segmentedControl];
	self.segmentedItem.titleView = segmentedControl;
	
	CGRect newFrame = CGRectMake(2, 17, 320, 330);
	gridVideoView = [[GridVideo alloc] initWithFrame:newFrame];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentExploreVideoPage];
	[[ManagerFlickogramAPI videoExemplar] getExploreVideosOnPage:pageInStringFormat withDelegate:self];
	
	//pull to refresh
	self.videoTable.pullArrowImage = [UIImage imageNamed:@"whiteArrow"];
	self.videoTable.pullBackgroundColor = [UIColor clearColor];
	self.videoTable.pullTextColor = [UIColor whiteColor];
	self.videoTable.loadMoreEnabled = NO;
}

- (void)changeSegment 
{
	NSInteger currentSegment = segmentedControl.selectedSegmentIndex;
//	UIImage *firstImage;
//	UIImage *secondImage;
	switch (currentSegment) {
		case 0: {
//			firstImage = [UIImage imageNamed:@"User-highlight"];
//			secondImage = [UIImage imageNamed:@"Hashtags-unhighlight"];
			self.searchBar.placeholder = @"Search for a user";
			break;
		}
		case 1: {
//			firstImage = [UIImage imageNamed:@"User-unhighlight"];
//			secondImage = [UIImage imageNamed:@"Hashtags-highlight"];
			self.searchBar.placeholder = @"Search for a hashtag";
			break;
		}
	}
//	[segmentedControl setImage:firstImage forSegmentAtIndex:0];
//	[segmentedControl setImage:secondImage forSegmentAtIndex:1];
	[self.table reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UISearchBar delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
	[self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        CGRect rect = self.searchBar.frame;
        rect.origin.y = 20;
        self.searchBar.frame = rect;
        
        rect = self.videoTable.frame;
        rect.origin.y = 44+20;
        self.videoTable.frame = rect;
    }
	[self.searchBar setShowsCancelButton:YES animated:YES];
	self.segmentedBar.hidden = NO;
	self.table.hidden = NO;
	[self changeSegment];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	if (isSearchFinished) {
		[usersArray removeAllObjects];
		countVideosByHashtag = 0;
		isSearchFinished = NO;
	}
	[self.table reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[self.searchBar setShowsCancelButton:NO];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        CGRect rect = self.searchBar.frame;
        rect.origin.y = 0;
        self.searchBar.frame = rect;
        
        rect = self.videoTable.frame;
        rect.origin.y = 44;
        self.videoTable.frame = rect;
    }
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	self.segmentedBar.hidden = YES;
	self.table.hidden = YES;
	self.searchBar.placeholder = @"Search users and hashtags";
	self.searchBar.text = @"";
	[self.searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	if (!isSearchStarted && [self.searchBar.text length]) {
		isSearchStarted = YES;
		isSearchFinished = NO;
		[usersArray removeAllObjects];
		countVideosByHashtag = 0;
		[self.table reloadData];
		NSString *searchString = self.searchBar.text;
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		currentPage = 1;
		NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
		[[ManagerFlickogramAPI searchExemplar] getVideosByTags:searchString onPage:pageInStringFormat withDelegate:self];
	}
}

#pragma mark - Table Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == self.videoTable) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) (count + 9) / 10;
 		return countCells;
	} else {
		if (isSearchFinished) {
			if (segmentedControl.selectedSegmentIndex == 0) {
				return [usersArray count];
			} else if (countVideosByHashtag > 0) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 1;
		}
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == self.videoTable) {
			return 135;
	} else {
		return 44;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == self.videoTable) {
		static NSString *cellId = @"GridVideoCell";
		GridVideoCell *cell = (GridVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GridVideoCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (GridVideoCell *) currentObject;
				}
				break;
			}
		}
		
		NSInteger currentCell =	indexPath.row * 10;
		NSInteger countVideos = [videosArray count];
		
		int currentButton = 0;
		for (int i = currentCell; (i < countVideos && i < currentCell+10); i++) {
			NSDictionary *videoDetail = [videosArray objectAtIndex:i];
			NSURL *thumbnailURL = [NSURL URLWithString:[videoDetail objectForKey:@"thumbnail"]];
			EGOImageButton *currentImageButton = [cell setThumbImageOfURL:thumbnailURL ForButton:currentButton];
			currentImageButton.tag = i;
			[currentImageButton addTarget:self action:@selector(showVideoForEGOImageButton:) forControlEvents:UIControlEventTouchUpInside];
			currentButton++;
		}
		return cell;
	} else {
		static NSString *cellIdentifier = @"ExploreCell";
		ExploreTableViewCell *cell = (ExploreTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if (cell == nil) {
			NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ExploreTableViewCell" owner:nil options:nil];
			for (id currentObject in topLevelObjects) {
				if ([currentObject isKindOfClass:[UITableViewCell class]]) {
					cell = (ExploreTableViewCell *) currentObject;
					[cell setPlaceholder];
				}
				break;
			}
		}

		if (isSearchFinished) {
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			if (segmentedControl.selectedSegmentIndex == 0) {
					cell.thumbUser.hidden = NO;
					NSDictionary *data = [usersArray objectAtIndex:indexPath.row];
					cell.username.text = [data objectForKey:@"user_name"];
					cell.userRealName.text = [NSString stringWithFormat:@"%@ %@", [data objectForKey:@"firstname"], [data objectForKey:@"lastname"]];
					[cell setPhotoUser:[data objectForKey:@"profile_picture"]];
			} else {
				cell.searchString.text = [NSString stringWithFormat:@"#%@", self.searchBar.text];
				cell.searchString.textColor = [UIColor blackColor];
				NSInteger totalVideo = countVideosByHashtag;
				cell.countVideos.text = [NSString stringWithFormat:@"%d videos", totalVideo];
			}
		} else if (isSearchStarted) {
			cell.searchString.text = @"Searching...";
		} else if ([self.searchBar.text length]) {
			cell.searchString.text = [NSString stringWithFormat:@"Search for \"%@\"", self.searchBar.text];
		} else {
			cell.searchString.text = @"";
		}
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == self.videoTable) {
		VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
		videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexPath.row] mutableCopy];
		[self.navigationController pushViewController:videoDetailVC animated:YES];
	} else {
		if (!isSearchFinished && !isSearchStarted && indexPath.row == 0 && [self.searchBar.text length]) {
			//start search
			isSearchStarted = YES;
			[self.table reloadData];
			NSString *searchString = self.searchBar.text;
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			currentPage = 1;
			NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
			[[ManagerFlickogramAPI searchExemplar] getVideosByTags:searchString onPage:pageInStringFormat withDelegate:self];
		} else {
			//users
			if (segmentedControl.selectedSegmentIndex == 0 && isSearchFinished) {
				ProfileViewController *profile = [[ProfileViewController alloc] init];
				profile.selectedUserID = [[usersArray objectAtIndex:indexPath.row] objectForKey:@"id"];
				profile.hasParentNavigationView = YES; //subview for it's view
				[self.navigationController pushViewController:profile animated:YES];
			} //hashtags
			else if (segmentedControl.selectedSegmentIndex == 1 && isSearchFinished) {
				HashtagVideoViewController *hashtagVideo = [[HashtagVideoViewController alloc] init];
				hashtagVideo.hashtag = self.searchBar.text;
				hashtagVideo.countVideo = countVideosByHashtag;
				[self.navigationController pushViewController:hashtagVideo animated:YES];
			}
		}
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == self.videoTable) {
		if (isShowMoreVideo) {
			NSInteger count = [videosArray count];
			NSInteger countCells = (NSInteger) ((count + 9) / 10) - 1;
			if (((segmentedControl.selectedSegmentIndex ==0) && (indexPath.row == countCells)) ||
				((segmentedControl.selectedSegmentIndex == 1) && (indexPath.row == count - 1))) {
				isShowMoreVideo = NO;
				currentExploreVideoPage++;
				[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
				NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentExploreVideoPage];
				[[ManagerFlickogramAPI videoExemplar] getExploreVideosOnPage:pageInStringFormat withDelegate:self];
			}
		}
	}
}

- (void)showVideoForEGOImageButton:(id)sender
{
	EGOImageButton *selectedButton = (EGOImageButton *)sender;
	NSInteger indexVideo = selectedButton.tag;
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexVideo] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	[self.searchBar resignFirstResponder];
}

#pragma mark - Search Delegate
- (void)getVideoByTagsSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isSearchStarted = NO;
	isSearchFinished = YES;
	[self.searchBar resignFirstResponder];
	countVideosByHashtag = [[data objectForKey:@"total_videos"] integerValue];
	NSInteger countUsers = [[data objectForKey:@"total_users"] integerValue];
	if ([[data objectForKey:@"users"] count]) {
		[self parseUsersDictionary:[data objectForKey:@"users"]];
	}
	
	BOOL isShowMore = [[data objectForKey:@"show_more"] boolValue];
	
	NSInteger currentCount = [usersArray count];
	if (isShowMore && currentCount < countUsers) {
		currentPage++;
		NSString *pageInStringFormat = [NSString stringWithFormat:@"%d", currentPage];
		NSString *searchString = self.searchBar.text;
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		[[ManagerFlickogramAPI searchExemplar] getVideosByTags:searchString onPage:pageInStringFormat withDelegate:self];
	}
	[self.table reloadData];
}

- (void)getVideoByTagsFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isSearchStarted = NO;
	isSearchFinished = NO;
	[self.searchBar resignFirstResponder];
	[self.table reloadData];

	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Get Explore Video Delegate
- (void)getExplorerVideosSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[_videoTable setUserInteractionEnabled:YES];
    
    
	NSInteger countVideo = [[data objectForKey:@"total_videos"] intValue];
	if (countVideo) {
		NSDictionary *videos = [data objectForKey:@"videos"];
		isShowMoreVideo = [[data objectForKey:@"show_more"] boolValue];
		[self parseVideoFromDictionary:videos];
	} else if (self.videoTable.pullTableIsRefreshing) {
		[self refreshTable];
	}

}

- (void)getExplorerVideosFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[_videoTable setUserInteractionEnabled:YES];
	[self refreshTable];
}

- (void)parseVideoFromDictionary:(NSDictionary *)videos
{
	if (self.videoTable.pullTableIsRefreshing) {
		NSString *lastDateAdded = @"0";
		if ([videosArray count] > 0) {
			lastDateAdded = [[videosArray objectAtIndex:0] objectForKey:@"date_added"];
		}
		NSInteger currentVideo = 0;
		BOOL isMoreVideo = YES;
		while (isMoreVideo) {
			NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
			if ([videos objectForKey:numberUserString]) {
				NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
				NSString *newDateAdded = [videoDetails objectForKey:@"date_added"];
				
				//compare last date with new date and stop if equal
				if ([lastDateAdded isEqualToString:newDateAdded]) {
					[self refreshTable];
					return;
				} else {
					[videosArray insertObject:videoDetails atIndex:currentVideo];
					
					if (isShowMoreVideo) {
						[videosArray removeLastObject];
					}
				}
				currentVideo++;
			} else {
				isMoreVideo = NO;
			}
		}
		[self refreshTable];
		return;
	}
	
	NSInteger currentVideo = 0;
	BOOL isMoreVideo = YES;
	while (isMoreVideo) {
		NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
		if ([videos objectForKey:numberUserString]) {
			NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
			[videosArray addObject:videoDetails];
			currentVideo++;
		} else {
			isMoreVideo = NO;
		}
	}
	[self.videoTable reloadData];
}


#pragma mark - Parse methods
- (void)parseUsersDictionary:(NSDictionary *)users 
{
	
	NSInteger currentUser = 0;
	BOOL isMoreUser = YES;
	
	while (isMoreUser) {
		NSString *numberUserString = [NSString stringWithFormat:@"%d", currentUser];
		if ([users objectForKey:numberUserString]) {
			NSDictionary *userDetails = [users objectForKey:numberUserString];
			[usersArray addObject:userDetails];
			currentUser++;
		} else {
			isMoreUser = NO;
		}
	}
	[self.table reloadData];
}


#pragma mark - Reset data
- (void)resetData
{
	isSearchFinished = NO;
	[usersArray removeAllObjects];
	[segmentedControl setSelectedSegmentIndex:0];
	[self searchBarCancelButtonClicked:nil];
}


#pragma mark - Refresh and load more methods

- (void) refreshTable
{
	// Code to actually refresh goes here.
    self.videoTable.pullLastRefreshDate = [NSDate date];
    self.videoTable.pullTableIsRefreshing = NO;
	[self.videoTable reloadData];
}

- (void) loadMoreDataToTable
{
    // Code to actually load more data goes here.
    self.videoTable.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
	[[ManagerFlickogramAPI videoExemplar] getExploreVideosOnPage:@"1" withDelegate:self];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

@end
