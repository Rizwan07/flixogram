//
//  FlickogramCaptureManager.h
//  Flickogram
//


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class FlickogramRecorder;
@protocol FlickogramCaptureManagerDelegate;

@interface FlickogramCaptureManager : NSObject

@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, assign) AVCaptureVideoOrientation orientation;
@property (nonatomic, strong) AVCaptureDeviceInput *videoInput;
@property (nonatomic, strong) AVCaptureDeviceInput *audioInput;
@property (nonatomic, strong) FlickogramRecorder *recorder;
@property (nonatomic, assign) id deviceConnectedObserver;
@property (nonatomic, assign) id deviceDisconnectedObserver;
@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, assign) id <FlickogramCaptureManagerDelegate> delegate;

- (BOOL) setupSession;
- (void) startRecording;
- (void) stopRecording;
- (BOOL) toggleCamera;
- (NSUInteger) cameraCount;
- (NSUInteger) micCount;
- (void)toggleFlashMode:(BOOL)status;
- (void)stopOperation;
- (NSURL *) tempFileURL;

@end

// These delegate methods can be called on any arbitrary thread. If the delegate does something with the UI when called, make sure to send it to the main thread.
@protocol FlickogramCaptureManagerDelegate <NSObject>
@optional
- (void) captureManager:(FlickogramCaptureManager *)captureManager didFailWithError:(NSError *)error;
- (void) captureManagerRecordingBegan:(FlickogramCaptureManager *)captureManager;
- (void) captureManagerRecordingFinished:(FlickogramCaptureManager *)captureManager;
- (void) captureManagerDeviceConfigurationChanged:(FlickogramCaptureManager *)captureManager;
@end
