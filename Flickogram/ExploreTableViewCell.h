//
//  ExploreTableViewCell.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
@interface ExploreTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *searchString;
@property (nonatomic, weak) IBOutlet UILabel *username;
@property (nonatomic, weak) IBOutlet UILabel *userRealName;
@property (nonatomic, weak) IBOutlet EGOImageView *thumbUser;
@property (nonatomic, weak) IBOutlet UILabel *countVideos;
- (void)setPhotoUser:(NSString *)photoUser;
- (void)setPlaceholder;

@end
