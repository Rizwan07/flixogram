//
//  AboutViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"

@interface AboutViewController : BaseViewController <FlickogramNotificationsDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewAbout;
@property (nonatomic, weak) IBOutlet UITextView *textView;

@end
