//
//  MainViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"
#import "GData.h"
#import "SingletonAuthorizationUser.h"
#import "EGOImageView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "LBYouTubePlayerViewController.h"
#import "CommentsTableViewController.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "DropDownMenuViewController.h"
#import "PullTableView.h"
#import "NIAttributedLabel.h"

@protocol MainViewDelegate <NSObject>
- (void)getFollowingVideosAfterLogin;
@end

@interface MainViewController : BaseViewController <FlickogramVideoDelegate,
													UITableViewDataSource, 
													UITableViewDelegate,	
													LBYouTubePlayerControllerDelegate,
													UIActionSheetDelegate,
													UIAlertViewDelegate,
                                                    UIWebViewDelegate,
													CommentsTableViewDelegate,
													PullTableViewDelegate,
                                                    NIAttributedLabelDelegate,
                                                    UITextFieldDelegate>
{
	NSDictionary *statesSharingInSocialNetworks;
	NSString *captionForSharing;
	NSString *linkForSharing;
	NSURL *videoURLForSharing;
	GDataServiceTicket *mUploadTicket;
	NSInteger currentPage;
	BOOL isShowMore;
	BOOL isUploadVideoFile;
	CGFloat uploadPercentage;	
	NSString *currentServiceName;
	UIProgressView *progressView;
	UIBarButtonItem *refreshItem;

	GTMOAuth2Authentication *authToken;
	GTMOAuth2Authentication *authTokenVimeo;
	
	DropDownMenuViewController *dropDownMenu;
	BOOL  isFullScreen;
	
	NSInteger currentCellSectionWithPlayingVideo;
	BOOL isPlayingVideoNow;
	
	NSString *videoIDForReporting;
}

@property (nonatomic, strong) IBOutlet PullTableView *table;
@property (nonatomic, strong) NSMutableArray *followingVideos;
@property (nonatomic, strong) NSString *tempDirectoryPath;
@property (nonatomic, strong) MPMoviePlayerController *player;

- (void)resetData;
- (void)openSession;
- (void)shareTwitter;
@end
