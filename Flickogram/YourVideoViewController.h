//
//  YourVideoViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"
#import "PullTableView.h"

@interface YourVideoViewController : BaseViewController <UITableViewDataSource,
														UITableViewDelegate,
														FlickogramAuthorizationDelegate,
														PullTableViewDelegate>
{
	UISegmentedControl *segmentedControl;
	NSMutableArray *videosArray;
	NSInteger currentPage;
	BOOL isShowMore;
}

@property (nonatomic, weak) IBOutlet PullTableView *table;

@end
