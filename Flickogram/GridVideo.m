
#import "GridVideo.h"
#import "JMImageCache.h"

@implementation GridVideo
@synthesize scroll = _scroll;
@synthesize isShowMoreButton = _isShowMoreButton;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		tempValue = 0;
		tempValueY = 0;
		currentNumberButton = 0;
		
		currentVideo = 1;
		currentIndexArray = 0;
		self.isShowMoreButton = NO;
		startPoint = self.frame.origin;
		self.scroll = [[UIScrollView alloc] initWithFrame:self.frame];
		[self addSubview:self.scroll];
		[self createShowMoreButton];
	}
    return self;
}

- (void)showGridVideoInQuantity:(NSInteger)countVideo fromDictionary:(NSDictionary *)videos
{
	CGRect newRect = CGRectMake(0, 0, 0, 0);
	NSInteger sizeBetweenVideo = 5;
	
	for (int i = 0; i < countVideo; i++) {
		NSString *numberVideoString = [NSString stringWithFormat:@"%d",i];
		switch (currentVideo) {
			case 1: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 60, 40);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 2: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 30, 40);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 3: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 60, 40);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				tempValue = startPoint.y + newRect.size.height;
				currentVideo++;
				break;
			}
			case 4: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 144, 80);
				startPoint.x = self.frame.origin.x;
				tempValueY = newRect.origin.y + newRect.size.height + sizeBetweenVideo;
				startPoint.y = tempValue + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 5: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 40, 40);
				tempValue =	startPoint.y;
				startPoint.y = startPoint.y + newRect.size.height + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 6: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 40, 40);
				startPoint.y = tempValue;
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 7: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 115, 85);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				startPoint.y = tempValueY;
				currentVideo++;
				break;
			}
			case 8: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 60, 45);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 9: {
				newRect = CGRectMake (startPoint.x, startPoint.y, 40, 45);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 10: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 33, 45);
				startPoint.x = self.frame.origin.x;
				startPoint.y = newRect.origin.y + newRect.size.height + sizeBetweenVideo;
				currentVideo = 1;
				break;
			}
		}
		NSDictionary *video = [NSDictionary dictionaryWithDictionary:[videos objectForKey:numberVideoString]];
		NSString *urlImage = [video objectForKey:@"thumbnail"];
		
		NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
		NSValue *rectObj = [NSValue valueWithCGRect:newRect];
		[parameters setObject:rectObj forKey:@"Frame"];
		[parameters setObject:urlImage forKey:@"URL"];
		
		
		EGOImageView *thumb = [[EGOImageView alloc] initWithFrame:newRect];
		thumb.imageURL = [NSURL URLWithString:urlImage];
		self.scroll.contentSize = CGSizeMake(self.frame.size.width, newRect.origin.y + newRect.size.height + 10);
		[self.scroll addSubview:thumb];
		
		currentNumberButton = i;
	}
}

- (void)showGridVideoInQuantity:(NSInteger)countVideo fromArray:(NSArray *)videos
{
	CGRect newRect = CGRectMake(0, 0, 0, 0);
	NSInteger sizeBetweenVideo = 5;
	
	for (int i = currentIndexArray; i < countVideo; i++) {
		switch (currentVideo) {
			case 1: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 60, 40);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 2: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 30, 40);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 3: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 60, 40);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				tempValue = startPoint.y + newRect.size.height;
				currentVideo++;
				break;
			}
			case 4: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 144, 80);
				startPoint.x = self.frame.origin.x;
				tempValueY = newRect.origin.y + newRect.size.height + sizeBetweenVideo;
				startPoint.y = tempValue + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 5: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 40, 40);
				tempValue =	startPoint.y;
				startPoint.y = startPoint.y + newRect.size.height + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 6: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 40, 40);
				startPoint.y = tempValue;
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 7: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 115, 85);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				startPoint.y = tempValueY;
				currentVideo++;
				break;
			}
			case 8: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 60, 45);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 9: {
				newRect = CGRectMake (startPoint.x, startPoint.y, 40, 45);
				startPoint.x = newRect.origin.x + newRect.size.width + sizeBetweenVideo;
				currentVideo++;
				break;
			}
			case 10: {
				newRect = CGRectMake(startPoint.x, startPoint.y, 33, 45);
				startPoint.x = self.frame.origin.x;
				startPoint.y = newRect.origin.y + newRect.size.height + sizeBetweenVideo;
				currentVideo = 1;
				break;
			}
		}
		NSDictionary *video = [videos objectAtIndex:i];
		NSString *urlImage = [video objectForKey:@"thumbnail"];
		
		EGOImageButton *thumbVideo = [[EGOImageButton alloc] initWithPlaceholderImage:[UIImage imageNamed:@"Thumbnail-frame"]];
		thumbVideo.frame = newRect;
		thumbVideo.tag = currentIndexArray;
		[thumbVideo addTarget:self action:@selector(touchUpInsideThumbVideo:) forControlEvents:UIControlEventTouchUpInside];
		[thumbVideo setImageURL:[NSURL URLWithString:urlImage]];
		[self.scroll addSubview:thumbVideo];
		
		if (self.isShowMoreButton) {
			CGRect showMoreRect = showMoreButton.frame;
			showMoreButton.frame = CGRectMake(showMoreRect.origin.x,
											  newRect.origin.y + newRect.size.height + 10,
											  showMoreRect.size.width,
											  showMoreRect.size.height);
			showMoreButton.hidden = NO;
			self.scroll.contentSize = CGSizeMake(self.frame.size.width,
												 showMoreButton.frame.origin.y + showMoreButton.frame.size.height + 10);
		} else {
			self.scroll.contentSize = CGSizeMake(self.frame.size.width, newRect.origin.y + newRect.size.height + 10);
		}
		currentIndexArray++;
	}

}

- (void)resetData
{
	tempValue = 0;
	tempValueY = 0;
	currentNumberButton = 0;
	
	currentVideo = 1;
	currentIndexArray = 0;
	startPoint = self.frame.origin;
	
	[self.scroll removeFromSuperview];
	self.scroll = nil;
	self.scroll = [[UIScrollView alloc] initWithFrame:self.frame];
	[self addSubview:self.scroll];
	[self createShowMoreButton];
}

- (void)createShowMoreButton
{
	//Add show more button
	showMoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[showMoreButton setTitle:@"Show More" forState:UIControlStateNormal];
	showMoreButton.frame = CGRectMake(121, 0, 79, 44);
	showMoreButton.showsTouchWhenHighlighted = YES;
	showMoreButton.titleLabel.font = [UIFont italicSystemFontOfSize:15];
	[showMoreButton addTarget:self action:@selector(touchInsideShowMoreButton) forControlEvents:UIControlEventTouchUpInside];
	showMoreButton.hidden = YES;
	[self.scroll addSubview:showMoreButton];
}

- (void)loadThumbnailVideo:(NSMutableDictionary *)parameters
{
	NSString *urlImage = [parameters objectForKey:@"URL"];
	NSValue *frameObj = [parameters objectForKey:@"Frame"];
	CGRect newFrame = [frameObj CGRectValue];
	
	NSData *data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: urlImage]];
	UIImage *thumbImage = [UIImage imageWithData:data];
	UIImageView *thumb = [[UIImageView alloc] initWithImage:thumbImage];
	thumb.frame = newFrame;
	
	[self.scroll addSubview:thumb];
}

- (void)touchUpInsideThumbVideo:(id)sender
{
	EGOImageButton *currentButton = (EGOImageButton *)sender;
	NSInteger selectedIndex = currentButton.tag;
	[self.delegate showVideoByIndexArray:selectedIndex];
}

- (void)touchInsideShowMoreButton
{
	[self.delegate showMoreVideos];
}
@end

