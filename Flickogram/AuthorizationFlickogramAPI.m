//
//  AuthorizationFlickogramAPI.m
//  Flickogram
//


#import "AuthorizationFlickogramAPI.h"
#import "AFNetworking.h"
#import "FlickogramConstantsURLAPI.h"

@implementation AuthorizationFlickogramAPI
@synthesize delegate = _delegate;

- (void)loginOnServerWithEmail:(NSString *)email 
					  password:(NSString *)password
					  delegate:(id<FlickogramAuthorizationDelegate>) delegate;
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:email forKey:@"email"];
	[parameters setObject:password forKey:@"password"];

	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:LoginMethodAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
									success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
										NSString *status = [JSON objectForKey:@"status"];
										if ([status isEqualToString:@"SUCCESS"]) {
											[delegate authorizationSuccessWithRecivedData:[JSON objectForKey:@"data"]];
										} else {
											NSString *error = [JSON objectForKey:@"error_message"];
											[delegate authorizationFailedWithError:error];
										}
									} 
							        failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
										NSLog(@"%@", error);
										NSString *errorString = @"Please confirm you have an internet connection and try again";
										[delegate authorizationFailedWithError:errorString];
									}];
    [operation start];
}

- (void)signupOnServerWithEmail:(NSString *)email 
					   password:(NSString *)password 
						  phone:(NSString *)phone
						picture:(UIImage *)picture 
					   username:(NSString *)username 
					   delegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:email forKey:@"email"];
    [parameters setObject:password forKey:@"password"];
    [parameters setObject:phone forKey:@"phone"];
	[parameters setObject:username forKey:@"user_name"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];

	NSData *imageData = UIImageJPEGRepresentation(picture, 0.5);
	NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" 
																		 path:SignupMethodAPI 
																   parameters:parameters
													constructingBodyWithBlock: 
									^(id <AFMultipartFormData>formData) {
		[formData appendPartWithFileData:imageData name:@"picture" fileName:@"avatar.jpg" mimeType:@"multipart/form-data"];
	}];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate signupSuccess];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate signupFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){ 
																							 NSLog(@"%@",error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate signupFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)viewProfileUserID:(NSString *)userID 
				 viewerID:(NSString *)viewerID 
				   onPage:(NSString *)page 
				 delegate:(id<FlickogramAuthorizationDelegate>)delegate;
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:viewerID forKey:@"viewer_id"];
	[parameters setObject:page forKey:@"page"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:UserProfileAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate getUserProfileSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate getUserProfileFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate getUserProfileFailedWithError:errorString];
																						 }];
    [operation start];

}

- (void)followUnFollowUserID:(NSString *)userID 
				  followerID:(NSString *)followerID 
				  indexArray:(NSInteger)currentIndex
				withDelegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:followerID forKey:@"follower_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:FollowUnFollowUserAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request 
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate followUnFollowUserSuccesWithRecivedData:JSON indexArray:currentIndex];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate followUnFollowUserFailedWithError:error];
																							 }
																						 } 
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate followUnFollowUserFailedWithError:errorString];
																						 }];
    [operation start];

}

- (void)updateProfilePictureForUserID:(NSString *)userID
							  picture:(UIImage *)picture
							 delegate:(id<FlickogramAuthorizationDelegate>)delegate;
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:userID forKey:@"user_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	
	NSData *imageData = UIImageJPEGRepresentation(picture, 0.5);
	NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST"
																		 path:UpdateProfilePictureAPI
																   parameters:parameters
													constructingBodyWithBlock:
									^(id <AFMultipartFormData>formData) {
										[formData appendPartWithFileData:imageData name:@"picture" fileName:@"avatar.jpg" mimeType:@"multipart/form-data"];
									}];
	
	AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate updateProfilePictureSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate updateProfilePictureFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate updateProfilePictureFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)deleteProfilePictureForUserID:(NSString *)userID
							 delegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:DeleteProfilePictureAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate deleteProfilePictureSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate deleteProfilePictureFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate deleteProfilePictureFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)updatePublicProfileForUserID:(NSString *)userID
							username:(NSString *)username
						   firstname:(NSString *)firstname
							lastname:(NSString *)lastname
							 website:(NSString *)website
							delegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:username forKey:@"user_name"];
	[parameters setObject:firstname forKey:@"first_name"];
	[parameters setObject:lastname forKey:@"last_name"];
	[parameters setObject:website forKey:@"website"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:UpdatePublicPofileAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate updatePublicProfileSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate updatePublicProfileFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate updatePublicProfileFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)updatePrivateProfileForUserID:(NSString *)userID
							birthdate:(NSString *)birthdate
							   gender:(NSString *)gender
								phone:(NSString *)phone
								email:(NSString *)email
							 delegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:birthdate forKey:@"birthdate"];
	[parameters setObject:gender forKey:@"gender"];
	[parameters setObject:phone forKey:@"phone_no"];
	[parameters setObject:email forKey:@"email"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:UpdatePrivateProfileAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate updatePrivateProfileSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate updatePrivateProfileFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate updatePrivateProfileFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)updateUserVideosPrivateForUserID:(NSString *)userID
						videosArePrivate:(NSString *)state
								delegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"user_id"];
	[parameters setObject:state forKey:@"videosArePrivate"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:UserVideosPrivateAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSString *status = [JSON objectForKey:@"status"];
																							 if ([status isEqualToString:@"SUCCESS"]) {
																								 [delegate updateUserVideosArePrivateSuccessWithRecivedData:JSON];
																							 } else {
																								 NSString *error = [JSON objectForKey:@"error_message"];
																								 [delegate updateUserVideosArePrivateFailedWithError:error];
																							 }
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSLog(@"%@", error);
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate updateUserVideosArePrivateFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)blockUser:(NSString *)userID
		blockerID:(NSString *)blockerID
		 delegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"blockee_id"];
	[parameters setObject:blockerID forKey:@"blocker_id"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:BlockUserAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							[delegate blockUserSuccessWithRecivedData:JSON];
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate blockUserFailedWithError:errorString];
																						 }];
    [operation start];
}

- (void)reportUser:(NSString *)userID
		reporterID:(NSString *)reporterID
		   reasonReport:(NSString *)reason
		  delegate:(id<FlickogramAuthorizationDelegate>)delegate
{
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setObject:userID forKey:@"reportee_id"];
	[parameters setObject:reporterID forKey:@"reporter_id"];
	[parameters setObject:reason forKey:@"reason"];
	
	NSURL *serviceURL = [NSURL URLWithString:BaseURL];
	AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:serviceURL];
	NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET" path:ReportUserAPI parameters:parameters];
    
    AFJSONRequestOperation * operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
																						 success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
																							 NSLog(@"%@", JSON);
																							 [delegate reportUserSuccessWithRecivedData:JSON];
																						 }
																						 failure:^(NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON){
																							 NSString *errorString = @"Please confirm you have an internet connection and try again";
																							 [delegate reportUserFailedWithError:errorString];
																						 }];
    [operation start];

}

@end
