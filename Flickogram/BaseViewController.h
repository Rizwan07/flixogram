//
//  ViewController.h
//  Flickogram
//
//  Created by Andrey Kudievskiy on 20.07.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"


@interface BaseViewController : GAITrackedViewController

@end
