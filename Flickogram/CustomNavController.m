//
//  CustomNavController.m
//  Flickogram
//
//  Created by Rizwan on 1/10/14.
//
//

#import "CustomNavController.h"
#import "MenuViewController.h"
#import "ECSlidingViewController.h"

@implementation CustomNavController

- (id)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        
        if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
            [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"header7"] forBarMetrics:UIBarMetricsDefault];
        }
        else {
            [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"header"] forBarMetrics:UIBarMetricsDefault];
            [self.navigationBar setBackgroundColor:[UIColor whiteColor]];
        }
        
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIColor whiteColor],UITextAttributeTextColor,
                                                   [UIColor blackColor], UITextAttributeTextShadowColor,
                                                   [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset, nil];
        
        [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]])
    {
        //self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isMenuVC"]) {
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isMenuVC"];
            
            MenuViewController *menuVC = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
            CustomNavController *customNavController = [[CustomNavController alloc] initWithRootViewController:menuVC];
            self.slidingViewController.underLeftViewController = customNavController;
        }
    }
    
    //  if (![self.slidingViewController.underRightViewController isKindOfClass:[UnderRightViewController class]]) {
    //    self.slidingViewController.underRightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"UnderRight"];
    //  }
    
//    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
}

@end
