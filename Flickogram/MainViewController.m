//
//  MainViewController.m
//  Flickogram

// Flickgram old facebook id 329609543791583


#import "MainViewController.h"
#import "FlickogramConstants.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "GDataServiceGoogleYouTube.h"
#import "GDataEntryYouTubeUpload.h"
#import "MainTableViewCell.h"
#import "HeaderTableView.h"
#import "LikersTableViewController.h"
#import "FlickogramAuthorization.h"
#import "AuthorizationViewController.h"
#import "CustomTabBarController.h"
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>

#import "XCDYouTubeVideoPlayerViewController.h"
#import "GDataEntryYouTubeChannel.h"

#import "CustomNavController.h"

#import "OptionsViewController.h"
#import "LikedVideoMostLikedVideoViewController.h"
#import "TrendingPopularViewController.h"
#import "ProfileViewController.h"
#import "ExploreViewController.h"
#import "AboutViewController.h"

#import "AppDelegate.h"
#import "YTVimeoExtractor.h"

#import "ECSlidingViewController.h"

#import "WebYoutubeViewController.h"

@interface MainViewController () {
    ACAccountStore *_accountStore;
    int currentSelectedIndex;
    NSString *videoTitle;
    UITextField *dummyTextField;
    IBOutlet UIButton *btnCross;
    
    IBOutlet UIWebView *webViewYoutube;
    
}
- (void)getListVideo;
- (void)showAuthDialog;
- (void)reportVideoWithReason:(NSString *)reasonReport;
- (IBAction)crossBtnPressed:(id)sender;
@end

@implementation MainViewController
@synthesize followingVideos = _followingVideos;
@synthesize table = _table;
@synthesize tempDirectoryPath = _tempDirectoryPath;
@synthesize player = _player;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UIImage *selectedImage = [UIImage imageNamed:@"tab1-active"];
		UIImage *unselectedImage = [UIImage imageNamed:@"tab1-inactive"];
		UITabBarItem *tabBarItem = [[UITabBarItem alloc] init];
		[tabBarItem setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
		self.tabBarItem = tabBarItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.screenName = @"Home";
    
    UIImageView *imageViewTitle = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 76, 40)];
    [imageViewTitle setImage:[UIImage imageNamed:@"logo-nav"]];
    
	self.navigationItem.titleView = imageViewTitle;
    
    currentSelectedIndex = 0;
    videoTitle = @"";
    
	//set Refresh button
	UIImage *refreshButtonImage = [UIImage imageNamed:@"icon-referesh"];
	UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[refreshButton setBackgroundImage:refreshButtonImage forState:UIControlStateNormal];
	refreshButton.frame = CGRectMake(0.0, 0.0, 25, 25);
	[refreshButton addTarget:self action:@selector(reloadVideo) forControlEvents:UIControlEventTouchUpInside];
	refreshItem = [[UIBarButtonItem alloc] initWithCustomView:refreshButton];
	self.navigationItem.rightBarButtonItem = refreshItem;

	//set Drop Down menu button
	UIImage *dropDownButtonImage = [UIImage imageNamed:@"icon-menu"];
	UIButton *dropDownButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[dropDownButton setBackgroundImage:dropDownButtonImage forState:UIControlStateNormal];
	dropDownButton.frame = CGRectMake(0.0, 0.0, 25, 20);
	[dropDownButton addTarget:self action:@selector(showDropDownMenu) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem	*dropDownItem = [[UIBarButtonItem alloc] initWithCustomView:dropDownButton];
//    dropDownItem.tintColor = [UIColor whiteColor];
	self.navigationItem.leftBarButtonItem = dropDownItem;
	
	//add observer for UploadVideoViewController
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(uploadVideoOnServer:)
												 name:NameNotificationUploadVideoOnServer
											   object:nil];
	
	//add observer for authorizations
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(getListVideo)
												 name:NameNotificationGetFollowingVideos
											   object:nil];
	
    //add observer for menu notificaton
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(getMenu:)
												 name:NameNotificationMenu
											   object:nil];

    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(youTubeStarted:) name:@"UIMoviePlayerControllerDidEnterFullscreenNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(youTubeFinished:) name:@"UIMoviePlayerControllerDidExitFullscreenNotification" object:nil];

    
	//pre-set
	//status for video playing
	currentCellSectionWithPlayingVideo = 0;
	isPlayingVideoNow = NO;
	
	uploadPercentage = 0;
	isUploadVideoFile = NO;
	progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(22, 45, 292, 9)];
	progressView.progressTintColor = [UIColor redColor];
	
	isShowMore = NO;
	currentPage = 1;
	self.followingVideos = [[NSMutableArray alloc] init];
	
	//drop down menu
	isFullScreen = NO;
	dropDownMenu = [[DropDownMenuViewController alloc] init];
	CGRect dropDownRect = dropDownMenu.view.frame;
	dropDownRect.origin.y -= dropDownRect.size.height;
	dropDownRect.size.height += 44;
	dropDownMenu.view.frame = dropDownRect;
    
    [dropDownMenu.mostLiked setAdjustsImageWhenHighlighted:NO];
    
	[dropDownMenu.bottomButton addTarget:self action:@selector(showDropDownMenu) forControlEvents:UIControlEventTouchUpInside];
	[dropDownMenu.setting addTarget:self action:@selector(showSetting) forControlEvents:UIControlEventTouchUpInside];
	[dropDownMenu.liked addTarget:self action:@selector(showLikedVideos) forControlEvents:UIControlEventTouchUpInside];
	[dropDownMenu.mostLiked addTarget:self action:@selector(showMostLiked) forControlEvents:UIControlEventTouchUpInside];
	[dropDownMenu.popular addTarget:self action:@selector(showPopularVideos) forControlEvents:UIControlEventTouchUpInside];
	[dropDownMenu.trending addTarget:self action:@selector(showTrendingVideos) forControlEvents:UIControlEventTouchUpInside];
	[dropDownMenu.about addTarget:self action:@selector(showAbout) forControlEvents:UIControlEventTouchUpInside];
    [dropDownMenu.logout addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
	
	//showAuthViewController
	BOOL isAuthCredentials = [FlickogramAuthorization checkSaveAuthCredentials];
	if (!isAuthCredentials) {
		[self showAuthDialog];
	} else {
		NSDictionary *data = [FlickogramAuthorization getAuthCredentials];
		[[SingletonAuthorizationUser authorizationUser] setUserID:[data objectForKey:@"id"]];
		[[SingletonAuthorizationUser authorizationUser] setUserName:[data objectForKey:@"user_name"]];
		[[SingletonAuthorizationUser authorizationUser] setProfilePicture:[data objectForKey:@"profile_picture"]];
		[self getListVideo];
	}
	
	videoIDForReporting = [[NSString alloc] init];
	
	//pull to refresh
	self.table.pullArrowImage = [UIImage imageNamed:@"whiteArrow"];
	self.table.pullBackgroundColor = [UIColor clearColor];
	self.table.pullTextColor = [UIColor whiteColor];
	self.table.loadMoreEnabled = NO;
}

-(void)youTubeStarted:(NSNotification *)notification{
    
    NSLog(@"full screen enter");
}

-(void)youTubeFinished:(NSNotification *)notification{
    NSLog(@"full screen exit");
    [self crossBtnPressed:btnCross];
}


- (void)showAuthDialog
{
	AuthorizationViewController *authorization = [[AuthorizationViewController alloc] init];
	CustomNavController *authorizationNavigationController = [[CustomNavController alloc] initWithRootViewController:authorization];
	[self.tabBarController presentViewController:authorizationNavigationController animated:NO completion:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
	if (isPlayingVideoNow) {
		if (!self.player.fullscreen) {
			isPlayingVideoNow = NO;
			[self.player stop];
			[self removeMovieNotificationHandlers];
			self.player = nil;
			[self.table reloadData];
		}
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//get following videos
- (void)getListVideo 
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	if (userID == nil) {
		userID = @"";
	}
	
	UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];

	UIBarButtonItem *activityItem = [[UIBarButtonItem alloc] initWithCustomView:activity];
	self.navigationItem.rightBarButtonItem = activityItem;
	[activity startAnimating];
		
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *page = [NSString stringWithFormat:@"%d", currentPage];
	[[ManagerFlickogramAPI videoExemplar] getFollowingVideosForUserID:userID onPage:page withDelegate:self];
}

//restore start state for new user
- (void)resetData
{
	[self.followingVideos removeAllObjects];
	[self.table reloadData];
	[self showAuthDialog];
}

#pragma mark - Get Following Videos Delegate
- (void)getFollowingVideosSuccesWithRecivedData:(NSMutableDictionary *)data
{
	if ([data objectForKey:@"total_videos"]) {
		[NSThread detachNewThreadSelector:@selector(parseVideoData:) toTarget:self withObject:data];
	} else if (self.table.pullTableIsRefreshing) {
		[self refreshTable];
	}
	self.navigationItem.rightBarButtonItem = refreshItem;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)getFollowingVideosFailedWithError:(NSString *)error
{
	[self refreshTable];
	self.navigationItem.rightBarButtonItem = refreshItem;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Get Video Details Delegate
- (void)getVideoDetailsSuccesWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)index
{
	if (!(index < [self.followingVideos count])) return; //if will refresh
	NSString *getVideoID = [[data objectForKey:@"video_data"] objectForKey:@"id"];
	NSString *currentVideoID = [[self.followingVideos objectAtIndex:index] objectForKey:@"id"];
	if ([getVideoID isEqualToString:currentVideoID]) {
		NSInteger countComments = [[[data objectForKey:@"video_data"] objectForKey:@"comments"] integerValue];
		NSInteger countLikes = [[[data objectForKey:@"video_data"] objectForKey:@"likes"] integerValue];
		
		if (countLikes) {
			[[self.followingVideos objectAtIndex:index] setObject:[data objectForKey:@"video_likes"] forKey:@"video_likes"];
		} 
		if (countComments) {
			[[self.followingVideos objectAtIndex:index] setObject:[data objectForKey:@"video_comments"] forKey:@"video_comments"];
		}
	}
    [self.table reloadData];
}

- (void)getVideoDetailsFailedWithError:(NSString *)error
{
	NSLog(@"%@", error);
}

#pragma mark - Like Video Delegate
- (void)likeVideoSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *username = [[SingletonAuthorizationUser authorizationUser] userName];
	NSString *profilePicture = [[SingletonAuthorizationUser authorizationUser] profilePicture];
	
	NSMutableDictionary *videoData = [self.followingVideos objectAtIndex:currentIndex];
	NSInteger countLikes = [[videoData objectForKey:@"likes"] integerValue];

	if (isPlayingVideoNow) {
		isPlayingVideoNow = NO;
		[self.player stop];
		[self removeMovieNotificationHandlers];
		self.player = nil;
	}
	
	if (countLikes) {
		NSString *keyLikeData = [NSString stringWithFormat:@"%d", countLikes];
		NSMutableDictionary *newLikeData = [[NSMutableDictionary alloc] init];
		[[videoData objectForKey:@"video_likes"] setObject:newLikeData forKey:keyLikeData];
		[[[videoData objectForKey:@"video_likes"] objectForKey:keyLikeData] setObject:userID forKey:@"user_id"];
		[[[videoData objectForKey:@"video_likes"] objectForKey:keyLikeData] setObject:profilePicture forKey:@"profile_picture"];
		[[[videoData objectForKey:@"video_likes"] objectForKey:keyLikeData] setObject:username forKey:@"user_name"];
		
		countLikes++;
		NSString *newCountLikes = [NSString stringWithFormat:@"%d", countLikes];
		[videoData setObject:newCountLikes forKey:@"likes"];
		[self.followingVideos replaceObjectAtIndex:currentIndex withObject:videoData];
		[self.table reloadData];
	} else {
		NSMutableDictionary *newVideoLikes = [[NSMutableDictionary alloc] init];
		[videoData setObject:newVideoLikes forKey:@"video_likes"];
		
		NSMutableDictionary *keyLikeData = [[NSMutableDictionary alloc] init];
		[[videoData objectForKey:@"video_likes"] setObject:keyLikeData forKey:@"0"];
		[[[videoData objectForKey:@"video_likes"] objectForKey:@"0"] setObject:userID forKey:@"user_id"];
		[[[videoData objectForKey:@"video_likes"] objectForKey:@"0"] setObject:profilePicture forKey:@"profile_picture"];
		[[[videoData objectForKey:@"video_likes"] objectForKey:@"0"] setObject:username forKey:@"user_name"];
		
		[videoData setObject:@"1" forKey:@"likes"];
		[self.followingVideos replaceObjectAtIndex:currentIndex withObject:videoData];
		[self.table reloadData];
	}
}

- (void)likeVideoFiledWithError:(NSString *)error
{
	NSLog(@"%@", error);
}

#pragma mark - Remove Video Delegate
- (void)removeVideoSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSDictionary *videoData = [self.followingVideos objectAtIndex:currentIndex];
	NSString *selectedUserID = [videoData objectForKey:@"user_id"];
	NSString *authorizedUserID = [[SingletonAuthorizationUser authorizationUser] userID];
	if ([selectedUserID isEqualToString:authorizedUserID]) {
		[self.followingVideos removeObjectAtIndex:currentIndex];
		[self.table reloadData];
	}
}

-(void)removeVideoFailedWithError:(NSString *)error
{	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Add Comments Delegate
- (void)addCommentSuccessWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex
{
	NSMutableDictionary *commentDetails = [data objectForKey:@"comment_details"];
	NSMutableDictionary *currentVideo = [self.followingVideos objectAtIndex:currentIndex];

	NSString *getVideoID = [commentDetails objectForKey:@"video_id"];
	NSString *currentVideoID = [currentVideo objectForKey:@"id"];
	
	if (isPlayingVideoNow) {
		isPlayingVideoNow = NO;
		[self.player stop];
		[self removeMovieNotificationHandlers];
		self.player = nil;
	}
	
	if ([getVideoID isEqualToString:currentVideoID]) {

		NSTimeInterval interval = [[NSDate date]timeIntervalSince1970];

		NSString *dateAdded = [NSString stringWithFormat:@"%d", (int)interval];
		[commentDetails setObject:dateAdded forKey:@"date_added"];
		
		NSInteger countComments = [[currentVideo objectForKey:@"comments"] integerValue];
		
		if (countComments) {
			NSDictionary *commentsDictionary = [currentVideo objectForKey:@"video_comments"];
			NSMutableArray *comments = [self parseDictionaryComments:commentsDictionary];
			[comments insertObject:commentDetails atIndex:0];
			NSDictionary *newComments = [self indexKeyedDictionaryFromArray:comments];
			
			[currentVideo setObject:newComments forKey:@"video_comments"];

			countComments++;
			NSString *newCountComments = [NSString stringWithFormat:@"%d", countComments];
			[currentVideo setObject:newCountComments forKey:@"comments"];
			
			[self.followingVideos replaceObjectAtIndex:currentIndex withObject:currentVideo];
			[self.table reloadData];
		} else {
			NSMutableDictionary *newVideoComments = [[NSMutableDictionary alloc] init];
			[currentVideo setObject:newVideoComments forKey:@"video_comments"];
			[[currentVideo objectForKey:@"video_comments"] setObject:commentDetails forKey:@"0"];
			[currentVideo setObject:@"1" forKey:@"comments"];
			[self.followingVideos replaceObjectAtIndex:currentIndex withObject:currentVideo];
			[self.table reloadData];
		}
	}
}

- (NSMutableArray *)parseDictionaryComments:(NSDictionary *)comments
{
	NSMutableArray *commentsArray = [[NSMutableArray alloc] init];
	NSInteger currentComment = 0;
	BOOL isMore = YES;
	while (isMore) {
		NSString *numberComment = [NSString stringWithFormat:@"%d", currentComment];
		if ([comments objectForKey:numberComment]) {
			[commentsArray addObject:[comments objectForKey:numberComment]];
			currentComment++;
		} else {
			isMore = NO;
		}
	}
	return commentsArray;
}

- (NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array
{
	id objectInstance;
	NSUInteger indexKey = 0;
	
	NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
	for (objectInstance in array) {
		[mutableDictionary setObject:objectInstance forKey:[NSString stringWithFormat:@"%d",indexKey]];
		indexKey++;
	}
	return mutableDictionary;
}

- (void)addCommentFailedWithError:(NSString *)error
{
	NSLog(@"%@", error);
}


#pragma mark - LBYouTubePlayerViewControllerDelegate

-(void)youTubePlayerViewController:(LBYouTubePlayerViewController *)controller didSuccessfullyExtractYouTubeURL:(NSURL *)videoURL {
	NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:controller.currentIndexArray];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:path];
	CGRect rectActivity = cell.activity.frame;
	cell.activity.frame = CGRectMake(rectActivity.origin.x + 10,
									 rectActivity.origin.y,
									 rectActivity.size.width,
									 rectActivity.size.height);
	
	self.player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
	[self.player prepareToPlay];
	self.player.shouldAutoplay = YES;
	[self installMovieNotificationObservers];
	[self.player.view setFrame: CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height) ]; 
	[cell.playbackBackground addSubview: self.player.view];
	cell.playbackBackground.hidden = NO;
	[self installMovieNotificationObservers];
	[self.player play];
	
	[cell addSubview:cell.activity];
	cell.activity.hidden = NO;
	[cell.activity startAnimating];
}

-(void)youTubePlayerViewController:(LBYouTubePlayerViewController *)controller failedExtractingYouTubeURLWithError:(NSError *)error {
    NSLog(@"Failed loading video due to error:%@", error);
}

#pragma mark - CommentsTableView Delegate
- (void)updateCommentsDictionary:(NSDictionary *)videoComments forIndexArray:(NSInteger)currentIndex
{
	NSDictionary *videoData = [self.followingVideos objectAtIndex:currentIndex];
	if ([videoData objectForKey:@"video_comments"]) {
		[[self.followingVideos objectAtIndex:currentIndex] setObject:videoComments forKey:@"video_comments"];
		NSInteger countComments = [[videoData objectForKey:@"comments"] integerValue];
		countComments--;
		NSString *countCommentString = [NSString stringWithFormat:@"%d", countComments];
		[[self.followingVideos objectAtIndex:currentIndex] setObject:countCommentString forKey:@"comments"];
		[self.table reloadData];
	}
}

- (void)removeVideoCommentKeyForIndexArray:(NSInteger)currentIndex
{
	if ([[self.followingVideos objectAtIndex:currentIndex] objectForKey:@"video_comments"]) {
		[[self.followingVideos objectAtIndex:currentIndex] removeObjectForKey:@"video_comments"];
		[[self.followingVideos objectAtIndex:currentIndex] setObject:@"0" forKey:@"comments"];
		[self.table reloadData];
	}
		
}

#pragma mark - Reflick Delegate
- (void)reflickVideoSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[self reloadVideo];
}

- (void)reflickVideoFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
													message:error
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Helper methods
- (void)parseVideoData:(NSMutableDictionary *)data
{
	@autoreleasepool {
		isShowMore = [[data objectForKey:@"show_more"] boolValue];
		
		//work with pull refresh data
		if (self.table.pullTableIsRefreshing) {
			NSString *lastDateAdded = @"0";
			if ([self.followingVideos count] > 0) {
				lastDateAdded = [[self.followingVideos objectAtIndex:0] objectForKey:@"date_added"];
			}
			
			NSInteger currentVideo = 0;
			BOOL isVideoOnPage = YES;
			while (isVideoOnPage) {
				NSString *numberVideo = [NSString stringWithFormat:@"%d", currentVideo];
				if ([[data objectForKey:@"videos"] objectForKey:numberVideo]) {
					NSMutableDictionary *videoData = [[data objectForKey:@"videos"] objectForKey:numberVideo];
					NSString *newDateAdded = [videoData objectForKey:@"date_added"];
					
					//compare last date with new date and stop if equal
					if ([lastDateAdded isEqualToString:newDateAdded]) {
						[self refreshTable];
						return;
					} else if ([self.followingVideos count] > currentVideo) {
                        
						[self.followingVideos insertObject:videoData atIndex:currentVideo];
						NSInteger countLikes = [[videoData objectForKey:@"likes"] integerValue];
						NSInteger countComments = [[videoData objectForKey:@"comments"] integerValue];
						
						if (countLikes || countComments) {
							NSInteger currentIndex = [self.followingVideos count] - 1;
							[[ManagerFlickogramAPI videoExemplar] getVideoDetailsOfVideoID:[videoData objectForKey:@"id"]
																					userID:[videoData objectForKey:@"user_id"]
																				indexArray:currentIndex
																			  withDelegate:self];
						}
						if (isShowMore) {
							[self.followingVideos removeLastObject];
						}

					}
					currentVideo++;
				} else {
					isVideoOnPage = NO;
				}
			}
			[self refreshTable];
			return;
		}
		
		//standart load
		NSInteger currentVideo = 0;
		BOOL isVideoOnPage = YES;
		while (isVideoOnPage) {
			NSString *numberVideo = [NSString stringWithFormat:@"%d", currentVideo];
			if ([[data objectForKey:@"videos"] objectForKey:numberVideo]) {
				NSMutableDictionary *videoData = [[data objectForKey:@"videos"] objectForKey:numberVideo];
				[self.followingVideos addObject:videoData];
				
				NSInteger countLikes = [[videoData objectForKey:@"likes"] integerValue];
				NSInteger countComments = [[videoData objectForKey:@"comments"] integerValue];
	
				if (countLikes || countComments) {
					NSInteger currentIndex = [self.followingVideos count] - 1;
					[[ManagerFlickogramAPI videoExemplar] getVideoDetailsOfVideoID:[videoData objectForKey:@"id"]
																			userID:[videoData objectForKey:@"user_id"]
																		indexArray:currentIndex
																	  withDelegate:self];
				}
				currentVideo++;
			} else {
				isVideoOnPage = NO;
			}
		}
		[self performSelectorOnMainThread:@selector(reloadTableOnMainThread) withObject:nil waitUntilDone:YES];
	}
}
   
- (void)reloadTableOnMainThread
{
	[self.table reloadData];
}

- (void)reloadVideo 
{
    isUploadVideoFile = NO;
    if (mUploadTicket) {
        [mUploadTicket cancelTicket];
    }
    [self setUploadTicket:nil];
    self.table.refreshEnabled = YES;
    
	currentPage = 1;
	[self.followingVideos removeAllObjects];
	[self.table reloadData];
	[self getListVideo];
}

- (void)getMenu:(NSNotificationCenter *)menuOptions {
    
    NSString *menuOption = [menuOptions valueForKey:@"object"];
    
    if ([menuOption isEqualToString:@"Setting"]) {
        [self showSetting];
    }
    else if ([menuOption isEqualToString:@"Most Viewed"]) {
        [self showMostLiked];
    }
    else if ([menuOption isEqualToString:@"Popular"]) {
        [self showPopularVideos];
    }
    else if ([menuOption isEqualToString:@"Like"]) {
        [self showLikedVideos];
    }
    else if ([menuOption isEqualToString:@"Trend"]) {
        [self showTrendingVideos];
    }
    else if ([menuOption isEqualToString:@"About"]) {
        [self showAbout];
    }
    else if ([menuOption isEqualToString:@"Log out"]) {
        [self logout];
    }
    
}

- (void)showDropDownMenu
{
    
    [self.slidingViewController anchorTopViewTo:ECRight];
    return;
	isFullScreen = !isFullScreen;  //ivar

	CustomTabBarController *customTabBar = (CustomTabBarController *) self.tabBarController;
    //[dropDownMenu.scrollView setContentSize:CGSizeMake(320, 305)];

	if (isFullScreen) {
		NSString *usernameAuth = [[SingletonAuthorizationUser authorizationUser] userName];
		NSString *thumbAuth = [[SingletonAuthorizationUser authorizationUser] profilePicture];
		dropDownMenu.username.text = usernameAuth;
		[dropDownMenu.userThumb setImageURL:[NSURL URLWithString:thumbAuth]];
		[self.tabBarController.view addSubview:dropDownMenu.view];
	
		[UIView animateWithDuration:0.5 animations: ^{
			CGRect newRect = CGRectMake(dropDownMenu.view.frame.origin.x,
										dropDownMenu.view.frame.origin.y + dropDownMenu.view.frame.size.height + 20,
										dropDownMenu.view.frame.size.width,
										dropDownMenu.view.frame.size.height);
			dropDownMenu.view.frame = newRect;
		} completion: ^(BOOL finished) {
			customTabBar.tabBar.hidden = YES;
			customTabBar.button.hidden = YES;
		}];
	} else {
		[UIView animateWithDuration:0.5 animations: ^{
			CGRect newRect = CGRectMake(dropDownMenu.view.frame.origin.x,
										dropDownMenu.view.frame.origin.y - dropDownMenu.view.frame.size.height - 20,
										dropDownMenu.view.frame.size.width,
										dropDownMenu.view.frame.size.height);
			dropDownMenu.view.frame = newRect;
			customTabBar.tabBar.hidden = NO;
			customTabBar.button.hidden = NO;
		} completion: ^(BOOL finished) {
			[dropDownMenu.view removeFromSuperview];
		}];
	}
}

#pragma mark - Drop Down Menu Events

- (void)showSetting {
    
	//[self showDropDownMenu];
    [self.slidingViewController resetTopView];
	OptionsViewController *options = [[OptionsViewController alloc] init];
	[self.navigationController pushViewController:options animated:YES];
}

- (void)showLikedVideos
{
	//[self showDropDownMenu];
    [self.slidingViewController resetTopView];
	LikedVideoMostLikedVideoViewController *likedVideo = [[LikedVideoMostLikedVideoViewController alloc] init];
	likedVideo.selectedVideoAPI = UserLikedVideosAPI;
	[self.navigationController pushViewController:likedVideo animated:YES];
}

- (void)showMostLiked
{
	//[self showDropDownMenu];
    [self.slidingViewController resetTopView];
	LikedVideoMostLikedVideoViewController *mostLikedVideo = [[LikedVideoMostLikedVideoViewController alloc] init];
	mostLikedVideo.selectedVideoAPI = MostLikedVideosAPI;
	[self.navigationController pushViewController:mostLikedVideo animated:YES];
}

- (void)showPopularVideos
{
	//[self showDropDownMenu]; //hide drop-down menu
    [self.slidingViewController resetTopView];
	TrendingPopularViewController *popularVideos = [[TrendingPopularViewController alloc] init];
	popularVideos.selectedVideoAPI = PopularVideosAPI;
	[self.navigationController pushViewController:popularVideos animated:YES];
}

- (void)showTrendingVideos
{
	//[self showDropDownMenu]; //hide drop-down menu
    [self.slidingViewController resetTopView];
	TrendingPopularViewController *trendingVideos = [[TrendingPopularViewController alloc] init];
	trendingVideos.selectedVideoAPI = TrendingVideosAPI;
	[self.navigationController pushViewController:trendingVideos animated:YES];
}

- (void)showAbout
{
	//[self showDropDownMenu]; //hide drop-down menu
    [self.slidingViewController resetTopView];
	AboutViewController *about = [[AboutViewController alloc] init];
	[self.navigationController pushViewController:about animated:YES];
}

- (void)logout {
    
    UIAlertView *alertLogout = [[UIAlertView alloc] initWithTitle:@"Are you sure?"
                                                          message:nil
                                                         delegate:self
                                                cancelButtonTitle:nil
                                                otherButtonTitles:@"Cancel", @"Log out", nil];
    [alertLogout setTag:10000];
    [alertLogout show];
}

- (void)logoutAndReset
{
	[FlickogramAuthorization removeAuthCredentials];
	
	//MainViewController == index 0
	UINavigationController *mainNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:0];
	[mainNavigationController popToRootViewControllerAnimated:NO];
	MainViewController *mainViewController = (MainViewController *) [mainNavigationController.viewControllers objectAtIndex:0];
	[mainViewController resetData];
	
	//ExploreViewController == index 1
	UINavigationController *exploreNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:1];
	[exploreNavigationController popToRootViewControllerAnimated:NO];
	ExploreViewController *exploreViewController = (ExploreViewController *) [exploreNavigationController.viewControllers objectAtIndex:0];
	[exploreViewController resetData];
	
	//NotificationViewController == index 3
	UINavigationController *notificationNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:1];
	[notificationNavigationController popToRootViewControllerAnimated:NO];
	
	//ProfileViewController == index 4
	UINavigationController *profileNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:4];
	[profileNavigationController popToRootViewControllerAnimated:NO];
	ProfileViewController *profileViewController = (ProfileViewController *) [profileNavigationController.viewControllers objectAtIndex:0];
	[profileViewController resetData];
	
	AppDelegate *delegate =	(AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate.rootViewController selectTabBarIndex:0];
}

#pragma mark - Actions for HeaderTableView button
- (void)touchReflickButton:(id)sender
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	UIButton *reflick = (UIButton *)sender;
	NSInteger currentVideo = reflick.tag;
	NSString *videoID = [[self.followingVideos objectAtIndex:currentVideo] objectForKey:@"id"];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI videoExemplar] reflickVideoForUserID:authUser videoID:videoID delegate:self];
}

#pragma mark - Actions for MainTableViewCell buttons
- (void)toggleLikeStateInSection:(id)sender
{
	UIButton *currentButton = (UIButton *)sender;
	NSInteger sectionTable = currentButton.tag;
	NSMutableDictionary *data = [self.followingVideos objectAtIndex:sectionTable];
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *videoID = [data objectForKey:@"id"];
	BOOL isAlreadyLiked = NO;
	NSInteger countLikes = [[data objectForKey:@"likes"] integerValue];
	if (countLikes) {
		if ([data objectForKey:@"video_likes"]) {
			for (NSString *likeKey in [data objectForKey:@"video_likes"]) {
				NSString *likeID = [[[data objectForKey:@"video_likes"] objectForKey:likeKey] objectForKey:@"user_id"];
				if ([userID isEqualToString:likeID]) {
					isAlreadyLiked = YES;
					break;
				} else {
					isAlreadyLiked = NO;
				}
			}
//			if (isAlreadyLiked) {
//				NSLog(@"AlreadyLiked");
//			}
		}
	}
	if (!isAlreadyLiked) {
		[[ManagerFlickogramAPI videoExemplar] likeVideoForUserID:userID videoID:videoID indexArray:sectionTable withDelegate:self];
	}
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"request %@", request);
    
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"error %@", error);
}

- (void)touchShowMoreButton:(id)sender 
{
	UIButton *currentButton = (UIButton *)sender;
	NSInteger sectionTable = currentButton.tag;
	NSDictionary *videoData = [self.followingVideos objectAtIndex:sectionTable];
	
	LikersTableViewController *likers = [[LikersTableViewController alloc] init];
	likers.videoLikes = [NSDictionary dictionaryWithDictionary:[videoData objectForKey:@"video_likes"]];
	[self.navigationController pushViewController:likers animated:YES];
}

- (void)touchPlayVideoButton:(id)sender
{
	UIButton *currentButton = (UIButton *)sender;
	NSInteger sectionTable = currentButton.tag;
	NSDictionary *videoData = [self.followingVideos objectAtIndex:sectionTable];
	
	if (isPlayingVideoNow) {
		isPlayingVideoNow = NO;
		currentCellSectionWithPlayingVideo = 0;
		[self.player stop];
		[self removeMovieNotificationHandlers];
		self.player = nil;
		[self.table reloadData];
	}
	
	NSString *videoType = [videoData objectForKey:@"video_type"];
	NSString *videoUrl = [videoData objectForKey:@"video_url"]; //@"http://player.vimeo.com/video/67433949"
    
    
    if ([videoType isEqualToString:@"youtube"]) {
        
        //videoUrl = @"https://www.youtube.com/v/sWcKtJj5_oY";
        
        videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
        if ([videoUrl rangeOfString:@"https"].location == NSNotFound) {
            videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"http" withString:@"https"];
        }
        
        NSLog(@"url %@, component %@", videoUrl, [videoUrl lastPathComponent]);

//        NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:sectionTable];
//		MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:path];
//		CGRect rectActivity = cell.activity.frame;
//		cell.activity.frame = CGRectMake(rectActivity.origin.x + 10,
//										 rectActivity.origin.y,
//										 rectActivity.size.width,
//										 rectActivity.size.height);
        
        NSString *embedHTML = @"\
        <html><head>\
        <style type=\"text/css\">\
        body {\
        background-color: transparent;\
        color: black;\
        }\
        </style>\
        </head><body style=\"margin:0\">\
        <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
        width=\"%0.0f\" height=\"%0.0f\"></embed>\
        </body></html>";
        
        NSString *html = [NSString stringWithFormat:embedHTML, videoUrl, webViewYoutube.frame.size.width, webViewYoutube.frame.size.height];
        
        [webViewYoutube loadHTMLString:html baseURL:nil];
        
        [webViewYoutube setHidden:NO];
        [btnCross setHidden:NO];
        
//        NSString *embedHTML = @"\
//        <html><head>\
//        <style type=\"text/css\">\
//        body {\
//        background-color: transparent;\
//        color: white;\
//        }\
//        </style>\
//        </head><body style=\"margin:0\">\
//        <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
//        width=\"%0.0f\" height=\"%0.0f\"></embed>\
//        </body></html>";
        
//        NSString *embedHTML = [NSString stringWithFormat:@"\
//        <html><head>\
//        <style type=\"text/css\">\
//        body {\
//        background-color: transparent;\
//        color: white;\
//        }\
//        </style>\
//        </head><body style=\"margin:0\">\
//        <video id=\"player\" width=\"100\" height=\"100\" controls=\"controls\" webkit-playsinline>\
//        <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
//        width=\"%0.0f\" height=\"%0.0f\"></embed>\
//        </body></html>", videoUrl, 100.0f, 100.0f];
        
        
//        <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
//        width=\"%0.0f\" height=\"%0.0f\"></embed>\

//        <iframe id=\"ytplayer\" type=\"text/html\" width=\"100%\"  src=\"http://www.youtube.com/%@?autoplay=1&controls=0&playsinline=1&modestbranding=1&rel=0&showinfo=0&autohide=1&html5=1\" webkit-playsinline frameborder=\"0\"></iframe>
        
//        NSString *html = [NSString stringWithFormat:embedHTML, videoUrl, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height];
        
//        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height)];
//        [webView setBackgroundColor:[UIColor clearColor]];
//        [webView setAllowsInlineMediaPlayback:YES];
//        [cell.myWebView loadHTMLString:embedHTML baseURL:nil];
//        cell.playbackBackground.hidden = NO;
//        cell.activity.hidden = NO;
//        [cell.activity startAnimating];

	}
    else {
        
        NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:sectionTable];
		MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:path];
		CGRect rectActivity = cell.activity.frame;
		cell.activity.frame = CGRectMake(rectActivity.origin.x + 10,
										 rectActivity.origin.y,
										 rectActivity.size.width,
										 rectActivity.size.height);
        
        //NSString *strNewUrl = [NSString stringWithFormat:@"http://vimeo.com/%@", [videoUrl lastPathComponent]];
        NSString *strNewUrl = [NSString stringWithFormat:@"http://player.vimeo.com/video/%@", [videoUrl lastPathComponent]];
        //NSString *strNewUrl = [NSString stringWithFormat:@"http://vimeo.com/api/rest/v2?format=json&method=vimeo.videos.getThumbnailUrls&video_id=%@", [videoUrl lastPathComponent]];
        if ([videoType isEqualToString:@"vimeo"]) {
            
//            NSString *embedHTML = @"\
//            <html><head>\
//            <style type=\"text/css\">\
//            body {\
//            background-color: transparent;\
//            color: black;\
//            }\
//            </style>\
//            </head><body style=\"margin:0\">\
//            <embed id=\"vimeo\" src=\"%@\" type=\"application/x-shockwave-flash\" \
//            width=\"%0.0f\" height=\"%0.0f\"></embed>\
//            </body></html>";
//            
//            NSString *html = [NSString stringWithFormat:embedHTML, strNewUrl, webViewYoutube.frame.size.width, webViewYoutube.frame.size.height];
//            
//            [webViewYoutube loadHTMLString:html baseURL:nil];
            [webViewYoutube loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strNewUrl]]];
            [webViewYoutube setHidden:NO];
            [btnCross setHidden:NO];
            
            
//            [YTVimeoExtractor fetchVideoURLFromURL:strNewUrl //@"http://vimeo.com/58600663"
//                                           quality:YTVimeoVideoQualityLow
//                                 completionHandler:^(NSURL *videoURL, NSError *error, YTVimeoVideoQuality quality) {
//                                     if (error) {
//                                         // handle error
//                                         NSLog(@"Video URL: %@", [videoURL absoluteString]);
//                                     } else {
//                                         NSLog(@"Video URL: %@", [videoURL absoluteString]);
//                                         dispatch_async(dispatch_get_main_queue(), ^{
//                                         
//                                             
//                                             self.player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
//                                             //[self.player prepareToPlay];
//                                             //self.player.movieSourceType = MPMovieSourceTypeStreaming;
//                                             [self installMovieNotificationObservers];
//                                             
//                                             [self.player.view setFrame: CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height) ];
//                                             [cell.playbackBackground addSubview:self.player.view];
//                                             cell.playbackBackground.hidden = NO;
//                                             
//                                             [cell addSubview:cell.activity];
//                                             cell.activity.hidden = NO;
//                                             [cell.activity startAnimating];
//                                             
//                                             [self.player play];
//                                         });
//                                     }
//                                 }];
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{

            self.player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:videoUrl]];
                
            //self.player.movieSourceType = MPMovieSourceTypeStreaming;
            [self installMovieNotificationObservers];
            
            [self.player.view setFrame: CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height) ];
            [cell.playbackBackground addSubview:self.player.view];
            cell.playbackBackground.hidden = NO;
            
            [cell addSubview:cell.activity];
            cell.activity.hidden = NO;
            [cell.activity startAnimating];
            
            [self.player play];
            });
        }
        
		
	}
	isPlayingVideoNow = YES;
	currentCellSectionWithPlayingVideo = sectionTable;
}

- (void)touchSendButton:(id)sender
{
	UIButton *sendButton = (UIButton *)sender;
	NSInteger currentSection = sendButton.tag;
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentSection];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
    dummyTextField = cell.textField;
    dummyTextField.delegate = self;
	NSString *comment = cell.textField.text;
	if ([comment length]) {
		NSDictionary *videoData = [self.followingVideos objectAtIndex:currentSection];
		NSString *videoID = [videoData objectForKey:@"id"];
		NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];	
		[[ManagerFlickogramAPI videoExemplar] addCommentOnVideo:videoID userID:userID comment:comment indexArray:currentSection withDelegate:self];
		cell.textField.text = @"";
		[cell.textField resignFirstResponder];
	}
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [dummyTextField resignFirstResponder];
}

- (void)touchShowCommentsButton:(id)sender
{
	UIButton *currentButton = (UIButton *)sender;
	NSInteger sectionTable = currentButton.tag;
	NSDictionary *videoData = [self.followingVideos objectAtIndex:sectionTable];
	CommentsTableViewController *comments = [[CommentsTableViewController alloc] init];
	comments.currentIndexArray = sectionTable;
	comments.delegate = self;
	if ([videoData objectForKey:@"video_comments"]) {
		comments.userComments = [[videoData objectForKey:@"video_comments"] copy];
	}
	[self.navigationController pushViewController:comments animated:YES];
}

- (void)touchStreamButton:(id)sender
{
	UIButton *currentButton = (UIButton *)sender;
	NSInteger sectionTable = currentButton.tag;
	NSDictionary *data = [self.followingVideos objectAtIndex:sectionTable];
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *selectedVideoUserID = [data objectForKey:@"user_id"];
	videoIDForReporting = [data objectForKey:@"id"];
	
	if ([userID isEqualToString:selectedVideoUserID]) {
//		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Video Options" 
//															delegate:self 
//												   cancelButtonTitle:@"Cancel" 
//											  destructiveButtonTitle:@"Delete" otherButtonTitles:nil];
//		action.tag = sectionTable;
//		[action showFromTabBar:self.tabBarController.tabBar];
		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Video Options"
															delegate:self
												   cancelButtonTitle:@"Cancel"
											  destructiveButtonTitle:@"Delete" otherButtonTitles:@"Save in Gallery",
                                 @"Share with Youtube",//@"Email Video",
                                 @"Share with Facebook",
                                 @"Share with Twitter", nil];
        
        //NSLog(@"buttons %@", [action valueForKey:@"_buttons"]);
        
        [[[action valueForKey:@"_buttons"] objectAtIndex:1] setImage:[UIImage imageNamed:@"btn-save.png"] forState:UIControlStateNormal];
        [[[action valueForKey:@"_buttons"] objectAtIndex:2] setImage:[UIImage imageNamed:@"btn-youtube.png"] forState:UIControlStateNormal];
        [[[action valueForKey:@"_buttons"] objectAtIndex:3] setImage:[UIImage imageNamed:@"btn-fb.png"] forState:UIControlStateNormal];
        [[[action valueForKey:@"_buttons"] objectAtIndex:4] setImage:[UIImage imageNamed:@"btn-twitter.png"] forState:UIControlStateNormal];
        
        currentSelectedIndex = sectionTable;
		action.tag = sectionTable;
		[action showFromTabBar:self.tabBarController.tabBar];
	} else {
		UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"In order to share other's video you have to reflix it"//@"Video Options"
															delegate:self
												   cancelButtonTitle:@"Cancel"
											  destructiveButtonTitle:@"Flag for review"
												   otherButtonTitles:nil];
		action.tag = 88888888;
		[action showFromTabBar:self.tabBarController.tabBar];
	}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (actionSheet.tag == 88888888) {
		if (buttonIndex == 0) {
			UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Reason for reporting:"
																delegate:self
													   cancelButtonTitle:@"Cancel"
												  destructiveButtonTitle:nil
													   otherButtonTitles:@"Nudity", @"Copyright", @"Terms of Use Violation", nil];
			action.tag = 99999999;
			[action showFromTabBar:self.tabBarController.tabBar];
		}
	} else if (actionSheet.tag == 99999999) {
		if (buttonIndex == 0) {
			[self reportVideoWithReason:@"Nudity"];
		} else if (buttonIndex == 1) {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Who owns this photo?"
																message:nil
															   delegate:self
													  cancelButtonTitle:@"Cancel"
													  otherButtonTitles:@"Submit", nil];
				alert.alertViewStyle = UIAlertViewStylePlainTextInput;
				alert.tag = 99999999;
				[alert show];
			} else if (buttonIndex == 2) {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reason for reporting:"
																message:nil
															   delegate:self
													  cancelButtonTitle:@"Cancel"
													  otherButtonTitles:@"Submit", nil];
				alert.alertViewStyle = UIAlertViewStylePlainTextInput;
				alert.tag = 99999999;
				[alert show];
			}
	}
    else {
		if (buttonIndex == 0) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Deletion"
															message:@"Delete this video?"
														   delegate:self
												  cancelButtonTitle:@"Don't Delete"
												  otherButtonTitles:@"Delete", nil];
			alert.tag = actionSheet.tag;
			[alert show];
		}
        else if (buttonIndex == 1) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:linkForSharing]];
                //CGFloat time = [NSDate timeIntervalSinceReferenceDate];
                NSString *fileStr = [@"Documents/FlixogramVid" stringByAppendingPathExtension:@"mov"];
                NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:fileStr];
                NSError *error;
                BOOL isSaved = [data writeToFile:filePath options:NSDataWritingAtomic error:&error];
                if (isSaved) {
                    NSFileManager *fileManage = [[NSFileManager alloc] init];
                    if ([fileManage fileExistsAtPath:filePath isDirectory:NO]) {
                        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(filePath)) {
                            UISaveVideoAtPathToSavedPhotosAlbum(filePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
                        }
                    }
                }
                else {
                    NSLog(@"not saved with error %@", error.localizedDescription);
                }
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

            });
            linkForSharing = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_url"];
        }
        else if (buttonIndex == 2) {
            
            WebYoutubeViewController *webYoutubeVC = [[WebYoutubeViewController alloc] initWithNibName:@"WebYoutubeViewController" bundle:nil];
            [self.navigationController pushViewController:webYoutubeVC animated:YES];
            
//            videoURLForSharing = [NSURL URLWithString:[[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_url"]];
//            if ([[videoURLForSharing absoluteString] rangeOfString:@"youtube"].location != NSNotFound) {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Youtube video can not share on Youtube" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
//            }
//            else {
//                
//                GTMOAuth2Authentication *auth = [self getAuthYoutube];
//				if (auth) {
//					// if the auth object contains an access token, didAuth is now true
//					//BOOL didAuth = [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:@"My App:Flickogram"
//					//														   authentication:auth];
////					if (didAuth) {
//						// retain the authentication object, which holds the auth tokens
//						// we can determine later if the auth object contains an access token
//						// by calling its -canAuthorize method
//						BOOL isSignedIn = [auth canAuthorize];
//						if (isSignedIn) {
//                            
//                            if (isUploadVideoFile) {
//                                
//                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                                                message:@"Already Uploading Video"
//                                                                               delegate:nil
//                                                                      cancelButtonTitle:@"OK"
//                                                                      otherButtonTitles:nil];
//                                [alert show];
//                                return;
//
//                            }
//                            
//                            self.table.refreshEnabled = NO;
//                            isUploadVideoFile = YES;
//                            uploadPercentage = 0;
//                            currentServiceName = @"Youtube Service";
//                            [self.table reloadData];
//                            authToken = [statesSharingInSocialNetworks objectForKey:@"AuthTokenYoutube"];
//                            
//                            //videoIDForReporting = [NSURL URLWithString:[[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_url"]];
//                            
//                            if (authToken == nil || authToken == NULL) {
//                                authToken = [self getAuthYoutube];
//                                if (authToken == nil || authToken == NULL) {
//                                    
//                                    [self loginOnYoutube];
//                                }
//                                else {
//                                    [self getAuthTokenYoutube];
//                                }
//                            }
//                            else {
//                                [self getAuthTokenYoutube];
//                            }
//						}
//					}
//                    else {
//                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                                        message:@"Please login with your Youtube account in Profile sharing settings"
//                                                                       delegate:nil
//                                                              cancelButtonTitle:@"OK"
//                                                              otherButtonTitles:nil];
//                        [alert show];
//                    }
////				}
////                else {
////                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
////                                                                    message:@"Please login with your Youtube account in Profile settings"
////                                                                   delegate:nil
////                                                          cancelButtonTitle:@"OK"
////                                                          otherButtonTitles:nil];
////                    [alert show];
////                }
//                
//            }
        }
        else if (buttonIndex == 3) {
            captionForSharing = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_description"];
            linkForSharing = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_url"];
            videoTitle = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_title"];
            [self openSession];
            //[self performSelectorInBackground:@selector(openSession) withObject:nil];
        }
        else if (buttonIndex == 4) {
            captionForSharing = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_title"];
            linkForSharing = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_url"];
            //videoTitle = [[self.followingVideos objectAtIndex:currentSelectedIndex] objectForKey:@"video_title"];

            [self shareTwitter];
        }
	}
}

- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    dispatch_async(dispatch_get_main_queue(), ^{

        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Could not save" message:@"In order to save video allow photo gallery permission to Flixogrm" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else {
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:error.localizedDescription
                                                                   message:@"Video saved to Gallery"
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
            [successAlert show];
        }
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == 99999999) {
		if (buttonIndex == 1) {
			NSString *reason = [[alertView textFieldAtIndex:0] text];
			[self reportVideoWithReason:reason];
		}
	} else if (alertView.tag == 10000) {
        if (buttonIndex == 1) {
            [self.slidingViewController resetTopView];
            [self logoutAndReset];
        }
        
    } else {
		if (buttonIndex == 1) {
			NSDictionary *data = [self.followingVideos objectAtIndex:alertView.tag];
			NSString *videoID = [data objectForKey:@"id"];
			NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			[[ManagerFlickogramAPI videoExemplar] removeVideoID:videoID userID:userID indexArray:alertView.tag withDelegate:self];
		}
	}
}

- (void)reportVideoWithReason:(NSString *)reasonReport
{
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI videoExemplar] reportVideo:videoIDForReporting
										   reporterID:userID
										 reasonReport:reasonReport
											 delegate:self];
}

#pragma mark - Report User Delegate
- (void)reportVideoSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Thanks"
														   message:@"We'll review this user as soon as possible."
														  delegate:nil
												 cancelButtonTitle:@"Dismiss"
												 otherButtonTitles:nil];
	[successAlert show];
}

- (IBAction)crossBtnPressed:(id)sender {
    
    [webViewYoutube stopLoading];
    [webViewYoutube setHidden:YES];
    [btnCross setHidden:YES];
}

- (void)reportVideoFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
													message:error
												   delegate:nil
										  cancelButtonTitle:@"Dismiss"
										  otherButtonTitles:nil];
	[alert show];
}

#pragma mark - Install notifications for MPMoviePlayer
/* Register observers for the various movie object notifications. */
-(void)installMovieNotificationObservers
{
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadStateDidChange:)
                                                 name:MPMoviePlayerLoadStateDidChangeNotification
                                               object:self.player];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.player];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mediaIsPreparedToPlayDidChange:)
                                                 name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                               object:self.player];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:self.player];

	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayerDidEnterFullscreen:)
                                                 name:MPMoviePlayerDidEnterFullscreenNotification
                                               object:self.player];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayrDidExitFullscreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.player];
}

#pragma mark Remove Movie Notification Handlers

/* Remove the movie notification observers from the movie object. */
-(void)removeMovieNotificationHandlers
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:self.player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:self.player];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.player];
	[[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerDidEnterFullscreenNotification object:self.player];
	[[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:self.player];
}

#pragma mark Movie Notification Handlers

/*  Notification called when the movie finished playing. */
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    NSNumber *reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	switch ([reason integerValue])
	{
            /* The end of the movie was reached. */
		case MPMovieFinishReasonPlaybackEnded:
            /*
             Add your code here to handle MPMovieFinishReasonPlaybackEnded.
             */
			cell.activity.hidden = YES;
			NSLog(@"Finish");
			break;
            
            /* An error was encountered during playback. */
		case MPMovieFinishReasonPlaybackError:
            NSLog(@"An error was encountered during playback");
			break;
            
            /* The user stopped playback. */
		case MPMovieFinishReasonUserExited:
			cell.activity.hidden = YES;
			NSLog(@"stop");
			break;
            
		default:
			break;
	}
}

/* Handle movie load state changes. */
- (void)loadStateDidChange:(NSNotification *)notification
{
	MPMoviePlayerController *player = notification.object;
	MPMovieLoadState loadState = player.loadState;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	
	/* The load state is not known at this time. */
	if (loadState & MPMovieLoadStateUnknown)
	{
		NSLog(@"LoadStateUnknown");
	}
	
	/* The buffer has enough data that playback can begin, but it
	 may run out of data before playback finishes. */
	if (loadState & MPMovieLoadStatePlayable)
	{
		cell.activity.hidden = YES;
		NSLog(@"LoadStatePlayable");
	}
	
	/* Enough data has been buffered for playback to continue uninterrupted. */
	if (loadState & MPMovieLoadStatePlaythroughOK)
	{
        // Add an overlay view on top of the movie view
		cell.activity.hidden = YES;
		NSLog(@"LoadStatePlaythroughOK");
	}
	
	/* The buffering of data has stalled. */
	if (loadState & MPMovieLoadStateStalled)
	{
		cell.activity.hidden = NO;
		[cell.activity startAnimating];
		NSLog(@"LoadStateStalled");
	}
}

/* Called when the movie playback state has changed. */
- (void) moviePlayBackStateDidChange:(NSNotification*)notification
{
	MPMoviePlayerController *player = notification.object;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	/* Playback is currently stopped. */
	if (player.playbackState == MPMoviePlaybackStateStopped)
	{
		cell.activity.hidden = YES;
		NSLog(@"PlaybackStopped");
	}
	/*  Playback is currently under way. */
	else if (player.playbackState == MPMoviePlaybackStatePlaying)
	{
		cell.activity.hidden = YES;
		NSLog(@"PlaybackPlaying");
	}
	/* Playback is currently paused. */
	else if (player.playbackState == MPMoviePlaybackStatePaused)
	{
		cell.activity.hidden = YES;
		NSLog(@"PlaybackPause");
	}
	/* Playback is temporarily interrupted, perhaps because the buffer
	 ran out of content. */
	else if (player.playbackState == MPMoviePlaybackStateInterrupted)
	{
		NSLog(@"PlaybackInterrupted");
	}
}

/* Notifies observers of a change in the prepared-to-play state of an object
 conforming to the MPMediaPlayback protocol. */
- (void) mediaIsPreparedToPlayDidChange:(NSNotification*)notification
{
	// Add an overlay view on top of the movie view
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	cell.activity.hidden = NO;
	[cell.activity startAnimating];
	NSLog(@"PreparedToPlayDidChange");
}

- (void)moviePlayerDidEnterFullscreen:(NSNotification *)notification
{
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	cell.activity.hidden = YES;
}

- (void)moviePlayrDidExitFullscreen:(NSNotification *)notification
{
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
	MainTableViewCell *cell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPath];
	cell.activity.hidden = YES;
}
 
#pragma mark - TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [self.followingVideos count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{	
	HeaderTableView *header;
	NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"HeaderTableView" owner:nil options:nil];
	for (id currentObject in topLevelObjects) {
		if ([currentObject isKindOfClass:[UIView class]]) {
			header = (HeaderTableView *) currentObject;
			break;
		}
	}
	
	//if now upload video on service
	if (isUploadVideoFile) {
		if (section == 0) {
			return nil;
		}
	} 
	
	header.userName.text = [[self.followingVideos objectAtIndex:section] objectForKey:@"user_name"];
	if ([[self.followingVideos objectAtIndex:section] objectForKey:@"location_name"] != [NSNull null]) {
		header.locationName.text = [[self.followingVideos objectAtIndex:section] objectForKey:@"location_name"];
	}
	header.imageView.placeholderImage = [UIImage imageNamed:@"Profile-thumb"];
	header.imageView.imageURL = [NSURL URLWithString:[[self.followingVideos objectAtIndex:section] objectForKey:@"profile_picture"]];
	
	NSString *currentUser = [[self.followingVideos objectAtIndex:section] objectForKey:@"user_id"];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];

	//show reflick button
	if (![authUser isEqualToString:currentUser]) {
		header.reflickButton.hidden = NO;
		header.reflickButton.tag = section;
		[header.reflickButton addTarget:self action:@selector(touchReflickButton:) forControlEvents:UIControlEventTouchUpInside];
	}
	
	return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	if (isUploadVideoFile) {
		if (section == 0) {
			return 0;
		}
	}
		
	return 46;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (isUploadVideoFile) {
		if (indexPath.section == 0) {
			return 60;
		}
	}
	NSDictionary *data = [self.followingVideos objectAtIndex:indexPath.section];
    int height = 370;
    switch ([[data objectForKey:@"comments"] intValue]) {
        case 0:
            height = 0;
            break;
        case 1:
            height = 20;
            break;
        case 2:
            height = 40;
            break;
        case 3:
            height = 60;
            break;
        default:
            height = 80;
            break;
    }
	return 290 + height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (isUploadVideoFile) {
		if (indexPath.section == 0) {
			static NSString *uploadCell = @"upload";
			UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:uploadCell];
			if (cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:uploadCell];
			}

			cell.backgroundColor = [UIColor blackColor];
			cell.textLabel.textAlignment = NSTextAlignmentCenter;
			cell.textLabel.textColor = [UIColor whiteColor];
			cell.textLabel.text = currentServiceName;
			[cell addSubview:progressView];
			return cell;
		}
	}
	NSDictionary *data = [self.followingVideos objectAtIndex:indexPath.section];
	static NSString *uniqueIdentifier = @"videoCell";
	MainTableViewCell *cell = (MainTableViewCell *)[tableView dequeueReusableCellWithIdentifier:uniqueIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MainTableViewCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (MainTableViewCell *) currentObject;
				NSInteger likeCount = [[data objectForKey:@"likes"] integerValue];
				if (likeCount) {
					if (likeCount > 7) likeCount = 7;
					[cell initLikedImage:likeCount];
				}
				break;
			}
		}
	}
	
    
    
	[cell setDelegateForEGOImageView];
	cell.countComments.text = [NSString stringWithString:[data objectForKey:@"comments"]];
	cell.countLikes.text = [NSString stringWithString:[data objectForKey:@"likes"]];
	cell.countViews.text = [NSString stringWithFormat:@"%@ views",[data objectForKey:@"views"]];
	cell.imageThumbnailVideo.imageURL = [NSURL URLWithString:[data objectForKey:@"thumbnail"]];
	
	cell.likeButton.tag = indexPath.section;
    
    //video_likes
    
	BOOL isLiked = NO;
	NSInteger countLikes = [cell.countLikes.text integerValue];
	if (countLikes) {
		if ([data objectForKey:@"video_likes"]) {
			for (NSString *likeKey in [data objectForKey:@"video_likes"]) {
				NSString *likeID = [[[data objectForKey:@"video_likes"] objectForKey:likeKey] objectForKey:@"user_id"];
				if ([[[SingletonAuthorizationUser authorizationUser] userID] isEqualToString:likeID]) {
					isLiked = YES;
					break;
				} else {
					isLiked = NO;
				}
			}
		}
	}
	[cell.likeButton setSelected:isLiked];
	[cell.likeButton addTarget:self action:@selector(toggleLikeStateInSection:) forControlEvents:UIControlEventTouchUpInside];
	
	cell.playButton.tag = indexPath.section;
	[cell.playButton addTarget:self action:@selector(touchPlayVideoButton:) forControlEvents:UIControlEventTouchUpInside];
    
	cell.streamButton.tag = indexPath.section;
	[cell.streamButton addTarget:self action:@selector(touchStreamButton:) forControlEvents:UIControlEventTouchUpInside];
	
	cell.sendButton.tag = indexPath.section;
	[cell.sendButton addTarget:self action:@selector(touchSendButton:) forControlEvents:UIControlEventTouchUpInside];
    
	cell.showComments.tag = indexPath.section;
	[cell.showComments addTarget:self action:@selector(touchShowCommentsButton:) forControlEvents:UIControlEventTouchUpInside];
	
//    NSString *videoType = [data objectForKey:@"video_type"];
//	NSString *videoUrl = [data objectForKey:@"video_url"];
//    
//    NSLog(@"url %@, component %@", videoUrl, [videoUrl lastPathComponent]);
//    
//    if ([videoType isEqualToString:@"youtube"]) {
//        videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
//        if ([videoUrl rangeOfString:@"https"].location == NSNotFound) {
//            videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"http" withString:@"https"];
//        }
//        
//        NSString *embedHTML = @"\
//        <html><head>\
//        <style type=\"text/css\">\
//        body {\
//        background-color: transparent;\
//        color: black;\
//        }\
//        </style>\
//        </head><body style=\"margin:0\">\
//        <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
//        width=\"%0.0f\" height=\"%0.0f\"></embed>\
//        </body></html>";
//        
////        NSString *youTubeVideoHTML =[NSString stringWithFormat:@"<html><head> <meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = \"292\"/></head> <body style=\"margin-top:0px;margin-left:0px\">\
////        <iframe width= \"%f\" height=\"%f\" src = \"%@?showinfo=0\"frameborder=\"0\" hd=\"1\" allowfullscreen></iframe></div></body></html>", cell.myWebView.frame.size.width, cell.myWebView.frame.size.height, videoUrl];
//
//        
//        NSString *html = [NSString stringWithFormat:embedHTML, videoUrl, cell.myWebView.frame.size.width, cell.myWebView.frame.size.height];
//        
//        UIWebView *webV = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, cell.playbackBackground.frame.size.width, cell.playbackBackground.frame.size.height)];
////        [webView setAllowsInlineMediaPlayback:YES];
////        _webView = cell.myWebView;
//        
//        [cell.playbackBackground addSubview:webV];
//        
//        [webV loadHTMLString:html baseURL:nil];
//        //[cell.myWebView setDelegate:self];
////        [cell.myWebView.scrollView setScrollEnabled:NO];
//        //[cell.myWebView setHidden:NO];
//        
////        [arrWebView replaceObjectAtIndex:indexPath.row withObject:cell.myWebView];
//        
//        [cell.playButton setHidden:YES];
//
//        cell.playbackBackground.hidden = NO;
//	}
//    else {
//        [cell.myWebView setHidden:YES];
//        [cell.playButton setHidden:NO];
//        [cell.playbackBackground setHidden:YES];
//    }
    
    if ([[data objectForKey:@"comments"] intValue] > 0) {
        
        int countComments = [[data objectForKey:@"comments"] intValue];
        
        for (int i = 0; i < countComments && i < 4; i++) {
            NSString *strCommnent = [[[data objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"comments"];
            NSString *strName = [[[data objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",i]] objectForKey:@"user_name"];
            
            if ([strName length] > 0 && [strCommnent length] > 0) {
                
                NIAttributedLabel *lblAttrText = [[NIAttributedLabel alloc] initWithFrame:CGRectMake(22, i*20, 264, 20)];
                [lblAttrText setDelegate:self];
                [lblAttrText setTag:indexPath.section];
                
                if (i == 1 && [[data objectForKey:@"comments"] intValue] > 4) {
                    [lblAttrText setText:[NSString stringWithFormat:@"view all %d messages", [[data objectForKey:@"comments"] intValue]]];
                    [lblAttrText addLink:[NSURL fileURLWithPath:lblAttrText.text] range:[lblAttrText.text rangeOfString:lblAttrText.text]];
                    //[lblAttrText setFont:[UIFont boldSystemFontOfSize:10] range:[lblAttrText.text rangeOfString:strName]];
                    [lblAttrText setFont:[UIFont systemFontOfSize:10]];
                    [lblAttrText setLinkColor:[UIColor lightGrayColor]];
                }
                else {
                    
                    if (i != 0  && [[data objectForKey:@"comments"] intValue] > 4) {
                        
                        strName = [[[data objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"user_name"];
                        strCommnent = [[[data objectForKey:@"video_comments"] objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"comments"];
                        //[lblAttrText setTag:i];
                    }
                    
                    [lblAttrText setText:[NSString stringWithFormat:@"%@ %@", strName, strCommnent]];
                    [lblAttrText addLink:[NSURL fileURLWithPath:strName] range:[lblAttrText.text rangeOfString:strName]];
                    [lblAttrText setFont:[UIFont systemFontOfSize:12]];
                    [lblAttrText setFont:[UIFont boldSystemFontOfSize:14] range:[lblAttrText.text rangeOfString:strName]];
                }
                [lblAttrText setNumberOfLines:0];
                [lblAttrText setLineBreakMode:NSLineBreakByTruncatingTail];
                [lblAttrText setTextColor:[UIColor darkGrayColor]];
                [lblAttrText setBackgroundColor:[UIColor clearColor]];
                [lblAttrText setDeferLinkDetection:YES];

            
                //[lblAttrText setFrame:CGRectMake(22, i*20, 264, 20)];
                
                [cell.viewComments addSubview:lblAttrText];
            }
        }
        
    }
    
//	if ([[self.followingVideos objectAtIndex:indexPath.section] objectForKey:@"video_likes"]) {
//		NSDictionary *profilePicturesLikeUsers = [[self.followingVideos objectAtIndex:indexPath.section] objectForKey:@"video_likes"];
//		NSInteger alreadyLiked = [[data objectForKey:@"likes"] integerValue];
//		if (alreadyLiked) {
//			int i = 0;
//			while  (i < alreadyLiked && i < 7) {
//				NSString *numberLikeUserPhoto = [NSString stringWithFormat:@"%d", i];
//				NSString *pathLikePhoto = [[profilePicturesLikeUsers objectForKey:numberLikeUserPhoto] objectForKey:@"profile_picture"];
//				[cell setLikedImageOnURL:pathLikePhoto forCurrentIndex:i];
//				i++;
//			}
//			if (alreadyLiked > 7) {
//				UIButton *showMoreButton = [cell addShowMoreButton];
//				showMoreButton.tag = indexPath.section;
//				[showMoreButton addTarget:self action:@selector(touchShowMoreButton:) forControlEvents:UIControlEventTouchUpInside];
//			}
//		}
//	}
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == ([self.followingVideos count] - 1)) {
		if (isShowMore) {
			currentPage++;
			isShowMore = NO;
			[self getListVideo];
		}
	}
}

- (void)attributedLabel:(NIAttributedLabel *)attributedLabel didSelectTextCheckingResult:(NSTextCheckingResult *)result atPoint:(CGPoint)point {
    
    NSString *strImageData = [result.URL.relativeString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //NSLog(@"relative String %@", strImageData);
    
    if ([strImageData rangeOfString:@"view all"].location != NSNotFound) {
        [self touchShowCommentsButton:attributedLabel];
    }
    else {
        NSDictionary *data = [[self.followingVideos objectAtIndex:attributedLabel.tag] objectForKey:@"video_comments"];
        NSLog(@"Date %@", data);
        int countComments = [[[self.followingVideos objectAtIndex:attributedLabel.tag] objectForKey:@"comments"] intValue];
        
        for (int i = 0 ; i < countComments && i < 4; i++) {
            
            if (i == 0 || countComments <= 4) {
                if ([[[data objectForKey:[NSString stringWithFormat:@"%d", i]] objectForKey:@"user_name"] isEqualToString:strImageData]) {
                    NSLog(@"found1 %@", strImageData);
                    
                    ProfileViewController *profile = [[ProfileViewController alloc] init];
                    profile.selectedUserID = [[data objectForKey:[NSString stringWithFormat:@"%d", i]] objectForKey:@"user_id"];
                    profile.hasParentNavigationView = YES; //subview for it's view
                    [self.navigationController pushViewController:profile animated:YES];

                    break;
                }
            }
            else {
                if ([[[data objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"user_name"] isEqualToString:strImageData]) {
                    NSLog(@"found3 %@", strImageData);
                    
                    ProfileViewController *profile = [[ProfileViewController alloc] init];
                    profile.selectedUserID = [[data objectForKey:[NSString stringWithFormat:@"%d",countComments-4+i]] objectForKey:@"user_id"];
                    profile.hasParentNavigationView = YES; //subview for it's view
                    [self.navigationController pushViewController:profile animated:YES];

                    
                    break;
                }
            }
        }
        

    }
}

//stop playing video when scroll next cell
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if (isPlayingVideoNow) {
		NSIndexPath *indexPathCell = [NSIndexPath indexPathForRow:0 inSection:currentCellSectionWithPlayingVideo];
		MainTableViewCell *mainCell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPathCell];
		
		if (![self.table.indexPathsForVisibleRows containsObject:indexPathCell] || !mainCell.playbackBackground.subviews) {
			isPlayingVideoNow = NO;
			[self.player stop];
			[self removeMovieNotificationHandlers];
			self.player = nil;
			
			//revome playing subviews
			MainTableViewCell *mainCell = (MainTableViewCell *) [self.table cellForRowAtIndexPath:indexPathCell];
			if (mainCell.playbackBackground.subviews) {
				NSArray *viewsToRemove = mainCell.playbackBackground.subviews;
				for (UIView *view in viewsToRemove) {
					[view removeFromSuperview];
				}
			}
		}
	}
}


#pragma mark - Notifications

//From Uploading and sharing
-(void)uploadVideoOnServer:(NSNotification *)notification
{	
	NSDictionary *dict = [notification userInfo];
	
	captionForSharing = [[dict objectForKey:@"Caption"] copy];
	NSString *userID = [dict objectForKey:@"UserID"];
	NSString *latitude = @"";
	NSString *longitude = @"";
	NSString *location = @"";
	
	if ([dict objectForKey:@"Geotag"]) {
		NSDictionary *geotag = [dict objectForKey:@"Geotag"];
		latitude = [geotag objectForKey:@"lat"];
		longitude = [geotag objectForKey:@"lng"];
		location = [geotag objectForKey:@"name"];
	}
	
	videoURLForSharing = [NSURL URLWithString:[dict objectForKey:@"VideoURL"]];
	
	NSString *videoType;
	UIInterfaceOrientation orientation = [self orientationForTrack:videoURLForSharing];
	if (UIInterfaceOrientationIsLandscape(orientation)) {
		videoType = @"landscape";
	} else {
		videoType = @"portrait";
	}
	statesSharingInSocialNetworks = [[dict objectForKey:@"Sharing"] copy];
	
	self.table.refreshEnabled = NO;
	
	[[ManagerFlickogramAPI videoExemplar] addVideoOnService:videoURLForSharing
												 titleVideo:captionForSharing
													 userID:userID
										   descriptionVideo:@"New video" 
												   tagVideo:@""
												   latitude:latitude
												  longitude:longitude
												   location:location
												  videoType:videoType
											   withDelegate:self];
	
	isUploadVideoFile = YES;
	uploadPercentage = 0;
	currentServiceName = @"Flixogram Service";
	NSMutableDictionary *newDictionary = [[NSMutableDictionary alloc] init];
	[newDictionary setObject:@"YES" forKey:@"Uploading"];
	[self.followingVideos insertObject:newDictionary atIndex:0];
	[self.table reloadData];
}

- (UIInterfaceOrientation)orientationForTrack:(NSURL *)url
{
	AVURLAsset *asset = [AVURLAsset assetWithURL:url];
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
	
    if (size.width == txf.tx && size.height == txf.ty)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}

#pragma mark - Video upload delegate
- (void)updateProgreesUploadWithCurrentValue:(long long)currentValue maxValue:(long long)maxValue
{
	uploadPercentage = (CGFloat) currentValue / (CGFloat) maxValue;
	[progressView setProgress:uploadPercentage];
}

- (void)uploadVideoSuccessWithRecivedData:(NSDictionary *)data
{
	isUploadVideoFile = NO;
	
	linkForSharing = [[data objectForKey:@"video_link"] copy];
	videoURLForSharing = [NSURL URLWithString:[data objectForKey:@"video_link"]];
	BOOL isFacebookSharing = [[statesSharingInSocialNetworks objectForKey:@"FacebookSharing"] boolValue];
	BOOL isTwitterSharing = [[statesSharingInSocialNetworks objectForKey:@"TwitterSharing"] boolValue];
	BOOL isYoutubeSharing = [[statesSharingInSocialNetworks objectForKey:@"YoutubeSharing"] boolValue];

	if (isFacebookSharing) {
//		if (FBSession.activeSession.isOpen) {
//			[self shareFacebook];
//		} else {
			[self openSession];
//		}
	}
	if (isTwitterSharing) {
		[self shareTwitter];
	}
	if (isYoutubeSharing) {
        
        WebYoutubeViewController *webYoutubeVC = [[WebYoutubeViewController alloc] initWithNibName:@"WebYoutubeViewController" bundle:nil];
        [self.navigationController pushViewController:webYoutubeVC animated:YES];
        
        [self reloadVideo];
        
//		self.table.refreshEnabled = NO;
//		isUploadVideoFile = YES;
//		uploadPercentage = 0;
//		currentServiceName = @"Youtube Service";
//		[self.table reloadData];
//		authToken = [statesSharingInSocialNetworks objectForKey:@"AuthTokenYoutube"];
//		[self getAuthTokenYoutube];
	} else {
		self.table.refreshEnabled = YES;
		[self reloadVideo];
	}
}

- (void)uploadVideoFailedWithError:(NSString *)error
{
	isUploadVideoFile = NO;
	if ([self.followingVideos count]) {
		if ([[self.followingVideos objectAtIndex:0] objectForKey:@"Uploading"]) {
			[self.followingVideos removeObjectAtIndex:0];
			self.table.refreshEnabled = YES;
			[self.table reloadData];
		}
	}
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Sharing
//Open session for facebook
- (void)openSession
{
    //NSArray *permissions = @[@"publish_stream", @"publish_actions"];
    NSArray *permissions = @[@"publish_actions"];
	[FBSession openActiveSessionWithPermissions:permissions
                                   allowLoginUI:YES
                              completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
         [FBSession setActiveSession:session];
		 if (error) {
			 UIAlertView *alertView = [[UIAlertView alloc]
									   initWithTitle:nil
									   message:error.localizedDescription
									   delegate:nil
									   cancelButtonTitle:@"OK"
									   otherButtonTitles:nil];
			 [alertView show];
		 } else {
             if (state == FBSessionStateOpen) {
                 
                 [self shareFacebook];
             }
		 }
	 }];  
}
//facebook share
- (void)shareFacebook
{
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    if ([linkForSharing rangeOfString:@"www.youtube.com"].location != NSNotFound) {
        
        NSDictionary *params = @{@"name": videoTitle,
                                 @"link": linkForSharing,//@"video.mov": data,
                                 @"contentType": @"video/quicktime",
                                 @"description": [captionForSharing stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
        [self postToFBWithGraphPath:@"me/feed" andParams:params];
        
    } else {
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:linkForSharing]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            
            if (data) {
                
                NSDictionary *params = @{//@"link" : @"http://developer.avenuesocial.com/azeemsal/flickogram/images/profile_pictures/1390909163.jpg",
                                         @"name": videoTitle,
                                         @"video.mov": data,
                                         @"contentType": @"video/quicktime",
                                         @"description": [captionForSharing stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
                //NSLog(@"hello %@", params);
                [self postToFBWithGraphPath:@"me/videos" andParams:params];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"This video can not upload to Facebook"
                                               delegate:self
                                      cancelButtonTitle:@"OK!"
                                      otherButtonTitles:nil] show];

                });
            }
         }];
    }
//	});
//    // Request access to the Twitter accounts
//    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//        
//        _accountStore = [[ACAccountStore alloc] init];
//        ACAccountType *accountType = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
//        
//        NSDictionary *options = @{
//                                  ACFacebookAppIdKey: @"329609543791583",//@"422711541193686",
//                                  ACFacebookPermissionsKey: @[@"publish_stream", @"publish_actions" ],
//                                  ACFacebookAudienceKey: ACFacebookAudienceFriends
//                                  };
//        
//        [_accountStore requestAccessToAccountsWithType:accountType options:options completion:^(BOOL granted, NSError *error){
//            if (granted) {
//                NSArray *accounts = [_accountStore accountsWithAccountType:accountType];
//                // Check if the users has setup at least one Twitter account
//                if (accounts.count > 0)
//                {
//                    
//                    
//                    ACAccount *fbAccount = [accounts objectAtIndex:0];
//                    
//                    NSURL *videourl = [NSURL URLWithString:@"https://graph.facebook.com/me/videos"];
//                    
//                    //NSString *strVideoPath = [[[[Singleton sharedSingleton] arrData] objectAtIndex:_selectedIndex] name];
//                    NSString *strVideoPath = [[NSBundle mainBundle] pathForResource:@"123" ofType:@"mp4"];
//                    NSURL *videoPathURL = [NSURL fileURLWithPath:strVideoPath isDirectory:NO];
//                    NSData *videoData = [NSData dataWithContentsOfFile:strVideoPath];
//                    
//                    NSDictionary *params = @{
//                                             @"title": @"Flixogram",
//                                             @"description": @"Tulum captured video"
//                                             };
//                    
//                    SLRequest *uploadRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
//                                                                  requestMethod:SLRequestMethodPOST
//                                                                            URL:videourl
//                                                                     parameters:params];
//                    [uploadRequest addMultipartData:videoData
//                                           withName:@"source"
//                                               type:@"video/quicktime"
//                                           filename:[videoPathURL absoluteString]];
//                    
//                    uploadRequest.account = fbAccount;
//                    
//                    [uploadRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
//                        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//                        if(error){
//                            NSLog(@"Error %@", error.localizedDescription);
//                        }else {
//                            NSLog(@"%@", responseString);
//                            
//                        }
//                    }];
//                }
//            }
//            else {
//                NSLog(@"Access denied");
//            }
//        }];
//    }
}

- (void)postToFBWithGraphPath:(NSString *)graphPath andParams:(NSDictionary *)params {
    
    [FBRequestConnection startWithGraphPath:graphPath
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                  NSString *alertText;
                                  if (error) {
                                      alertText = [NSString stringWithFormat:@"error: domain = %@, code = %d", error.domain, error.code];
                                  } else {
                                      alertText = @"Video posted to Facebook";
                                      //alertText = [NSString stringWithFormat:@"Posted action, id: %@", [result objectForKey:@"id"]];
                                  }
                                  // Show the result in an alert
                                  [[[UIAlertView alloc] initWithTitle:@"Result"
                                                              message:alertText
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK!"
                                                    otherButtonTitles:nil] show];
                              });
                          }];
}

//twitter share
- (void)shareTwitter
{
	//First, we need to obtain the account instance for the user's Twitter account

    ACAccountStore *store = [[ACAccountStore alloc] init];
	ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    if ([store respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)]) {
		if( [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] ) {

            [store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
				if (!granted) {
					// The user rejected your request
					NSLog(@"User rejected access to the account.");
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
				}
				else {
					// Grab the available accounts
					NSArray *twitterAccounts = [store accountsWithAccountType:twitterAccountType];
					if ([twitterAccounts count] > 0) {
                        
                        ACAccount *account = [twitterAccounts objectAtIndex:0];
                        // Now make an authenticated request to our endpoint
                        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                        NSString *status = [NSString stringWithFormat:@"%@, %@", [captionForSharing stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                            linkForSharing];
                        [params setObject:status forKey:@"status"];
                        
                        //  The endpoint that we wish to call
                        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1/statuses/update.json"];
                        //  Build the request with our parameter
                        
                        //TWRequest *request = [[TWRequest alloc] initWithURL:url parameters:params requestMethod:TWRequestMethodPOST];
                        SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:params];
                        
                        // Attach the account object to this request
                        [request setAccount:account];
                        [request performRequestWithHandler: ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                            if (!responseData) {
                                // inspect the contents of error
                                NSLog(@"%@", error);
                            }
                            else {
                                NSError *jsonError;
                                NSArray *timeline = [NSJSONSerialization JSONObjectWithData:responseData
                                                                                    options:NSJSONReadingMutableLeaves 
                                                                                      error:&jsonError];
                                if (timeline) {
                                    // at this point, we have an object that we can parse
                                    NSLog(@"%@", timeline);
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        
                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                                    message:@"Video link Tweeted"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                                        [alert show];
                                    });
                                }
                                else {
                                    // inspect the contents of jsonError
                                    NSLog(@"%@", jsonError);
                                }
                            }
                        }];
					}
				}
			}];
        }
        else {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Please setup Twitter account in your settings"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];

        }
	}
}


//Work with Youtube
- (GTMOAuth2Authentication *)getAuthYoutube {
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:kMyClientID clientSecret:kMyClientSecret];
	return auth;
}

- (void)loginOnYoutube
{
	NSString *scope = @"https://gdata.youtube.com";
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	
	GTMOAuth2ViewControllerTouch *viewController;
	viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:scope
																clientID:kMyClientID
															clientSecret:kMyClientSecret
														keychainItemName:kKeychainItemName
																delegate:self
														finishedSelector:@selector(viewController:finishedWithAuth:error:)];
	
	[[self navigationController] pushViewController:viewController animated:YES];
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
	if (error != nil) {
		// Authentication failed
		NSLog(@"%@", error);
	} else {
		// Authentication succeeded
		authToken = auth;
		[self performSelectorOnMainThread:@selector(getAuthTokenYoutube) withObject:nil waitUntilDone:NO];
	}
}

//Share Youtube
- (void)getAuthTokenYoutube
{
	NSString *urlStr = @"https://accounts.google.com/o/oauth2/auth";
	NSURL *url = [NSURL URLWithString:urlStr];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[authToken authorizeRequest:request
				  delegate:self
		 didFinishSelector:@selector(authentication:request:finishedWithError:)];
}

- (void)authentication:(GTMOAuth2Authentication *)auth
               request:(NSMutableURLRequest *)request
     finishedWithError:(NSError *)error {
	if (error != nil) {
		// Authorization failed
		NSLog(@"Auth token = %@", error);
	} else {
		// Authorization succeeded
        NSLog(@"auth %@", auth);
		authToken = auth;
        
//        GDataEntryYouTubeChannel *entyYTChannel = [GDataEntryYouTubeChannel channelEntry];
//        GDataFeedYouTubeChannel *feedYTChannel = [GDataFeedYouTubeChannel channelFeed];
//        
//        NSLog(@"ytchannel %@, %@", entyYTChannel, feedYTChannel);
		[self uploadVideoOnYoutube];
	}
}

- (GDataServiceGoogleYouTube *)youTubeService {
	static GDataServiceGoogleYouTube* service = nil;
	if (!service) {
		service = [[GDataServiceGoogleYouTube alloc] init];
		[service setShouldCacheDatedData:YES];
		[service setServiceShouldFollowNextLinks:YES];
		[service setIsServiceRetryEnabled:YES];
	}
	return service;
}

- (void)uploadVideoOnYoutube
{
    [progressView setProgress:0.0];
    [self setUploadTicket:nil];
    
    NSString *devKey = @"AI39si7GQ3cqX8NdiVh7vJgFU6HNjcedSFcVT877fa2YFBDqLI7RsrvKxamkKAU5PXrPUeLIDujoU6wbYk4D-S-sEthpJARG0A";
	
    GDataServiceGoogleYouTube *service = [self youTubeService];
	[service setAuthorizer:authToken];
	[[service authorizer] setShouldAuthorizeAllRequests:YES];
    [service setYouTubeDeveloperKey:devKey];
    
    if ([[videoURLForSharing host] rangeOfString:@"vimeo"].location == NSNotFound) {
        
        if ([[videoURLForSharing host] rangeOfString:@"youtube"].location != NSNotFound) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Youtube video can not upload on youtube" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            NSString *strVideoUrl = [videoURLForSharing absoluteString];
            
            NSArray *arr = [strVideoUrl componentsSeparatedByString:@"="];
            if ([arr count] > 0) {
                if ([arr[1] rangeOfString:@"&"].location != NSNotFound) {
                    arr = [arr[1] componentsSeparatedByString:@"&"];
                    strVideoUrl = arr[0];
                }
                else {
                    strVideoUrl = arr[1];
                }
            }
            
            NSLog(@"video url %@, id %@", [videoURLForSharing host], strVideoUrl);

            //videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:@"VpZmIiIXuZ0"];
            
            //[self youtubeShareNowWithServiceObj:service data:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://r5---sn-a8au-p5qz.googlevideo.com/videoplayback?ms=au&id=o-AOQqCWTdlD2u_gZpWrKn6_D4bKp7uLmJT09ulnWfd_Q8&ratebypass=yes&mv=m&mt=1397473450&expire=1397499384&sver=3&sparams=id%2Cip%2Cipbits%2Citag%2Cratebypass%2Csource%2Cupn%2Cexpire&ipbits=0&source=youtube&upn=FMs3dfmCrBA&signature=E476E66E85714DE7E7FFA5C28FF6D1CB3E6896CB.7A7C28E0B0730C38AA759AFE70149B736557139A&ip=162.210.196.172&key=yt5&fexp=931323%2C927618%2C932103%2C938648%2C914092%2C916612%2C927619%2C937417%2C913434%2C936916%2C934022%2C936923&itag=22"]]];
        }
        else {
            [self youtubeShareNowWithServiceObj:service data:[NSData dataWithContentsOfURL:videoURLForSharing]];
        }
    }
    else {
        NSString *strNewUrl = [NSString stringWithFormat:@"http://player.vimeo.com/video/%@", [videoURLForSharing lastPathComponent]];
        
        [YTVimeoExtractor fetchVideoURLFromURL:strNewUrl //@"http://vimeo.com/58600663"
                                       quality:YTVimeoVideoQualityLow
                             completionHandler:^(NSURL *videoURL, NSError *error, YTVimeoVideoQuality quality) {
                                 if (error) {
                                     // handle error
                                     NSLog(@"Video URL: %@", [videoURL absoluteString]);
                                 } else {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         [self youtubeShareNowWithServiceObj:service data:[NSData dataWithContentsOfURL:videoURL]];
                                     });
                                 }
                             }];
        
    }
    
}

- (void)youtubeShareNowWithServiceObj:(GDataServiceGoogleYouTube *)service data:(NSData *)data {
    
    NSURL *url = [GDataServiceGoogleYouTube youTubeUploadURLForUserID:kGDataServiceDefaultUser];
    
    // load the file data
    NSString *path = [videoURLForSharing path];
    //NSData *data = [NSData dataWithContentsOfURL:videoURLForSharing];
    NSString *filename = [path lastPathComponent];
    
    // gather all the metadata needed for the mediaGroup
    NSString *titleStr = @"YouTubeVideo";
    GDataMediaTitle *title = [GDataMediaTitle textConstructWithString:titleStr];
    
    NSString *categoryStr = @"Entertainment";
    GDataMediaCategory *category = [GDataMediaCategory mediaCategoryWithString:categoryStr];
    [category setScheme:kGDataSchemeYouTubeCategory];
    
    NSString *descStr = @"Video";
    GDataMediaDescription *desc = [GDataMediaDescription textConstructWithString:descStr];
    
    NSString *keywordsStr = @"Flixogram";
    GDataMediaKeywords *keywords = [GDataMediaKeywords keywordsWithString:keywordsStr];
    
    BOOL isPrivate = NO;
    
    GDataYouTubeMediaGroup *mediaGroup = [GDataYouTubeMediaGroup mediaGroup];
    [mediaGroup setMediaTitle:title];
    [mediaGroup setMediaDescription:desc];
    [mediaGroup addMediaCategory:category];
    [mediaGroup setMediaKeywords:keywords];
    [mediaGroup setIsPrivate:isPrivate];
    
    NSString *mimeType = [GDataUtilities MIMETypeForFileAtPath:path
											   defaultMIMEType:@"video/mov"];
    
    // create the upload entry with the mediaGroup and the file data
    GDataEntryYouTubeUpload *entry;
    entry = [GDataEntryYouTubeUpload uploadEntryWithMediaGroup:mediaGroup
                                                          data:data
                                                      MIMEType:mimeType
                                                          slug:filename];
	
    SEL progressSel = @selector(ticket:hasDeliveredByteCount:ofTotalByteCount:);
    [service setServiceUploadProgressSelector:progressSel];
    
    GDataServiceTicket *ticket;
    ticket = [service fetchEntryByInsertingEntry:entry
                                      forFeedURL:url
                                        delegate:self
                               didFinishSelector:@selector(uploadTicket:finishedWithEntry:error:)];
    
    NSLog(@"auth token %@", [ticket authToken]);
    
    [self setUploadTicket:ticket];

}

// progress callback
- (void)ticket:(GDataServiceTicket *)ticket hasDeliveredByteCount:(unsigned long long)numberOfBytesRead 
ofTotalByteCount:(unsigned long long)dataLength 
{	
	uploadPercentage = (CGFloat) numberOfBytesRead / (CGFloat) dataLength;
	[progressView setProgress:uploadPercentage];
}

// upload callback
- (void)uploadTicket:(GDataServiceTicket *)ticket
   finishedWithEntry:(GDataEntryYouTubeVideo *)videoEntry
               error:(NSError *)error {
	isUploadVideoFile = NO;
	if ([self.followingVideos count]) {
		[self reloadVideo];
	}
	
    if (error != nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:[NSString stringWithFormat:@"Error: %@", 
                                                                 [error description]] 
                                                       delegate:nil 
                                              cancelButtonTitle:@"Ok" 
                                              otherButtonTitles:nil];
        
        [alert show];
    }
    [self setUploadTicket:nil];
	
	self.table.refreshEnabled = YES;
}

#pragma mark -
#pragma mark Setters

- (GDataServiceTicket *)uploadTicket {
    return mUploadTicket;
}

- (void)setUploadTicket:(GDataServiceTicket *)ticket {
    mUploadTicket = ticket;
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
	// Code to actually refresh goes here.
    self.table.pullLastRefreshDate = [NSDate date];
    self.table.pullTableIsRefreshing = NO;
	[self.player stop];
	[self removeMovieNotificationHandlers];
	self.player = nil;
	[self.table reloadData];
}

- (void) loadMoreDataToTable
{
    // Code to actually load more data goes here.
    self.table.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
	UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
	UIBarButtonItem *activityItem = [[UIBarButtonItem alloc] initWithCustomView:activity];
	self.navigationItem.rightBarButtonItem = activityItem;
	[activity startAnimating];
	
    [dummyTextField resignFirstResponder];
    
	NSString *userID = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI videoExemplar] getFollowingVideosForUserID:userID onPage:@"1" withDelegate:self];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

@end
