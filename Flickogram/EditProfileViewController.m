//
//  EditProfileViewController.m
//  Flickogram
//

#import "EditProfileViewController.h"
#import "SingletonAuthorizationUser.h"

@interface EditProfileViewController ()
- (void)initializeTextFields;
- (void)setTextIntoTextFieldsFromDictionary:(NSDictionary *)data;
- (void)dismissTextFields;
- (void)showPickerView:(id)sender;
@end

@implementation EditProfileViewController
@synthesize table = _table;
@synthesize activityIndicator = _activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.screenName = @"Edit Profile";
    
	//set back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	self.title = @"Edit Profile";
	
	reusableIdentifiers = [NSArray arrayWithObjects:@"UsernameCell", @"FirstnameCell", @"LastnameCell", @"WebsiteCell", @"emailCell", @"phoneCell", @"gender", @"birthdate", nil];
    
	[self initializeTextFields];
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI authorizationExemplar] viewProfileUserID:authUser viewerID:authUser onPage:@"1" delegate:self];
	
	//PickerView With Done button
	actionSheet = [[UIActionSheet alloc] initWithTitle:nil
											  delegate:nil
									 cancelButtonTitle:nil
								destructiveButtonTitle:nil
									 otherButtonTitles:nil];
	
	[actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
	CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
	pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
	pickerView.showsSelectionIndicator = YES;
	pickerView.dataSource = self;
	pickerView.delegate = self;
	[actionSheet addSubview:pickerView];
	UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
	closeButton.momentary = YES;
	closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
	closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
	closeButton.tintColor = [UIColor blackColor];
	[closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
	[actionSheet addSubview:closeButton];
	
	//Pre-set sex and birthday button/label/array
	
	//Gender
	sexButton = [UIButton buttonWithType:UIButtonTypeCustom];
	sexButton.frame = CGRectMake(67, 10, 80, 25);
	[sexButton setImage:[UIImage imageNamed:@"Drop-Down-2.png"] forState:UIControlStateNormal];
	[sexButton setImage:[UIImage imageNamed:@"Drop-Down-2-selected"] forState:UIControlStateSelected];
	[sexButton addTarget:self action:@selector(showPickerView:) forControlEvents:UIControlEventTouchUpInside];
	sexButton.tag =	GenderType;
	
	sexLabel = [[UILabel alloc] init];
	sexLabel.frame = CGRectMake(73, 13, 50, 17);
	sexLabel.font = [UIFont systemFontOfSize:13];
	sexLabel.backgroundColor = [UIColor clearColor];
	
	genderData = [NSArray arrayWithObjects:@"--------", @"Male", @"Female", nil];
	currentGender = 0;
	
	//Birthday Month
	birthdayMonthButton = [UIButton buttonWithType:UIButtonTypeCustom];
	birthdayMonthButton.frame = CGRectMake(67, 10, 95, 25);
	[birthdayMonthButton setImage:[UIImage imageNamed:@"Drop-Down-1"] forState:UIControlStateNormal];
	[birthdayMonthButton setImage:[UIImage imageNamed:@"Drop-Down-1-selected"] forState:UIControlStateSelected];
	[birthdayMonthButton addTarget:self action:@selector(showPickerView:) forControlEvents:UIControlEventTouchUpInside];
	birthdayMonthButton.tag = MonthType;
	
	birthdayMonthLabel = [[UILabel alloc] init];
	birthdayMonthLabel.frame = CGRectMake(73, 13, 65, 17);
	birthdayMonthLabel.font = [UIFont systemFontOfSize:13];
	birthdayMonthLabel.backgroundColor = [UIColor clearColor];
	
	birthdayMonthData = [NSArray arrayWithObjects:@"---", @"January", @"February", @"March", @"April", @"May", @"June",
													@"July", @"August", @"September", @"October", @"November", @"December", nil];
	currentBirthdayMonth = 0;
	
	//Birthday Day
	birthdayDayButton = [UIButton buttonWithType:UIButtonTypeCustom];
	birthdayDayButton.frame = CGRectMake(165, 10, 50, 25);
	[birthdayDayButton setImage:[UIImage imageNamed:@"Drop-Down-3"] forState:UIControlStateNormal];
	[birthdayDayButton setImage:[UIImage imageNamed:@"Drop-Down-3-selected"] forState:UIControlStateSelected];
	[birthdayDayButton addTarget:self action:@selector(showPickerView:) forControlEvents:UIControlEventTouchUpInside];
	birthdayDayButton.tag = DayType;
	
	birthdayDayLabel = [[UILabel alloc] init];
	birthdayDayLabel.frame = CGRectMake(171, 13, 20, 17);
	birthdayDayLabel.font = [UIFont systemFontOfSize:13];
	birthdayDayLabel.backgroundColor = [UIColor clearColor];
	
	birthdayDayData = [[NSMutableArray alloc] initWithObjects:@"---", nil];
	for (int i = 1; i <=31; i++) {
		[birthdayDayData addObject:[NSString stringWithFormat:@"%d", i]];
	}
	currentBirthdayDay = 0;
	
	//Birthday Year
	birthdayYearButton = [UIButton buttonWithType:UIButtonTypeCustom];
	birthdayYearButton.frame = CGRectMake(218, 10, 70, 25);
	[birthdayYearButton setImage:[UIImage imageNamed:@"Drop-Down-4"] forState:UIControlStateNormal];
	[birthdayYearButton setImage:[UIImage imageNamed:@"Drop-Down-4-selected"] forState:UIControlStateSelected];
	[birthdayYearButton addTarget:self action:@selector(showPickerView:) forControlEvents:UIControlEventTouchUpInside];
	birthdayYearButton.tag = YearType;
	
	birthdayYearLabel = [[UILabel alloc] init];
	birthdayYearLabel.frame = CGRectMake(224, 13, 40, 17);
	birthdayYearLabel.font = [UIFont systemFontOfSize:13];
	birthdayYearLabel.backgroundColor = [UIColor clearColor];
	
	birthdayYearData = [[NSMutableArray alloc] initWithObjects:@"---", nil];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy"];
	NSString *yearString = [formatter stringFromDate:[NSDate date]];
	NSInteger currentYear = [yearString integerValue];
	
	for (int i = 1900; i <= currentYear; i++) {
		[birthdayYearData addObject:[NSString stringWithFormat:@"%d", i]];
	}
	currentBirthdayYear = 0;
}

- (void)viewDidAppear:(BOOL)animated
{
	
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardShow)
												 name:UIKeyboardDidShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardHide)
												 name:UIKeyboardWillHideNotification
											   object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper methods
- (void)initializeTextFields
{
	//public section
	CGRect newFrame = CGRectMake(10, 15, 290, 30);
	
	username = [[UITextField alloc] init];
    username.placeholder = @"Username";
	username.borderStyle = UITextBorderStyleNone;
	username.font = [UIFont boldSystemFontOfSize:14];
	username.frame = newFrame;
	username.delegate = self;
	
	firstname = [[UITextField alloc] init];
	firstname.placeholder = @"Firstname";
	firstname.borderStyle = UITextBorderStyleNone;
	firstname.font = [UIFont boldSystemFontOfSize:14];
	firstname.frame = newFrame;
	firstname.delegate = self;
	
	lastname = [[UITextField alloc] init];
	lastname.placeholder = @"Lastname";
	lastname.borderStyle = UITextBorderStyleNone;
	lastname.font = [UIFont boldSystemFontOfSize:14];
	lastname.frame = newFrame;
	lastname.delegate = self;
	
	website = [[UITextField alloc] init];
	website.placeholder = @"Website";
	website.borderStyle = UITextBorderStyleNone;
	website.font = [UIFont boldSystemFontOfSize:14];
	website.frame = newFrame;
	website.delegate = self;
	
	//private section
	email = [[UITextField alloc] init];
	email.placeholder = @"Website";
	email.borderStyle = UITextBorderStyleNone;
	email.font = [UIFont boldSystemFontOfSize:14];
	email.frame = newFrame;
	email.delegate = self;
	
	phone = [[UITextField alloc] init];
	phone.placeholder = @"Phone Number";
	phone.borderStyle = UITextBorderStyleNone;
	phone.font = [UIFont boldSystemFontOfSize:14];
	phone.frame = newFrame;
	phone.delegate = self;
}

- (void)setTextIntoTextFieldsFromDictionary:(NSDictionary *)data
{
	username.text = [data objectForKey:@"user_name"];
	firstname.text = [data objectForKey:@"firstname"];
	lastname.text = [data objectForKey:@"lastname"];
	website.text = [data objectForKey:@"website"];
	email.text = [data objectForKey:@"email"];
	phone.text = [data objectForKey:@"phone_no"];

	NSString *genderStr = [data objectForKey:@"gender"];
	NSInteger count = [genderData count];
	for (int i = 0; i < count; i++) {
		NSString *gender = [genderData objectAtIndex:i];
		if ([gender isEqualToString:genderStr]) {
			currentGender = i;
			break;
		} else {
			currentGender = 0;
		}
	}
	sexLabel.text = [genderData objectAtIndex:currentGender];
	
	NSString *birthdate = [data objectForKey:@"birthdate"];
	if (![birthdate isEqual:[NSNull null]]) {
		NSArray *date = [birthdate componentsSeparatedByString:@"-"];

		NSString *year = [date objectAtIndex:0];
		NSString *month = [date objectAtIndex:1];
		NSString *day = [date objectAtIndex:2];
	
		NSInteger currentDay = [day integerValue];
		currentBirthdayDay = currentDay;
	
		NSInteger currentMonth = [month integerValue];
		currentBirthdayMonth = currentMonth;
		
		NSInteger count = [birthdayYearData count];
		for (int i = 0; i < count; i++) {
			NSString *yearStr = [birthdayYearData objectAtIndex:i];
			if ([year isEqualToString:yearStr]) {
				currentBirthdayYear = i;
				break;
			} else {
				currentBirthdayYear = 0;
			}
		}
		birthdayYearLabel.text = [birthdayYearData objectAtIndex:currentBirthdayYear];
	}
	birthdayDayLabel.text = [birthdayDayData objectAtIndex:currentBirthdayDay];
	birthdayMonthLabel.text = [birthdayMonthData objectAtIndex:currentBirthdayMonth];
	birthdayYearLabel.text = [birthdayYearData objectAtIndex:currentBirthdayYear];
	
	[self.table reloadData];
}

- (void)dismissTextFields
{
	[username resignFirstResponder];
    [lastname resignFirstResponder];
	[firstname resignFirstResponder];
	[website resignFirstResponder];
	[email resignFirstResponder];
	[phone resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
	[self dismissTextFields];
    return YES;
}

- (void)keyboardShow
{
	CGRect newFrame = self.table.frame;
	newFrame.size.height -= 160;
	self.table.frame = newFrame;
}

- (void)keyboardHide
{
	CGRect newFrame = self.table.frame;
	newFrame.size.height += 160;
	self.table.frame = newFrame;
}

#pragma mark - TableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows = 0;
	switch (section) {
		case 0: {	//Public profile
			numberOfRows = 4;
			break;
		}
		case 1: {	//Submit
			numberOfRows = 1;
			break;
		}
		case 2: {	//Private profile
			numberOfRows = 4;
			break;
		}
		case 3: {	//Submit
			numberOfRows = 1;
			break;
		}
	}
	return numberOfRows;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *header;
	if (section == 0) { //public section
		header = @"Public Profile";
	} else if (section == 2) { //private section
		header = @"Private Profile";
	}
	return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *CellIdentifier = @"EditProfileCell";
	if (indexPath.section == 0) {
		CellIdentifier = [reusableIdentifiers objectAtIndex:indexPath.row];
	} else if (indexPath.section == 2) {
		CellIdentifier = [reusableIdentifiers objectAtIndex:indexPath.row + 4]; //+4 count elements in 0 section
	}
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									  reuseIdentifier:CellIdentifier];
    }
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	if (indexPath.section == 0) { //public profile
		NSInteger currentRow = indexPath.row;
		switch (currentRow) {
			case 0: {
				[cell.contentView addSubview:username];
				break;
			}
			case 1: {
				[cell.contentView addSubview:firstname];
				break;
			}
			case 2: {
				[cell.contentView addSubview:lastname];
				break;
			}
			case 3: {
				[cell.contentView addSubview:website];
				break;
			}
		}
	} else if (indexPath.section == 2) { //private profile
		NSInteger currentRow = indexPath.row;
		switch (currentRow) {
			case 0: {
				[cell.contentView addSubview:email];
				break;
			}
			case 1: {
				[cell.contentView addSubview:phone];
				break;
			}
			case 2: {
				cell.textLabel.text = @"		     sex";
				cell.textLabel.textColor = [UIColor blueColor];
				cell.textLabel.font = [UIFont systemFontOfSize:14];
				[cell.contentView addSubview:sexButton];
				[cell.contentView addSubview:sexLabel];
				break;
			}
			case 3: {
				cell.textLabel.text = @"birthday";
				cell.textLabel.textColor = [UIColor blueColor];
				cell.textLabel.font = [UIFont systemFontOfSize:14];
				
				[cell.contentView addSubview:birthdayMonthButton];
				[cell.contentView addSubview:birthdayMonthLabel];
				
				[cell.contentView addSubview:birthdayDayButton];
				[cell.contentView addSubview:birthdayDayLabel];
				
				[cell.contentView addSubview:birthdayYearButton];
				[cell.contentView addSubview:birthdayYearLabel];
				break;
			}
		}
	} else { //submit button
		UILabel *submit = [[UILabel alloc] init];
		submit.frame = CGRectMake(123, 10, 59, 21);
		submit.font = [UIFont boldSystemFontOfSize:14];
		submit.text = @"Submit";
		submit.backgroundColor = [UIColor clearColor];
		[cell.contentView addSubview:submit];
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1) {
		[self dismissTextFields];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
		NSString *usernameStr = username.text;
		NSString *firstnameStr = firstname.text;
		NSString *lastnameStr = lastname.text;
		NSString *websiteStr = website.text;
		[[ManagerFlickogramAPI authorizationExemplar] updatePublicProfileForUserID:authUser
																		  username:usernameStr
																		 firstname:firstnameStr
																		  lastname:lastnameStr
																		   website:websiteStr
																		  delegate:self];
	} else if (indexPath.section == 3) {
		[self dismissTextFields];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
		NSString *emailStr = email.text;
		NSString *phoneStr = phone.text;
		NSString *genderStr = [genderData objectAtIndex:currentGender];
		NSString *dayStr = [birthdayYearData objectAtIndex:currentBirthdayYear];
		
		NSString *birthdate = [NSString stringWithFormat:@"%@-%d-%d", dayStr, currentBirthdayMonth, currentBirthdayDay];
		[[ManagerFlickogramAPI authorizationExemplar] updatePrivateProfileForUserID:authUser
																		  birthdate:birthdate
																			 gender:genderStr
																			  phone:phoneStr
																			  email:emailStr
																		   delegate:self];
	}
}

#pragma mark - Update Public Profile Delegate
- (void)updatePublicProfileSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSDictionary *userInfo = [data objectForKey:@"user_data"];
	[self setTextIntoTextFieldsFromDictionary:userInfo];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully"
													message:@"Your profile information has been saved!"
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];

	[[SingletonAuthorizationUser authorizationUser] setUserName:[userInfo objectForKey:@"user_name"]];
	[[SingletonAuthorizationUser authorizationUser] setFirstName:[userInfo objectForKey:@"firstname"]];
	[[SingletonAuthorizationUser authorizationUser] setLastName:[userInfo objectForKey:@"lastname"]];
}

- (void)updatePublicProfileFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:error
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Update Private Profile Delegate
- (void)updatePrivateProfileSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSDictionary *userInfo = [data objectForKey:@"user_data"];
	[self setTextIntoTextFieldsFromDictionary:userInfo];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully"
													message:@"Your profile information has been saved!"
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
	[[SingletonAuthorizationUser authorizationUser] setEmail:[userInfo objectForKey:@"email"]];
}

- (void)updatePrivateProfileFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:error
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Get Profile User Info delegate
- (void)getUserProfileSuccessWithRecivedData:(NSDictionary *)data
{
	[self.activityIndicator stopAnimating];
	self.table.hidden = NO;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSDictionary *userInfo = [data objectForKey:@"user_detail"];
	[self setTextIntoTextFieldsFromDictionary:userInfo];
}

- (void)getUserProfileFailedWithError:(NSString *)error
{
	[self.activityIndicator stopAnimating];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:error
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Picker View
- (void)showPickerView:(id)sender
{
	UIButton *currentButton = (UIButton *)sender;
	selectedPickerType = currentButton.tag;
	
	[pickerView reloadAllComponents];
	
	if (selectedPickerType == GenderType) {
		[pickerView selectRow:currentGender inComponent:0 animated:NO];
	} else if (selectedPickerType == MonthType) {
		[pickerView selectRow:currentBirthdayMonth inComponent:0 animated:NO];
	} else if (selectedPickerType == DayType) {
		[pickerView selectRow:currentBirthdayDay inComponent:0 animated:NO];
	} else if (selectedPickerType == YearType) {
		[pickerView selectRow:currentBirthdayYear inComponent:0 animated:NO];
	}
	
	[actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
	[actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

- (void)dismissActionSheet:(id)sender
{
	[actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	if (selectedPickerType == GenderType) {
		return [genderData count];
	} else if (selectedPickerType == MonthType) {
		return [birthdayMonthData count];
	} else if (selectedPickerType == DayType) {
		return [birthdayDayData count];
	} else if (selectedPickerType == YearType) {
		return [birthdayYearData count];
	}
	return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	NSString *title;
	if (selectedPickerType == GenderType) {
		title = [genderData objectAtIndex:row];
	} else if (selectedPickerType == MonthType) {
		title = [birthdayMonthData objectAtIndex:row];
	} else if (selectedPickerType == DayType) {
		title = [birthdayDayData objectAtIndex:row];
	} else if (selectedPickerType == YearType) {
		title = [birthdayYearData objectAtIndex:row];
	}
	return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if (selectedPickerType == GenderType) {
		currentGender = row;
		sexLabel.text = [genderData objectAtIndex:currentGender];
	} else if (selectedPickerType == MonthType) {
		currentBirthdayMonth = row;
		birthdayMonthLabel.text = [birthdayMonthData objectAtIndex:currentBirthdayMonth];
	} else if (selectedPickerType == DayType) {
		currentBirthdayDay = row;
		birthdayDayLabel.text = [birthdayDayData objectAtIndex:currentBirthdayDay];
	} else if (selectedPickerType == YearType) {
		currentBirthdayYear = row;
		birthdayYearLabel.text = [birthdayYearData objectAtIndex:currentBirthdayYear];
	}
	[self.table reloadData];
}

@end
