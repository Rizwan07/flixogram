//
//  SignUpViewController.h
//  Flickogram


#import "BaseViewController.h"
#import "ManagerFlickogramAPI.h"

@interface SignUpViewController : BaseViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, FlickogramAuthorizationDelegate> {
	UIImage *avatarImage;
}

@property (nonatomic, unsafe_unretained) IBOutlet UIScrollView *scrolling;
@property (nonatomic, unsafe_unretained) IBOutlet UITextField *email;
@property (nonatomic, unsafe_unretained) IBOutlet UITextField *username;
@property (nonatomic, unsafe_unretained) IBOutlet UITextField *password;
@property (nonatomic, unsafe_unretained) IBOutlet UITextField *phone;
@property (nonatomic, unsafe_unretained) IBOutlet UIButton *picture;

- (IBAction)changePicture:(id)sender;
@end
