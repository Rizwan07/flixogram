//
//  FlickogramMotionEffects.h
//  Flickogram
//


#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol FlickogramMotionEffectsDelegate <NSObject>
@optional
- (void)applyEffectFinish:(NSURL *)videoWithEffect;
- (void)addAudioOnVideoSuccess:(NSURL *)musicVideo;
@end

@interface FlickogramMotionEffects : NSObject 

@property (nonatomic, assign) id <FlickogramMotionEffectsDelegate> delegate;
@property (nonatomic, assign) BOOL isNeedRotation;

- (void)applySlowMotionVideoPath:(NSURL *)urlMovie 
				startSeconds:(CGFloat)start
			   finishSeconds:(CGFloat)finish
					isRotate:(BOOL)rotate;

- (void)applyStillMotionVideoPath:(NSURL *)urlMovie 
					 startSeconds:(CGFloat)start
					finishSeconds:(CGFloat)finish
						 isRotate:(BOOL)rotate;

- (void)addAudio:(NSURL *)audioURL toVideo:(NSURL *)videoURL withVolume:(CGFloat)volume isRotate:(BOOL)rotate;

@end
