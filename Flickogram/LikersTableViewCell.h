//
//  LikersTableViewCell.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"

@interface LikersTableViewCell : UITableViewCell 

@property (nonatomic, weak) IBOutlet EGOImageView *photo;
@property (nonatomic, weak) IBOutlet UILabel *username;
@property (nonatomic, weak) IBOutlet UILabel *profileName;
@property (nonatomic, weak) IBOutlet UIButton *followToggle;		   

- (void)setPhotoUser:(NSString*)photoUser;
- (void)setPlaceholder; 

@end
