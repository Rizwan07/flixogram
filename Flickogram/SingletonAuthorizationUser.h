//
//  SingletonAuthorizationUser.h
//  Odesker
//


#import <Foundation/Foundation.h>
#import "EGOImageView.h"

@interface SingletonAuthorizationUser : NSObject

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *profilePicture;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;

@property (nonatomic) EGOImageView *__strong *imageView;

+(SingletonAuthorizationUser*) authorizationUser;

@end
