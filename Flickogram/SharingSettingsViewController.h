//
//  SharingSettingsViewController.h
//  Flickogram
//

#import "BaseViewController.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTMOAuth2Authentication.h"
@interface SharingSettingsViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate> {
    
    UIImageView *imageViewTwitter;
    UIImageView *imageViewFacebook;
    UIImageView *imageViewYoutube;
    
	UIButton *toggleTwitterState;
	UIButton *toggleFacebookState;
	UIButton *toggleYoutubeState;
	UILabel *titleTwitter;
	UILabel *titleFacebook;
	UILabel *titleYoutube;
	NSArray *twitterAccounts;
}

@property (nonatomic, strong) IBOutlet UITableView *table;

@end
