//
//  FlickogramAuthorization.m
//  Flickogram


#import "FlickogramAuthorization.h"

@implementation FlickogramAuthorization

+ (BOOL)checkSaveAuthCredentials
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if ([defaults objectForKey:@"id"]) {
		return YES;
	} return NO;
}

+ (void)saveAuthCredentialsFromDictionary:(NSDictionary *)authCredentials
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString * userID = [authCredentials objectForKey:@"id"];
	NSString * username = [authCredentials objectForKey:@"user_name"];
	NSString * profilePicture = [authCredentials objectForKey:@"profile_picture"];
	
	[defaults setObject:userID forKey:@"id"];
	[defaults setObject:username forKey:@"user_name"];
	[defaults setObject:profilePicture forKey:@"profile_picture"];
	[defaults synchronize];
}

+ (void)removeAuthCredentials
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults removeObjectForKey:@"id"];
	[defaults removeObjectForKey:@"user_name"];
	[defaults removeObjectForKey:@"profile_picture"];
	[defaults synchronize];
}

+ (NSMutableDictionary *)getAuthCredentials
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString * userID = [defaults objectForKey:@"id"];
	NSString * username = [defaults objectForKey:@"user_name"];
	NSString * profilePicture = [defaults objectForKey:@"profile_picture"];
	
	
	NSMutableDictionary *authCredentials = [[NSMutableDictionary alloc] init];
	[authCredentials setObject:userID forKey:@"id"];
	[authCredentials setObject:username forKey:@"user_name"];
	[authCredentials setObject:profilePicture forKey:@"profile_picture"];
	return authCredentials;
}

@end
