//
//  DetailProfileViewController.m
//  Flickogram
//

#import "DetailProfileViewController.h"
#import "SingletonAuthorizationUser.h"
#import "LikersTableViewCell.h"
#import "ProfileViewController.h"

@interface DetailProfileViewController ()

@end

@implementation DetailProfileViewController
@synthesize detailType = _detailType;
@synthesize selectedUserID = _selectedUserID;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

 	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	currentPage = 0;
	isShowMore = NO;
	flickogramUsers = [[NSMutableArray alloc] init];
	
	[self getDataFromWebService];
}

- (void)getDataFromWebService
{
	currentPage++;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *page = [NSString stringWithFormat:@"%d", currentPage];
	if (self.detailType == ShowFollowers) {
		[[ManagerFlickogramAPI searchExemplar] getListFollowersForUserID:self.selectedUserID
																viewerID:authUser
																  onPage:page withDelegate:self];
	} else if (self.detailType == ShowFollowings) {
		[[ManagerFlickogramAPI searchExemplar] getListFollowingsForUserID:self.selectedUserID
																  vieweID:authUser
																   onPage:page
															 withDelegate:self];
	}
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [flickogramUsers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"DetailFriendCell";
	LikersTableViewCell *cell = (LikersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LikersTableViewCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (LikersTableViewCell *) currentObject;
				[cell setPlaceholder];
			}
			break;
		}
	}
		
	NSDictionary *data = [flickogramUsers objectAtIndex:indexPath.row];
	cell.username.text = [data objectForKey:@"user_name"];
	[cell setPhotoUser:[data objectForKey:@"profile_picture"]];
	cell.profileName.text = [NSString stringWithFormat:@"%@ %@", [data objectForKey:@"firstname"], [data objectForKey:@"lastname"]];
		
	cell.followToggle.hidden = NO;
	cell.followToggle.tag = indexPath.row;
	[cell.followToggle addTarget:self action:@selector(toggleStateFollowing:) forControlEvents:UIControlEventTouchUpInside];
	cell.accessoryType = UITableViewCellAccessoryNone;
		
	
	NSString *userID;
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	if (self.detailType == ShowFollowers) {
		userID = [[flickogramUsers objectAtIndex:indexPath.row] objectForKey:@"follower_id"];
	} else {
		userID = [[flickogramUsers objectAtIndex:indexPath.row] objectForKey:@"following_id"];
	}
	
	if ([authUser isEqualToString:userID]) {
		cell.followToggle.hidden = YES;
	}
	
	BOOL isFollowing = [[data objectForKey:@"is_following"] boolValue];
	if (isFollowing) {
		cell.followToggle.selected = YES;
	} else {
		cell.followToggle.selected = NO;
	}
	
	return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	ProfileViewController *profile = [[ProfileViewController alloc] init];
	if (self.detailType == ShowFollowers) {
		profile.selectedUserID = [[flickogramUsers objectAtIndex:indexPath.row] objectForKey:@"follower_id"];
	} else {
		profile.selectedUserID = [[flickogramUsers objectAtIndex:indexPath.row] objectForKey:@"following_id"];
	}
	profile.hasParentNavigationView = YES; //subview for it's view
	[self.navigationController pushViewController:profile animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSInteger lastCell = [flickogramUsers count] - 1;
	if (lastCell == indexPath.row) {
		if (isShowMore) {
			isShowMore = NO;
			[self getDataFromWebService];
		}
	}
}
#pragma mark - Following
- (void)toggleStateFollowing:(id)sender
{
	UIButton *followButton = (UIButton *)sender;
	NSInteger currentIndex = followButton.tag;
	NSString *userID;
	if (self.detailType == ShowFollowers) {
		userID = [[flickogramUsers objectAtIndex:currentIndex] objectForKey:@"follower_id"];
	} else {
		userID = [[flickogramUsers objectAtIndex:currentIndex] objectForKey:@"following_id"];
	}
	NSString *followerID = [[SingletonAuthorizationUser authorizationUser] userID];
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI authorizationExemplar] followUnFollowUserID:userID followerID:followerID indexArray:currentIndex withDelegate:self];
}

- (void)followUnFollowUserSuccesWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSMutableDictionary *currentUser = [flickogramUsers objectAtIndex:currentIndex];
	BOOL isFollowing = [[currentUser objectForKey:@"is_following"] boolValue];
	isFollowing = !isFollowing;
	NSString *isFollowingString = [NSString stringWithFormat:@"%d", isFollowing];
	[currentUser setObject:isFollowingString forKey:@"is_following"];
	[flickogramUsers replaceObjectAtIndex:currentIndex withObject:currentUser];
	[self.tableView reloadData];
}

- (void)followUnFollowUserFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}


#pragma mark - Followers Delegate
- (void)getListFollowersSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isShowMore = [[data objectForKey:@"show_more"] boolValue];
	NSMutableDictionary *followers = [data objectForKey:@"followers"];
	for (NSDictionary *currentUser in followers) {
		[flickogramUsers addObject:currentUser];
	}
	[self.tableView reloadData];
	
}

- (void)getListFollowersFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];			
}

#pragma mark - Followings Delegate
- (void)getListFollowingsSuccessWithRecivedData:(NSMutableDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isShowMore = [[data objectForKey:@"show_more"] boolValue];
	NSMutableDictionary *followers = [data objectForKey:@"followings"];
	for (NSDictionary *currentUser in followers) {
		[flickogramUsers addObject:currentUser];
	}
	[self.tableView reloadData];
}

- (void)getListFollowingsFailedWithError:(NSString *)error
{
	NSLog(@"%@", error);
}

@end
