//
//  GridVideoCell.m
//  Flickogram
//


#import "GridVideoCell.h"
#import "JMImageCache.h"

@implementation GridVideoCell
@synthesize thumbButton1 = _thumbButton1;
@synthesize thumbButton2 = _thumbButton2;
@synthesize thumbButton3 = _thumbButton3;
@synthesize thumbButton4 = _thumbButton4;
@synthesize thumbButton5 = _thumbButton5;
@synthesize thumbButton6 = _thumbButton6;
@synthesize thumbButton7 = _thumbButton7;
@synthesize thumbButton8 = _thumbButton8;
@synthesize thumbButton9 = _thumbButton9;
@synthesize thumbButton10 = _thumbButton10;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (EGOImageButton *)setThumbImageOfURL:(NSURL *)thumbURL ForButton:(ImageButton)currentButton
{
	EGOImageButton *currentImageButton;
	switch (currentButton) {
		case ImageButton01: {
			self.thumbButton1.hidden = NO;
			self.thumbButton1.imageURL = thumbURL;
			currentImageButton = self.thumbButton1;
			break;
		}
		case ImageButton02: {
			self.thumbButton2.hidden = NO;
			self.thumbButton2.imageURL = thumbURL;
			currentImageButton = self.thumbButton2;
			break;
		}
		case ImageButton03: {
			self.thumbButton3.hidden = NO;
			self.thumbButton3.imageURL = thumbURL;
			currentImageButton = self.thumbButton3;
			break;
		}
		case ImageButton04: {
			self.thumbButton4.hidden = NO;
			self.thumbButton4.imageURL = thumbURL;
			currentImageButton = self.thumbButton4;
			break;
		}
		case ImageButton05: {
			self.thumbButton5.hidden = NO;
			self.thumbButton5.imageURL = thumbURL;
			currentImageButton = self.thumbButton5;
			break;
		}
		case ImageButton06: {
			self.thumbButton6.hidden = NO;
			self.thumbButton6.imageURL = thumbURL;
			currentImageButton = self.thumbButton6;
			break;
		}
		case ImageButton07: {
			self.thumbButton7.hidden = NO;
			self.thumbButton7.imageURL = thumbURL;
			currentImageButton = self.thumbButton7;
			break;
		}
		case ImageButton08: {
			self.thumbButton8.hidden = NO;
			self.thumbButton8.imageURL = thumbURL;
			currentImageButton = self.thumbButton8;
			break;
		}
		case ImageButton09: {
			self.thumbButton9.hidden = NO;
			self.thumbButton9.imageURL = thumbURL;
			currentImageButton = self.thumbButton9;
			break;
		}
		case ImageButton10: {
			self.thumbButton10.hidden = NO;
			self.thumbButton10.imageURL = thumbURL;
			currentImageButton = self.thumbButton10;
			break;
		}
	}
	return currentImageButton;
}


@end
