//
//  FlickogramVideoEditingViewController.m
//  Flickogram
//


#import "FlickogramVideoEditingViewController.h"
#import "UploadVideoViewController.h"
#import "FlickogramConstants.h"

@interface FlickogramVideoEditingViewController () {
    
    FlickogramMotionEffects *motionEffects;
}
- (void)lockScreen;
- (void)unlockScreen;
- (void)applySlowMotion;
- (void)applyStillMotion;
- (void)backToRecord;
- (UIInterfaceOrientation)orientationForTrack:(NSURL *)url;
@end

@implementation FlickogramVideoEditingViewController
@synthesize pathMovie = _pathMovie;
@synthesize player = _player;
@synthesize playerLayer = _playerLayer;
@synthesize videoPreviewView = _videoPreviewView;
@synthesize play = _play;
@synthesize grayBackground = _grayBackground;
@synthesize activityIndicator = _activityIndicator;
@synthesize slowMotionButton = _slowMotionButton;
@synthesize stillMotionButton = _stillMotionButton;
@synthesize currentMotion = _currentMotion;
@synthesize toggleButton = _toggleButton;
@synthesize volumeSlider = _volumeSlider;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Flixogram Video Editing";
    
	pathForUpload = [self.pathMovie copy];
	
	self.navigationController.navigationBar.hidden = NO;
	self.navigationItem.title = @"Edit";
	
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(backToRecord) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	//set Next button
	UIImage *nextButtonImage = [UIImage imageNamed:@"next"];
	UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[nextButton setBackgroundImage:nextButtonImage forState:UIControlStateNormal];
	nextButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[nextButton addTarget:self action:@selector(uploadVideo) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *nextItem = [[UIBarButtonItem alloc] initWithCustomView:nextButton];
	self.navigationItem.rightBarButtonItem = nextItem;
	
	//set video player
    NSURL *fileURL = self.pathMovie;
    
    AVPlayerItem *playerItem = [self getPlayerItemWithFileUrl:fileURL shouldSetDuration:YES];
    
	self.player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
	self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
	self.playerLayer.frame = self.videoPreviewView.layer.bounds;
	[self.videoPreviewView.layer insertSublayer:self.playerLayer atIndex:0];
	
	//create slider
	slider = [[FlickogramFrameSlider alloc] initWithFrame:CGRectMake(0, 239, 320, 44)];
	slider.minimumRange = 1.0;
	slider.minimumValue = 0;
	slider.maximumValue = duration;
    
	[slider setSelectedMinimumValue:0];
	[slider setSelectedMaximumValue:duration];
	
	isMusicAdded = NO;
	rotate = NO;
	
	//determine need rotate video or not
	NSString *pathForEqual = [NSHomeDirectory() stringByAppendingPathComponent:NameRecordedVideoFile];
	NSURL *videoNormal = [NSURL URLWithString:pathForEqual];
	NSString *firstFile = [videoNormal lastPathComponent];
	NSString *secondFile = [self.pathMovie lastPathComponent];
	
	UIInterfaceOrientation orientationVideo = [self orientationForTrack:self.pathMovie];
	if ([secondFile rangeOfString:@"trim"].location == NSNotFound) { //if file from library
		rotate = [firstFile isEqualToString:secondFile];
		if (rotate) {
			if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
				rotate = YES;
			} else {
				rotate = NO;
			}
		}
	} else {
		if (UIInterfaceOrientationIsPortrait(orientationVideo)) {
			rotate = YES;
		} else {
			rotate = NO;
		}
	}
}

- (AVPlayerItem *)getPlayerItemWithFileUrl:(NSURL *)fileUrl shouldSetDuration:(BOOL)isDuration {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        AVURLAsset *asset = [AVURLAsset assetWithURL: fileUrl];
        if (isDuration) {
            
            duration = CMTimeGetSeconds(asset.duration);
        }
        return [AVPlayerItem playerItemWithAsset: asset];
    }
    else {
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:fileUrl];
        if (isDuration) {
            
            duration =  CMTimeGetSeconds (playerItem.duration);
        }
        return playerItem;
    }
}

- (UIInterfaceOrientation)orientationForTrack:(NSURL *)url
{
//	AVURLAsset *asset = [AVURLAsset assetWithURL:url];
//    if (asset != nil) {
//        
//        AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
//        CGSize size = [videoTrack naturalSize];
//        CGAffineTransform txf = [videoTrack preferredTransform];
//        
//        if (size.width == txf.tx && size.height == txf.ty)
//            return UIInterfaceOrientationLandscapeRight;
//        else if (txf.tx == 0 && txf.ty == 0)
//            return UIInterfaceOrientationLandscapeLeft;
//        else if (txf.tx == 0 && txf.ty == size.width)
//            return UIInterfaceOrientationPortraitUpsideDown;
//        else
//            return UIInterfaceOrientationPortrait;
//    }
//    else {
        return UIInterfaceOrientationPortrait;
//    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - IBActions

- (void)backToRecord
{
	[self resetPlaybackProgress];
	self.navigationController.navigationBar.hidden = YES;
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)uploadVideo
{
	UploadVideoViewController *uploadView = [[UploadVideoViewController alloc] init];
	uploadView.videoForUpload = pathForUpload;
    uploadView.duration = duration;
	[self.navigationController pushViewController:uploadView animated:YES];
}

- (IBAction)playMovie:(id)sender
{
	self.play.hidden = YES;
	playingTimer = [NSTimer scheduledTimerWithTimeInterval:1 
														target:self
													  selector:@selector(playback) 
													  userInfo:nil 
													   repeats:YES];
	[self.player play];
}

- (IBAction)pauseMovie:(id)sender
{
	self.play.hidden = NO;
	[self.player pause];
    
    if (self.play) {
        [self resetPlaybackProgress];
    }
    
}

- (IBAction)slowMotion:(id)sender 
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isSlowMotionFirstTime"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSlowMotionFirstTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Slow Motion" message:@"Helps you to capture video in a slow frequency" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
	self.slowMotionButton.hidden = YES;
	self.stillMotionButton.hidden = YES;
	self.currentMotion.hidden = NO;
	self.currentMotion.text = @"Slow Motion";
	//set dual slider
    [self.view addSubview:slider];
	[slider createFrameSliderForVideoURL:self.pathMovie];
	self.toggleButton.selected = YES;
	
	motion = SlowMotion;
}

- (IBAction)stillMotion:(id)sender
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isStillMotionFirstTime"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isStillMotionFirstTime"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Still Motion" message:@"Helps you to capture frame by frame video" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
	self.slowMotionButton.hidden = YES;
	self.stillMotionButton.hidden = YES;
	self.currentMotion.hidden = NO;
	self.currentMotion.text = @"Still Motion";
	//set dual slider
	[self.view addSubview:slider];
	[slider createFrameSliderForVideoURL:self.pathMovie];
	self.toggleButton.selected = YES;
	
	motion = StillMotion;

}

- (IBAction)actionToggleButton:(id)sender
{
	if (self.toggleButton.selected) {
		self.toggleButton.selected = NO;
		switch (motion) {
			case SlowMotion: {
				[self applySlowMotion];
				break;
			}
			case StillMotion: {
				[self applyStillMotion];
				break;
			}
		}
	} else {
		MPMediaPickerController *picker =[[MPMediaPickerController alloc] initWithMediaTypes: MPMediaTypeAnyAudio];
        picker.delegate = self;
        picker.allowsPickingMultipleItems = NO;
        [self presentViewController:picker animated:YES completion:nil];
	}
}

#pragma mark - Motions
- (void)applySlowMotion
{	
	[self lockScreen];
	motionEffects = [[FlickogramMotionEffects alloc] init];
	motionEffects.delegate = self;
	
	if (isMusicAdded) {
		self.pathMovie = pathForUpload;
	}
	
	[motionEffects applySlowMotionVideoPath:self.pathMovie
							startSeconds:slider.selectedMinimumValue 
						   finishSeconds:slider.selectedMaximumValue
								isRotate:rotate];
}

- (void)applyStillMotion
{
	[self lockScreen];	
	motionEffects = [[FlickogramMotionEffects alloc] init];
	motionEffects.delegate = self;
	
	if (isMusicAdded) {
		self.pathMovie = pathForUpload;
	}
	[
	 motionEffects applyStillMotionVideoPath:self.pathMovie
							startSeconds:slider.selectedMinimumValue 
						   finishSeconds:slider.selectedMaximumValue
								isRotate:rotate];
}

#pragma mark - Player
- (void)playback
{
	CGFloat currentTime = CMTimeGetSeconds([self.player currentTime]);
	if (currentTime >= (duration-1)) {
		[self resetPlaybackProgress];
		self.play.hidden = NO;
	}
}

- (void)resetPlaybackProgress
{
	[self.player pause];
	[playingTimer invalidate];
	playingTimer = nil;
	CMTime newTime = CMTimeMakeWithSeconds(0, 600);
	[self.player seekToTime: newTime];
}

#pragma mark - Lock/unlock screen
- (void)lockScreen
{
	[self resetPlaybackProgress];
	self.grayBackground.hidden = NO;
	[self.activityIndicator startAnimating];
	self.view.userInteractionEnabled = NO;
}

- (void)unlockScreen
{
	[self.activityIndicator stopAnimating];
	self.grayBackground.hidden = YES;
	self.view.userInteractionEnabled = YES;	
	
	AVPlayerItem *playerItem = [self getPlayerItemWithFileUrl:urlToPlay shouldSetDuration:YES];
    //[AVPlayerItem playerItemWithURL:urlToPlay];
	//duration =  CMTimeGetSeconds (playerItem.duration);
	[self.player replaceCurrentItemWithPlayerItem:playerItem];
}

#pragma mark - Auido From Lib
- (void)mediaPicker: (MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
	[self lockScreen];
    NSArray *mediaItems = mediaItemCollection.items;
    NSLog(@"media items %@", mediaItems);
    if ([mediaItems count]) {
        
        MPMediaItem *currentItem = [mediaItems objectAtIndex:0];
        NSURL *assetURL = [currentItem valueForProperty:MPMediaItemPropertyAssetURL];
        FlickogramMotionEffects *audioEffect = [[FlickogramMotionEffects alloc] init];
        audioEffect.delegate = self;
        CGFloat currentVolume = self.volumeSlider.value;
        [audioEffect addAudio:assetURL toVideo:self.pathMovie withVolume:currentVolume isRotate:rotate];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - FlickogramEffectsMotionDelegate
- (void)applyEffectFinish:(NSURL *)videoWithEffect;
{
	self.pathMovie = [videoWithEffect copy];
	urlToPlay = [videoWithEffect copy];
	pathForUpload = [videoWithEffect copy];
	[self performSelectorOnMainThread:@selector(unlockScreen) withObject:nil waitUntilDone:YES];
	slider.userInteractionEnabled = NO;
}

- (void)addAudioOnVideoSuccess:(NSURL *)musicVideo
{
	pathForUpload = [musicVideo copy];
	urlToPlay = [musicVideo copy];
	isMusicAdded = YES;
	[self performSelectorOnMainThread:@selector(unlockScreen) withObject:nil waitUntilDone:YES]; 
}

@end
