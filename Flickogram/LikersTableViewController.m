//
//  LikersTableViewController.m
//  Flickogram
//

#import "LikersTableViewController.h"
#import "LikersTableViewCell.h"
#import "EGOImageLoader.h"

@interface LikersTableViewController ()
- (void)parseDictionaryLikes;
@end

@implementation LikersTableViewController
@synthesize videoLikes = _videoLikes;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	self.title = @"LIKERS";

	likers = [[NSMutableArray alloc] init];
	
	[self parseDictionaryLikes];
	NSLog(@"%@", self.videoLikes);
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)parseDictionaryLikes
{
	NSInteger currentLiker = 0;
	BOOL isMore = YES;
	while (isMore) {
		NSString *numberLiker = [NSString stringWithFormat:@"%d", currentLiker];
		if ([self.videoLikes objectForKey:numberLiker]) {
			[likers addObject:[self.videoLikes objectForKey:numberLiker]];
			currentLiker++;
		} else {
			isMore = NO;
		}
	}
	[self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [likers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LikersCell";
	LikersTableViewCell *cell = (LikersTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LikersTableViewCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (LikersTableViewCell *) currentObject;
				[cell setPlaceholder];
			}
			break;
		}
	}
	NSDictionary *data = [likers objectAtIndex:indexPath.row];
	cell.username.text = [data objectForKey:@"user_name"];
	cell.profileName.text = [NSString stringWithFormat:@"%@ %@", [data objectForKey:@"firstname"], [data objectForKey:@"lastname"]];
	[cell setPhotoUser:[data objectForKey:@"profile_picture"]];
    return cell;
}

@end
