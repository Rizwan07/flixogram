//
//  AboutViewController.m
//  Flickogram
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize textView = _textView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	//back button
    
    self.title = @"About Us";
    self.screenName = @"About";
    
    [self.viewAbout.layer setCornerRadius:5];
    [self.viewAbout.layer setBorderWidth:2];
    [self.viewAbout.layer setMasksToBounds:YES];

    
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;

//    [self.textView.layer setCornerRadius:5];
//    [self.textView.layer setBorderWidth:2];
//    [self.textView.layer setMasksToBounds:YES];
//	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//	[[ManagerFlickogramAPI notificationsExemplar] getAboutUsWithDelegate:self];
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Get About Us Delegate
- (void)getAboutUsSuccessWithRecievedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString *content = [data objectForKey:@"content"];
	content = [content stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	self.textView.text = content;
}

- (void)getAboutUsFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	self.textView.text = error;
}

@end
