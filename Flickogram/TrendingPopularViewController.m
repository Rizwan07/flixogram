//
//  TrendingPopularViewController.m
//  Flickogram

#import "TrendingPopularViewController.h"
#import "GridVideoCell.h"
#import "VideoDetailViewController.h"
#import "SingletonAuthorizationUser.h"

@interface TrendingPopularViewController ()
- (void)setStringFromViewType;
- (void)getPopularVideo;
- (void)getTrendingVideo;
@end

@implementation TrendingPopularViewController
@synthesize table = _table;
@synthesize selectedVideoAPI = _selectedVideoAPI;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
	//back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	videosArray = [[NSMutableArray alloc] init];
	currentPage = 1;
	isShowMore = NO;
	type = Today;
	isLoadingVideo = NO;
	
	[self setStringFromViewType];
	
	if (self.selectedVideoAPI == PopularVideosAPI) {
		self.title = @"Popular";
        self.screenName = @"Popular";
		[self getPopularVideo];
	} else if (self.selectedVideoAPI == TrendingVideosAPI) {
		self.title = @"Trending";
        self.screenName = @"Trending";
		[self getTrendingVideo];
	}
	
	//pull to refresh
	self.table.pullArrowImage = [UIImage imageNamed:@"whiteArrow"];
	self.table.pullBackgroundColor = [UIColor clearColor];
	self.table.pullTextColor = [UIColor whiteColor];
	self.table.loadMoreEnabled = NO;
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)setStringFromViewType
{
	switch (type) {
		case Today: {
			currentType = @"today";
			break;
		}
		case ThisWeek: {
			currentType = @"thisweek";
			break;
		}
		case All: {
			currentType = @"all";
			break;
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Get Videos Methods
- (void)getPopularVideo
{
	isLoadingVideo = YES;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *currentPageInString = [NSString stringWithFormat:@"%d", currentPage];
	[[ManagerFlickogramAPI videoExemplar] getPopularVideosOnPage:currentPageInString
														withType:currentType
													withDelegate:self];
}

- (void)getTrendingVideo
{
	isLoadingVideo = YES;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *currentPageInString = [NSString stringWithFormat:@"%d", currentPage];
	[[ManagerFlickogramAPI videoExemplar] getTrendingVideosOnPage:currentPageInString
														 withType:currentType
														 delegate:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger count = [videosArray count];
	NSInteger countCells = (NSInteger) (count + 9) / 10;
	return countCells;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 135;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

	static NSString *cellId = @"GridVideoCell";
	GridVideoCell *cell = (GridVideoCell *)[tableView dequeueReusableCellWithIdentifier:cellId];
	if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GridVideoCell" owner:nil options:nil];
		for (id currentObject in topLevelObjects) {
			if ([currentObject isKindOfClass:[UITableViewCell class]]) {
				cell = (GridVideoCell *) currentObject;
			}
			break;
		}
	}
	
	NSInteger currentCell =	indexPath.row * 10;
	NSInteger countVideos = [videosArray count];
	
	int currentButton = 0;
	for (int i = currentCell; (i < countVideos && i < currentCell+10); i++) {
		NSDictionary *videoDetail = [videosArray objectAtIndex:i];
		NSURL *thumbnailURL = [NSURL URLWithString:[videoDetail objectForKey:@"thumbnail"]];
		EGOImageButton *currentImageButton = [cell setThumbImageOfURL:thumbnailURL ForButton:currentButton];
		currentImageButton.tag = i;
		[currentImageButton addTarget:self action:@selector(showVideoForEGOImageButton:) forControlEvents:UIControlEventTouchUpInside];
		currentButton++;
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (isShowMore) {
		NSInteger count = [videosArray count];
		NSInteger countCells = (NSInteger) ((count + 9) / 10) - 1;
		if (indexPath.row == countCells) {
			isShowMore = NO;
			currentPage++;
			if (self.selectedVideoAPI == PopularVideosAPI) {
				[self getPopularVideo];
			} else if (self.selectedVideoAPI == TrendingVideosAPI) {
				[self getTrendingVideo];
			}
		}
	}
}

- (void)showVideoForEGOImageButton:(id)sender
{
	EGOImageButton *selectedButton = (EGOImageButton *)sender;
	NSInteger indexVideo = selectedButton.tag;
	VideoDetailViewController *videoDetailVC = [[VideoDetailViewController alloc] init];
	videoDetailVC.videoDetail = [[videosArray objectAtIndex:indexVideo] mutableCopy];
	[self.navigationController pushViewController:videoDetailVC animated:YES];
}

#pragma mark - IBActions
- (IBAction)showTodayVideos:(id)sender
{
	if (isLoadingVideo) return;
    
    [btnToday setSelected:YES];
    [btnThisWeek setSelected:NO];
    [btnAll setSelected:NO];
    
	currentPage = 1;
	isShowMore = NO;
	[videosArray removeAllObjects];
	[self.table reloadData];
	type = Today;
	[self setStringFromViewType];
	if (self.selectedVideoAPI == PopularVideosAPI) {
		[self getPopularVideo];
	} else if (self.selectedVideoAPI == TrendingVideosAPI) {
		[self getTrendingVideo];
	}
}

- (IBAction)showThisWeekVideos:(id)sender
{
	if (isLoadingVideo) return;
    
    [btnToday setSelected:NO];
    [btnThisWeek setSelected:YES];
    [btnAll setSelected:NO];
    
	currentPage = 1;
	isShowMore = NO;
	[videosArray removeAllObjects];
	[self.table reloadData];
	type = ThisWeek;
	[self setStringFromViewType];
	if (self.selectedVideoAPI == PopularVideosAPI) {
		[self getPopularVideo];
	} else if (self.selectedVideoAPI == TrendingVideosAPI) {
		[self getTrendingVideo];
	}
}

- (IBAction)showAllVideos:(id)sender
{
	if (isLoadingVideo) return;
    
    [btnToday setSelected:NO];
    [btnThisWeek setSelected:NO];
    [btnAll setSelected:YES];

	currentPage = 1;
	isShowMore = NO;
	[videosArray removeAllObjects];
	[self.table reloadData];
	type = All;
	[self setStringFromViewType];
	if (self.selectedVideoAPI == PopularVideosAPI) {
		[self getPopularVideo];
	} else if (self.selectedVideoAPI == TrendingVideosAPI) {
		[self getTrendingVideo];
	}
}

#pragma mark - Popular Video Delegate
- (void)getPopularVideoSuccessWithRecivedData:(NSDictionary *)data
{
	isLoadingVideo = NO;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isShowMore = [[data objectForKey:@"show_more"] boolValue];
	if ([data objectForKey:@"videos"]) {
		NSDictionary *videos = [data objectForKey:@"videos"];
		[self parseVideoFromDictionary:videos];
	} else if (self.table.pullTableIsRefreshing) {
		[self refreshTable];
	}
}

- (void)getPopularVideoFailedWithError:(NSString *)error
{
	[self refreshTable];
	isLoadingVideo = NO;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSLog(@"%@", error);
}

- (void)getTrendingVideosSuccessWithRecivedData:(NSDictionary *)data
{
	isLoadingVideo = NO;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	isShowMore = [[data objectForKey:@"show_more"] boolValue];
	if ([data objectForKey:@"videos"]) {
		NSDictionary *videos = [data objectForKey:@"videos"];
		[self parseVideoFromDictionary:videos];
	} else if (self.table.pullTableIsRefreshing) {
		[self refreshTable];
	}
}

- (void)getTrendingVideosFailedWithError:(NSString *)error
{
	[self refreshTable];
	isLoadingVideo = NO;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSLog(@"%@", error);
}

#pragma mark - Parse Video
- (void)parseVideoFromDictionary:(NSDictionary *)videos
{
	if (self.table.pullTableIsRefreshing) {
		NSString *lastDateAdded = @"0";
		if ([videosArray count] > 0) {
			lastDateAdded = [[videosArray objectAtIndex:0] objectForKey:@"date_added"];
		}
		NSInteger currentVideo = 0;
		BOOL isMoreVideo = YES;
		while (isMoreVideo) {
			NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
			if ([videos objectForKey:numberUserString]) {
				NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
				NSString *newDateAdded = [videoDetails objectForKey:@"date_added"];
				
				//compare last date with new date and stop if equal
				if ([lastDateAdded isEqualToString:newDateAdded]) {
					[self refreshTable];
					return;
				} else {
					[videosArray insertObject:videoDetails atIndex:currentVideo];
					if (isShowMore) {
						[videosArray removeLastObject];
					}
				}
				currentVideo++;
			} else {
				isMoreVideo = NO;
			}
		}
		[self refreshTable];
		return;
	}
	
	NSInteger currentVideo = 0;
	BOOL isMoreVideo = YES;
	while (isMoreVideo) {
		NSString *numberUserString = [NSString stringWithFormat:@"%d", currentVideo];
		if ([videos objectForKey:numberUserString]) {
			NSMutableDictionary *videoDetails = [videos objectForKey:numberUserString];
			[videosArray addObject:videoDetails];
			currentVideo++;
		} else {
			isMoreVideo = NO;
		}
	}
	[self.table reloadData];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
	// Code to actually refresh goes here.
    self.table.pullLastRefreshDate = [NSDate date];
    self.table.pullTableIsRefreshing = NO;
	[self.table reloadData];
}

- (void) loadMoreDataToTable
{
    // Code to actually load more data goes here.
    self.table.pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
	isLoadingVideo = YES;
	if (self.selectedVideoAPI == PopularVideosAPI) {
		[[ManagerFlickogramAPI videoExemplar] getPopularVideosOnPage:@"1"
															withType:currentType
														withDelegate:self];
	} else if (self.selectedVideoAPI == TrendingVideosAPI) {
		[[ManagerFlickogramAPI videoExemplar] getTrendingVideosOnPage:@"1"
															 withType:currentType
															 delegate:self];
	}
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}


@end
