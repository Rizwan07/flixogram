//
//  FlickogramProtocolsAPI.h
//  Flickogram
//


#import <UIKit/UIKit.h>

@protocol FlickogramAuthorizationDelegate <NSObject>
@optional
- (void)authorizationSuccessWithRecivedData:(NSDictionary *)data;
- (void)authorizationFailedWithError:(NSString *)error;

- (void)signupSuccess;
- (void)signupFailedWithError:(NSString *)error;

- (void)getUserProfileSuccessWithRecivedData:(NSDictionary *)data;
- (void)getUserProfileFailedWithError:(NSString *)error;

- (void)followUnFollowUserSuccesWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex;
- (void)followUnFollowUserFailedWithError:(NSString *)error;

- (void)updateProfilePictureSuccessWithRecivedData:(NSDictionary *)data;
- (void)updateProfilePictureFailedWithError:(NSString *)error;

- (void)deleteProfilePictureSuccessWithRecivedData:(NSDictionary *)data;
- (void)deleteProfilePictureFailedWithError:(NSString *)error;

- (void)updatePublicProfileSuccessWithRecivedData:(NSDictionary *)data;
- (void)updatePublicProfileFailedWithError:(NSString *)error;

- (void)updatePrivateProfileSuccessWithRecivedData:(NSDictionary *)data;
- (void)updatePrivateProfileFailedWithError:(NSString *)error;

- (void)updateUserVideosArePrivateSuccessWithRecivedData:(NSDictionary *)data;
- (void)updateUserVideosArePrivateFailedWithError:(NSString *)error;

- (void)blockUserSuccessWithRecivedData:(NSDictionary *)data;
- (void)blockUserFailedWithError:(NSString *)error;

- (void)reportUserSuccessWithRecivedData:(NSDictionary *)data;
- (void)reportUserFailedWithError:(NSString *)error;
@end

@protocol FlickogramVideoDelegate <NSObject>
@optional
- (void)getPopularVideoSuccessWithRecivedData:(NSDictionary *)data;
- (void)getPopularVideoFailedWithError:(NSString *)error;

- (void)updateProgreesUploadWithCurrentValue:(long long)currentValue maxValue:(long long)maxValue;
- (void)uploadVideoSuccessWithRecivedData:(NSDictionary *)data;
- (void)uploadVideoFailedWithError:(NSString *)error;

- (void)getFollowingVideosSuccesWithRecivedData:(NSMutableDictionary *)data;
- (void)getFollowingVideosFailedWithError:(NSString *)error;

- (void)getVideoDetailsSuccesWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)index;
- (void)getVideoDetailsFailedWithError:(NSString *)error;

- (void)likeVideoSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex;
- (void)likeVideoFiledWithError:(NSString *)error;

- (void)addCommentSuccessWithRecivedData:(NSMutableDictionary *)data indexArray:(NSInteger)currentIndex;
- (void)addCommentFailedWithError:(NSString *)error;

- (void)removeCommentSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex;
- (void)removeCommentFailedWithError:(NSString *)error;

- (void)removeVideoSuccessWithRecivedData:(NSDictionary *)data indexArray:(NSInteger)currentIndex;
- (void)removeVideoFailedWithError:(NSString *)error;

- (void)getExplorerVideosSuccessWithRecivedData:(NSDictionary *)data;
- (void)getExplorerVideosFailedWithError:(NSString *)error;

- (void)getSuggestedVideosSuccessWithRecivedData:(NSMutableDictionary *)data;
- (void)getSuggestedVideosFailedWithError:(NSString *)error;

- (void)getUserLikedVideosSuccessWithRecivedData:(NSDictionary *)data;
- (void)getUserLikedVideosFailedWithError:(NSString *)error;

- (void)reflickVideoSuccessWithRecivedData:(NSDictionary *)data;
- (void)reflickVideoFailedWithError:(NSString *)error;

- (void)getMostLikedVideosSuccessWithRecivedData:(NSDictionary *)data;
- (void)getMostLikedVideosFailedWithError:(NSString *)error;

- (void)getTrendingVideosSuccessWithRecivedData:(NSDictionary *)data;
- (void)getTrendingVideosFailedWithError:(NSString *)error;

- (void)reportVideoSuccessWithRecivedData:(NSDictionary *)data;
- (void)reportVideoFailedWithError:(NSString *)error;
@end

@protocol FlickogramNotificationsDelegate <NSObject>
@optional
- (void)notificationFollowingSectionSuccessWithRecievedData:(NSDictionary *)data;
- (void)notificationFollowingSectionFailedWithError:(NSString *)error;

- (void)notificationNewsSectionSuccessWithRecievedData:(NSDictionary *)data;
- (void)notificationNewsSectionFailedWithError:(NSString *)error;

- (void)getAboutUsSuccessWithRecievedData:(NSDictionary *)data;
- (void)getAboutUsFailedWithError:(NSString *)error;
@end

@protocol FlickogramSearchDelegate <NSObject>
@optional
- (void)getVideoByTagsSuccessWithRecivedData:(NSDictionary *)data;
- (void)getVideoByTagsFailedWithError:(NSString *)error;

- (void)findFriendsSuccessWithRecievedData:(NSDictionary *)data;
- (void)findFriendsFailedWithError:(NSString *)error;

- (void)searchUsersByTagsSuccessWithRecivedData:(NSDictionary *)data;
- (void)searchUsersByTagsFailedWithError:(NSString *)error;

- (void)getListFollowersSuccessWithRecivedData:(NSMutableDictionary *)data;
- (void)getListFollowersFailedWithError:(NSString *)error;

- (void)getListFollowingsSuccessWithRecivedData:(NSMutableDictionary *)data;
- (void)getListFollowingsFailedWithError:(NSString *)error;
@end

@protocol YoutubeDelegate <NSObject>
- (void)loginSuccess:(NSString *)authToken;
- (void)loginFailure:(NSString *)error;
@end

@protocol FoursquareDelegate <NSObject>
- (void)searchCompleteSuccess:(NSDictionary *)data;
- (void)searchFailedWithError:(NSString *)error;
@end

