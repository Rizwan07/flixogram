//
//  SignUpViewController.m
//  Flickogram
//

#import "SignUpViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "FindFriendsViewController.h"
#import "SingletonAuthorizationUser.h"
#import "FlickogramAuthorization.h"

@interface SignUpViewController ()
- (void)doneSignup;
- (void)back;
@end

@implementation SignUpViewController
@synthesize scrolling = _scrolling;
@synthesize username = _username;
@synthesize email = _email;
@synthesize password = _password;
@synthesize phone = _phone;
@synthesize picture;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{	
    [super viewDidLoad];
    self.screenName = @"Sign up";
	self.navigationItem.title = @"SIGN UP";
	
	//set Done button
	UIImage *doneButtonImage = [UIImage imageNamed:@"next"];
	UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[doneButton setBackgroundImage:doneButtonImage forState:UIControlStateNormal];
	doneButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[doneButton addTarget:self action:@selector(doneSignup) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
	self.navigationItem.rightBarButtonItem = doneItem;
	
	//set Back button
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
    
	avatarImage = [UIImage imageNamed:@"Profile-thumb"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField 
{
	[textField resignFirstResponder];
	//self.scrolling.contentSize = self.view.frame.size;
	return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (self.view.frame.origin.y != -40) {
//        [UIView transitionWithView:self.view duration:0.4f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//            CGRect rect = self.view.frame;
//            rect.origin.y = -40;
//            [self.view setFrame:rect];
//            
//        } completion:^(BOOL finished){
//        }
//         ];
//    }
    [self.scrolling setContentSize:CGSizeMake(320, 240)];
    [self.scrolling setScrollEnabled:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
//    [UIView transitionWithView:self.view duration:0.4f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//        CGRect rect = self.view.frame;
//        rect.origin.y = 0;
//        [self.view setFrame:rect];
//        
//    } completion:^(BOOL finished){
//    }
//     ];
    [self.scrolling setContentOffset:CGPointMake(0, 0)];
    [self.scrolling setScrollEnabled:NO];
}

- (IBAction)changePicture:(id)sender
{
	UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    mediaUI.allowsEditing = NO;
	mediaUI.delegate = self;
	
    [self presentViewController: mediaUI animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *) info 
{	
    NSString *mediaType = [NSString stringWithString: [info objectForKey: UIImagePickerControllerMediaType]];
    // Handle a movied picked from a photo album
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
		avatarImage = [(UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage] copy];
	}
    CFRelease((__bridge CFTypeRef)(mediaType));
	[self.picture setImage:avatarImage forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)checkEmailFormat:(NSString *)emailText
{
    BOOL status;
    status = YES;
    NSString *emailString;
    emailString = emailText;
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
	
    if (([emailTest evaluateWithObject:emailString] != YES) || [emailString isEqualToString:@""])
    {
        status =  NO;
    }
	
    return status;
}

- (void)doneSignup
{
	NSString *email = self.email.text;
	NSString *password = self.password.text;
	NSString *username = self.username.text;
	NSString *phone = self.phone.text;
	
    if ([email length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please, enter Email" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
		[alert show];

    }
    else if ([username length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please, enter your Username" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
		[alert show];

    }
    else if ([password length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please, enter your Password" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
		[alert show];

    }
    else {
    
        if ([self checkEmailFormat:email]) {
            
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            [[ManagerFlickogramAPI authorizationExemplar] signupOnServerWithEmail:email
                                                                         password:password
                                                                            phone:phone picture:avatarImage
                                                                         username:username
                                                                            delegate:self];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please, check your Email" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
            [alert show];
        }
    }
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SignUp delegate
- (void)signupSuccess
{
	NSString *email = self.email.text;
	NSString *password = self.password.text;
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI authorizationExemplar] loginOnServerWithEmail:email password:password delegate:self];
}

- (void)signupFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];	
}
#pragma mark - Authorization Delegate
- (void)authorizationSuccessWithRecivedData:(NSDictionary *)data
{	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString * userID = [data objectForKey:@"id"];
	NSString * username = [data objectForKey:@"user_name"];
	NSString * profilePicture = [data objectForKey:@"profile_picture"];
	
	//NSUserDefault set
	NSDictionary *authCredentials = [NSDictionary dictionaryWithObjectsAndKeys:userID, @"id", username, @"user_name", profilePicture, @"profile_picture", nil];
	[FlickogramAuthorization saveAuthCredentialsFromDictionary:authCredentials];
	
	[[SingletonAuthorizationUser authorizationUser] setUserID:userID];
	[[SingletonAuthorizationUser authorizationUser] setUserName:username];
	[[SingletonAuthorizationUser authorizationUser] setProfilePicture:profilePicture];
	[[SingletonAuthorizationUser authorizationUser] setEmail:[data objectForKey:@"email"]];
	[[SingletonAuthorizationUser authorizationUser] setPassword:[data objectForKey:@"password"]];

	
	FindFriendsViewController *findFriends = [[FindFriendsViewController alloc] init];
	findFriends.navigationItem.hidesBackButton = YES;
	[self.navigationController pushViewController:findFriends animated:YES];
}

- (void)authorizationFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

@end
