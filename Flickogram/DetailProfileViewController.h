//
//  DetailProfileViewController.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "ManagerFlickogramAPI.h"

typedef enum {
	ShowFollowers,
	ShowFollowings
} DetailType;

@interface DetailProfileViewController : UITableViewController <FlickogramSearchDelegate, FlickogramAuthorizationDelegate> {
	NSInteger currentPage;
	BOOL isShowMore;
	NSMutableArray *flickogramUsers;
}

@property (nonatomic, strong) NSString *selectedUserID;
@property (nonatomic) DetailType detailType;

@end
