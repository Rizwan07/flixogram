//
//  OptionsViewController.m
//  Flickogram

#import "OptionsViewController.h"
#import "FindFriendsViewController.h"
#import "LikedVideoMostLikedVideoViewController.h"
#import "SingletonAuthorizationUser.h"
#import "EditProfileViewController.h"
#import "FlickogramAuthorization.h"
#import "SharingSettingsViewController.h"

#import "AppDelegate.h"
#import "MainViewController.h"
#import "ExploreViewController.h"
#import "ProfileViewController.h"
#import "YourVideoViewController.h"

@interface OptionsViewController ()
- (void)logoutAndReset;
@end

@implementation OptionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Options";
    
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if ([defaults objectForKey:@"VideosArePrivate"]) {
		isVideosPrivate = YES;
	} else {
		isVideosPrivate = NO;
	}
	self.title = @"Options";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table Delegate/Source methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger countCells = 0;
	tableSection = section;
	switch (tableSection) {
		case FirstSection:
			countCells = 2;
			break;
		case SecondSection:
			countCells = 2;
			break;
		case ThirdSection:
			countCells = 4;
			break;
		case FourthSection:
			countCells = 1;
			break;
	}
	return countCells;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	NSString *titleForHeader;
	tableSection = section;
	switch (tableSection) {
		case FirstSection:
			break;
		case SecondSection:
			titleForHeader = @"Videos";
			break;
		case ThirdSection:
			titleForHeader = @"Account";
			break;
		case FourthSection:
			titleForHeader = @"Privacy";
			break;
	}
	return titleForHeader;
}
   
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *CellIdentifier = @"OptionsCell";
	if (indexPath.section == FourthSection) {
		CellIdentifier = @"VideosArePrivateCell";
	}
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									  reuseIdentifier:CellIdentifier];
    }
	tableSection = indexPath.section;
	switch (tableSection) {
		case FirstSection: {
			if (indexPath.row == 0) {
				cell.textLabel.text = @"Find friends";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			} else if (indexPath.row == 1) {
				cell.textLabel.text = @"Invite friends";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			}
			break;
		}
		case SecondSection: {
			if (indexPath.row == 0) {
				cell.textLabel.text = @"Your videos";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			} else if (indexPath.row == 1) {
				cell.textLabel.text = @"Videos you've liked";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			}
			break;
		}
		case ThirdSection: {
			if (indexPath.row == 0) {
				cell.textLabel.text = @"Edit profile";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			} else if (indexPath.row == 1) {
				cell.textLabel.text = @"Edit sharing settings";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			} else	if (indexPath.row == 2) {
				cell.textLabel.text = @"Change profile picture";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			} else if (indexPath.row == 3) {
				cell.textLabel.text = @"Log out";
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			}
			break;
		}
		case FourthSection: {
			cell.textLabel.text = @"Videos are private";
			CGRect privateSwitcherFrame = CGRectMake(215, 8, 79, 27);
			UISwitch *privateSwitcher = [[UISwitch alloc] initWithFrame:privateSwitcherFrame];
			[privateSwitcher addTarget:self action:@selector(swithesEvent) forControlEvents:UIControlEventValueChanged];
			privateSwitcher.on = isVideosPrivate;
			cell.accessoryView = privateSwitcher;
			break;
		}
	}
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	tableSection = indexPath.section;
	switch (tableSection) {
		case FirstSection: { //Find and invite friends
			if (indexPath.row == 0) {
				//show Find Friends
				FindFriendsViewController *findFriends = [[FindFriendsViewController alloc] init];
				findFriends.isShowBackButton = YES;
				findFriends.isShowSuggestedUsers = YES;
				[self.navigationController pushViewController:findFriends animated:YES];
			} else if (indexPath.row == 1) {
				//show Invite Friends
				ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
				picker.peoplePickerDelegate = self;
				// Display only a person's phone, email, and birthdate
				NSArray *displayedItems = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonPhoneProperty], nil];
				
				
				picker.displayedProperties = displayedItems;
				// Show the picker
				[self presentViewController:picker animated:YES completion:nil];
				
			}
			break;
		}
		case SecondSection: { //Liked videos
			if (indexPath.row == 0) {
				YourVideoViewController *yourVideo = [[YourVideoViewController alloc] init];
				[self.navigationController pushViewController:yourVideo animated:YES];
			} else if (indexPath.row == 1) {
				LikedVideoMostLikedVideoViewController *likedVideo = [[LikedVideoMostLikedVideoViewController alloc] init];
				likedVideo.selectedVideoAPI = UserLikedVideosAPI;
				[self.navigationController pushViewController:likedVideo animated:YES];
			}
			break;
		}
		case ThirdSection: { //Account
			if (indexPath.row == 0) {
				//edit profile
				EditProfileViewController *editProfile = [[EditProfileViewController alloc] init];
				[self.navigationController pushViewController:editProfile animated:YES];
			} else if (indexPath.row == 1) {
				//edit sharing settings
				SharingSettingsViewController *sharing = [[SharingSettingsViewController alloc] init];
				[self.navigationController pushViewController:sharing animated:YES];
			} else	if (indexPath.row == 2) {
				//change profile picture
				UIActionSheet *changePicture = [[UIActionSheet alloc] initWithTitle:@"Set a profile picture"
																		   delegate:self
																  cancelButtonTitle:@"Cancel"
															 destructiveButtonTitle:@"Remove current picture"
																  otherButtonTitles:@"Take picture", @"Choose from Library", nil];
				[changePicture showFromTabBar:self.tabBarController.tabBar];
				
			} else if (indexPath.row == 3) {
				//Log out
				UIAlertView *alertLogout = [[UIAlertView alloc] initWithTitle:@"Are you sure?"
																	  message:nil
																	 delegate:self
															cancelButtonTitle:nil
															otherButtonTitles:@"Cancel", @"Log out", nil];
				[alertLogout show];
			}
			break;
		}
		case FourthSection:
			break;
	}
}

- (void)swithesEvent
{
	isVideosPrivate = !isVideosPrivate;
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if (isVideosPrivate) {
		[defaults setBool:isVideosPrivate forKey:@"VideosArePrivate"];
	} else {
		[defaults removeObjectForKey:@"VideosArePrivate"];
	}
	NSString *userAuth = [[SingletonAuthorizationUser authorizationUser] userID];
	NSString *privateState;
	if (isVideosPrivate) {
		privateState = @"ON";
	} else {
		privateState = @"OFF";
	}
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	[[ManagerFlickogramAPI authorizationExemplar] updateUserVideosPrivateForUserID:userAuth
																  videosArePrivate:privateState
																		  delegate:self];
}
#pragma mark - MessageUI
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	if (result == MessageComposeResultCancelled) {
		[controller dismissViewControllerAnimated:YES completion:nil];
	} else if (result == MessageComposeResultSent) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Done"
														message:@"Thanks! Your invite was sent."
													   delegate:nil
											  cancelButtonTitle:nil
											  otherButtonTitles:@"Dismiss", nil];
		[alert show];
		[controller dismissViewControllerAnimated:YES completion:nil];
	}
}

#pragma mark - Action Sheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch (buttonIndex) {
		case 0: { //Remove photo
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
			NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
            if (authUser) {
                
                [[ManagerFlickogramAPI authorizationExemplar] deleteProfilePictureForUserID:authUser delegate:self];
            }
			break;
		}
		case 1: { //Take picture
			[self startCameraController];
			break;
		}
		case 2: {
			[self startMediaBrowser];
			break;
		}
	}
}

- (void)startCameraController
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
		return;
	}
	
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
	cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
	
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = self;
	
	[self presentViewController:cameraUI animated:YES completion:nil];
}

- (void)startMediaBrowser
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO) {
		return;
	}
	
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
	cameraUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
	
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    cameraUI.delegate = self;
	
	[self presentViewController:cameraUI animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage;
	originalImage = (UIImage *) [info objectForKey:UIImagePickerControllerOriginalImage];
	[self dismissViewControllerAnimated:YES completion:^(void){
		[self changeProfilePhotoOnImage:originalImage];
	}];
}

- (void)changeProfilePhotoOnImage:(UIImage *)newPhoto
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	NSString *authUser = [[SingletonAuthorizationUser authorizationUser] userID];
	[[ManagerFlickogramAPI authorizationExemplar] updateProfilePictureForUserID:authUser picture:newPhoto delegate:self];
}

#pragma mark - Update Photo Delegate
- (void)updateProfilePictureSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString *profilePicture = [[data objectForKey:@"user_data"] objectForKey:@"profile_picture"];
	[[SingletonAuthorizationUser authorizationUser] setProfilePicture:profilePicture];
    
    if (profilePicture) {
        
        [[NSUserDefaults standardUserDefaults] setObject:profilePicture forKey:@"profile_picture"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Profile pic changed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView setTag:1001];
    [alertView show];
    
}

- (void)updateProfilePictureFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - Delete Photo Delegate
- (void)deleteProfilePictureSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	NSString *profilePicture = [[data objectForKey:@"user_data"] objectForKey:@"profile_picture"];
	[[SingletonAuthorizationUser authorizationUser] setProfilePicture:profilePicture];
    
    if (profilePicture) {
        
        [[NSUserDefaults standardUserDefaults] setObject:profilePicture forKey:@"profile_picture"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Your profile pic removed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView setTag:1001];
    [alertView show];

}

- (void)deleteProfilePictureFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

#pragma mark - User Videos Private Delegate
- (void)updateUserVideosArePrivateSuccessWithRecivedData:(NSDictionary *)data
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (void)updateUserVideosArePrivateFailedWithError:(NSString *)error
{
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
	[alert show];
}

#pragma mark - Logout and reset
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 1001) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else {
        if (buttonIndex == 1) {
            [self logoutAndReset];
        }
    }
}
- (void)logoutAndReset
{
	[FlickogramAuthorization removeAuthCredentials];
	
	//MainViewController == index 0
	UINavigationController *mainNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:0];
	[mainNavigationController popToRootViewControllerAnimated:NO];
	MainViewController *mainViewController = (MainViewController *) [mainNavigationController.viewControllers objectAtIndex:0];
	[mainViewController resetData];
	
	//ExploreViewController == index 1
	UINavigationController *exploreNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:1];
	[exploreNavigationController popToRootViewControllerAnimated:NO];
	ExploreViewController *exploreViewController = (ExploreViewController *) [exploreNavigationController.viewControllers objectAtIndex:0];
	[exploreViewController resetData];
	
	//NotificationViewController == index 3
	UINavigationController *notificationNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:1];
	[notificationNavigationController popToRootViewControllerAnimated:NO];
	
	//ProfileViewController == index 4
	UINavigationController *profileNavigationController = (UINavigationController *)[self.tabBarController.viewControllers objectAtIndex:4];
	[profileNavigationController popToRootViewControllerAnimated:NO];
	ProfileViewController *profileViewController = (ProfileViewController *) [profileNavigationController.viewControllers objectAtIndex:0];
	[profileViewController resetData];
	
	AppDelegate *delegate =	(AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate.rootViewController selectTabBarIndex:0];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate methods
// Displays the information of a selected person
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
	return YES;
}


// Does not allow users to perform default actions such as dialing a phone number, when they select a person property.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
	  shouldContinueAfterSelectingPerson:(ABRecordRef)person
								property:(ABPropertyID)property
							  identifier:(ABMultiValueIdentifier)identifier
{
	if (property == kABPersonPhoneProperty) {
    	ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
    	for (CFIndex i = 0; i < ABMultiValueGetCount(multiPhones); i++) {
    		if (identifier == ABMultiValueGetIdentifierAtIndex (multiPhones, i)) {
    			CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
    			
    			NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
    			CFRelease(phoneNumberRef);
				[self dismissViewControllerAnimated:NO completion:^{
					BOOL isCanSendMessage =	[MFMessageComposeViewController canSendText];
					if (isCanSendMessage) {
						MFMessageComposeViewController *messageCompose = [[MFMessageComposeViewController alloc] init];
						messageCompose.messageComposeDelegate = self;
						messageCompose.recipients = [NSArray arrayWithObject:phoneNumber];
						messageCompose.body = @"Download Flickogram to explore my videos: http://example.com";
						[self presentViewController:messageCompose animated:NO completion:nil];
					}
				}];
    		}
    	}
        CFRelease(multiPhones);
    }
	return NO;
}


// Dismisses the people picker and shows the application when users tap Cancel.
- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


@end
