//
//  SharingSettingsViewController.m
//  Flickogram
//

#import "SharingSettingsViewController.h"
#import "TwitterSettings.h"
#import "GDataEntryYouTubeUpload.h"
#import "GDataServiceGoogleYouTube.h"
#import "GDataYouTubeConstants.h"
#import <Accounts/Accounts.h>
#import <FacebookSDK/FacebookSDK.h>

@interface SharingSettingsViewController () {
    GTMOAuth2Authentication *authToken;
}
- (void)changeTwitterState;
- (void)changeFacebookState;
- (void)changeYoutubeState;
@end

@implementation SharingSettingsViewController
@synthesize table = _table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Share Settings";
    
	UIImage *backButtonImage = [UIImage imageNamed:@"back"];
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
	backButton.frame = CGRectMake(0.0, 0.0, 50, 30);
	[backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
	self.navigationItem.leftBarButtonItem = backItem;
	
    imageViewTwitter = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn-twitter"]];
    [imageViewTwitter setFrame:CGRectMake(5, 5, 25, 25)];
    imageViewFacebook = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn-fb"]];
    [imageViewFacebook setFrame:CGRectMake(5, 5, 25, 25)];
    imageViewYoutube = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn-youtube"]];
    [imageViewYoutube setFrame:CGRectMake(5, 5, 25, 25)];
    
	//twitter toggle
	toggleTwitterState = [UIButton buttonWithType:UIButtonTypeCustom];
	[toggleTwitterState setImage:[UIImage imageNamed:@"button-off"] forState:UIControlStateNormal];
	[toggleTwitterState setImage:[UIImage imageNamed:@"button-on"] forState:UIControlStateSelected];
	[toggleTwitterState setFrame:CGRectMake(5+30, 5, 30, 30)];
	
	//facebook toggle
	toggleFacebookState = [UIButton buttonWithType:UIButtonTypeCustom];
	[toggleFacebookState setImage:[UIImage imageNamed:@"button-off"] forState:UIControlStateNormal];
	[toggleFacebookState setImage:[UIImage imageNamed:@"button-on"] forState:UIControlStateSelected];
	[toggleFacebookState setFrame:CGRectMake(5+30, 5, 30, 30)];
	
	//Youtube toggle
	toggleYoutubeState = [UIButton buttonWithType:UIButtonTypeCustom];
	[toggleYoutubeState setImage:[UIImage imageNamed:@"button-off"] forState:UIControlStateNormal];
	[toggleYoutubeState setImage:[UIImage imageNamed:@"button-on"] forState:UIControlStateSelected];
	[toggleYoutubeState setFrame:CGRectMake(5+30, 5, 30, 30)];
	
	titleTwitter = [[UILabel alloc] initWithFrame:CGRectMake(35+30, 10, 250, 20)];
	titleTwitter.text = @"Twitter";
	titleTwitter.backgroundColor = [UIColor clearColor];
	
	titleFacebook = [[UILabel alloc] initWithFrame:CGRectMake(35+30, 10, 250, 20)];
	titleFacebook.text = @"Facebook";
	titleFacebook.backgroundColor = [UIColor clearColor];
	
	titleYoutube = [[UILabel alloc] initWithFrame:CGRectMake(35+30, 10, 250, 20)];
	titleYoutube.text = @"Youtube";
	titleYoutube.backgroundColor = [UIColor clearColor];
	
	self.title = @"Sharing Settings";
}

- (void)back
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeTwitterState
{
	toggleTwitterState.selected = !toggleTwitterState.selected;
	[self.table reloadData];
}

- (void)changeFacebookState
{
	toggleFacebookState.selected = !toggleFacebookState.selected;
	[self.table reloadData];
}

- (void)changeYoutubeState
{
	toggleYoutubeState.selected = !toggleYoutubeState.selected;
	[self.table reloadData];
}

#pragma mark - UITableView Delegate/DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return	1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//return 3;
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"SharingCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
									  reuseIdentifier:CellIdentifier];
    }
	NSInteger currentRow = indexPath.row;
	switch (currentRow) {
		case 0: {
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			if ([defaults objectForKey:@"TwitterAccountIdentifier"]) {
				toggleTwitterState.selected = YES;
			} else {
				toggleTwitterState.selected = NO;
			}
            [cell.contentView addSubview:imageViewTwitter];
			[cell.contentView addSubview:titleTwitter];
			[cell.contentView addSubview:toggleTwitterState];
			break;
		}
		case 1: {
			if ((FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) ||
				FBSession.activeSession.state == FBSessionStateOpen) {
				toggleFacebookState.selected = YES;
			} else {
				toggleFacebookState.selected = NO;
			}
            [cell.contentView addSubview:imageViewFacebook];
			[cell.contentView addSubview:titleFacebook];
			[cell.contentView addSubview:toggleFacebookState];
			break;
		}
		case 2: {
			GTMOAuth2Authentication *auth = [self getAuthYoutube];
			BOOL authSuccess = NO;
			if (auth) {
				BOOL didAuth = [GTMOAuth2ViewControllerTouch authorizeFromKeychainForName:@"My App:Flickogram" authentication:auth];
				if (didAuth) {
					BOOL isSignedIn = [auth canAuthorize];
					if (isSignedIn) {
						authSuccess = YES;
						toggleYoutubeState.selected = YES;
					}
				}
			}
			if (!authSuccess) {
				toggleYoutubeState.selected = NO;
			}
            [cell.contentView addSubview:imageViewYoutube];
			[cell.contentView addSubview:titleYoutube];
			[cell.contentView addSubview:toggleYoutubeState];
			break;
		}
	}
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.table deselectRowAtIndexPath:indexPath animated:YES];
	
	NSInteger currentRow = indexPath.row;
	switch (currentRow) {
		case 0: {
			if (toggleTwitterState.selected) {
				UIActionSheet *actionSheetTwitter = [[UIActionSheet alloc] initWithTitle:@"Unlink your Twitter account?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Unlink" otherButtonTitles:nil];
				actionSheetTwitter.tag = 0;
				[actionSheetTwitter showFromTabBar:self.tabBarController.tabBar];
			} else {
				[self chooseTwitterAccount];
			}
			break;
		}
		case 1: {
			if (toggleFacebookState.selected) {
				UIActionSheet *actionSheetFacebook = [[UIActionSheet alloc] initWithTitle:@"Unlink your Facebook account?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Unlink" otherButtonTitles:nil];
				actionSheetFacebook.tag = 1;
				[actionSheetFacebook showFromTabBar:self.tabBarController.tabBar];
			} else {
				[self openSession];
			}
			break;
		}
		case 2: {
			if (toggleYoutubeState.selected) {
				UIActionSheet *actionSheetYoutube = [[UIActionSheet alloc] initWithTitle:@"Unlink your Youtube account?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Unlink" otherButtonTitles:nil];
				actionSheetYoutube.tag = 2;
				[actionSheetYoutube showFromTabBar:self.tabBarController.tabBar];
			} else {
				[self loginOnYoutube];
			}
			break;
		}
	}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSInteger currentActionSheet = actionSheet.tag;
	if (currentActionSheet == 99) { //choose twitter users
		NSInteger count = [twitterAccounts count];
		if (buttonIndex != count) {
			ACAccount *account = [twitterAccounts objectAtIndex:buttonIndex];
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
			[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
		}
	} else {
		if (buttonIndex == 0) {
			NSInteger currentSocialNetwork = actionSheet.tag;
			if (currentSocialNetwork == 0) { //twitter
				NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
				[defaults removeObjectForKey:@"TwitterAccountIdentifier"];
			} else if (currentSocialNetwork == 1) { //facebook
				[FBSession.activeSession closeAndClearTokenInformation];
			} else if (currentSocialNetwork == 2) { //youtube
				[GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:@"My App:Flickogram"];
			}
		}
	}
	[self.table reloadData];
}

- (void)reloadTable
{
	[self.table reloadData];
}

#pragma mark - Social Networks
//Choose twitter account
- (void)chooseTwitterAccount
{
	//  First, we need to obtain the account instance for the user's Twitter account
	ACAccountStore *store = [[ACAccountStore alloc] init];
	ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
	//  Request permission from the user to access the available Twitter accounts
	
	//IOS 6 work
	if ([store respondsToSelector:@selector(requestAccessToAccountsWithType:options:completion:)]) {
		if( [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] ) {
			[store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
				if (!granted) {
					// The user rejected your request
					NSLog(@"User rejected access to the account.");
				}
				else {
					// Grab the available accounts
					twitterAccounts = [store accountsWithAccountType:twitterAccountType];
					if ([twitterAccounts count] > 0) {
						if ([twitterAccounts count] > 1) {
							[self performSelectorOnMainThread:@selector(showActionSheet) withObject:self waitUntilDone:NO];
						} else {
							// Use the first account for simplicity
							ACAccount *account = [twitterAccounts objectAtIndex:0];
							NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
							[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
							[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:YES];
						}
					} else {
						[self performSelectorOnMainThread:@selector(showAlertViewTwitterForIOS6) withObject:nil waitUntilDone:NO];
					}
				}
				
			}];
		} else {
			[self performSelectorOnMainThread:@selector(showAlertViewTwitterForIOS6) withObject:nil waitUntilDone:NO];
		}
	}
	//ios 5
	else {
        [store requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
		//[store requestAccessToAccountsWithType:twitterAccountType withCompletionHandler:^(BOOL granted, NSError *error) {
			if (!granted) {
				// The user rejected your request
				NSLog(@"User rejected access to the account.");
			}
			else {
				// Grab the available accounts
				twitterAccounts = [store accountsWithAccountType:twitterAccountType];
				if ([twitterAccounts count] > 0) {
					if ([twitterAccounts count] > 1) {
						[self performSelectorOnMainThread:@selector(showActionSheet) withObject:self waitUntilDone:NO];
					} else {
						// Use the first account for simplicity
						ACAccount *account = [twitterAccounts objectAtIndex:0];
						NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
						[defaults setObject:account.identifier forKey:@"TwitterAccountIdentifier"];
						[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:YES];
					}
				} else {
					[self performSelectorOnMainThread:@selector(showAlertViewTwitter) withObject:nil waitUntilDone:NO];
				}
			}
		}];
	}
}

- (void)showActionSheet
{
	UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Twitter"
														delegate:self
											   cancelButtonTitle:nil
										  destructiveButtonTitle:nil
											   otherButtonTitles:nil];
	for (ACAccount *account in twitterAccounts) {
		[action addButtonWithTitle:account.username];
	}
	NSInteger numberOfButtons = action.numberOfButtons;
	[action addButtonWithTitle:@"Cancel"];
	action.cancelButtonIndex = numberOfButtons;
	action.tag = 99;
	[action showFromTabBar:self.tabBarController.tabBar];
}

- (void)showAlertViewTwitterForIOS6
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
													message:@"There are no Twitter accounts configured. You can add or create a Twitter account in iPhone Settings."
												   delegate:self cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

- (void)showAlertViewTwitter
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Twitter Accounts"
													message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings."
												   delegate:self cancelButtonTitle:@"Cancel"
										  otherButtonTitles:@"Settings", nil];
	alert.tag = 1;
	[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == 1) { //Twitter alert view
		if (buttonIndex == 1) {
			[TwitterSettings openTwitterAccounts];
		}
	}
}

//Open session for facebook
- (void)openSession
{
	NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_actions", nil];
	[FBSession openActiveSessionWithPermissions:permissions
                                   allowLoginUI:YES
                              completionHandler:
	 ^(FBSession *session,
	   FBSessionState state, NSError *error) {
		 if (error) {
			 UIAlertView *alertView = [[UIAlertView alloc]
									   initWithTitle:@"Error"
									   message:error.localizedDescription
									   delegate:nil
									   cancelButtonTitle:@"OK"
									   otherButtonTitles:nil];
			 alertView.tag = 3;
			 [alertView show];
		 } else {
			 [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:YES];
		 }
	 }];
}

//Work with Youtube
- (GTMOAuth2Authentication *)getAuthYoutube {
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:kMyClientID clientSecret:kMyClientSecret];
	return auth;
}

- (void)loginOnYoutube
{
	NSString *scope = @"https://gdata.youtube.com";
	NSString *kKeychainItemName = @"My App:Flickogram";
	NSString *kMyClientID = @"127003560135-8fmf8gr7cd64etrq06fr379fkpucam31.apps.googleusercontent.com";     // pre-assigned by service
	NSString *kMyClientSecret = @"JNYcDUcWevxEtaSz57B0UJH2"; // pre-assigned by service
	
	GTMOAuth2ViewControllerTouch *viewController;
	viewController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:scope
																clientID:kMyClientID
															clientSecret:kMyClientSecret
														keychainItemName:kKeychainItemName
																delegate:self
														finishedSelector:@selector(viewController:finishedWithAuth:error:)];
	
	[[self navigationController] pushViewController:viewController animated:YES];
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
	if (error != nil) {
		// Authentication failed
		NSLog(@"%@", error);
	} else {
		// Authentication succeeded
		[self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
	}
}

////Share Youtube
//- (void)getAuthTokenYoutube
//{
//    authToken = [statesSharingInSocialNetworks objectForKey:@"AuthTokenYoutube"];
//    
//	NSString *urlStr = @"https://accounts.google.com/o/oauth2/auth";
//	NSURL *url = [NSURL URLWithString:urlStr];
//	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//	[authToken authorizeRequest:request
//                       delegate:self
//              didFinishSelector:@selector(authentication:request:finishedWithError:)];
//}
//
//- (void)authentication:(GTMOAuth2Authentication *)auth
//               request:(NSMutableURLRequest *)request
//     finishedWithError:(NSError *)error {
//	if (error != nil) {
//		// Authorization failed
//		NSLog(@"Auth token = %@", error);
//	} else {
//		// Authorization succeeded
//		authToken = auth;
//		[self uploadVideoOnYoutube];
//	}
//}
//
//- (GDataServiceGoogleYouTube *)youTubeService {
//	static GDataServiceGoogleYouTube* service = nil;
//	if (!service) {
//		service = [[GDataServiceGoogleYouTube alloc] init];
//		[service setShouldCacheDatedData:YES];
//		[service setServiceShouldFollowNextLinks:YES];
//		[service setIsServiceRetryEnabled:YES];
//	}
//	return service;
//}
//
//- (void)uploadVideoOnYoutube
//{
//    NSString *devKey = @"AI39si7GQ3cqX8NdiVh7vJgFU6HNjcedSFcVT877fa2YFBDqLI7RsrvKxamkKAU5PXrPUeLIDujoU6wbYk4D-S-sEthpJARG0A";
//	
//    GDataServiceGoogleYouTube *service = [self youTubeService];
//	[service setAuthorizer:authToken];
//	[[service authorizer] setShouldAuthorizeAllRequests:YES];
//    [service setYouTubeDeveloperKey:devKey];
//    
//    NSURL *url = [GDataServiceGoogleYouTube youTubeUploadURLForUserID:kGDataServiceDefaultUser];
//	
//    // load the file data
//    
//    NSString *strUrl = [[NSBundle mainBundle] pathForResource:@"123" ofType:@"mp4"];
//    NSURL *videoURLForSharing = [NSURL URLWithString:strUrl];
//    
//    NSString *path = [videoURLForSharing path];
//    NSData *data = [NSData dataWithContentsOfURL:videoURLForSharing];
//    NSString *filename = [path lastPathComponent];
//    
//    // gather all the metadata needed for the mediaGroup
//    NSString *titleStr = @"YouTubeVideo";
//    GDataMediaTitle *title = [GDataMediaTitle textConstructWithString:titleStr];
//    
//    NSString *categoryStr = @"Entertainment";
//    GDataMediaCategory *category = [GDataMediaCategory mediaCategoryWithString:categoryStr];
//    [category setScheme:kGDataSchemeYouTubeCategory];
//    
//    NSString *descStr = @"Video";
//    GDataMediaDescription *desc = [GDataMediaDescription textConstructWithString:descStr];
//    
//    NSString *keywordsStr = @"Flixogram";
//    GDataMediaKeywords *keywords = [GDataMediaKeywords keywordsWithString:keywordsStr];
//    
//    BOOL isPrivate = NO;
//    
//    GDataYouTubeMediaGroup *mediaGroup = [GDataYouTubeMediaGroup mediaGroup];
//    [mediaGroup setMediaTitle:title];
//    [mediaGroup setMediaDescription:desc];
//    [mediaGroup addMediaCategory:category];
//    [mediaGroup setMediaKeywords:keywords];
//    [mediaGroup setIsPrivate:isPrivate];
//    
//    NSString *mimeType = [GDataUtilities MIMETypeForFileAtPath:path
//											   defaultMIMEType:@"video/mov"];
//    
//    // create the upload entry with the mediaGroup and the file data
//    GDataEntryYouTubeUpload *entry;
//    entry = [GDataEntryYouTubeUpload uploadEntryWithMediaGroup:mediaGroup
//                                                          data:data
//                                                      MIMEType:mimeType
//                                                          slug:filename];
//	
//    SEL progressSel = @selector(ticket:hasDeliveredByteCount:ofTotalByteCount:);
//    [service setServiceUploadProgressSelector:progressSel];
//    
//    GDataServiceTicket *ticket;
//    ticket = [service fetchEntryByInsertingEntry:entry
//                                      forFeedURL:url
//                                        delegate:self
//                               didFinishSelector:@selector(uploadTicket:finishedWithEntry:error:)];
//    
//    [self setUploadTicket:ticket];
//}


@end
