//
//  SuggestUserCell.m
//  Flickogram
//


#import "SuggestUserCell.h"

@implementation SuggestUserCell
@synthesize username = _username;
@synthesize userThumb = _userThumb;
@synthesize bottomText = _bottomText;
@synthesize firstThumb = _firstThumb;
@synthesize secondThumb = _secondThumb;
@synthesize thirdThumb = _thirdThumb;
@synthesize fourthThumb = _fourthThumb;
@synthesize toggleFollow = _toggleFollow;

@synthesize firstBorder = _firstBorder;
@synthesize secondBorder = _secondBorder;
@synthesize thirdBorder = _thirdBorder;
@synthesize fourthBorder = _fourthBorder;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPhotoUser:(NSString *)photoUser
{
	self.userThumb.imageURL = [NSURL URLWithString:photoUser];
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
	[super willMoveToSuperview:newSuperview];
	
	if(!newSuperview) {
		[self.userThumb cancelImageLoad];
	}
}

- (void)setVideosThumbFromDictionary:(NSDictionary *)videos
{
	NSInteger currentThumb = 1;
	for (NSDictionary *currentVideo in videos) {
		NSString *videoThumbnail = [currentVideo objectForKey:@"thumbnail"];
		switch (currentThumb) {
			case 1: {
				self.firstBorder.hidden = NO;
				self.firstThumb.imageURL = [NSURL URLWithString:videoThumbnail];
				self.firstThumb.tag = currentThumb;
				break;
			}
			case 2: {
				self.secondBorder.hidden = NO;
				self.secondThumb.imageURL = [NSURL URLWithString:videoThumbnail];
				self.secondThumb.tag = currentThumb;
				break;
			}
			case 3: {
				self.thirdBorder.hidden = NO;
				self.thirdThumb.imageURL = [NSURL URLWithString:videoThumbnail];
				self.thirdThumb.tag = currentThumb;
				break;
			}
			case 4: {
				self.fourthBorder.hidden = NO;
				self.fourthThumb.imageURL = [NSURL URLWithString:videoThumbnail];
				self.fourthThumb.tag = currentThumb;
				break;
			}
		}
		currentThumb++;
	}
	
}


@end
