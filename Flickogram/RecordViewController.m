//
//  ViewController.m
//  Flickogram


#import "RecordViewController.h"
#import "FlickogramCaptureManager.h"
#import "FlickogramRecorder.h"
#import "FlickogramConstants.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "FlickogramVideoEditingViewController.h"
#import "ManagerFlickogramAPI.h"

@interface RecordViewController (FlickogramCaptureManagerDelegate) <FlickogramCaptureManagerDelegate, VideoEffectFrameDelegate>
@end

@implementation RecordViewController

@synthesize recordButton = _recordButton;
@synthesize captureManager = _captureManager;
@synthesize videoPreviewView = _videoPreviewView;
@synthesize captureVideoPreviewLayer = _captureVideoPreviewLayer;
@synthesize cameraToggleButton = _cameraToggleButton;
@synthesize recLabel = _recLabel;
@synthesize recordTimer = _recordTimer;
@synthesize timerLabel = _timerLabel;
@synthesize playerLayer = _playerLayer;
@synthesize player = _player;
@synthesize playButton = _playButton;
@synthesize panelEffects = _panelEffects;
@synthesize progressBar = _progressBar;
@synthesize grayBackground = _grayBackground;
@synthesize percentageCompletion = _percentageCompletion;
@synthesize flachToggleButton = _flachToggleButton;
@synthesize closeRecordViewButton = _closeRecordViewButton;
@synthesize topBarBackground = _topBarBackground;
@synthesize showGalleryButton = _showGalleryButton;
@synthesize showColorFilterButton = _showColorFilterButton;
@synthesize bottomPanel = _bottomPanel;

- (void)viewDidLoad
{
	self.navigationController.navigationBar.hidden = YES;
	UIImage *backgroundNavigationBarImage = [UIImage imageNamed:@"header"];
	[self.navigationController.navigationBar setBackgroundImage:backgroundNavigationBarImage forBarMetrics:UIBarMetricsDefault];
	
	if ([self captureManager] == nil) {
		FlickogramCaptureManager *manager = [[FlickogramCaptureManager alloc] init];
		[self setCaptureManager:manager];
		
		[[self captureManager] setDelegate:self];
		
		if ([[self captureManager] setupSession]) {
			// Create video preview layer and add it to the UI
			AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[[self captureManager] session]];
			UIView *view = [self videoPreviewView];
			CALayer *viewLayer = [view layer];
			[viewLayer setMasksToBounds:YES];
			
			CGRect bounds = [view bounds];
			[newCaptureVideoPreviewLayer setFrame:bounds];
					
			if ([newCaptureVideoPreviewLayer respondsToSelector:@selector(connection)])
			{
				if ([newCaptureVideoPreviewLayer.connection isVideoOrientationSupported])
				{
					[newCaptureVideoPreviewLayer.connection setVideoOrientation:(AVCaptureVideoOrientation)self.interfaceOrientation];
				}
			}
//			else
//			{
//				// Deprecated in 6.0; here for backward compatibility
//				if ([newCaptureVideoPreviewLayer isOrientationSupported])
//				{
//					[newCaptureVideoPreviewLayer setOrientation:AVCaptureVideoOrientationPortrait];
//				}
//			}

            // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[[self captureManager] session] startRunning];
            });

			[newCaptureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
			
			[viewLayer insertSublayer:newCaptureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
			
			[self setCaptureVideoPreviewLayer:newCaptureVideoPreviewLayer];
            
			//Create panel with color effects
			self.panelEffects = [[ScrollingPanelEffects alloc] initWithFrame:CGRectMake(0, 320, 320, 70)];
			self.panelEffects.hidden = YES;
			self.panelEffects.showsHorizontalScrollIndicator = NO;
			[self.view insertSubview:self.panelEffects belowSubview:self.grayBackground];
		
			//create notifications
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(startFilteringVideo)
														name:NameNotificationStartFilteringVideo
													  object:nil]; 
			
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(finishFilteringVideo:)
														 name:NameNotificationFinishFilteringVideo
													   object:nil]; 
			isFlashMode = NO; //toggle flash
			isRecordingMade = NO; //finish record
			isNowRecord = NO; //now there is a recording
		}		
	}
	[super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Toolbar Actions
- (IBAction)toggleCamera:(id)sender
{
    //Toggle between cameras when there is more than one
	if (!isNowRecord) {
		[[self captureManager] toggleCamera];
	}
}

- (IBAction)toggleRecording:(id)sender
{
	// Start recording if there isn't a recording running. Stop recording if there is.
    [[self recordButton] setEnabled:NO];
	
    [self removePlayer];
	if (!self.captureManager.session.running) {
		
		[[[self captureManager] session] startRunning];
	}
    
    if (![[[self captureManager] recorder] isRecording]) {
		numberSecondsRecorded = 0;
		self.timerLabel.text = @"00:00";
		self.panelEffects.hidden = YES;
        [[self captureManager] startRecording];
		self.recLabel.hidden = NO;
		self.recordTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(increaseRecordingTime) userInfo:nil repeats:YES];
	}
    else {
        [[self captureManager] stopRecording];
		[self.recordTimer invalidate];
	}
}

- (IBAction)showEffects:(id)sender
{
	if (isRecordingMade) {
		self.panelEffects.hidden = !self.panelEffects.hidden;
	}
}

// on/off flash
- (IBAction)toggleFlash:(id)sender
{
	if (isFlashMode) {
		[self.captureManager toggleFlashMode:NO];
		isFlashMode = NO;
	} else {
		[self.captureManager toggleFlashMode:YES];
		isFlashMode = YES;
	}
}

- (IBAction)closeVideoRecord:(id)sender
{
	self.captureManager.delegate = nil;
	if ([[[self captureManager] recorder] isRecording]) {
		[[self captureManager] stopRecording];
		[self.recordTimer invalidate];
	}
	
	if (self.player) {
		[self resetPlaybackProgress];
	}


	[[NSNotificationCenter defaultCenter] removeObserver:self.panelEffects.effects];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	self.captureManager = nil;
	self.videoPreviewView = nil;
	self.captureVideoPreviewLayer = nil;
	self.recordTimer = nil;
	self.player = nil;
	self.playerLayer = nil;
	self.playButton = nil;
	self.panelEffects = nil;
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (AVPlayerItem *)getPlayerItemWithFileUrl:(NSURL *)fileUrl shouldSetDuration:(BOOL)isDuration {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        AVURLAsset *asset = [AVURLAsset assetWithURL: fileUrl];
        if (isDuration) {
            
            duration = CMTimeGetSeconds(asset.duration);
        }
        return [AVPlayerItem playerItemWithAsset: asset];
    }
    else {
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:fileUrl];
        if (isDuration) {
            
            duration =  CMTimeGetSeconds (playerItem.duration);
        }
        return playerItem;
    }
}

#pragma mark - Movie Picker
- (IBAction)pickMovieFromGallery:(id)sender 				  
{
	if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO) || isNowRecord) {
		return;
	}
	
	if (self.player) {
		[self resetPlaybackProgress];
	}
	
	UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    mediaUI.allowsEditing = NO;
	mediaUI.delegate = self;
	
    [self presentViewController:mediaUI animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *) info {
    NSString *mediaType = [NSString stringWithString: [info objectForKey: UIImagePickerControllerMediaType]];
    // Handle a movied picked from a photo album
    if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
		NSURL *url = [info objectForKey:UIImagePickerControllerMediaURL];
		pathMovie = [url copy];
        
		NSDictionary *options = [NSDictionary dictionaryWithObject:pathMovie
															forKey:@"MoviePath"];
		[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationChangeMoviePath
															object:self 
														  userInfo:options];
		isRecordingMade = YES;
		self.panelEffects.hidden = NO;
        self.playButton.hidden = YES;
		if (!self.player) {
			[self createPlayer];	
		} else {
//			AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:pathMovie];
//			duration =  CMTimeGetSeconds (playerItem.duration);
            AVPlayerItem *playerItem = [self getPlayerItemWithFileUrl:pathMovie shouldSetDuration:YES];
			[self.player replaceCurrentItemWithPlayerItem:playerItem];
		}
        
        NSLog(@"duration %.f", duration);
        
    }
    CFRelease((__bridge CFTypeRef)(mediaType));
	mediaType = nil;
    [picker dismissViewControllerAnimated:YES completion:nil];
    canUpload = YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    self.playButton.hidden = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Recording time
- (void)increaseRecordingTime 
{
	self.timerLabel.text = [self labelStringForTime:numberSecondsRecorded];
	numberSecondsRecorded++;
    
    if (numberSecondsRecorded > 60) {
        [self toggleRecording:_recordButton];
    }
}

//convert to 00:00 format
- (NSString*) labelStringForTime: (NSUInteger) time {
	int minutes = ((int) time / 60) % 60;
	int seconds = (int) time % 60;
	return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

#pragma mark - Player
- (void)createPlayer
{
//	NSURL *fileURL = pathMovie;
//	AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:fileURL];
//	duration =  CMTimeGetSeconds (playerItem.duration);
    AVPlayerItem *playerItem = [self getPlayerItemWithFileUrl:pathMovie shouldSetDuration:YES];
	self.player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
	self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
	
	[self.videoPreviewView.layer insertSublayer:self.playerLayer atIndex:1];
	
	self.playerLayer.frame = self.videoPreviewView.layer.bounds;
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
}

- (void)removePlayer
{
	[self.player pause];
	self.player = nil;
	[self.playerLayer removeFromSuperlayer];
	self.playerLayer = nil;
	[self resetPlaybackProgress];
	
	self.playButton.hidden = YES;
}

- (IBAction)togglePlay:(id)sender
{
	
	playingTimer = [NSTimer scheduledTimerWithTimeInterval:1 
													target:self
												  selector:@selector(playback) 
												  userInfo:nil 
												   repeats:YES];
	[self.player play];
	self.playButton.selected = YES;
	self.playButton.hidden = YES;
	
}

- (IBAction)pauseVideo:(id)sender
{
	if (self.playButton.selected) {
		[self.player pause];
		[playingTimer invalidate];
		playingTimer = nil;
		self.playButton.selected = NO;
		self.playButton.hidden = NO;
        if (self.player) {
            [self resetPlaybackProgress];
        }
	}
}

- (void)playback
{
	CGFloat currentTime = CMTimeGetSeconds([self.player currentTime]);
	if (currentTime >= duration) {
		[self resetPlaybackProgress];
	}
}

- (void)resetPlaybackProgress
{
	[self.player pause];
	[playingTimer invalidate];
	playingTimer = nil;
	self.playButton.selected = NO;
	self.playButton.hidden = NO;
	CMTime newTime = CMTimeMakeWithSeconds(0, 600);
	[self.player seekToTime: newTime];
}

#pragma mark - Helper methods
- (IBAction)showVideoMotionEditor:(id)sender
{
	if (self.player) {
		[self resetPlaybackProgress];
	}
    
    if (canUpload) {
        
        FlickogramVideoEditingViewController *videoEditing = [[FlickogramVideoEditingViewController alloc] init];
        videoEditing.pathMovie = pathMovie;
        [self.navigationController pushViewController:videoEditing animated:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please give photo gallery access to filxogram in order to upload video" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)showAllElements:(id)sender
{
	if (self.player) {
		[self resetPlaybackProgress];
	}
	self.flachToggleButton.hidden = NO;
	self.cameraToggleButton.hidden = NO;
	self.closeRecordViewButton.hidden =	NO;
	self.topBarBackground.hidden = NO;
	self.panelEffects.hidden = NO;
	self.showGalleryButton.hidden = NO;
	self.showColorFilterButton.hidden = NO;
	self.recordButton.hidden = NO;
	self.bottomPanel.hidden = YES;
	self.playButton.hidden = YES;
}

- (void)dismissAllElements
{
	self.flachToggleButton.hidden = YES;
	self.cameraToggleButton.hidden = YES;
	self.closeRecordViewButton.hidden = YES;
	self.topBarBackground.hidden = YES;
	self.panelEffects.hidden = YES;
	self.showGalleryButton.hidden = YES;
	self.showColorFilterButton.hidden = YES;
	self.recordButton.hidden = YES;
	self.bottomPanel.hidden = NO;
}

#pragma mark - Notifications
- (void)startFilteringVideo
{
	//Notification for progress bar
//	[[NSNotificationCenter defaultCenter] addObserver:self
//											 selector:@selector(frameCompleted:)
//												 name:NameNotificationCompletedFilteringFrame
//											   object:nil];

    [self.panelEffects.effects setDelegate:self];
    
	self.grayBackground.hidden = NO;
	self.progressBar.hidden = NO;
	self.percentageCompletion.hidden = NO;
	
	self.view.userInteractionEnabled = NO;
    NSLog(@"start filter");
}

-(void)finishFilteringVideo:(NSNotification *)notification
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NameNotificationCompletedFilteringFrame object:nil];
	self.percentageCompletion.hidden = YES;
	self.percentageCompletion.text = @"";
	self.progressBar.progress = 0;
	self.progressBar.hidden = YES;
	self.grayBackground.hidden = YES;
	self.view.userInteractionEnabled = YES;
	self.playButton.hidden = NO;
	[self dismissAllElements]; //dismiss all display elements
	
	NSDictionary *dict = [notification userInfo];
	NSURL *fileURL = [dict objectForKey:@"MoviePath"];
	pathMovie = [fileURL copy];
//	AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:fileURL];
//	duration =  CMTimeGetSeconds (playerItem.duration);
    AVPlayerItem *playerItem = [self getPlayerItemWithFileUrl:pathMovie shouldSetDuration:YES];
	[self.player replaceCurrentItemWithPlayerItem:playerItem];
    
    NSLog(@"done filter");
}

//progress bar
//- (void)frameCompleted:(NSNotification *)notification
//{
//	NSDictionary *dict = [notification userInfo];
//	NSNumber *currentFrameObj = [dict objectForKey:@"CurrentFrame"];
//	NSNumber *countFrameObj = [dict objectForKey:@"CountFrame"]; 
//	CGFloat currentFrame = [currentFrameObj floatValue];
//	CGFloat countFrame = [countFrameObj floatValue];
//	CGFloat progress = currentFrame / countFrame;
//	CGFloat onePercent = countFrame / 100.0;
//	NSInteger currentPercent = currentFrame / onePercent;
//	self.progressBar.progress = progress;
//	self.percentageCompletion.text = [NSString stringWithFormat:@"%d %%", currentPercent];
//}


@end

@implementation RecordViewController (FlickogramCaptureManagerDelegate)

- (void)frameCompleted:(NSDictionary *)dict {
    
    NSNumber *currentFrameObj = [dict objectForKey:@"CurrentFrame"];
	NSNumber *countFrameObj = [dict objectForKey:@"CountFrame"];
	CGFloat currentFrame = [currentFrameObj floatValue];
	CGFloat countFrame = [countFrameObj floatValue];
	CGFloat progress = currentFrame / countFrame;
	CGFloat onePercent = countFrame / 100.0;
	NSInteger currentPercent = currentFrame / onePercent;
	self.progressBar.progress = progress;
	self.percentageCompletion.text = [NSString stringWithFormat:@"%d %%", currentPercent];
}

- (void)captureManager:(FlickogramCaptureManager *)captureManager didFailWithError:(NSError *)error
{
	CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
															message:[error localizedFailureReason]
														   delegate:nil
												  cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
												  otherButtonTitles:nil];
		[alertView show];
		isNowRecord = NO;
        canUpload = NO;
	});
    
}

- (void)captureManagerRecordingBegan:(FlickogramCaptureManager *)captureManager
{
	isNowRecord = YES;
	isRecordingMade = NO;
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
        [[self recordButton] setEnabled:YES];
        self.grayBackground.hidden = YES;
	});
}

- (void)captureManagerRecordingFinished:(FlickogramCaptureManager *)captureManager
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
        [[self recordButton] setEnabled:YES];
		self.recLabel.hidden = YES;
    });
	self.panelEffects.hidden = NO;
    self.playButton.hidden = YES;
	isRecordingMade = YES;
	isNowRecord = NO;
	[[[self captureManager] session] stopRunning];
	pathMovie = [[self captureManager] tempFileURL];
	NSDictionary *options = [NSDictionary dictionaryWithObject:pathMovie
														forKey:@"MoviePath"];
	@try {
		[[NSNotificationCenter defaultCenter] postNotificationName:NameNotificationChangeMoviePath
															object:self
														  userInfo:options];
	}
	@catch (NSException *exception) {
		NSLog(@"%@", exception);
	}
	@finally {
		NSLog(@"here");
	}
	
    if (!self.player) {
        [self createPlayer];
    } else {
        AVPlayerItem *playerItem = [self getPlayerItemWithFileUrl:pathMovie shouldSetDuration:YES];
        [self.player replaceCurrentItemWithPlayerItem:playerItem];
    }
    canUpload = YES;
}

@end
