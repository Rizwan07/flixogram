//
//  GridVideoCell.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "EGOImageButton.h"

typedef enum {
	ImageButton01,
	ImageButton02,
	ImageButton03,
	ImageButton04,
	ImageButton05,
	ImageButton06,
	ImageButton07,
	ImageButton08,
	ImageButton09,
	ImageButton10,
} ImageButton;

@interface GridVideoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton1;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton2;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton3;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton4;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton5;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton6;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton7;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton8;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton9;
@property (nonatomic, weak) IBOutlet EGOImageButton *thumbButton10;

- (EGOImageButton *)setThumbImageOfURL:(NSURL *)thumbURL ForButton:(ImageButton)currentButton;

@end
