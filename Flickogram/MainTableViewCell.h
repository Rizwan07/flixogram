//
//  MainTableViewCell.h
//  Flickogram
//

#import <UIKit/UIKit.h>
#import "EGOImageView.h"
#import "NIAttributedLabel.h"
#import <QuartzCore/QuartzCore.h>

@interface MainTableViewCell : UITableViewCell <EGOImageViewDelegate> {
	NSMutableArray *imageLiked;
	NSInteger lastItemCoord;
}

@property (nonatomic, weak) IBOutlet UILabel *countComments;
@property (nonatomic, weak) IBOutlet UILabel *countLikes;
@property (nonatomic, weak) IBOutlet UILabel *countViews;
@property (nonatomic, weak) IBOutlet EGOImageView *imageThumbnailVideo;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activity;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollingLikePanel;
@property (nonatomic, weak) IBOutlet UIButton *likeButton;
@property (nonatomic, weak) IBOutlet UIButton *playButton;
@property (nonatomic, weak) IBOutlet UIView *playbackBackground;
@property (nonatomic, weak) IBOutlet UIButton *streamButton;
@property (nonatomic, weak) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UIButton *sendButton;
@property (nonatomic, weak) IBOutlet UIButton *showComments;
@property (strong, nonatomic) IBOutlet UIView *viewComments;
@property (strong, nonatomic) IBOutlet NIAttributedLabel *nimbusAttributedLbk;

- (void)setDelegateForEGOImageView;
- (void)setLikedImageOnURL:(NSString *)url forCurrentIndex:(NSInteger)currentIndex;
- (void)initLikedImage:(NSInteger)count;
- (UIButton *)addShowMoreButton;
-(IBAction)textFieldReturn:(id)sender;

@end
