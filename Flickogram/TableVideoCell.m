//
//  TableVideoCell.m
//  Flickogram


#import "TableVideoCell.h"
#import "JMImageCache.h"

@implementation TableVideoCell
@synthesize thumbnailVideo = _thumbnailVideo;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setThumbnail:(NSString *)thumbnailURL
{
	[self.thumbnailVideo setImageWithURL:[NSURL URLWithString:thumbnailURL]];
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
	[super willMoveToSuperview:newSuperview];
}

@end
