//
//  MainTableViewCell.m
//  Flickogram
//


#import "MainTableViewCell.h"

@implementation MainTableViewCell
@synthesize countLikes= _countLikes;
@synthesize countViews = _countViews;
@synthesize countComments = _countComments;
@synthesize imageThumbnailVideo = _imageThumbnailVideo;
@synthesize activity = _activity;
@synthesize scrollingLikePanel = _scrollingLikePanel;
@synthesize likeButton = _likeButton;
@synthesize playButton = _playButton;
@synthesize playbackBackground = _playbackBackground;
@synthesize streamButton = _streamButton;
@synthesize textField = _textField;
@synthesize sendButton = _sendButton;
@synthesize showComments = _showComments;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
	[super willMoveToSuperview:newSuperview];
	
	if(!newSuperview) {
		[self.imageThumbnailVideo cancelImageLoad];
	}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initLikedImage:(NSInteger)countImage
{
	imageLiked = [[NSMutableArray alloc] init];
	NSInteger startPositionX = 5; //14
	NSInteger positionForY = 4; //245
	NSInteger distance = 4;
	NSInteger size = 34; 
	lastItemCoord = 0;
	CGRect scrollRect = self.scrollingLikePanel.frame;
	for (int currentIndex = 0; currentIndex < countImage; currentIndex++) {
		NSInteger coordXForIndex = startPositionX + (size * (currentIndex)) + (distance * (currentIndex));
		EGOImageView *pictureProfileLike = [[EGOImageView alloc] initWithPlaceholderImage:[UIImage imageNamed:@"Profile-thumb"]];
		CGRect rect = CGRectMake(coordXForIndex, positionForY, size, size); 
		pictureProfileLike.frame = rect;
		[self.scrollingLikePanel addSubview:pictureProfileLike];
		[imageLiked addObject:pictureProfileLike];
		
		lastItemCoord = pictureProfileLike.frame.origin.x + pictureProfileLike.frame.size.width + distance;
		
		self.scrollingLikePanel.contentSize = CGSizeMake(pictureProfileLike.frame.origin.x + pictureProfileLike.frame.size.width, 
														 scrollRect.size.height);
	}
}

- (UIButton *)addShowMoreButton
{
	UIButton *showMore = [UIButton buttonWithType:UIButtonTypeCustom];
	[showMore setTitle:@"Show More" forState:UIControlStateNormal];
	showMore.titleLabel.font = [UIFont systemFontOfSize:12];
	showMore.frame = CGRectMake(lastItemCoord, 4, 70, 34);
	showMore.showsTouchWhenHighlighted = YES;
	[self.scrollingLikePanel addSubview:showMore];
	CGRect scrollRect = self.scrollingLikePanel.frame;
	self.scrollingLikePanel.contentSize = CGSizeMake(showMore.frame.origin.x + showMore.frame.size.width, scrollRect.size.height);
	return showMore;
}

- (IBAction)textFieldReturn:(id)sender
{
	[sender resignFirstResponder];
} 

- (void)setDelegateForEGOImageView
{
	self.imageThumbnailVideo.delegate = self;
}
- (void)imageViewLoadedImage:(EGOImageView *)imageView 
{
	self.imageThumbnailVideo.image = [imageView.image copy];
	[self.activity stopAnimating];
}
- (void)imageViewFailedToLoadImage:(EGOImageView *)imageView error:(NSError *)error
{
	[self.activity stopAnimating];
}

- (void)setLikedImageOnURL:(NSString *)url forCurrentIndex:(NSInteger)currentIndex
{
	NSNumber *currentNumber = [NSNumber numberWithInteger:currentIndex];
	NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:currentNumber, @"index", url, @"path", nil];
	[NSThread detachNewThreadSelector:@selector(setImages:) toTarget:self withObject:data];
}

- (void)setImages:(NSDictionary *)data
{
	NSInteger currentIndex = [[data objectForKey:@"index"] integerValue];
	NSString *url = [data objectForKey:@"path"];
	[[imageLiked objectAtIndex:currentIndex]setImageURL:[NSURL URLWithString:url]];
	currentIndex++;
}
@end
