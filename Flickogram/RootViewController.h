//
//  RootViewController.h
//  Flixogram
//
//  Created by Rizwan on 1/23/14.
//
//

#import "BaseViewController.h"

@interface RootViewController : BaseViewController

- (void)selectTabBarIndex:(NSInteger)indexTabBar;

@end
