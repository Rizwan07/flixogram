/*
 Copyright (c) 2010, Stig Brautaset.
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:

   Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   Neither the name of the the author nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "FLJsonStreamParserState.h"
#import "FLJsonStreamParser.h"

#define SINGLETON \
+ (id)sharedInstance { \
    static id state = nil; \
    if (!state) { \
        @synchronized(self) { \
            if (!state) state = [[self alloc] init]; \
        } \
    } \
    return state; \
}

@implementation FLJsonStreamParserState

+ (id)sharedInstance { return nil; }

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	return NO;
}

- (FLJsonStreamParserStatus)parserShouldReturn:(FLJsonStreamParser*)parser {
	return FLJsonStreamParserWaitingForData;
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {}

- (BOOL)needKey {
	return NO;
}

- (NSString*)name {
	return @"<aaiie!>";
}

- (BOOL)isError {
    return NO;
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateStart

SINGLETON

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	return token == FLJson_token_array_start || token == FLJson_token_object_start;
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {

	FLJsonStreamParserState *state = nil;
	switch (tok) {
		case FLJson_token_array_start:
			state = [FLJsonStreamParserStateArrayStart sharedInstance];
			break;

		case FLJson_token_object_start:
			state = [FLJsonStreamParserStateObjectStart sharedInstance];
			break;

		case FLJson_token_array_end:
		case FLJson_token_object_end:
			if (parser.supportMultipleDocuments)
				state = parser.state;
			else
				state = [FLJsonStreamParserStateComplete sharedInstance];
			break;

		case FLJson_token_eof:
			return;

		default:
			state = [FLJsonStreamParserStateError sharedInstance];
			break;
	}


	parser.state = state;
}

- (NSString*)name { return @"before outer-most array or object"; }

@end

#pragma mark -

@implementation FLJsonStreamParserStateComplete

SINGLETON

- (NSString*)name { return @"after outer-most array or object"; }

- (FLJsonStreamParserStatus)parserShouldReturn:(FLJsonStreamParser*)parser {
	return FLJsonStreamParserComplete;
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateError

SINGLETON

- (NSString*)name { return @"in error"; }

- (FLJsonStreamParserStatus)parserShouldReturn:(FLJsonStreamParser*)parser {
	return FLJsonStreamParserError;
}

- (BOOL)isError {
    return YES;
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateObjectStart

SINGLETON

- (NSString*)name { return @"at beginning of object"; }

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	switch (token) {
		case FLJson_token_object_end:
		case FLJson_token_string:
			return YES;
			break;
		default:
			return NO;
			break;
	}
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	parser.state = [FLJsonStreamParserStateObjectGotKey sharedInstance];
}

- (BOOL)needKey {
	return YES;
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateObjectGotKey

SINGLETON

- (NSString*)name { return @"after object key"; }

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	return token == FLJson_token_keyval_separator;
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	parser.state = [FLJsonStreamParserStateObjectSeparator sharedInstance];
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateObjectSeparator

SINGLETON

- (NSString*)name { return @"as object value"; }

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	switch (token) {
		case FLJson_token_object_start:
		case FLJson_token_array_start:
		case FLJson_token_true:
		case FLJson_token_false:
		case FLJson_token_null:
		case FLJson_token_number:
		case FLJson_token_string:
			return YES;
			break;

		default:
			return NO;
			break;
	}
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	parser.state = [FLJsonStreamParserStateObjectGotValue sharedInstance];
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateObjectGotValue

SINGLETON

- (NSString*)name { return @"after object value"; }

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	switch (token) {
		case FLJson_token_object_end:
		case FLJson_token_separator:
			return YES;
			break;
		default:
			return NO;
			break;
	}
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	parser.state = [FLJsonStreamParserStateObjectNeedKey sharedInstance];
}


@end

#pragma mark -

@implementation FLJsonStreamParserStateObjectNeedKey

SINGLETON

- (NSString*)name { return @"in place of object key"; }

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
    return FLJson_token_string == token;
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	parser.state = [FLJsonStreamParserStateObjectGotKey sharedInstance];
}

- (BOOL)needKey {
	return YES;
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateArrayStart

SINGLETON

- (NSString*)name { return @"at array start"; }

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	switch (token) {
		case FLJson_token_object_end:
		case FLJson_token_keyval_separator:
		case FLJson_token_separator:
			return NO;
			break;

		default:
			return YES;
			break;
	}
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	parser.state = [FLJsonStreamParserStateArrayGotValue sharedInstance];
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateArrayGotValue

SINGLETON

- (NSString*)name { return @"after array value"; }


- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	return token == FLJson_token_array_end || token == FLJson_token_separator;
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	if (tok == FLJson_token_separator)
		parser.state = [FLJsonStreamParserStateArrayNeedValue sharedInstance];
}

@end

#pragma mark -

@implementation FLJsonStreamParserStateArrayNeedValue

SINGLETON

- (NSString*)name { return @"as array value"; }


- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token {
	switch (token) {
		case FLJson_token_array_end:
		case FLJson_token_keyval_separator:
		case FLJson_token_object_end:
		case FLJson_token_separator:
			return NO;
			break;

		default:
			return YES;
			break;
	}
}

- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok {
	parser.state = [FLJsonStreamParserStateArrayGotValue sharedInstance];
}

@end

