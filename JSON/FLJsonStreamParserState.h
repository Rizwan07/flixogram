/*
 Copyright (c) 2010, Stig Brautaset.
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 
   Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
  
   Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 
   Neither the name of the the author nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <Foundation/Foundation.h>

#import "FLJsonTokeniser.h"
#import "FLJsonStreamParser.h"

@interface FLJsonStreamParserState : NSObject
+ (id)sharedInstance;

- (BOOL)parser:(FLJsonStreamParser*)parser shouldAcceptToken:(FLJson_token_t)token;
- (FLJsonStreamParserStatus)parserShouldReturn:(FLJsonStreamParser*)parser;
- (void)parser:(FLJsonStreamParser*)parser shouldTransitionTo:(FLJson_token_t)tok;
- (BOOL)needKey;
- (BOOL)isError;

- (NSString*)name;

@end

@interface FLJsonStreamParserStateStart : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateComplete : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateError : FLJsonStreamParserState
@end


@interface FLJsonStreamParserStateObjectStart : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateObjectGotKey : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateObjectSeparator : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateObjectGotValue : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateObjectNeedKey : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateArrayStart : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateArrayGotValue : FLJsonStreamParserState
@end

@interface FLJsonStreamParserStateArrayNeedValue : FLJsonStreamParserState
@end
